#include <iostream>
#include <string>
#include <queue>
#include <mutex>
#include <condition_variable>

using namespace std;

template <class K>
class Channel
{
private:
    std::queue<K> queue_;
    std::mutex mutex_;
    std::condition_variable cv_producer_;
    std::condition_variable cv_consumer_;
    size_t capacity_;
    bool closed_ = false;

public:
    Channel(size_t capacity = 0) : capacity_(capacity) {}

    bool send(K value)
    {
        std::unique_lock<std::mutex> lock(mutex_);
        cv_producer_.wait(lock, [this]()
                          {
            // 对于无缓冲的channel，我们应该等待直到有消费者准备好
            return (capacity_ == 0 && queue_.empty() == true) || queue_.size() < capacity_ || closed_; });

        if (closed_ == true)
            return false;

        queue_.push(value);
        cv_consumer_.notify_one();
        return true;
    }

    bool receive(K &value)
    {
        std::unique_lock<std::mutex> lock(mutex_);
        cv_consumer_.wait(lock, [this]()
                          { return !queue_.empty() || closed_; });
        if (closed_ == true && queue_.empty() == true)
            return false;

        value = queue_.front();
        queue_.pop();
        cv_producer_.notify_one();
        return true;
    }

    void close()
    {
        std::unique_lock<std::mutex> lock(mutex_);
        closed_ = true;
        cv_producer_.notify_all();
        cv_consumer_.notify_all();
    }
};

// 示例使用
int main(void)
{
    Channel<int> ch(10); //

    std::thread producer([&]()
                         {
        for(int  i = 0; i < 5; i++){
            ch.send(i);
            std::cout << "send: " << i << std::endl;
        }
        ch.close(); });

    std::thread consumer([&]()
                         {
        std::this_thread::sleep_for(std::chrono::seconds(2));
        int value = 0;
        while((ch.receive(value))){
            std::cout << "receive: " << value << std::endl;
        } });

    producer.join();
    consumer.join();

    return 0;
}
