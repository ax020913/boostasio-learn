#pragma once

#include <iostream>

class myClass
{
public:
    myClass(int data) : data_(data) {}

    friend std::ostream &operator<<(std::ostream &cout, const myClass &my)
    {
        cout << my.data_;
        return cout;
    }

private:
    int data_;
};