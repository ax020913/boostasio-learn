#pragma once

#include <iostream>
#include <thread>
#include <vector>
#include <list>
#include <map>
#include <iterator>
#include <memory>
#include <mutex>
#include <shared_mutex>

template <class Key, class Value, class Hash = std::hash<Key>>
class threadsafe_lookup_table
{
private:
    // 桶类型
    class bucket_type
    {
        friend class threadsafe_lookup_table;

    private:
        // 存储元素的类型为 pair，由 key 和 value 构成
        typedef std::pair<Key, Value> bucket_value;
        // 由链表存储元素构
        typedef std::list<bucket_value> bucket_data;
        // 链表的迭代器
        // 上面是用模板定义了一种类型 bucket_value，再用 bucket_value 去定义一种类型的时候，得用 typename 的哦
        typedef typename bucket_data::iterator bucket_iterator; 
        // 链表数据
        bucket_data data;
        // 改用共享锁
        // 1. std::shared_mutex：使得读线程之间支持并发的读
        // 2. mutable：加锁解锁也是修改了 mutex_ 的(对于有些防止修改内部成员变量的成员函数，成员变量为 mutable 修饰时，也是可以修改的)
        mutable std::shared_mutex mutex_; 

        // 查找操作，在 list 中找到匹配的 key 值，然后返回迭代器
        bucket_iterator find_entry_for(const Key &key)
        {
            return std::find_if(data.begin(), data.end(),
                                [&](bucket_value const &item)
                                {
                                    return item.first == key;
                                });
        }

    public:
        // 查找 key 值，找到返回对应的 value，未找到则返回默认值
        Value value_for(Key const &key, Value const &default_value)
        {
            std::shared_lock<std::shared_mutex> lock(mutex_);
            bucket_iterator const found_entry = find_entry_for(key);
            return (found_entry == data.end()) ? default_value : found_entry->second;
        }
        // 添加 key 和 value，找到则更新，没找到则添加
        void add_or_update_mapping(Key const &key, Value const &value)
        {
            std::unique_lock<std::shared_mutex> lock(mutex_);
            bucket_iterator const found_entry = find_entry_for(key);
            if (found_entry == data.end())
                data.push_back(bucket_value(key, value));
            else
                found_entry->second = value;
        }
        // 删除对应的 key
        void remove_mapping(Key const &key)
        {
            std::unique_lock<std::shared_mutex> lock(mutex_);
            bucket_iterator const found_entry = find_entry_for(key);
            if (found_entry != data.end())
                data.erase(found_entry);
        }
    };
    // 用 vector 存储桶类型
    std::vector<std::unique_ptr<bucket_type>> buckets;
    // hash<Key> 哈希表，哈希函数用来根据生成哈希值
    Hash hasher;
    // 根据 key 生成数字，并对桶的大小取余得到下标，根据下标返回对应的桶智能指针
    bucket_type &get_bucket(Key const &key) const
    {
        std::size_t const bucket_index = hasher(key) % buckets.size();
        return *buckets[bucket_index];
    }

public:
    threadsafe_lookup_table(unsigned num_buckets = 19, Hash const &hasher_ = Hash())
        : buckets(num_buckets), hasher(hasher_)
    {
        for (unsigned i = 0; i < num_buckets; i++)
            buckets[i].reset(new bucket_type);
    }
    threadsafe_lookup_table(threadsafe_lookup_table const &other) = delete;
    threadsafe_lookup_table &operator=(threadsafe_lookup_table const &other) = delete;

    Value value_for(Key const &key, Value const &default_value = Value())
    {
        return get_bucket(key).value_for(key, default_value);
    }
    void add_or_update_mapping(Key const &key, Value const &value)
    {
        get_bucket(key).add_or_update_mapping(key, value);
    }
    void remove_mapping(Key const &key)
    {
        get_bucket(key).remove_mapping(key);
    }

    std::map<Key, Value> get_map()
    {
        std::vector<std::unique_lock<std::shared_mutex>> locks;
        for (unsigned i = 0; i < buckets.size(); ++i)
            locks.push_back(std::unique_lock<std::shared_mutex>(buckets[i]->mutex_));

        std::map<Key, Value> res;
        for (unsigned i = 0; i < buckets.size(); ++i)
        {
            // 需用 typename 告诉编译器 bucket_type::bucket_iterator 是一个类型，以后再实例化
            // 当然此处可简写成 auto it = buckets[i]->data.begin();
            typename bucket_type::bucket_iterator it = buckets[i]->data.begin();
            for (; it != buckets[i]->data.end(); ++it)
                res.insert(*it);
        }
        return res;
    }
};