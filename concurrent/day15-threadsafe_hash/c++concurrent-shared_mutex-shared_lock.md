### `C++` 的 `std::shared_lock`(共享锁) 和 `std::shared_mutex`(共享互斥量)

#### 1. `std::shared_mutex` 和 `std::condition_variable` 的使用场景
#### 1.1 `std::shared_mutex` 的使用场景(来自 `gpt`)

`std::shared_mutex` 主要用于管理对共享资源的并发访问，特别是在读取和写入的情况下。这种读写锁的设计允许多个线程同时读取共享资源，但在写入时要求独占锁。以下是一些 `std::shared_mutex` 的使用场景：

1. **读多写少的情况**：当共享资源被频繁读取而很少被写入时，使用 `std::shared_mutex` 可以提高并发性，允许多个线程同时读取。这可以有效地减小读取操作之间的互斥开销。

2. **读写分离的数据结构**：当应用程序使用读写分离的数据结构时，例如，一个包含只读数据的缓存，而写入只发生在初始化或更新时，`std::shared_mutex` 可以有效地支持这种模型。

3. **提高性能**：在某些情况下，使用互斥锁可能会导致性能瓶颈，因为互斥锁在写入时会完全阻塞其他线程的读取和写入。使用 `std::shared_mutex` 可以更细粒度地控制对共享资源的访问，从而提高并发性。

4. **读取操作较长的任务**：如果读取共享资源的操作可能会占用较长时间，使用 `std::shared_mutex` 允许其他线程在读取时并发执行，提高整体性能。

5. **实时系统中的资源管理**：在实时系统中，资源管理可能需要在某些情况下读取，而在其他情况下写入。`std::shared_mutex` 提供了一种合适的方式来管理这种情况。

请注意，`std::shared_mutex` 是在 `C++14` 引入的，因此要确保你的编译器和环境支持 `C++14` 或更高的标准。在使用时，需要小心避免潜在的死锁和竞态条件，正确地选择共享锁和独占锁，并确保对共享资源的访问是线程安全的。


##### 1.2 `std::condition_variable` 的使用场景(来自 `gpt`)

`std::condition_variable` 是 `C++` 标准库中用于线程间同步的工具之一，通常与 `std::mutex` 结合使用。它的主要用途是在一个线程等待某个条件成立时，允许其他线程通知它继续执行。以下是一些 `std::condition_variable` 的使用场景：

1. **生产者-消费者问题**：当存在一个生产者线程负责生成数据，而一个或多个消费者线程负责处理这些数据时，`std::condition_variable` 可以用于在生产者生成数据时通知等待的消费者线程。

2. **任务分配**：在多线程任务分配中，某个线程可能等待某个任务完成，而另一个线程完成任务后可以通过 `std::condition_variable` 通知等待的线程。

3. **线程池**：在线程池中，主线程可能等待工作队列中有任务可用，而工作线程在完成任务后通过 `std::condition_variable` 通知主线程有任务可执行。

4. **资源管理**：当多个线程需要竞争某个共享资源时，例如一个有限的连接池，`std::condition_variable` 可以用于在资源可用时通知等待的线程。

5. **定时等待**：`std::condition_variable` 可以与 `std::chrono` 结合使用，实现定时等待。例如，一个线程等待某个条件成立，但如果在一定时间内条件没有成立，它可以继续执行其他任务。

6. **多阶段任务**：在多阶段任务中，某个阶段的线程可能需要等待其他阶段的线程完成，`std::condition_variable` 可以用于协调各个阶段的执行。

7. **事件通知**：当某个事件发生时，可以使用 `std::condition_variable` 来通知等待该事件的线程。



#### 2. std::shared_mutex 的一些性质

多线程情况下，许多函数如果依然按照原有的**独占锁**去串行执行，效率会很低，虽然解决了线程安全的问题。

如果要修改之前多线程的独占锁，在修改后，要求函数 1 函数 2 要能并发执行，且前两者并发执行时与函数 3，函数 4，函数 5 是互斥的。而函数 3，4，5 与所有函数都互斥。(例如函数 3 运行的时候，函数 1 2 4 5 都是卡着等待函数 3 执行完毕再继续执行)。用 C++ 的 std::shared_lock 和 std::shared_mutex 就可以实现了。

std::shared_mutex 类是一个同步原语，可用于保护共享数据被多个读线程同时**访问**，但只能被一个写线程**修改**共享数据，std::shared_mutex 拥有二个访问级别：

- 共享: 多个线程能共享同一互斥的所有权(**使用共享锁**)。
- 独占性: 仅一个线程能占有互斥(**使用独占锁**)。

1. 若一个线程已获取独占性锁（通过 lock、try_lock），则无其他线程能获取该锁（包括共享的），不然就死锁了。

2. 仅当任何线程均未获取独占性锁时，共享锁能被多个线程获取（通过 lock_shared 、try_lock_shared）。

3. 在一个线程内，同一时刻只能获取一个锁（共享或独占性）。

4. 共享互斥体在能由任何数量的线程同时读共享数据，但一个线程只能在无其他线程同时读写时写同一数据时特别有用。

std::shared_mutex 类满足共享互斥体(SharedMutex)和标准布局类型(StandardLayoutType)的所有要求。

以上是 cppreferenced 的原文。翻译一遍就是这样的逻辑

当 std::mutex 是 std::shared_mutex 的时候:
1. 当有函数使用共享锁时，能够与其他享有共享锁的函数也能并发同步执行。而和所有独占锁的函数是互斥的。

2. 当有一个函数使用独占锁时，其他所有的用同一个Mutex带锁的函数都是与其互斥的。

自然就可以解决原有的**独占锁**去串行执行，效率很低的问题了。

|   已知的独占锁   |                    简单描述                    |
| :--------------: | :--------------------------------------------: |
|       lock       |   锁定互斥，若互斥不可用则阻塞(公开成员函数)   |
|     try_lock     | 尝试锁定互斥，若互斥不可用则返回(公开成员函数) |
|      unlock      |             解锁互斥(公开成员函数)             |
| std::unique_lock |                 自动加锁和解锁                 |
| std::lock_guard  |                 自动加锁和解锁                 |


|   已知的共享锁   |                          简单描述                          |
| :--------------: | :--------------------------------------------------------: |
|   lock_shared    |   为共享所有权锁定互斥，若互斥不可用则阻塞(公开成员函数)   |
| try_lock_shared  | 尝试为共享所有权锁定互斥，若互斥不可用则返回(公开成员函数) |
|  unlock_shared   |             解锁互斥(共享所有权)(公开成员函数)             |
| std::shared_lock |                       自动加锁和解锁                       |



#### 3. 直接拿来用一用
##### 3.1 共享锁和共享互斥量实现多线程并发的 `std::map`

线程和写线程是串行的(不是并发)，写线程和读线程之间是串行的(不是并发)，读线程和读线程之间是并发的。

在 `C++` 标准库中，`std::map` 是一个单线程容器，不直接支持并发读写操作。如果在多线程环境中需要对 `std::map` 进行读写操作，你需要手动添加互斥锁来保护并发访问，以防止数据竞争和不一致性。

然而，`C++11` 引入了标准库的并发支持，其中包括了 `std::shared_mutex`(共享互斥锁)和 `std::map` 结合使用的方法。`std::shared_mutex` 支持读写分离的锁策略，允许多个线程并发地读取数据，但在写入数据时会互斥锁定。

以下是一个使用 `std::shared_mutex` 来实现读写安全的 `std::map` 的示例：
```
#include <iostream>
#include <map>
#include <thread>
#include <mutex>
#include <shared_mutex>
#include <condition_variable>
#include <chrono>
 
// std::map<key, value> myMap_;
std::map<int, int> myMap_;
std::shared_mutex mapMutex_;
// bool able_use = false; // 最开始是不能读取数据的
// std::mutex mutex_;
// std::condition_variable condition_variable_;
void writeMap(int key, int value) {
    std::unique_lock<std::shared_mutex> lock(mapMutex_); // std::unique_lock 独占锁(写串行)
    // error
    // std::unique_lock<std::mutex> lock(mutex_);
    // condition_variable_.wait(lock, [&](){
    //     return able_use == false; 
    //     // return true：不会挂起线程并不会释放锁，继续向下执行
    //     // return false：挂起线程等待 notify 并释放锁
    // });
    myMap_[key] = value;
}
int readMap(int key) {
    std::shared_lock<std::shared_mutex> lock(mapMutex_); // std::shared_lock 共享锁(读并发)
    // error
    // condition_variable_.wait(lock, [&](){
    //     return able_use == true;
    // });
    auto it = myMap_.find(key);
    if (it != myMap_.end())
        return it->second;
    else 
        return -1; // 返回默认值，表示未找到
}
 
// std::thread myThread(callBack, args);
int write(int count){
    for(int key = 0; key < count; key++)
        writeMap(key, key + 1);
    return 0;
}
int read(int count){
    for(int key = 0; key < count; key++){
        int value = readMap(key);
        if(value != -1) 
            std::cout << "key = " << key << " value = " << value << std::endl;
        else 
            std::cout << "key = " << key << " value = " << "nullptr" << std::endl;
    }
    return 0;
}

int main(void) {
    std::thread writer(write, 100); // 插入 100 个数据
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::thread reader(read, 10); // 查找 10 个数据

    writer.join();
    reader.join();
 
    return 0;
}
```
在上述示例中，我们使用 `std::shared_lock 和 std::shared_mutex` 来保护对 `myMap` 的并发访问。其他的读线程 `std::shared_lock<std::shared_mutex> lock(mapMutex_)` 是可以的，在读取数据时可以允许多个线程同时访问，从而实现了读线程之间的并发(此时写线程是加不上锁的)。

`writeMap` 函数使用 `std::unique_lock` 来获得独占的写入访问权限，在写入数据时会阻止其他线程的任何访问。其他的写线程和读线程都是加不上锁的(有了写线程就会串行，但是串行取决于锁的精度，互斥区域小的话其他部分就是并行的)。

需要注意的是，`C++17` 引入了 `std::shared_mutex`，如果你的编译环境支持 `C++17`，可以使用上述示例。如果不支持，你可以考虑使用第三方的并发容器库，如 `tbb::concurrent_hash_map`，它为多线程环境下的并发读写提供了更好的性能支持。



##### 3.2 那读操作不加锁不是更好吗？

这是为了在多线程读的时候，有一个线程要写了，那么是得防止写线程进行写操作的（防止线程写操作完，之前的读线程读取到的数据不是原来的数据，造成数据不一致线程安全的问题）。防止线程进行写操作就是读线程都得加锁，而且得是 `std::shared_lock<std::shared_mutex>`，因为其他的读线程也可以这样加锁，就实现了读并发。

**实现读线程并发的必要。**

对比之前读线程是使用的是 `std::unique_lock<std::mutex> lock(mutex_)`，是实现了线程安全的 `std::map` 但是读线程之间不是并发的。



##### 3.3 上面多线程并发的 `std::map` 不还是有串行的部分吗？ 那么共享锁和共享互斥量的用处何在？

虽然有串行的部分，但是并行的部分还是占绝对优势的。用很多的看黑板报同学(数量多而且占用访问的时间多)和很少的写黑板报同学(数量少而且占用访问的时间少)来模拟理解上面的流程、`shared_lock` 和 `shared_mutex` 的优势、读写锁中写锁优先。

**这也算是 `std::shared_mutex` 和 `std::shared_lock` 出现的需要吧，看得见的且可榨取的优势，哈哈哈。**


##### 2.4 关于 `std::condition_variable` 使用不上的问题？ (写线程写完了可以通知读线程)
`std::condition_variable` 的使用标配是 `std::unique_lock<std::mutex>` 所以我们上面的例子是使用不上的。

**解释一：**
但是关于写线程写完了可以通知读线程，其实也是没有必要的。

1. 因为写线程写完了锁就释放了，如果有读线程来的话自然加上锁了。

2. 又因为写线程正在写，有读线程来了的话，自然是加不上锁的。后面不就回到了 1 的情况了吗。

**解释二：**
因为读线程执行的 `readMap` 函数肯定不用使用条件变量的。

有读线程在读取的话，是不用挂起其他的读线程的。要的就是读线程的并发。

所以读线程都不用 `std::condition_variable` 了，那么写线程还需要通知读线程吗？

**得了解 `std::condition_variable` 和 `std::shared_mutex`、`std::shared_lock` 的使用场景。**

```
void writeMap(int key, int value) {
    std::unique_lock<std::shared_mutex> lock(mapMutex_); // std::unique_lock 独占锁(写串行)
    // error
    // std::unique_lock<std::mutex> lock(mutex_);
    // condition_variable_.wait(lock, [&](){
    //     return able_use == false; 
    //     // return true：不会挂起线程并不会释放锁，继续向下执行
    //     // return false：挂起线程等待 notify 并释放锁
    // });
    myMap_[key] = value;
}
int readMap(int key) {
    std::shared_lock<std::shared_mutex> lock(mapMutex_); // std::shared_lock 共享锁(读并发)
    // error
    // condition_variable_.wait(lock, [&](){
    //     return able_use == true;
    // });
    auto it = myMap_.find(key);
    if (it != myMap_.end())
        return it->second;
    else 
        return -1; // 返回默认值，表示未找到
}
```

