### 利用栅栏实现同步
我们通过原子操作实战实现了无锁队列，今天完善一下无锁的原子操作剩余的知识，包括`Relaese`和`Acquire`内存序在什么情况下是存在危险的，以及我们可以利用栅栏机制实现同步等等。

#### 1. 线程可见顺序
我们提到过除了 `memory_order_seq_cst` 顺序，其他的顺序都不能保证原子变量修改的值在其他多线程中看到的顺序是一致的。

但是可以通过同步机制保证一个线程对原子变量的修改对另一个原子变量可见。通过`“Syncronizes With”` 的方式达到先行的效果。

但是我们说的先行是指 `“A Syncronizes With B ”`， 如果`A` 的结果被B读取，则`A` 先行于`B`。

有时候我们线程`1`对 `A` 的 `store` 操作采用 `release` 内存序，而线程 `2` 对 `B` 的 `load` 采用 `acquire` 内存序，并不能保证 `A` 一定比 `B` 先执行。因为两个线程并行执行无法确定先后顺序，我们指的先行不过是说如果 `B` 读取了 `A` 操作的结果，则称 `A` 先行于 `B` 。

我们看下面的一段案例:
```
#include <iostream>
#include <atomic>
#include <thread>
#include <cassert>
std::atomic<bool> x, y;
std::atomic<int> z;
void write_x()
{
    x.store(true, std::memory_order_release); //1
}
void write_y()
{
    y.store(true, std::memory_order_release); //2
}
void read_x_then_y()
{
    while (!x.load(std::memory_order_acquire));
    if (y.load(std::memory_order_acquire))   //3
        ++z;
}
void read_y_then_x()
{
    while (!y.load(std::memory_order_acquire));
    if (x.load(std::memory_order_acquire))   //4
        ++z;
}
```
我们写一个函数测试，函数 `TestAR` 中初始化 `x` 和 `y` 为 `false` ， 启动 `4` 个线程 `a`, `b`, `c`, `d`，分别执行 `write_x`, `write_y`, `read_x_then_y`, `read_y_then_x`。
```
void TestAR()
{
    x = false;
    y = false;
    z = 0;
    std::thread a(write_x);
    std::thread b(write_y);
    std::thread c(read_x_then_y);
    std::thread d(read_y_then_x);
    a.join();
    b.join();
    c.join();
    d.join();
    assert(z.load() != 0); //5
    std::cout << "z value is " << z.load() << std::endl;
}
```
**上面的测试代码的 `z` 可能会是 `0`，`1`，`2` 的。**

有的读者可能会觉`5`处的断言不会被触发，他们认为`c`和`d`肯定会有一个线程对`z`执行`++`操作。他们的思路是这样的。

`1` 如果`c`线程执行`read_x_then_y`没有对`z`执行加加操作，那么说明`c`线程读取的`x`值为`true`, `y`值为`false`。

`2` 之后`d`线程读取时，如果保证执行到`4`处说明`y`为`true`，等`d`线程执行`4`处代码时`x`必然为`true`。

`3` 他们的理解是如果`x`先被`store`为`true`，`y`后被`store`为`true`，`c`线程看到`y`为`false`时`x`已经为`true`了，那么`d`线程`y`为`true`时`x`也早就为`true`了，所以`z`一定会执行加加操作。

上述理解是不正确的，我们提到过即便是`releas`和`acquire`顺序也不能保证多个线程看到的一个变量的值是一致的，更不能保证看到的多个变量的值是一致的。

变量`x`和`y`的载入操作`3`和`4`有可能都读取`false`值（与宽松次序的情况一样），因此有可能令断言触发错误。变量`x`和`y`分别由不同线程写出，所以两个释放操作都不会影响到对方线程。

看下图:

![Alt text](image.png)

无论`x`和`y`的`store`顺序谁先谁后，线程`c`和线程`d`读取的`x`和`y`顺序都不一定一致。

从`CPU`的角度我们可以这么理解:

![Alt text](image-1.png)

在一个`4`核`CPU`结构的主机上，`a`,`b`,`c`,`d`分别运行在不同的`CPU`内核上。

`a`执行`x.store(true)`先被线程`c`读取，而此时线程`b`对`y`的`store`还没有被`c`读取到新的值，所以此时`c`读取的`x`为`true`，`y`为`false`。

同样的道理，`d`可以读取`b`修改`y`的最新值，但是没来的及读取`x`的最新值，那么读取到`y`为`true`，`x`为`false`。

即使我们采用`release`和`acquire`方式也不能保证全局顺序一致。如果一个线程对变量执行`release`内存序的`store`操作，另一个线程不一定会马上读取到。这个大家要理解。



#### 2. 栅栏
有时候我们可以通过栅栏保证指令编排顺序。

看下面一段代码:
```
#include <atomic>
#include <thread>
#include <assert.h>
std::atomic<bool> x,y;
std::atomic<int> z;
void write_x_then_y()
{
    x.store(true,std::memory_order_relaxed); // 1
    y.store(true,std::memory_order_relaxed);   // 2
}
void read_y_then_x()
{
    while(!y.load(std::memory_order_relaxed));  // 3
    if(x.load(std::memory_order_relaxed))  // 4
        ++z;
}
int main()
{
    x=false;
    y=false;
    z=0;
    std::thread a(write_x_then_y);
    std::thread b(read_y_then_x);
    a.join();
    b.join();
    assert(z.load()!=0);  //5
}
```
上面的代码我们都采用的是 `memory_order_relaxed`, 所以无法保证 `a` 线程将 `x`, `y` 修改后`b`线程看到的也是先修改`x`，再修改`y`的值。`b`线程可能先看到`y`被修改为`true`，`x`后被修改为`true`，那么`b`线程执行到`4`处时`x`可能为`false`导致`z`不会加加，`5`处断言会被触发。

那我们之前做法可以解决这个问题:
```
void write_x_then_y3()
{
    x.store(true, std::memory_order_relaxed); // 1
    y.store(true, std::memory_order_release);   // 2
}
void read_y_then_x3()
{
    while (!y.load(std::memory_order_acquire));  // 3
    if (x.load(std::memory_order_relaxed))  // 4
        ++z;
}
```
可以通过`std::memory_order_release`和`std::memory_order_acquire`形成同步关系。

线程`a`执行`write_x_then_y3`，线程`b`执行`read_y_then_x3`，如果线程`b`执行到`4`处，说明`y`已经被线程`a`设置为`true`。

线程`a`执行到`2`，也必然执行了`1`，因为是`memory_order_release`的内存顺序，所以线程`a`能`2`操作之前的指令在`2`之前被写入内存。

同样的道理，线程`b`在`3`处执行的是`memory_order_acquire`的内存顺序，所以能保证`4`不会先于`3`写入内存，这样我们能知道`1`一定先行于`4``.

进而推断出`z`会加加，所以不会触发`assert(z.load() != 0);`的断言。

其实我们可以通过栅栏机制保证指令的写入顺序。栅栏的机制和`memory_order_release`类似。
```
void write_x_then_y_fence()
{
    x.store(true, std::memory_order_relaxed);  //1
    std::atomic_thread_fence(std::memory_order_release);  //2
    y.store(true, std::memory_order_relaxed);  //3
}
void read_y_then_x_fence()
{
    while (!y.load(std::memory_order_relaxed));  //4
    std::atomic_thread_fence(std::memory_order_acquire); //5
    if (x.load(std::memory_order_relaxed))  //6
        ++z;
}
```
我们写一个函数测试上面的逻辑:
```
void TestFence()
{
    x = false;
    y = false;
    z = 0;
    std::thread a(write_x_then_y_fence);
    std::thread b(read_y_then_x_fence);
    a.join();
    b.join();
    assert(z.load() != 0);   //7
}
```
`7`处的断言也不会触发。我们可以分析一下，

线程`a`运行`write_x_then_y_fence`，线程`b`运行`read_y_then_x_fence`。

当线程`b`执行到`5`处时说明`4`已经结束，此时线程`a`看到`y`为`true`，那么线程`a`必然已经执行完`3``.

尽管`4`和`3`我们采用的是`std::memory_order_relaxed`顺序，但是通过逻辑关系保证了`3`的结果同步给`4`，进而`”3 happens-before 4”`。

因为我们采用了栅栏`std::atomic_fence`所以，`5`处能保证`6`不会先于`5`写入内存，(`memory_order_acquire`保证其后的指令不会先于其写入内存)。

`2`处能保证`1`处的指令先于`2`写入内存，进而`”1 happens-before 6”`, `1`的结果会同步给 `6`。

所以`”atomic_thread_fence”`其实和`”release-acquire”`相似，都是保证`memory_order_release`之前的指令不会排到其后，`memory_order_acquire`之后的指令不会排到其之前。

