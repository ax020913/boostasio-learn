### `std::thread` 的一些使用上的问题

#### 1. 线程存储的问题
```
// vector 的 push_back(会调用插入对象的拷贝构造函数) 和 emplace_back 对比
void param_function(int value)
{
    while (true)
    {
        std::cout << "param is " << value << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}
void use_vector(int count)
{
    std::vector<std::thread> threads;
    for (unsigned i = 0; i < count; i++)
    {
        // threads.push_back(param_function, i); // error
        threads.emplace_back(param_function, i);
    }
    for (auto &entry : threads)
        entry.join();
}
```

#### 2. 线程归属权转移的问题
```
// std::thread 和 std::jthread
void dangerour_use(void)
{
    std::thread t2([](int value)
                   {
        while (true)
        {
            cout << "hello world " << value << endl;
            this_thread::sleep_for(chrono::seconds(1));
        } },
                   100);
    // 先 join 那么下面的 t2 = std::move(t1) 是不会有问题的，但是 main thread 就会在这里等待 t2 了，error
    // t2.join();

    // 现象：t2 正在管理线程运行 print 函数，error
    // 问题：得出一个结论，就是不要将一个线程的管理权交给一个已经绑定线程的变量，否则会触发线程的 terminate 函数引发崩溃
    // 解决：std::jthread 就是为了解决这个问题的，所以 t1 和 t2 换成 std::jthread 类型的就不会有问题了
    std::thread t1;
    t2 = std::move(t1);

    t2.join();
}
```
**得出一个结论，就是不要将一个线程的管理权交给一个已经绑定线程的变量，否则会触发线程的 `terminate` 函数引发崩溃。**
`std::jthread` 就可以解决上面的问题。


#### 3. `std::thread` 相对 `std::jthread` 的一些区别(为什么 `std::jthread` 出现 ?)
在 `C++` 中， `std::thread` 和 `std::jthread` 都是用于创建和管理线程的类。它们之间有一些区别和特点：

1. `std::thread` 是 `C++11` 引入的线程类，而 `std::jthread` 是 `C++20` 引入的线程类。因此， `std::jthread` 是 `std::thread` 的一个扩展，提供了更多的功能和安全性。

2. `std::thread` 对象创建后，可以通过调用 `join()` 或 `detach()` 函数来管理线程的生命周期。而 `std::jthread` 对象创建后，可以通过析构函数自动管理线程的生命周期，无需显式调用 `join()` 或 `detach()` 函数。

3. `std::jthread` 对象的析构函数会自动调用 `join()` 函数，等待线程的完成。这样可以避免线程成为孤儿线程，确保线程的资源被正确释放。

4. `std::jthread` 对象还提供了一些额外的功能，如获取线程的 `ID` 、检查线程是否 `joinable` 等。

综上所述，相对于 `std::thread` ， `std::jthread` 提供了更方便和安全的线程管理方式，可以自动管理线程的生命周期，避免了一些潜在的问题。如果使用 `C++20` 或更高版本的编译器，建议使用 `std::jthread` 来创建和管理线程。如果使用较旧的编译器，只能使用 `std::thread` ，并需要手动调用 `join()` 或 `detach()` 函数来管理线程的生命周期。

**以后前先判断是否有线程绑定任务，如果有则等待，任务执行结束后再析构。**


#### 4. 线程在并行计算的简单使用

