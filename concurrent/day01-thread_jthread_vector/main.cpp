#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <vector>
#include <numeric> // 并行计算的头文件

using namespace std;

// thread 的存储问题
// vector 的 push_back(会调用插入对象的拷贝构造函数) 和 emplace_back 对比
void param_function(int value)
{
    while (true)
    {
        std::cout << "param is " << value << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}
void use_vector(int count)
{
    std::vector<std::thread> threads;
    for (unsigned i = 0; i < count; i++)
    {
        // threads.push_back(param_function, i); // error
        // emplace_back 是 C++11 引入的一个成员函数，用于在容器的末尾直接构造元素，而不是先创建一个临时对象再将其拷贝或移动到容器。这可以在某些情况下提高性能，因为它避免了额外的拷贝或移动操作。
        // 在这个例子中，emplace_back 直接在 threads 中构造了一个新的 std::thread 对象，调用了 param_function 函数并传递了参数 i。
        threads.emplace_back(param_function, i);
        // push_back 是标准容器的成员函数，用于将一个已经构造好的对象添加到容器的末尾。
        // 在这个例子中，先创建了一个临时的 std::thread 对象，通过 param_function 函数和参数 i 进行构造，然后将这个临时对象拷贝或移动到 threads 容器中。
        threads.push_back(std::thread(param_function, i));  
    }
    for (auto &entry : threads)
        entry.join();
}

// std::thread 和 std::jthread
void dangerour_use(void)
{
    std::thread t2([](int value)
                   {
        while (true)
        {
            cout << "hello world " << value << endl;
            this_thread::sleep_for(chrono::seconds(1));
        } },
                   100);
    // 先 join 那么下面的 t2 = std::move(t1) 是不会有问题的，但是 main thread 就会在这里等待 t2 了，error
    // t2.join();

    // 现象：t2 正在管理线程运行 print 函数，error
    // 问题：得出一个结论，就是不要将一个线程的管理权交给一个已经绑定线程的变量，否则会触发线程的 terminate 函数引发崩溃
    // 解决：std::jthread 就是为了解决这个问题的，所以 t1 和 t2 换成 std::jthread 类型的就不会有问题了
    std::thread t1;
    t2 = std::move(t1);

    t2.join();
}
// 模拟 std::jthread 解决上面的问题
class joining_thread
{
    std::thread _t;

public:
    joining_thread() noexcept = default;
    template <class CallBack, class... Args>
    explicit joining_thread(CallBack &&func, Args &&...args)
        : _t(std::forward<CallBack>(func), std::forward<Args>(args)...) {}

    explicit joining_thread(std::thread t) noexcept : _t(std::move(t)) {}
    joining_thread &operator=(std::thread other) noexcept
    {
        // 如果当前线程可汇合，则汇合等待线程完成再赋值
        if (joinable() == true)
            join();

        _t = std::move(other);
        return *this;
    }

    joining_thread(joining_thread &&other) noexcept : _t(std::move(other._t)) {}
    joining_thread &operator=(joining_thread &&other) noexcept
    {
        // 如果当前线程可汇合，则汇合等待线程完成再赋值
        if (joinable() == true)
            join();

        _t = std::move(other._t);
        return *this;
    }

    ~joining_thread()
    {
        if (joinable() == true)
            join();
    }

    void swap(joining_thread &other) noexcept
    {
        _t.swap(other._t);
    }

    std::thread::id get_id() const noexcept
    {
        return _t.get_id();
    }

    bool joinable() const noexcept
    {
        return _t.joinable() == true ? true : false;
    }

    void join()
    {
        _t.join();
    }

    void detach()
    {
        _t.detach();
    }

    std::thread &as_thread() noexcept
    {
        return _t;
    }

    const std::thread &as_thread() const noexcept
    {
        return _t;
    }
};
void use_jointhread()
{
    // 1. 根据线程构造函数构造 joining_thread
    joining_thread j1([](int value)
                      {
        for(int i = 0; i < value; i++){
            std::cout << "in thread id " << std::this_thread::get_id() << " cur index is " << i << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        } },
                      10);

    // 2. 根据 thread 构造 joining_thread
    joining_thread j2(std::thread([](int value)
                                  {
        for (int i = 0; i < value; i++)
        {
            std::cout << "in thread id " << std::this_thread::get_id() << " cur index is " << i << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        } },
                                  10));

    // 3. 根据 thread 构造 j3
    joining_thread j3(std::thread([](int value)
                                  {
        for (int i = 0; i < value; i++)
        {
            std::cout << "in thread id " << std::this_thread::get_id() << " cur index is " << i << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        } },
                                  10));

    // 4. 把 j3 赋值给 j1，joining_thread 内部会等待 j1 汇合结束后
    // 再将 j3 赋值给 j1
    j1 = std::move(j3);
}

// 并行计算的简单使用
template <class Iterator, class K>
struct accumulate_block
{
    void operator()(Iterator first, Iterator last, K &result)
    {
        result = std::accumulate(first, last, result);
    }
};
template <class Iterator, class K>
K parallel_accumulate(Iterator first, Iterator last, K init)
{
    unsigned long const length = std::distance(first, last);
    if (!length)
        return init;

    unsigned long const min_per_thread = 25;
    unsigned const long max_threads = (length + min_per_thread - 1) / min_per_thread;
    unsigned const long hardware_threads = std::thread::hardware_concurrency();
    unsigned const long num_threads = std::min(hardware_threads != 0 ? hardware_threads : 2, max_threads);
    unsigned const long block_size = length / num_threads;

    std::vector<K> results(num_threads);
    std::vector<std::thread> threads(num_threads - 1);
    Iterator block_start = first;
    for (unsigned long i = 0; i < (num_threads - 1); i++)
    {
        Iterator block_end = block_start;
        std::advance(block_end, block_size);
        threads[i] = std::thread(
            accumulate_block<Iterator, K>(),
            block_start, block_end, std::ref(results[i]));
        block_start = block_end;
    }
    accumulate_block<Iterator, K>()(
        block_start, last, results[num_threads - 1]);

    for (auto &entry : threads)
        entry.join();
    return std::accumulate(results.begin(), results.end(), init);
}
void use_parallel_acc()
{
    int count = 100000000;
    std::vector<int> array(count);
    // 在 push_back 的同时 array.size() 也是在变化的哦
    // for (int i = 0; i < array.size(); i++)
    for (int i = 0; i < count; i++)
        array.push_back(i);

    int sum_result = 0;
    sum_result = parallel_accumulate<std::vector<int>::iterator, int>(array.begin(), array.end(), sum_result);
    std::cout << "sum_result is " << sum_result << std::endl;
}

int main(void)
{
    // 1. thread 的存储问题
    // vector 的 push_back(会调用插入对象的拷贝构造函数) 和 emplace_back 对比
    use_vector(10);

    // 2. std::thread 和 std::jthread 的对比
    // dangerour_use();
    // 3. 模拟 std::jthread 解决上面的问题
    // use_jointhread();

    // 4. 并行计算的简单使用
    // use_parallel_acc();
    // std::this_thread::sleep_for(std::chrono::seconds(1));

    return 0;
}
