#pragma once

#include <iostream>
#include <string>
#include <chrono>
#include <future>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <thread>
#include <vector>
#include <functional>

class ThreadPool
{
public:
    ThreadPool(const ThreadPool &) = delete;
    ThreadPool &operator=(const ThreadPool &) = delete;

    static ThreadPool &ThreadPoolInstance()
    {
        static ThreadPool ThreadPoolInstance_;
        return ThreadPoolInstance_;
    }

    using Task = std::packaged_task<void()>;

    ~ThreadPool()
    {
        stop();
    }

    int idleThreadCount()
    {
        return thread_num_;
    }

    //
    template <class F, class... Args>
    auto commit(F &&f, Args &&...args) -> std::future<decltype(f(args...))>
    {
        using RetType = decltype(f(args...));
        if (stop_.load())
            return std::future<RetType>();

        auto task = std::make_shared<std::packaged_task<RetType()>>(
            std::bind(std::forward<F>(f), std::forward<Args>(args)...));

        std::future<RetType> ret = task->get_future();
        {
            std::lock_guard<std::mutex> lock(mutex_);
            task_queue_.emplace([task]
                                { (*task)(); });
        }
        condition_variable_.notify_one();
        return ret;
    }

private:
    ThreadPool(unsigned int num = 5)
        : stop_(false)
    {
        {
            if (num < 1)
                thread_num_ = 1;
            else
                thread_num_ = num;
        }

        // 1. 虽然我们的 thread_num_ 是有变化的，但是是一个 atomic_int 变量的哦
        // 2. 并且我们的操作是在构造函数里面的哦，只会执行一次的哦
        start();
    }

    void start()
    {
        for (int i = 0; i < thread_num_; i++)
        {
            thread_pool_.emplace_back([this]()
                                      {
                while(!this->stop_.load()){
                    Task task;
                    {
                        std::unique_lock<std::mutex> lock(this->mutex_);
                        this->condition_variable_.wait(lock, [this](){
                            return this->stop_.load() || !this->task_queue_.empty();
                        });
                        if(this->task_queue_.empty() == true)
                            return ;
                        
                        // 下面的得 std::move 的哦
                        task = std::move(this->task_queue_.front());
                        this->task_queue_.pop();
                    }
                    this->thread_num_--;
                    task();
                    this->thread_num_++;
                } });
        }
    }

    void stop()
    {
        stop_ = true;
        condition_variable_.notify_all();

        for (auto &threadPool : thread_pool_)
        {
            if (threadPool.joinable() == true)
            {
                std::cout << "join thread " << threadPool.get_id() << std::endl;
                threadPool.join();
            }
        }
    }

    std::mutex mutex_;
    std::condition_variable condition_variable_;
    std::atomic_bool stop_;
    std::atomic_int thread_num_;
    std::queue<Task> task_queue_;
    std::vector<std::thread> thread_pool_;
};
