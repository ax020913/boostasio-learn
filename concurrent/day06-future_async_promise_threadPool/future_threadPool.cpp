#include "future_threadPool.h"

#include <iostream>
#include <string>
#include <chrono>
#include <future>

// std::future、std::async 的使用
// 定义一个异步任务
std::string fetchDataFromDB(std::string query)
{
    // 模拟一个异步任务，比如从数据库中获取数据
    std::this_thread::sleep_for(std::chrono::seconds(1));
    return "Data: " + query;
}
void use_async()
{
    // 使用 std::async 异步调用 fetchDataFromDB
    std::future<std::string> resultFromDB = std::async(std::launch::async | std::launch::deferred, fetchDataFromDB, "Data");

    // 主线程中做其他的事情
    std::cout << "doing something else ..." << std::endl;

    // 从 future 中获取数据
    std::string dbData = resultFromDB.get();
    std::cout << dbData << std::endl;
}

//
// std::future、std::thread、std::package_task 的使用
int my_task()
{
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "my task run is " << std::endl;
    return 43;
}
void use_package()
{
    // 创建一个包装了任务的 std::package_task 对象
    std::packaged_task<int()> task(my_task);

    // 获取与任务关联的 std:future 对象
    std::future<int> result = task.get_future();

    // 另一个线程上执行任务
    std::thread tthread(std::move(task));
    // 将线程和主线程分离开，以便主线程可以等待任务完成
    tthread.detach();

    // 等待任务完成并获取结果
    int value = result.get();
    std::cout << "the result is " << value << std::endl;
}

//
// std::future, std::promise, std::thread的使用
void promise_set_value(std::promise<int> promise)
{
    // 设置 promise 的值
    std::this_thread::sleep_for(std::chrono::seconds(1));
    promise.set_value(43);
    std::cout << "promise set value success" << std::endl;
}
void use_promise()
{
    // 创建一个 promise 对象
    std::promise<int> promise;
    // 获取与 promise 相关联的 future 对象
    std::future<int> future = promise.get_future();
    // 在新的 std::thread 中设置 promise 的值
    std::thread myThread(promise_set_value, std::move(promise));

    // 在 main thread 中获取 future 中的值
    std::cout << "waiting fot the thread to set the value ..." << std::endl;
    std::cout << "value seet by the thread: " << future.get() << std::endl;

    myThread.join();
}
void use_promise_danger()
{
    // 创建一个 promise 对象
    std::promise<int> promise;
    // 获取与 promise 相关联的 future 对象
    std::future<int> future = promise.get_future();
    // 在新的 std::thread 中设置 promise 的值
    std::thread myThread(promise_set_value, std::move(promise));

    // 在 main thread 中获取 future 中的值
    std::cout << "waiting fot the thread to set the value ..." << std::endl;
    std::cout << "value seet by the thread: " << future.get() << std::endl;

    myThread.join();
}

//
// std::future, std::promise, std::thread 的使用
void myFunction(std::promise<int> &&promise)
{
    // 模拟一些工作
    std::this_thread::sleep_for(std::chrono::seconds(1));
    // 设置一些值
    promise.set_value(42);
}
void threadFunction(std::shared_future<int> future)
{
    try
    {
        int result = future.get();
        std::cout << " get result = " << result << std::endl;
    }
    catch (std::future_error &error_code)
    {
        std::cout << "future error: " << error_code.what() << std::endl;
    }
}
void use_shared_future_danger()
{
    std::promise<int> promise;
    std::shared_future<int> future = promise.get_future();

    // 将 promise 移动到线程中去
    std::thread myThread1(myFunction, std::move(promise));

    // 多个线程使用 shared() 方法获取新的 shared_future 对象
    std::thread myThread2(threadFunction, std::move(future));
    std::thread myThread3(threadFunction, std::move(future));

    myThread1.join();
    myThread2.join();
    myThread3.join();
}

//
// std::thread, std::future, std::promise 的使用
void set_exception(std::promise<void> promise)
{
    try
    {
        // 抛出一个异常
        throw std::runtime_error("an error occurrend!");
    }
    catch (...)
    {
        // 设置 promise 的异常
        promise.set_exception(std::current_exception());
    }
}
void use_promise_exception()
{
    std::promise<void> promise;
    std::future<void> future = promise.get_future();
    std::thread myThread(set_exception, std::move(promise));

    // 在主线程中获取 future 的异常
    try
    {
        std::cout << "waiting for this thread to set the exception ..." << std::endl;
        future.get();
    }
    catch (const std::exception &error_code)
    {
        std::cout << "exception set by the thread: " << error_code.what() << std::endl;
    }

    myThread.join();
}
// std::promise 被销毁了就不能用了，并且相关联的 std::future 也是不能用了的哦
void use_promise_destruct()
{
    std::thread myThread;
    std::future<int> future;
    {
        // 创建一个 promise 对象
        std::promise<int> promise;
        // 获取与 promise 相关联的 future 对象
        future = promise.get_future();
        // 在新线程中设置 promise 的值
        myThread = std::thread(promise_set_value, std::move(promise));
    }
    // 在主线程中获取 future 的值
    std::cout << "Waiting for the thread to set the value...\n";
    std::cout << "Value set by the thread: " << future.get() << '\n';
    myThread.join();
}

//
void may_throw()
{
    // 这里我们抛出一个异常。在实际的程序中，这可能在任何地方发生。
    throw std::runtime_error("Oops, something went wrong!");
}
void use_future_exception()
{
    std::future<void> result(std::async(std::launch::async | std::launch::deferred, may_throw));

    try
    {
        // 获取结果（如果在获取结果时发生了异常，那么会重新抛出这个异常）
        result.get();
    }
    catch (const std::exception &error_code)
    {
        // 捕获并打印异常
        std::cout << "catch exception: " << error_code.what() << std::endl;
    }
}

int main(void)
{
    // async, future, promise
    // use_async();
    // use_package();
    // use_promise();
    // use_shared_future();
    // use_shared_future_danger();
    // use_promise_exception();
    // use_promise_destruct();
    // use_future_exception();

    // threadPool
    auto &thPool1 = ThreadPool::ThreadPoolInstance();
    auto &thPool2 = ThreadPool::ThreadPoolInstance();
    std::cout << "thPool1 si " << &thPool1 << std::endl;
    std::cout << "thPool2 si " << &thPool2 << std::endl;

    int count = 0;
    ThreadPool::ThreadPoolInstance().commit([](int &count)
                                            {
        count = 1024;
        std::cout << "threadId = " << std::this_thread::get_id()
                  << " inner set count is " << count << std::endl; },
                                            count);
    // 先卡住主线程，让上面的线程覆盖一下 count 的值
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "count is " << count << std::endl;

    ThreadPool::ThreadPoolInstance().commit([](int &count)
                                            {
        count = 2048;
        std::cout << "threadId = " << std::this_thread::get_id()
                  << " inner set count is " << count << std::endl; },
                                            std::ref(count));
    // 先卡住主线程，让上面的线程覆盖一下 count 的值
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "count is " << count << std::endl;

    return 0;
}
