### `C++` 并发三剑客 `future` 、 `promise` 和 `async`


#### 1. `std::async` 的用法
`std::async` 是一个用于异步执行函数的模板函数，它返回一个 `std::future` 对象，该对象用于获取函数的返回值。

以下是一个使用 `std::async` 的示例：
```
// 定义一个异步任务
std::string fetchDataFromDB(std::string query){
    // 模拟一个异步任务，比如从数据库中获取数据
    std::this_thread::sleep_for(std::chrono::seconds(1));
    return "Data: " + query;
}
void use_async(){
    // 使用 std::async 异步调用 fetchDataFromDB
    std::future<std::string> resultFromDB = std::async(std::launch::async | std::launch::deferred, fetchDataFromDB, "Data");

    // 主线程中做其他的事情
    std::cout << "doing something else ..." << std::endl;

    // 从 future 中获取数据
    std::string dbData = resultFromDB.get();
    std::cout << dbData << std::endl;
}
```
这个示例中，`std::async` 创建了一个新的线程（或从内部线程池中挑选一个线程）并自动与一个 `std::promise` 对象相关联。`std::promise` 对象被传递给 `fetchDataFromDB` 函数，函数的返回值被存储在 `std::future` 对象中。在主线程中，我们可以使用 `std::future::get` 方法从 `std::future` 对象中获取数据。注意，在使用 `std::async` 的情况下，我们必须使用 `std::launch::async` 标志来明确表明我们希望函数异步执行。

上面的例子输出：
```
doing something else ...
Data: Data
```

#### 2. `async` 的启动策略
`std::async` 函数可以接受几个不同的启动策略，这些策略在 `std::launch` 枚举中定义。除了`std::launch::async` 之外，还有以下启动策略：

`std::launch::deferred`：这种策略意味着任务将在调用`std::future::get()`或`std::future::wait()`函数时延迟执行。换句话说，任务将在需要结果时同步执行。
`std::launch::async | std::launch::deferred`：这种策略是上面两个策略的组合。任务可以在一个单独的线程上异步执行，也可以延迟执行，具体取决于实现。
默认情况下，`std::async`使用`std::launch::async | std::launch::deferred`策略。这意味着任务可能异步执行，也可能延迟执行，具体取决于实现。需要注意的是，不同的编译器和操作系统可能会有不同的默认行为。

#### 3. `future` 的 `wait` 和 `get`
`std::future::get()` 和 `std::future::wait()` 是 `C++` 中用于处理异步任务的两个方法，它们的功能和用法有一些重要的区别。

##### `std::future::get()`:
`std::future::get()` 是一个阻塞调用，用于获取 `std::future` 对象表示的值或异常。如果异步任务还没有完成，`get()` 会阻塞当前线程，直到任务完成。如果任务已经完成，`get()` 会立即返回任务的结果。重要的是，`get()` 只能调用一次，因为它会移动或消耗掉 `std::future` 对象的状态。一旦 `get()` 被调用，`std::future` 对象就不能再被用来获取结果。

##### `std::future::wait()`:
`std::future::wait()` 也是一个阻塞调用，但它与 `get()` 的主要区别在于 `wait()` 不会返回任务的结果。它只是等待异步任务完成。如果任务已经完成，`wait()` 会立即返回。如果任务还没有完成，`wait()` 会阻塞当前线程，直到任务完成。与 `get()` 不同，`wait()` 可以被多次调用，它不会消耗掉 `std::future` 对象的状态。

总结一下，这两个方法的主要区别在于：

- `std::future::get()` 用于获取并返回任务的结果，而 `std::future::wait()` 只是等待任务完成。
- `get()` 只能调用一次，而 `wait()` 可以被多次调用。
- 如果任务还没有完成，`get()` 和 `wait()` 都会阻塞当前线程，但 `get()` 会一直阻塞直到任务完成并返回结果，而 `wait()` 只是在等待任务完成。

你可以使用`std::future`的`wait_for()`或`wait_until()`方法来检查异步操作是否已完成。这些方法返回一个表示操作状态的`std::future_status`值。
```
if(fut.wait_for(std::chrono::seconds(0)) == std::future_status::ready) {  
    // 操作已完成  
} else {  
    // 操作尚未完成  
}
```

#### 4. 将任务和 `future` 关联
`std::packaged_task` 和 `std::future` 是 `C++11` 中引入的两个类，它们用于处理异步任务的结果。

`std::packaged_task`是一个可调用目标，它包装了一个任务，该任务可以在另一个线程上运行。它可以捕获任务的返回值或异常，并将其存储在`std::future`对象中，以便以后使用。

`std::future`代表一个异步操作的结果。它可以用于从异步任务中获取返回值或异常。

以下是使用`std::packaged_task`和`std::future`的基本步骤：

1. 创建一个`std::packaged_task`对象，该对象包装了要执行的任务。
2. 调用`std::packaged_task`对象的`get_future()`方法，该方法返回一个与任务关联的`std::future`对象。
3. 在另一个线程上调用`std::packaged_task`对象的`operator()`，以执行任务。
4. 在需要任务结果的地方，调用与任务关联的`std::future`对象的`get()`方法，以获取任务的返回值或异常。
以下是一个简单的示例代码：
```
// std::future、std::thread、std::package_task 的使用
int my_task()
{
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "my task run is " << std::endl;
    return 43;
}
void use_package()
{
    // 创建一个包装了任务的 std::package_task 对象
    std::packaged_task<int()> task(my_task);

    // 获取与任务关联的 std:future 对象
    std::future<int> result = task.get_future();

    // 另一个线程上执行任务
    std::thread tthread(std::move(task));
    // 将线程和主线程分离开，以便主线程可以等待任务完成
    tthread.detach();

    // 等待任务完成并获取结果
    int value = result.get();
    std::cout << "the result is " << value << std::endl;
}
```
在上面的示例中，我们创建了一个包装了任务的`std::packaged_task`对象，并获取了与任务关联的`std::future`对象。然后，我们在另一个线程上执行任务，并等待任务完成并获取结果。最后，我们输出结果。

我们可以使用 `std::function` 和 `std::package_task` 来包装带参数的函数。`std::package_task` 是一个模板类，它包装了一个可调用对象，并允许我们将其作为异步任务传递。

上面的例子输出:
```
my task run is 
the result is 43
```

#### 5. `promise` 用法
`C++11`引入了`std::promise`和`std::future`两个类，用于实现异步编程。`std::promise`用于在某一线程中设置某个值或异常，而`std::future`则用于在另一线程中获取这个值或异常。

下面是`std::promise`的基本用法：
```
// std::future, std::promise, std::thread的使用
void promise_set_value(std::promise<int> promise)
{
    // 设置 promise 的值
    std::this_thread::sleep_for(std::chrono::seconds(1));
    promise.set_value(43);
    std::cout << "promise set value success" << std::endl;
}
void use_promise()
{
    // 创建一个 promise 对象
    std::promise<int> promise;
    // 获取与 promise 相关联的 future 对象
    std::future<int> future = promise.get_future();
    // 在新的 std::thread 中设置 promise 的值
    std::thread myThread(promise_set_value, std::move(promise));

    // 在 main thread 中获取 future 中的值
    std::cout << "waiting fot the thread to set the value ..." << std::endl;
    std::cout << "value seet by the thread: " << future.get() << std::endl;

    myThread.join();
}
```
上面的例子输出:
```
waiting fot the thread to set the value ...
value seet by the thread: promise set value success
43
```

#### 6. 共享类型的 `future`
当我们需要多个线程等待同一个执行结果时，需要使用`std::shared_future`

以下是一个适合使用`std::shared_future`的场景，多个线程等待同一个异步操作的结果：

假设你有一个异步任务，需要多个线程等待其完成，然后这些线程需要访问任务的结果。在这种情况下，你可以使用`std::shared_future`来共享异步任务的结果。
```
void myFunction(std::promise<int> &&promise)
{
    // 模拟一些工作
    std::this_thread::sleep_for(std::chrono::seconds(1));
    // 设置一些值
    promise.set_value(42);
}
void threadFunction(std::shared_future<int> future)
{
    try
    {
        int result = future.get();
        std::cout << "result = " << result << std::endl;
    }
    catch (std::future_error &error_code)
    {
        std::cout << "future error: " << error_code.what() << std::endl;
    }
}
void use_shared_future()
{
    std::promise<int> promise;
    std::shared_future<int> future = promise.get_future();

    // 将 promise 移动到线程中去
    std::thread myThread1(myFunction, std::move(promise));

    // 多个线程使用 shared() 方法获取新的 shared_future 对象
    std::thread myThread2(threadFunction, future);
    std::thread myThread3(threadFunction, future);

    myThread1.join();
    myThread2.join();
    myThread3.join();
}
```
在这个示例中，我们创建了一个`std::promise<int>`对象`promise`和一个与之关联的`std::shared_future<int>`对象 `future` 。然后，我们将 `promise` 对象移动到另一个线程 `myThread1` 中，该线程将执行 `myFunction` 函数，并在完成后设置 `promise` 的值。我们还创建了两个线程 `myThread2` 和 `myThread3` ，它们将等待`future` 对象的结果。如果`myThread1`成功地设置了`promise`的值，那么`future.get()`将返回该值。这些线程可以同时访问和等待`future`对象的结果，而不会相互干扰。

但是大家要注意，如果一个`future`被移动给两个`shared_future`是错误的。
```
void use_shared_future_danger()
{
    std::promise<int> promise;
    std::shared_future<int> future = promise.get_future();

    // 将 promise 移动到线程中去
    std::thread myThread1(myFunction, std::move(promise));

    // 多个线程使用 shared() 方法获取新的 shared_future 对象
    std::thread myThread2(threadFunction, std::move(future));
    std::thread myThread3(threadFunction, std::move(future));

    myThread1.join();
    myThread2.join();
    myThread3.join();
}
```
这种用法是错误的，一个 `future` 通过隐式构造传递给 `shared_future` 之后，这个 `shared_future` 被移动传递给两个线程是不合理的，因为第一次移动后 `ared_future` 的生命周期被转移了，接下来 `myThread3` 构造时用的 `std::move(future)future`已经失效了，会报错，一般都是 `no state` 之类的错误。


#### 7. 异常处理
`std::future` 是`C++`的一个模板类，它用于表示一个可能还没有准备好的异步操作的结果。你可以通过调用 `std::future::get` 方法来获取这个结果。如果在获取结果时发生了异常，那么 `std::future::get` 会重新抛出这个异常。

以下是一个例子，演示了如何在 `std::future` 中获取异常：
```
void may_throw()
{
    // 这里我们抛出一个异常。在实际的程序中，这可能在任何地方发生。
    throw std::runtime_error("Oops, something went wrong!");
}
void use_future_exception()
{
    std::future<void> result(std::async(std::launch::async | std::launch::deferred, may_throw));

    try
    {
        // 获取结果（如果在获取结果时发生了异常，那么会重新抛出这个异常）
        result.get();
    }
    catch (const std::exception &error_code)
    {
        // 捕获并打印异常
        std::cout << "catch exception: " << error_code.what() << std::endl;
    }
}
```
在这个例子中，我们创建了一个异步任务 `may_throw`，这个任务会抛出一个异常。然后，我们创建一个 `std::future` 对象 `result` 来表示这个任务的结果。在 `main` 函数中，我们调用 `result.get()` 来获取任务的结果。如果在获取结果时发生了异常，那么 `result.get()` 会重新抛出这个异常，然后我们在 `catch` 块中捕获并打印这个异常。

上面的例子输出:
```
waiting for this thread to set the exception ...
exception set by the thread: an error occurrend!
```

### 线程池
`std::thread`

`atomic`

`mutex`

`condition_variable`

`queue`

`std::future`

`std::packaged_task`

我们可以利用上面提到的`std::packaged_task`和`std::promise`构建线程池，提高程序的并发能力。
先了解什么是线程池：

线程池是一种多线程处理形式，它处理过程中将任务添加到队列，然后在创建线程后自动启动这些任务。线程池线程都是后台线程。每个线程都使用默认的堆栈大小，以默认的优先级运行，并处于多线程单元中。如果某个线程在托管代码中空闲（如正在等待某个事件）,则线程池将插入另一个辅助线程来使所有处理器保持繁忙。如果所有线程池线程都始终保持繁忙，但队列中包含挂起的工作，则线程池将在一段时间后创建另一个辅助线程但线程的数目永远不会超过最大值。超过最大值的线程可以排队，但他们要等到其他线程完成后才启动。

线程池可以避免在处理短时间任务时创建与销毁线程的代价，它维护着多个线程，等待着监督管理者分配可并发执行的任务，从而提高了整体性能。

下面是提供的一套线程池源码:
```
#pragma once

#include <iostream>
#include <string>
#include <chrono>
#include <future>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <thread>
#include <vector>
#include <functional>

class ThreadPool
{
public:
    ThreadPool(const ThreadPool &) = delete;
    ThreadPool &operator=(const ThreadPool &) = delete;

    static ThreadPool &ThreadPoolInstance()
    {
        static ThreadPool ThreadPoolInstance_;
        return ThreadPoolInstance_;
    }

    using Task = std::packaged_task<void()>;

    ~ThreadPool()
    {
        stop();
    }

    int idleThreadCount()
    {
        return thread_num_;
    }

    //
    template <class F, class... Args>
    auto commit(F &&f, Args &&...args) -> std::future<decltype(f(args...))>
    {
        using RetType = decltype(f(args...));
        if (stop_.load())
            return std::future<RetType>();

        auto task = std::make_shared<std::packaged_task<RetType()>>(
            std::bind(std::forward<F>(f), std::forward<Args>(args)...));

        std::future<RetType> ret = task->get_future();
        {
            std::lock_guard<std::mutex> lock(mutex_);
            task_queue_.emplace([task]
                                { (*task)(); });
        }
        condition_variable_.notify_one();
        return ret;
    }

private:
    ThreadPool(unsigned int num = 5)
        : stop_(false)
    {
        {
            if (num < 1)
                thread_num_ = 1;
            else
                thread_num_ = num;
        }

        // 1. 虽然我们的 thread_num_ 是有变化的，但是是一个 atomic_int 变量的哦
        // 2. 并且我们的操作是在构造函数里面的哦，只会执行一次的哦
        start();
    }

    void start()
    {
        for (int i = 0; i < thread_num_; i++)
        {
            thread_pool_.emplace_back([this]()
                                      {
                while(!this->stop_.load()){
                    Task task;
                    {
                        std::unique_lock<std::mutex> lock(this->mutex_);
                        this->condition_variable_.wait(lock, [this](){
                            return this->stop_.load() || !this->task_queue_.empty();
                        });
                        if(this->task_queue_.empty() == true)
                            return ;
                        
                        // 下面的得 std::move 的哦
                        task = std::move(this->task_queue_.front());
                        this->task_queue_.pop();
                    }
                    this->thread_num_--;
                    task();
                    this->thread_num_++;
                } });
        }
    }

    void stop()
    {
        stop_ = true;
        condition_variable_.notify_all();

        for (auto &threadPool : thread_pool_)
        {
            if (threadPool.joinable() == true)
            {
                std::cout << "join thread " << threadPool.get_id() << std::endl;
                threadPool.join();
            }
        }
    }

    std::mutex mutex_;
    std::condition_variable condition_variable_;
    std::atomic_bool stop_;
    std::atomic_int thread_num_;
    std::queue<Task> task_queue_;
    std::vector<std::thread> thread_pool_;
};


```

测试：
```
int main(void){
    auto &thPool1 = ThreadPool::ThreadPoolInstance();
    auto &thPool2 = ThreadPool::ThreadPoolInstance();
    std::cout << "thPool1 si " << &thPool1 << std::endl;
    std::cout << "thPool2 si " << &thPool2 << std::endl;

    int count = 0;
    ThreadPool::ThreadPoolInstance().commit([](int &count)
                                            {
        count = 1024;
        std::cout << "threadId = " << std::this_thread::get_id()
                  << " inner set count is " << count << std::endl; },
                                            count);
    // 先卡住主线程，让上面的线程覆盖一下 count 的值
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "count is " << count << std::endl;

    ThreadPool::ThreadPoolInstance().commit([](int &count)
                                            {
        count = 2048;
        std::cout << "threadId = " << std::this_thread::get_id()
                  << " inner set count is " << count << std::endl; },
                                            std::ref(count));
    // 先卡住主线程，让上面的线程覆盖一下 count 的值
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "count is " << count << std::endl;

    return 0;
}
```

结果：
```
thPool1 si 0x4484e0
thPool2 si 0x4484e0
threadId = 139875344738048 inner set count is 1024
count is 0
threadId = 139875344738048 inner set count is 2048
count is 2048
join thread 139875378308864
join thread 139875369916160
join thread 139875361523456
join thread 139875353130752
join thread 139875344738048
```
