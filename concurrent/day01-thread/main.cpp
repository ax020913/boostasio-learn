#define _CRT_SECURE_NO_DEPRECATE

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <thread>
#include <memory>
#include <numeric>

using namespace std;

// void thread_work(std::string &str) // error
void thread_work(std::string str)
{
    std::cout << str << std::endl;
}

void thread_work_()
{
    cout << "this is t4 thread" << endl;
}

class background_task
{
public:
    void operator()()
    {
        std::cout << "background_task called" << std::endl;
    }
};

struct func
{
    int &_i;
    func(int &i) : _i(i) {}
    void operator()()
    {
        for (int i = 0; i < 3; i++)
        {
            _i = i;
            std::cout << "_i is " << _i << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }
};

void oops()
{
    int some_local_state = 0;
    func myfunc(some_local_state);
    std::thread funcThread(myfunc);
    // 隐患: 访问局部变量，局部变量可能会随着 } 结束而回收 或 随着主线程退出而回收
    funcThread.detach();
}
void use_join()
{
    int some_local_state = 0;
    func myfunc(some_local_state);
    std::thread funcThread(myfunc);
    funcThread.join();
}
void catch_exception()
{
    int some_local_state = 0;
    func myfunc(some_local_state);
    std::thread functhread{myfunc};
    try
    {
        // 本线程做一些事情
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    catch (std::exception &e)
    {
        functhread.join();
        throw;
    }

    functhread.join();
}

class thread_guard
{
private:
    std::thread &_t;

public:
    // 1. std::thread 虽然不能赋值，但是还是可以传引用的
    // 2. const、&、基类的构造函数必须在初始化列表进行初始化
    explicit thread_guard(std::thread &t) : _t(t) {}
    ~thread_guard()
    {
        // join 只能调用一次
        // join()函数只能调用一次的原因是为了避免多个线程同时等待同一个线程的完成，从而导致竞争条件和未定义行为。
        if (_t.joinable() == true)
        {
            _t.join();
        }
    }

    thread_guard(const thread_guard &t) = delete;
    thread_guard &operator=(const thread_guard &t) = delete;
};
void auto_guard()
{
    int some_local_state = 0;
    func myfunc(some_local_state);
    std::thread funcThread(myfunc);

    // funcThread.join();
    thread_guard g(funcThread);
    // 本线程做一些事情
    std::cout << "auto guard finished" << std::endl;
}

void print_str(int i, std::string const &str)
{
    std::cout << "i is " << i << " str is " << str << std::endl;
}
void danger_oops(int som_param)
{
    char buffer[1024];
    sprintf(buffer, "%i", som_param);
    // 在线程内部将 char const* 转化为 std::string
    // 指针常量  char * const p  指针本身不能变(const 后面的 p 指针不能改变)
    // 常量指针  char const * p / const char * p 指向的内容不能变(const 后面的 *p 内容不能改变)
    std::thread t(print_str, 3, buffer);
    t.detach();
    std::cout << "danger oops finished" << std::endl;
}
void safe_oops(int som_param)
{
    char buffer[1024];
    sprintf(buffer, "%i", som_param);
    std::thread t(print_str, 3, std::string(buffer));
    t.detach();
}

void change_param(int &param)
{
    param++;
}
void ref_oops(int some_param)
{
    std::cout << "before change, param is " << some_param << std::endl;
    // 需使用引用显示转换
    std::thread t(change_param, std::ref(some_param));
    t.join();
    std::cout << "after change, param is " << some_param << std::endl;
}

class X
{
public:
    void de_lengthhy_work()
    {
        std::cout << "do_lengthy_work" << std::endl;
    }
};
void bind_class_oops()
{
    X my_x;
    std::thread t(&X::de_lengthhy_work, &my_x); // 类成员的隐藏的 this 指针成员
    t.join();
}

void deal_unique(std::unique_ptr<int> p)
// void deal_shared(std::shared_ptr<int> p)
{
    std::cout << "ptr data is " << *p << std::endl;
    (*p)++;
    std::cout << "after ptr data is " << *p << std::endl;
}
void move_oops()
{
    auto p = std::make_unique<int>(100); // make_unique 在 c++14 提出
    // auto p = std::make_shared<int>(100); // make_shared 在 c++11 提出
    std::thread t(deal_unique, std::move(p));
    // std::thread t(deal_shared, std::move(p));
    t.join();
    // p 给 move 了，就不能再使用了
    // std::cout << "after ptr data is " << *p << std::endl;
}

int main()
{
    std::string str = "this is t1 thread";
    // 1. 通过 () 初始化并启动一个线程
    std::thread t1(thread_work, str);
    // 2. 主线程等待子线程退出
    t1.join();

    // 3. t2 被当作函数对象的定义，其类型为返回 std::thread, 参数为 background_task
    // std::thread t2(background_task());
    // t2.join();
    // 可以多加一层
    std::thread t2((background_task()));
    t2.join();
    // 也是可以使用 {} 的方式初始化的哦
    std::thread t3{background_task()};
    t3.join();
    std::thread t4(thread_work_);
    t4.join();

    // 4. lambda 表达式
    std::thread t5([](std::string str)
                   { std::cout << str << std::endl; },
                   "this is t5 thread");
    t5.join();

    // 5. detach 的注意事项
    oops();
    // 防止主线程退出过快，需要停顿一下，让子线程跑起来 detach
    std::this_thread::sleep_for(std::chrono::seconds(1));

    // 6. join 的用法
    use_join();

    // 7. 捕获异常
    catch_exception();

    // 8. 自动守卫
    auto_guard();

    // 危险可能存在崩溃
    danger_oops(100);
    std::this_thread::sleep_for(std::chrono::seconds(1));
    // 安全，提前转化
    safe_oops(100);
    std::this_thread::sleep_for(std::chrono::seconds(1));

    // 绑定引用
    ref_oops(100);
    // 绑定类的成员函数
    bind_class_oops();
    // 通过 std::move 传递参数
    move_oops();

    return 0;
}
