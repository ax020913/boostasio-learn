### 一. `std::thread` 的使用细节

#### 1.1 直接传递一个回调函数，再加上需要传递的参数
```
void thread_work(std::string str){
    std::cout << str << std::endl;
}
std::string str = "this is t1 thread";
std::thread t1(thread_work, str);
t1.join();
```

#### 1.2 重载()的应用

```
class background_task{
public:
    void operator()(){
        std::cout << "background_task called" << std::endl;
    }
};

// 3. t2 被当作函数对象的定义，其类型为返回 std::thread, 参数为 background_task
// std::thread t2(background_task());
// t2.join();
// 可以多加一层
std::thread t2((background_task()));
t2.join();

// 也是可以使用 {} 的方式初始化的哦
std::thread t3{background_task()};
t3.join();

std::thread t4(thread_work_);
t4.join();
```

#### 1.3 直接用 `lambda` 表达式
```
std::thread t5([](std::string str){ 
    std::cout << str << std::endl; 
    },
    "this is t5 thread"
);
t5.join();
```


### 二. 线程退出的问题

#### 2.1 `detach` 的注意事项
隐患: 访问局部变量，局部变量可能会随着 `}` 结束而回收 或 随着主线程退出而回收
```
struct func{
    int &_i;
    func(int &i) : _i(i) {}
    void operator()(){
        for (int i = 0; i < 3; i++){
            _i = i;
            std::cout << "_i is " << _i << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }
};

void oops(){
    int some_local_state = 0;
    func myfunc(some_local_state);
    std::thread funcThread(myfunc);
    funcThread.detach();
}

int main(void){
    oops();

    return 0；
}
```
join 来了：
```
struct func{
    int &_i;
    func(int &i) : _i(i) {}
    void operator()(){
        for (int i = 0; i < 3; i++){
            _i = i;
            std::cout << "_i is " << _i << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }
};

void use_join(){
    int some_local_state = 0;
    func myfunc(some_local_state);
    std::thread funcThread(myfunc);
    funcThread.join();
}

int main(void){
    use_join();

    return 0;
}
```

#### 2.2 `join` 和 `detach` 的区别
`detach`: `detach` 是子线程和线程分离，但是子线程可能会面临父线程先退出，子线程成孤儿线程的问题(**子线程孤儿问题**)。也可能子线程使用了父线程的资源，因为父线程先退出，其相应的资源被释放，队子线程造成的问题(**父线程资源问题**)。

`join`: `join` 是父线程等待子线程先退出，父得得知子线程退出的信息。所以 `join` 和 `detach` 都是只能由父线程调用的，并且只能调用一次。

**可以使用 `join` 函数或其他同步机制来管理线程的生命周期。**

#### 2.3 捕获异常
```
struct func{
    int &_i;
    func(int &i) : _i(i) {}
    void operator()(){
        for (int i = 0; i < 3; i++){
            _i = i;
            std::cout << "_i is " << _i << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }
};

void catch_exception(){
    int some_local_state = 0;
    func myfunc(some_local_state);
    std::thread functhread{myfunc};
    try{
        // 本线程做一些事情
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    catch (std::exception &e){
        functhread.join();
        throw;
    }

    functhread.join();
}

int main(void){
    catch_exception();

    return 0;
}
```

#### 2.4 自动守卫
```
struct func{
    int &_i;
    func(int &i) : _i(i) {}
    void operator()(){
        for (int i = 0; i < 3; i++){
            _i = i;
            std::cout << "_i is " << _i << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }
};

class thread_guard{
private:
    std::thread &_t;

public:
    // 1. std::thread 虽然不能赋值，但是还是可以传引用的
    // 2. const、&、基类的构造函数必须在初始化列表进行初始化
    explicit thread_guard(std::thread &t) : _t(t) {}
    ~thread_guard(){
        // join 只能调用一次
        // join()函数只能调用一次的原因是为了避免多个线程同时等待同一个线程的完成，从而导致竞争条件和未定义行为。
        if (_t.joinable() == true)
            _t.join();
    }

    thread_guard(const thread_guard &t) = delete;
    thread_guard &operator=(const thread_guard &t) = delete;
};
void auto_guard(){
    int some_local_state = 0;
    func myfunc(some_local_state);
    std::thread funcThread(myfunc);

    // funcThread.join();
    thread_guard g(funcThread);
    // 本线程做一些事情
    std::cout << "auto guard finished" << std::endl;
}

int main(void){
    auto_guard();

    return 0;
}
```


### 三. `std::thread` 回调函数传参问题

#### 3.1 值传递
```
void print_str(int i, std::string const &str){
    std::cout << "i is " << i << " str is " << str << std::endl;
}
void danger_oops(int som_param){
    char buffer[1024];
    sprintf(buffer, "%i", som_param);
    // 在线程内部将 char const* 转化为 std::string
    // 指针常量  char * const p  指针本身不能变(const 后面的 p 指针不能改变)
    // 常量指针  char const * p / const char * p 指向的内容不能变(const 后面的 *p 内容不能改变)
    std::thread t(print_str, 3, buffer);
    t.detach();
    std::cout << "danger oops finished" << std::endl;
}
void safe_oops(int som_param){
    char buffer[1024];
    sprintf(buffer, "%i", som_param);
    std::thread t(print_str, 3, std::string(buffer));
    t.detach();
}

int main(void){
    danger_oops(100);
    safe_oops(100);

    return 0;
}
```

#### 3.2 `std::ref` 引用
```
void change_param(int &param){
    param++;
}
void ref_oops(int some_param){
    std::cout << "before change, param is " << some_param << std::endl;
    // 需使用引用显示转换
    std::thread t(change_param, std::ref(some_param));
    t.join();
    std::cout << "after change, param is " << some_param << std::endl;
}

int main(void){
    ref_oops(100);

    return 0;
}
```

#### 3.3 `std::move` 注意原值不能再用(悬空)
```
void deal_unique(std::unique_ptr<int> p){
// void deal_shared(std::shared_ptr<int> p){
    std::cout << "ptr data is " << *p << std::endl;
    (*p)++;
    std::cout << "after ptr data is " << *p << std::endl;
}
void move_oops(){
    auto p = std::make_unique<int>(100); // make_unique 在 c++14 提出
    // auto p = std::make_shared<int>(100); // make_shared 在 c++11 提出
    std::thread t(deal_unique, std::move(p));
    // std::thread t(deal_shared, std::move(p));
    t.join();
    // p 给 move 了，就不能再使用了
    // std::cout << "after ptr data is " << *p << std::endl;
}

int main(void){
    move_oops();

    return 0;
}
```
