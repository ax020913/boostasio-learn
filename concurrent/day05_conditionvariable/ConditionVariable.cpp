// #pragma once // 防止头文件的重复包含。一般是写在头文件里面的

#include <iostream>
#include <string>

#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

// 下面是实现 t1 线程和 t2 线程交替打印 1 2 1 2 ......
// void PoorImplemention() 和 void ResonableImplemention() 两种实现，后面使用条件变量是更好的,打印的时候是更流畅的，更好的的利用cpu的效率。
// c++ 里面的 unique_lock 可以配合 mutex 和 condition_variable 一起来使用
int num = 1;
std::mutex mutex_;
std::condition_variable cvA;
std::condition_variable cvB;

// mutex 的实现：
void PoorImplemention()
{
    std::thread t1([]()
                   {
        for(;;){
            {
                std::lock_guard<std::mutex> lock(mutex_);
                if(num == 1){
                    std::cout << "htread A print 1 ......" << std::endl;
                    num++;
                    continue;
                }
            }
            // 如果不 sleep 的话，就会不断地执行 if 判断，消耗的是cpu的上下文切换
            std::this_thread::sleep_for(std::chrono::milliseconds(500)); // sleep 消耗性能
        } });

    std::thread t2([]()
                   {
        for(;;){
            {
                std::lock_guard<std::mutex> lock(mutex_);
                if(num == 2){
                    std::cout << "htread B print 2 ......" << std::endl;
                    num--;
                    continue;
                }
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(500)); // sleep 消耗性能
        } });

    t1.join();
    t2.join();
}

// condition_variable 的实现：
void ResonableImplemention()
{
    // 下面线程里面使用的都是全局变量
    std::thread t1([]()
                   {
        for(;;){
            {
                std::unique_lock<std::mutex> lock(mutex_);
                
                // 下面的两种写法都是可以的哦
                // 写法一：
                // 细节：不过注意下面的是 while 而不是 if，因为可能是 os 执行类似于 notify 唤醒的操作的，所以还得判断的哦。
                // while(num != 1){ 
                //     cvA.wait(lock);
                // }
                // 写法二：
                // 返回false是挂起等待，等待notify通知
                cvA.wait(lock, [](){
                    return num == 1;
                });
                
                num++;
                std::cout << "thread A print 1 ......" << std::endl;
                cvB.notify_all();
            }
        } });

    std::thread t2([]()
                   {
        for(;;){
            std::unique_lock<std::mutex> lock(mutex_);
            cvB.wait(lock, [](){
                return num == 2;
            });

            num --;
            std::cout << "thread B print 2 ......" << std::endl;
            cvA.notify_all();
        } });
    t1.join();
    t2.join();
}

// int main(void)
// {
//     // PoorImplemention();
//     // ResonableImplemention();

//     return 0;
// }

////////////////////////////////////////////////////////////////////////////

// 线程安全的队列实现：

#include <iostream>
#include <string>
#include <queue>
#include <thread>
#include <memory>
#include <mutex>
#include <condition_variable>

template <class K>
class threadsafe_queue
{
private:
    // public:
    std::queue<K> data_queue;
    std::mutex mutex_;
    std::condition_variable data_cond;

public:
    threadsafe_queue() {}
    // & 是为了防止过多的拷贝的开销，const 是为了避免我们这个的函数修改 other
    threadsafe_queue(threadsafe_queue const &other)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        data_queue = other.data_queue;
    }

    void push(K new_value)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        data_queue.push(new_value);

        // 有数据了，通知其他人来，可以使用操作数据了
        data_cond.notify_all();
    }

    // 使用了 condition_variable 的线程可以挂起等待pop（wait_and_pop）
    void wait_and_pop(K &value)
    {
        std::unique_lock<std::mutex> lock(mutex_);

        // 得有数据才能 pop，不然挂起等待
        data_cond.wait(lock, [this]()
                       { return !data_queue.empty(); });
        value = data_queue.front();
        data_queue.pop();
    }
    std::shared_ptr<K> wait_and_pop()
    {
        std::unique_lock<std::mutex> lock(mutex_);
        data_cond.wait(lock, [this]()
                       { return !data_queue.empty(); });
        // 注意：res 虽然是一个局部变量，但是下面 return res 是得其计数器为 2，执行完析构函数的话计数器--也是1，不会被销毁的
        std::shared_ptr<K> res(std::make_shared<K>(data_queue.front()));
        data_queue.pop();
        return res;
    }

    // 没有使用 condition_variable 的线程可以判断能否pop（try_pop）
    bool try_pop(K &value)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        // 队列为空就返回 false
        if (data_queue.empty())
            return false;
        value = data_queue.front();
        data_queue.pop();
        return true;
    }
    std::shared_ptr<K> try_pop()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        // 队列为空就返回一个 null
        if (data_queue.empty())
            return std::shared_ptr<K>();
        std::shared_ptr<K> res(std::make_shared<K>(data_queue.front()));
        data_queue.pop();
        return res;
    }

    bool empty()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        return data_queue.empty();
    }
};

// 一个生产者，两个消费者模拟测试上面的 threadsafe_queue
void test_threadsafe_queue()
{
    threadsafe_queue<int> safe_queue;
    std::mutex mutex_print;

    std::thread producer([&]()
                         {
        for(int i = 0; ; i++){
            safe_queue.push(i);
            {
                std::lock_guard<std::mutex> lock(mutex_print);
                std::cout << "producer thread push data is " << i << std::endl;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        } });
    std::thread consumer1([&]()
                          {
        for(;;){
            auto data = safe_queue.wait_and_pop();
            {
                std::lock_guard<std::mutex> lock(mutex_print);
                std::cout << "consumer1 thread wait and pop data is " << *data << std::endl;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        } });
    std::thread consumer2([&]()
                          {
        for(;;){
            auto data = safe_queue.wait_and_pop();
            {
                std::lock_guard<std::mutex> lock(mutex_print);
                std::cout << "consumer2 thread wait and pop data is " << *data << std::endl;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        } });

    producer.join();
    consumer1.join();
    consumer2.join();
}

int main(void)
{
    test_threadsafe_queue();

    return 0;
}
