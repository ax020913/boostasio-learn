// unique_lock.cpp : 此文件包含 main 函数。程序执行将在此开始并结束
//

#include <iostream>
#include <mutex>
#include <map>
#include <thread>
#include <unistd.h>

// C++17 的共享锁
// gcc/g++ --version 看一看当前系统的版本号，一般刚刚开始的版本是很低的，可以百度提高版本号
#include <shared_mutex>

using namespace std;

std::mutex mutex_;
int shared_data = 0;
void use_unique()
{
    // lock可以自动解锁，也是可以手动解锁的哦
    std::unique_lock<std::mutex> lock(mutex_);

    // 下面可不能再用 lock.lock() 加锁了，上面一行已经默认加好锁了的哦。不然就会有下面的死锁的报错。
    // lock.lock();
    // terminate called after throwing an instance of 'std::system_error'
    // what():  Resource deadlock avoided
    // Aborted

    std::cout << "lock success" << std::endl;
    shared_data++;

    lock.unlock();
}

// owns_lock 可判断是否占有锁
void ows_unique()
{
    std::unique_lock<std::mutex> lock(mutex_);
    shared_data++;
    if (lock.owns_lock())
    {
        std::cout << "owns lock" << std::endl;
    }
    else
    {
        std::cout << "doesn't own lock" << std::endl;
    }

    lock.unlock();
    if (lock.owns_lock())
    {
        std::cout << "owns lock" << std::endl;
    }
    else
    {
        std::cout << "doesn't own lock" << std::endl;
    }
}

// 延时加锁
void defer__lock()
{
    // 延时加锁
    std::unique_lock<std::mutex> lock(mutex_, std::defer_lock); // 还没有加锁的哦

    // 业务的处理.......
    std::cout << "业务的处理中......" << std::endl;

    // 可以加锁
    lock.lock();
    // 可以自动析构解锁，也是可以手动解锁的
    lock.unlock();
}

// 同时使用 owns 和 defer
void use_owns_defer()
{
    std::unique_lock<std::mutex> lock(mutex_);
    // 判断是否拥有锁
    if (lock.owns_lock())
        std::cout << "main thread has the lock" << std::endl;
    else
        std::cout << "main thread does not have the lock" << std::endl;

    std::thread t([]()
                  {
		std::unique_lock<std::mutex> lock(mutex_, std::defer_lock); // 还没有加锁的哦

		// 判断是否拥有锁
		if (lock.owns_lock())
		{
			std::cout << "Thread has the lock." << std::endl;
		}
		else
		{
			std::cout << "Thread does not have the lock." << std::endl;
            // return;
		}

        // sleep(5);

		// 加锁
        // 出现了死锁的现象(是一个概率事件，main thread 和 thread 看谁跑得快) ==> 上面的 owns_lock() 并不是一个阻塞的操作，
        // 如果上面，我们的 thread does not have the lock 的话，我们是可以直接 return 退出的（有一种 try_lock 的感觉了，哈哈哈）
		lock.lock();

		// 判断是否拥有锁
		if (lock.owns_lock())
		{
			std::cout << "Thread has the lock." << std::endl;
		}
		else
		{
			std::cout << "Thread does not have the lock." << std::endl;
		}

		// 解锁
		lock.unlock(); });

    lock.unlock();
    t.join();
}

// 支持领养操作
void use_own_adopt()
{
    mutex_.lock();
    std::unique_lock<std::mutex> lock(mutex_, std::adopt_lock); // 还没有加锁的哦
    if (lock.owns_lock())
        std::cout << "owns lock" << std::endl;
    else
        std::cout << "does not have the lock" << std::endl;
    lock.unlock();
}
// 之前的交换代码可以可以用如下方式等价实现
int a = 10;
int b = 99;
std::mutex mtx1;
std::mutex mtx2;
void safe_swap()
{
    std::lock(mtx1, mtx2);
    std::unique_lock<std::mutex> lock1(mtx1, std::adopt_lock);
    std::unique_lock<std::mutex> lock2(mtx2, std::adopt_lock);
    std::swap(a, b);
    // 错误用法
    // mtx1.unlock();
    // mtx2.unlock();
}

void safe_swap2()
{
    std::unique_lock<std::mutex> lock1(mtx1, std::defer_lock);
    std::unique_lock<std::mutex> lock2(mtx2, std::defer_lock);
    // 需用lock1,lock2加锁
    std::lock(lock1, lock2);
    // 错误用法
    // std::lock(mtx1, mtx2); // 但是，好像也是没有报错的
    std::swap(a, b);
}

// 转移互斥量所有权
// 互斥量本身不支持move操作，但是unique_lock支持(mutex 和 thread 都不支持赋值)
std::unique_lock<std::mutex> get_lock()
{
    std::unique_lock<std::mutex> lock(mutex_);
    shared_data++;
    return lock;
}
void use_return()
{
    // unique_lock 把使用权转移了，转移给不同的线程可以
    std::unique_lock<std::mutex> lock(get_lock());
    shared_data++;
}

// 锁粒度表示加锁的精细程度。
// 一个锁的粒度要足够大，以保证可以锁住要访问的共享数据。
// 一个锁的粒度要足够小，以保证非共享的数据不被锁住影响性能。
void precision_lock()
{
    std::unique_lock<std::mutex> lock(mutex_);
    shared_data++;
    lock.unlock();

    // 不设计共享数据的耗时操作不要放在锁内执行
    std::this_thread::sleep_for(std::chrono::seconds(1));

    lock.lock();
    shared_data++;
}

// C++17 标准shared_mutex
// C++14  提供了 shared_time_mutex
// C++11 无上述互斥，想使用可以利用boost库
class DNService
{
public:
    DNService() {}
    // 读操作采用共享锁
    std::string QueryDNS(std::string dnsname)
    {
        std::shared_lock<std::shared_mutex> shared_locks(_shared_mtx);
        auto iter = _dns_info.find(dnsname);
        if (iter != _dns_info.end())
            return iter->second;

        return "";
    }

    // 写操作采用独占锁
    void AddDNSInfo(std::string dnsname, std::string dnsentry)
    {
        std::lock_guard<std::shared_mutex> guard_locks(_shared_mtx);
        _dns_info.insert(std::make_pair(dnsname, dnsentry));
    }

private:
    std::map<std::string, std::string> _dns_info;
    mutable std::shared_mutex _shared_mtx;
};

// 利用递归锁
class RecursiveDemo
{
public:
    RecursiveDemo() {}
    // 函数调用函数的时候，防止都是调用同一个锁（死锁）
    // 下面的情况不适用std::recursive_mutex使用std::mutex就会死锁的哦
    bool QueryStudent(std::string name)
    {
        std::lock_guard<std::recursive_mutex> recursive_lock(_recursive_mtx);
        // std::lock_guard<std::mutex> recursive_lock(_mutex);
        auto iter_find = _students_info.find(name);
        if (iter_find == _students_info.end())
            return false;

        return true;
    }
    void AddScore(std::string name, int score)
    {
        std::lock_guard<std::recursive_mutex> recursive_lock(_recursive_mtx);
        // std::lock_guard<std::mutex> recursive_lock(_mutex);
        if (!QueryStudent(name))
        {
            _students_info.insert(std::make_pair(name, score));
            return;
        }

        _students_info[name] = _students_info[name] + score;
    }

    // 不推荐采用递归锁，使用递归锁说明设计思路并不理想，需优化设计
    // 推荐拆分逻辑，将共有逻辑拆分为统一接口
    void AddScoreAtomic(std::string name, int score)
    {
        std::lock_guard<std::recursive_mutex> recursive_lock(_recursive_mtx);
        auto iter_find = _students_info.find(name);
        if (iter_find == _students_info.end())
        {
            _students_info.insert(std::make_pair(name, score));
            return;
        }

        _students_info[name] = _students_info[name] + score;
        return;
    }

private:
    std::map<std::string, int> _students_info;
    std::recursive_mutex _recursive_mtx;
    std::mutex _mutex;
};

int main(void)
{
    // std::cout << "hello world" << std::endl;

    // use_unique();
    // ows_unique();
    // defer__lock();
    // use_owns_defer();
    // use_own_adopt();
    // safe_swap();
    // safe_swap2();
    // use_return();

    RecursiveDemo test;
    test.AddScore("trw", 87);

    return 0;
}