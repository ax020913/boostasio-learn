#include <iostream>
#include <string>
#include <thread>
#include <atomic>
#include <cassert>
#include <vector>
#include <algorithm>
#include <mutex>
#include <memory>
#include <chrono>

using namespace std;

// store: 赋值， load: get值
// std::memory_order_relaxed 宽松内存序测试
void testOrderRelaxed(){
    std::atomic<bool> rx = false, ry = false;

    // sequenc before
    std::thread myThread1([&](){
        // 因为是 std::memory_order_relaxed 所以下面的 1 和 2 执行指令的先后顺序是不确定的
        rx.store(true, std::memory_order_relaxed); // 1
        ry.store(true, std::memory_order_relaxed); // 2
    });

    std::thread myThread2([&](){
        while(!ry.load(std::memory_order_relaxed)); // 3
        // assert(false); // false == 0 ---> error
        // rx.store(false, std::memory_order_relaxed); // error
        assert(rx.load(std::memory_order_relaxed)); // 4
    });

    myThread1.join();
    myThread2.join();
}

// std::memory_order_seq_cst 测试
std::atomic<bool> x = false, y = false;
std::atomic<int> z = 0;
void write_x_then_y(){
    x.store(true, std::memory_order_seq_cst); // 1
    y.store(true, std::memory_order_seq_cst); // 2
}
void write_y_then_x(){
    while(!y.load(std::memory_order_seq_cst)){ // 3
        std::cout << "y load false" << std::endl;
    }

    if(x.load(std::memory_order_seq_cst)){ // 4
        z++;
    }
}
void testOrderSeqCst(){
    std::thread t1(write_x_then_y);
    std::thread t2(write_y_then_x);
    t1.join();
    t2.join();
    assert(z.load() != 0); // 5
}

// std::memory_order_release, std::memory_order_acquire
void testReleaseAcquire(){
    std::atomic<bool> rx = false, ry = false;

    // 下面的 std::memory_order_release 是得 cpu 执行的指令是先执行 1， 后执行 2 的
    // ---> 2, 3 是同步的 ---> 先 2 后 3
    // t1 是单线程的 ---> 1 ---> 2。 ---> 3 ---> 4(不会奔溃)
    std::thread t1([&](){
        rx.store(true, std::memory_order_relaxed); // 1
        ry.store(true, std::memory_order_release); // 2
    });

    std::thread t2([&](){
        // sequence before
        while(!ry.load(std::memory_order_acquire)); // 3 
        assert(rx.load(std::memory_order_relaxed)); // 4
    });

    t1.join();
    t2.join();
}

// 多个 std::thread 使用 std::memory_order_release 作用于同一个变量，后面的 std::memory_order_acquire 只能同步前面多个 std::memory_order_release 中最先执行的一个 ---> bug
void ReleasAcquireDanger() {
	std::atomic<int> xd{0}, yd{ 0 };
	std::atomic<int> zd;

	std::thread t1([&]() {
		xd.store(1, std::memory_order_release); // 1
		yd.store(1, std::memory_order_release); // 2
    });

    std::thread t2([&]() {
		yd.store(2, std::memory_order_release);  // 3
    });

	std::thread t3([&]() {
        // 下面的 4 只能同步上面多个 std::memory_order_release 中的一个，其他的部分代码有 std::memory_order_release 的会执行有问题 ---> bug
		while (!yd.load(std::memory_order_acquire)); // 4
		assert(xd.load(std::memory_order_acquire) == 1); // 5
    });

	t1.join();
	t2.join();
	t3.join();
}

// std::memory_order_release 和 std::memory_order_acquire 的使用
void ReleaseSequence() {
	std::vector<int> data;
	std::atomic<int> flag{ 0 };
	
	std::thread t1([&]() {
		data.push_back(42);  // 1
		flag.store(1, std::memory_order_release); // 2 (A)
    });

	std::thread t2([&]() {
		int expected = 1;
        // flag == expected 的话，就会被替换成 2
		while (!flag.compare_exchange_strong(expected, 2, std::memory_order_relaxed)) // 3
			expected = 1;
    });

	std::thread t3([&]() {
        // 2， 4 同步
		while (flag.load(std::memory_order_acquire) < 2); // 4
		assert(data.at(0) == 42); // 5
    });

	t1.join();
	t2.join();
	t3.join();
}

// std::memory_order_release 和 std::memory_order_consume 的使用
void ConsumeDependency() {
	std::atomic<std::string*> ptr;
	int data;

	std::thread t1([&]() {
		std::string* p = new std::string("Hello World"); // 1
		data = 42; // 2
		ptr.store(p, std::memory_order_release); // 3
    });

	std::thread t2([&]() {
		std::string* p2;
        // 3, 4 同步
		while (!(p2 = ptr.load(std::memory_order_consume))); // 4
		assert(*p2 == "Hello World"); // 5
		assert(data == 42); // 6
    });

	t1.join();
	t2.join();
}

// 智能指针双重检测隐藏的问题
class singleAuto{
private:
    singleAuto(){}
    singleAuto(const singleAuto&) = delete;
    singleAuto& operator=(const singleAuto&) = delete;
public:
    ~singleAuto(){
        std::cout << "singleAuto auto delete success" << std::endl;
    }

    static std::shared_ptr<singleAuto> getInstance(){
        // 1
        if(single != nullptr) return single;

        // 2
        single_mutex.lock();

        // 3
        if(single != nullptr){
            single_mutex.unlock();
            return single;
        }

        // 4 
        // *****：new 操作是有三条指令的，可能会有不同的执行顺序：
        // 第一种情况：
        // 1 为对象allocate(类似与malloc)一块内存空间
        // 2 调用construct构造对象
        // 3 将构造到的对象地址返回
        // 第二种情况：
        // 1 为对象allocate一块内存空间
        // 2 先将开辟的空间地址返回
        // 3 调用construct构造对象

        // 隐藏的问题(不过几乎很难碰到)：
        // 如果是第二种情况，在4处还未构造对象就将地址返回赋值给single，而此时有线程运行至1处判断single不为空直接返回单例实例，如果该线程调用这个单例的成员函数就会崩溃。
        single = std::shared_ptr<singleAuto>(new singleAuto);
        single_mutex.unlock();
        return single;
    }
private:
    static std::shared_ptr<singleAuto> single;
    static std::mutex single_mutex;
};
// 下面对于 class singleAuto static 成员的定义前面的 static 就不用加了 
std::shared_ptr<singleAuto> singleAuto::single = nullptr;
std::mutex singleAuto::single_mutex;

void testSingleAuto(){
    std::thread t1([](){
        std::cout << "thread t1 singletion address is 0x: " << singleAuto::getInstance() << std::endl;
    });

    std::thread t2([](){
        std::cout << "thread t2 singletion address is 0x: " << singleAuto::getInstance() << std::endl;
    });

    t1.join();
    t2.join();
}

// std::memory_order_release 和 std::memory_order_acquire 实现对一个 std::atomic<bool> 变量的同步，解决上面的问题
class SingleMemoryModel{
private:
    SingleMemoryModel(){}
    SingleMemoryModel(const SingleMemoryModel&) = delete;
    SingleMemoryModel& operator=(const SingleMemoryModel&) = delete;
public:
    ~SingleMemoryModel(){
        std::cout << "SingleMemoryModel auto delete success" << std::endl;
    }

    static std::shared_ptr<SingleMemoryModel> getInstance(){
        // 1
        // if(single != nullptr) return single;
        if(single_init.load(std::memory_order_acquire)) return single;

        // 2
        // 进入下面的线程只会有一个(std::mutex 的同步权限是很大的，std::atomic 的权限是很小的)
        single_mutex.lock();

        // 3
        // if(single != nullptr){
        if(single_init.load(std::memory_order_relaxed)){
            single_mutex.unlock();
            return single;
        }

        // 4 
        single = std::shared_ptr<SingleMemoryModel>(new SingleMemoryModel);
        single_init.store(true, std::memory_order_release);
        single_mutex.unlock();
        return single;
    }
private:
    static std::shared_ptr<SingleMemoryModel> single;
    static std::mutex single_mutex;
    static std::atomic<bool> single_init;
};
// 下面对于 class SingleMemoryModel static 成员的定义前面的 static 就不用加了 
std::shared_ptr<SingleMemoryModel> SingleMemoryModel::single = nullptr;
std::mutex SingleMemoryModel::single_mutex;
std::atomic<bool> SingleMemoryModel::single_init = false;

void testSingleMemoryModel(){
    std::thread t1([](){
        std::cout << "thread t1 singletion address is 0x: " << singleAuto::getInstance() << std::endl;
    });

    std::thread t2([](){
        std::cout << "thread t2 singletion address is 0x: " << singleAuto::getInstance() << std::endl;
    });

    t1.join();
    t2.join();
}

int main(void){
    // testOrderRelaxed(); // std::memory_order_relaxed 宽松内存序测试
    // testOrderSeqCst(); // std::memory_order_seq_cst
    // testReleaseAcquire(); // std::memory_order_release, std::memory_order_acquire
    // ReleasAcquireDanger(); // 多个 std::thread 使用 std::memory_order_release 作用于同一个变量 --> bug
    // ReleaseSequence();
    // ConsumeDependency(); // std::memory_order_release 和 std::memory_order_consume 的使用

    // testSingleAuto();
    testSingleMemoryModel();

    return 0;
}