#include <iostream>
#include <list>
#include <algorithm>
#include <future>
#include "future_threadPool.h"

// c++ 版本的快速排序算法
template <typename T>
void quick_sort_recursive(T arr[], int start, int end)
{
    if (start >= end)
        return;
    T key = arr[start];
    int left = start, right = end;
    while (left < right)
    {
        while (arr[right] >= key && left < right)
            right--;
        while (arr[left] <= key && left < right)
            left++;
        std::swap(arr[left], arr[right]);
    }

    if (arr[left] < key)
    {
        std::swap(arr[left], arr[start]);
    }

    quick_sort_recursive(arr, start, left - 1);
    quick_sort_recursive(arr, left + 1, end);
}
template <typename T>
void quick_sort(T arr[], int len)
{
    quick_sort_recursive(arr, 0, len - 1);
}
void test_quick_sort()
{
    int num_arr[] = {6, 1, 0, 7, 5, 2, 9, -1};
    int length = sizeof(num_arr) / sizeof(int);
    quick_sort(num_arr, length);
    std::cout << "sorted result is ";
    for (int i = 0; i < length; i++)
        std::cout << " " << num_arr[i];

    std::cout << std::endl;
}

// 函数式编程
template <typename T>
std::list<T> sequential_quick_sort(std::list<T> input)
{
    if (input.empty())
    {
        return input;
    }
    std::list<T> result;

    // 1. 将 input 中的第一个元素放入 result 中，并且将这第一个元素从 input 中删除
    result.splice(result.begin(), input, input.begin());

    // 2. 取 result 的第一个元素，将来用这个元素做切割，切割 input 中的列表。
    T const &pivot = *result.begin();

    // 3. std::partition 是一个标准库函数，用于将容器或数组中的元素按照指定的条件进行分区，
    // 使得满足条件的元素排在不满足条件的元素之前。
    // 所以经过计算 divide_point 指向的是 input 中第一个大于等于 pivot 的元素
    auto divide_point = std::partition(input.begin(), input.end(),
                                       [&](T const &t)
                                       { return t < pivot; });

    // 4. 我们将小于 pivot 的元素放入 lower_part 中
    std::list<T> lower_part;
    lower_part.splice(lower_part.end(), input, input.begin(),
                      divide_point);

    // 5. 我们将 lower_part 传递给 sequential_quick_sort 返回一个新的有序的从小到大的序列
    // lower_part 中都是小于 divide_point 的值
    auto new_lower(
        sequential_quick_sort(std::move(lower_part)));
    // 6. 我们剩余的 input 列表传递给 sequential_quick_sort 递归调用， input 中都是大于 divide_point 的值。
    auto new_higher(
        sequential_quick_sort(std::move(input)));
    // 7. 到此时 new_higher 和 new_lower 都是从小到大排序好的列表
    // 将 new_higher 拼接到 result 的尾部
    result.splice(result.end(), new_higher);
    // 将 new_lower 拼接到 result 的头部
    result.splice(result.begin(), new_lower);
    return result;
}
void test_sequential_quick()
{
    std::list<int> numlists = {6, 1, 0, 7, 5, 2, 9, -1};
    auto sort_result = sequential_quick_sort(numlists);
    std::cout << "sorted result is ";
    for (auto iter = sort_result.begin(); iter != sort_result.end(); iter++)
        std::cout << " " << (*iter);
    std::cout << std::endl;
}

//
// 并行版本
template <typename T>
std::list<T> parallel_quick_sort(std::list<T> input)
{
    if (input.empty())
    {
        return input;
    }
    std::list<T> result;
    result.splice(result.begin(), input, input.begin());
    T const &pivot = *result.begin();
    auto divide_point = std::partition(input.begin(), input.end(),
                                       [&](T const &t)
                                       { return t < pivot; });
    std::list<T> lower_part;
    lower_part.splice(lower_part.end(), input, input.begin(),
                      divide_point);
    // 1. 因为 lower_part 是副本，所以并行操作不会引发逻辑错误，这里可以启动 future 做排序
    std::future<std::list<T>> new_lower(
        std::async(&parallel_quick_sort<T>, std::move(lower_part)));

    // 2
    auto new_higher(
        parallel_quick_sort(std::move(input)));
    result.splice(result.end(), new_higher);
    result.splice(result.begin(), new_lower.get());
    return result;
}
void test_parallen_sort()
{
    std::list<int> numlists = {6, 1, 0, 7, 5, 2, 9, -1};
    auto sort_result = parallel_quick_sort(numlists);
    std::cout << "sorted result is ";
    for (auto iter = sort_result.begin(); iter != sort_result.end(); iter++)
        std::cout << " " << (*iter);
    std::cout << std::endl;
}

// 线程池版本
// 并行版本
template <typename T>
std::list<T> thread_pool_quick_sort(std::list<T> input)
{
    if (input.empty())
    {
        return input;
    }
    std::list<T> result;
    result.splice(result.begin(), input, input.begin());
    T const &pivot = *result.begin();
    auto divide_point = std::partition(input.begin(), input.end(),
                                       [&](T const &t)
                                       { return t < pivot; });
    std::list<T> lower_part;
    lower_part.splice(lower_part.end(), input, input.begin(),
                      divide_point);
    // 1. 因为 lower_part 是副本，所以并行操作不会引发逻辑错误，这里投递给线程池处理
    auto new_lower = ThreadPool::ThreadPoolInstance().commit(&parallel_quick_sort<T>, std::move(lower_part));
    // 2
    auto new_higher(
        parallel_quick_sort(std::move(input)));
    result.splice(result.end(), new_higher);
    result.splice(result.begin(), new_lower.get());
    return result;
}
void test_thread_pool_sort()
{
    std::list<int> numlists = {6, 1, 0, 7, 5, 2, 9, -1};
    auto sort_result = thread_pool_quick_sort(numlists);
    std::cout << "sorted result is ";
    for (auto iter = sort_result.begin(); iter != sort_result.end(); iter++)
        std::cout << " " << (*iter);
    std::cout << std::endl;
}

int main(void)
{
    // test_quick_sort();
    // test_sequential_quick();
    // test_parallen_sort();
    test_thread_pool_sort();

    return 0;
}
