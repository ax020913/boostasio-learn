#pragma once

#include <iostream>
#include <string>
#include <mutex>
#include <atomic>
#include <memory>

template <class K, size_t Cap>
class CircularQueSeq : private std::allocator<K>
{
public:
    // 注意下面初始化的时候 head_ == tail_ == 0 说明我们的环形队列头尾指针指向同一个位置的是是kong的队列
    CircularQueSeq()
        : max_size_(Cap + 1), data_(std::allocator<K>::allocate(max_size_)), atomic_using_(false), head_(0), tail_(0) {}
    CircularQueSeq(const CircularQueSeq &queue) = delete;
    CircularQueSeq &operator=(const CircularQueSeq &queue) volatile = delete;
    CircularQueSeq &operator=(const CircularQueSeq &queue) = delete;
    ~CircularQueSeq()
    {
        // 调用内部元素的析构函数
        while (head_ != tail_)
        {
            std::allocator<K>::destroy(data_ + head_);
            head_ = (head_ + 1) % max_size_;
        }
        // 调用回收操作
        std::allocator<K>::deallocate(data_, max_size_);
    }

    // 先实现一个可变参数列表版本的插入函数最为基础的函数
    template <class... Args>
    bool emplace(Args &&...args)
    {
        // 最开始的时候肯定是先执行插入函数的(atomic_using_ == false ---> use-expected == false)
        // 下面的 compare_exchange_strong 函数的功能就是：
        // 1. 如果 atomic_using_ 和 use_expected 相等，那么返回 【true】，并且 atomic_using_ = desired = true
        // 2. 如果 atomic_using_ 和 use_expected 不相等，那么返回 【false】，并且 atomic_using_ = desired = true
        // 第一处的 do while
        bool use_expected = false;
        bool use_desired = false;
        do
        {
            use_expected = false;
            use_desired = true;
        } while (!atomic_using_.compare_exchange_strong(use_expected, use_desired));

        // 判断队列是否满了
        if ((tail_ + 1) % max_size_ == head_)
        {
            std::cout << "circularQueSeq is full!!!" << std::endl;
            // 第二处的 do while
            do
            {
                // atomic_using_ 从上面过来 atomic_using_ == desired == true 的
                use_expected = true;
                use_desired = false;
            } while (!atomic_using_.compare_exchange_strong(use_expected, use_desired));
            return false;
        }
        // else 下面的情况是队列没有满的情况

        // 在尾部位置构造一个 K 类型的对象，构造参数为 args...
        std::allocator<K>::construct(data_ + tail_, std::forward<Args>(args)...);
        // 更新尾部元素的位置
        tail_ = (tail_ + 1) % max_size_;

        // 第二处的 do while
        do
        {
            // atomic_using_ 从上面过来 atomic_using_ == desired == true 的
            use_expected = true;
            use_desired = false;
        } while (!atomic_using_.compare_exchange_strong(use_expected, use_desired));

        return true;
    }
    // push 实现两个版本，一个接受左值引用，一个接受左值引用：
    // 接受左值引用
    bool push(K &value)
    {
        std::cout << "called push const K& value version" << std::endl;
        return emplace(value);
    }
    // 接受左值引用
    bool push(K &&value)
    {
        std::cout << "called push const K&& value version" << std::endl;
        return emplace(std::move(value));
    }

    // pop
    bool pop(K &value)
    {
        // 第一处的 do while
        bool use_expected = false;
        bool use_desired = false;
        do
        {
            use_expected = false;
            use_desired = true;
        } while (!atomic_using_.compare_exchange_strong(use_expected, use_desired));
        // 判断头部和尾部指针是否重合，如果重合则队列为空
        if (head_ == tail_)
        {
            std::cout << "circularQueSeq empty" << std::endl;
            // 第二处的 do while
            do
            {
                // atomic_using_ 从上面过来 atomic_using_ == desired == true 的
                use_expected = true;
                use_desired = false;
            } while (!atomic_using_.compare_exchange_strong(use_expected, use_desired));
            return false;
        }
        // else 下面的情况是队列不为空的情况

        // 取出头部指针指向的数据
        value = std::move(data_[head_]);
        // 更新头部指针
        head_ = (head_ + 1) % max_size_;

        // 第二处的 do while
        do
        {
            // atomic_using_ 从上面过来 atomic_using_ == desired == true 的
            use_expected = true;
            use_desired = false;
        } while (!atomic_using_.compare_exchange_strong(use_expected, use_desired));
        return true;
    }

private:
    size_t max_size_;
    K *data_;
    // std::mutex mutex_; 用下面权限更小的 std::atomic atomic_using_ 来代替了
    // true 则表示的是正在进行 push 操作，false 同理(注意：push 和 pop 操作是不能同时进行的)
    // 所以需要下面的 atomic_using_ 来同步 push 和 pop 两个操作的
    std::atomic<bool> atomic_using_;
    size_t head_ = 0;
    size_t tail_ = 0;
};