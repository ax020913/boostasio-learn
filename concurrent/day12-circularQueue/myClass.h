#pragma once

#include <iostream>

class myClass
{
public:
    myClass() : data_(0) {}
    myClass(int data) : data_(data) {}
    myClass(const myClass &mc)
    {
        data_ = mc.data_;
    }
    myClass(const myClass &&mc) noexcept
    {
        data_ = std::move(mc.data_);
    }
    myClass &operator=(const myClass &other)
    {
        data_ = other.data_;
        return *this;
    }

    friend std::ostream &operator<<(std::ostream &cout, const myClass &myclass)
    {
        cout << "myclass data is " << myclass.data_;
        return cout;
    }

private:
    int data_;
};