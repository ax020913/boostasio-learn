#pragma once

#include <iostream>
#include <memory>

template <typename T, size_t Cap>
class CircularQueSync : private std::allocator<T>
{
public:
    CircularQueSync()
        : max_size_(Cap + 1), data_(std::allocator<T>::allocate(max_size_)), head_(0), tail_(0), tail_update_(0) {}
    CircularQueSync(const CircularQueSync &) = delete;
    CircularQueSync &operator=(const CircularQueSync &) volatile = delete;
    CircularQueSync &operator=(const CircularQueSync &) = delete;
    ~CircularQueSync()
    {
        // 调用内部元素的析构函数
        while (head_ != tail_)
        {
            std::allocator<T>::destroy(data_ + head_);
            head_ = (++head_) % max_size_;
        }
        // 调用回收操作
        std::allocator<T>::deallocate(data_, max_size_);
    }

    // push
    bool push(const T &val)
    {
        size_t t;
        do
        {
            t = tail_.load(std::memory_order_relaxed); // 5
            // 判断队列是否满
            if ((t + 1) % max_size_ == head_.load(std::memory_order_acquire))
            {
                std::cout << "circular que full ! " << std::endl;
                return false;
            }

        } while (!tail_.compare_exchange_strong(t,
                                                (t + 1) % max_size_, std::memory_order_release, std::memory_order_relaxed)); // 6

        data_[t] = val;
        size_t tailup;
        do
        {
            tailup = t;

        } while (!tail_update_.compare_exchange_strong(tailup,
                                                       (tailup + 1) % max_size_, std::memory_order_release, std::memory_order_relaxed)); // 7

        std::cout << "called push data success " << val << std::endl;
        return true;
    }

    // pop
    bool pop(T &val)
    {
        size_t h;
        do
        {
            h = head_.load(std::memory_order_relaxed); // 1 处
            // 判断头部和尾部指针是否重合，如果重合则队列为空
            if (h == tail_.load(std::memory_order_acquire)) // 2 处
            {
                std::cout << "circular que empty ! " << std::endl;
                return false;
            }

            // 判断如果此时要读取的数据和 tail_update_ 是否一致，如果一致说明尾部数据未更新完
            if (h == tail_update_.load(std::memory_order_acquire)) // 3 处
            {
                return false;
            }
            val = data_[h]; // 2 处

        } while (!head_.compare_exchange_strong(h,
                                                (h + 1) % max_size_, std::memory_order_release, std::memory_order_relaxed)); // 4 处
        std::cout << "pop data success, data is " << val << std::endl;
        return true;
    }

private:
    size_t max_size_;
    T *data_;
    std::atomic<size_t> head_;
    std::atomic<size_t> tail_;
    std::atomic<size_t> tail_update_;
};