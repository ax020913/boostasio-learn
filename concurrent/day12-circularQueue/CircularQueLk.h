#pragma once

#include <iostream>
#include <mutex>
#include <memory>

template <class K, size_t Cap>
class CircularQueLk : private std::allocator<K>
{
public:
    // 注意下面初始化的时候 head_ == tail_ == 0 说明我们的环形队列头尾指针指向同一个位置的是是kong的队列
    CircularQueLk()
        : max_size_(Cap + 1), data_(std::allocator<K>::allocate(max_size_)), head_(0), tail_(0) {}
    CircularQueLk(const CircularQueLk *queue) = delete;
    CircularQueLk &operator=(const CircularQueLk &queue) volatile = delete;
    CircularQueLk &operator=(const CircularQueLk &queue) = delete;
    ~CircularQueLk()
    {
        // 循环销毁
        std::lock_guard<std::mutex> lock(mutex_);
        // 调用内部元素的析构函数
        while (head_ != tail_)
        {
            std::allocator<K>::destroy(data_ + head_);
            head_ = (head_ + 1) % max_size_;
        }
        // 调用回收操作
        std::allocator<K>::deallocate(data_, max_size_);
    }

    // 先实现一个可变参数列表版本的插入函数最为基准的函数
    template <class... Args>
    bool emplace(Args &&...args)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        // 判断队列是否满了
        if ((tail_ + 1) % max_size_ == head_)
        {
            std::cout << "circularQueLk full!!!" << std::endl;
            return false;
        }
        // 在尾部位置构造一个 K 类型的对象，构造参数为 args...
        std::allocator<K>::construct(data_ + tail_, std::forward<Args>(args)...);
        // 更新尾部元素位置
        tail_ = (tail_ + 1) % max_size_;
        return true;
    }
    // push 实现两个版本，一个接受左值引用，一个接受左值引用：
    // 接受左值引用
    bool push(const K &value)
    {
        std::cout << "circularQueLk push const K& version" << std::endl;
        return emplace(value);
    }
    // 接受左值引用(接受右值引用版本，当然也可以接受左值引用，T&&为万能引用)
    bool push(const K &&value)
    {
        std::cout << "circularQueLk push const K&& version" << std::endl;
        return emplace(std::move(value));
    }

    // pop
    bool pop(K &value)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        // 判断头部和尾部指针是否重合，如果重合则队列为空
        if (head_ == tail_)
        {
            std::cout << "circularQueLk is empty" << std::endl;
            return false;
        }
        // 取出头部指针指向的数据
        value = std::move(data_[head_]);
        // 更新头部指针
        head_ = (head_ + 1) % max_size_;
        return true;
    }

private:
    size_t max_size_; // max_size_ = Cap + 1 注意环形队列的样子哦
    K *data_;
    std::mutex mutex_;
    size_t head_ = 0;
    size_t tail_ = 0;
};
