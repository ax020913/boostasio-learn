#pragma once

#include <iostream>
#include <string>
#include <mutex>
#include <memory>

template <class K, size_t Cap>
class CircularQueLight : private std::allocator<K>
{
public:
    CircularQueLight()
        : max_size_(Cap + 1), data_(std::allocator<K>::allocate(max_size_)), head_(0), tail_(0), tail_update_(0) {}
    CircularQueLight(const CircularQueLight &queue) = delete;
    CircularQueLight &operator=(const CircularQueLight &queue) volatile = delete;
    CircularQueLight &operator=(const CircularQueLight &queue) = delete;
    ~CircularQueLight()
    {
        // 调用内部元素的析构函数
        while (head_ != tail_)
        {
            std::allocator<K>::destroy(data_ + head_);
            head_ = (head_ + 1) % max_size_;
        }
        // 调用回收操作
        std::allocator<K>::deallocate(data_, max_size_);
    }

    // push
    bool push(const K &value)
    {
        size_t t;
        do
        {
            t = tail_.load(); // 1
            // 判断队列是否满了
            if ((t + 1) % max_size_ == head_.load())
            {
                std::cout << "circularQueLight full!!!" << std::endl;
                return false;
            }
        } while (!tail_.compare_exchange_strong(t, (t + 1) % max_size_)); // 3

        data_[t] = value; // 2
        size_t tailup;
        do
        {
            tailup = t;
        } while (!tail_.compare_exchange_strong(t, (t + 1) % max_size_));

        std::cout << "called push data success " << value << std::endl;
        return true;
    }

    // pop
    bool pop(K &value)
    {
        size_t h;
        do
        {
            h = head_.load(); // 1
            // 判断头部位置是否和尾部位置重合，如果重合则队列为空
            if (h == tail_.load())
            {
                std::cout << "circularQueLight empty!!!" << std::endl;
                return false;
            }

            // 判断如果此时要读取的数据和 tail_update 是否一致，如果一致说明尾部数据未更新完(pop和push可能在同一个位置)
            if (h == tail_update_.load())
            {
                return false;
            }
            value = data_[h];                                             // 2
        } while (!head_.compare_exchange_strong(h, (h + 1) % max_size_)); // 3

        std::cout << "pop data success, data is " << value << std::endl;
        return true;
    }

private:
    size_t max_size_;
    K *data_;
    std::atomic<size_t> head_;
    std::atomic<size_t> tail_;
    // 尾部指针作为插入位置 ---> 先得tail_位置push数据才能head_位置pop数据(同步关系)
    std::atomic<size_t> tail_update_;
};