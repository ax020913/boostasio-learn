### 几种版本的 `CirculauQueue` 环形队列


#### 1. `CircularQueLk`(`mutex`版本) 的一些问题

用的是 `std::mutex` 来实现的环形队列。

##### 1.1 `error: ‘destroy’ is not a member of ‘std::allocator<myClass>’` 和 `error: ‘construct’ is not a member of ‘std::allocator<myClass>’`。
下面是用 `g++ -o ma main.cpp -std=c++20 -lpthread`, `c++20` 来编译的。
```
[ax@localhost day12-circularQueue]$ make
g++ -o ma main.cpp -std=c++20 -lpthread
In file included from main.cpp:1:
CircularQueLk.h: In instantiation of ‘CircularQueLk<K, Cap>::~CircularQueLk() [with K = myClass; long unsigned int Cap = 5]’:
main.cpp:11:31:   required from here
CircularQueLk.h:24:39: error: ‘destroy’ is not a member of ‘std::allocator<myClass>’
   24 |             std::allocator<K>::destroy(data_ + head_);
      |             ~~~~~~~~~~~~~~~~~~~~~~~~~~^~~~~~~~~~~~~~~
CircularQueLk.h: In instantiation of ‘bool CircularQueLk<K, Cap>::emplace(Args&& ...) [with Args = {const myClass&}; K = myClass; long unsigned int Cap = 5]’:
CircularQueLk.h:54:23:   required from ‘bool CircularQueLk<K, Cap>::push(const K&) [with K = myClass; long unsigned int Cap = 5]’
main.cpp:14:15:   required from here
CircularQueLk.h:43:37: error: ‘construct’ is not a member of ‘std::allocator<myClass>’
   43 |         std::allocator<K>::construct(data_ + tail_, std::forward<Args>(args)...);
      |         ~~~~~~~~~~~~~~~~~~~~~~~~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CircularQueLk.h: In instantiation of ‘bool CircularQueLk<K, Cap>::emplace(Args&& ...) [with Args = {const myClass}; K = myClass; long unsigned int Cap = 5]’:
CircularQueLk.h:60:23:   required from ‘bool CircularQueLk<K, Cap>::push(const K&&) [with K = myClass; long unsigned int Cap = 5]’
main.cpp:15:15:   required from here
CircularQueLk.h:43:37: error: ‘construct’ is not a member of ‘std::allocator<myClass>’
make: *** [ma] Error 1
[ax@localhost day12-circularQueue]$ 
```
下面是用 `g++ -o ma main.cpp -std=c++11 -lpthread`, `c++11` 来编译的。
```
[ax@localhost day12-circularQueue]$ make
g++ -o ma main.cpp -std=c++11 -lpthread
[ax@localhost day12-circularQueue]$ ls
```
解决：
`c++11` 之后就不提供 `std::allocator` 的 `construct` 和 `destroy` 成员变量了。

得用新的成员变量：`std::allocator_traits<Alloc>::construct`，`C++20`之后用这个替代 `constructor`。



#### 2. `CircularQueSeq`(一个`std::atomic`版本) 的一些问题

用的是 `std::atomic` 替换上面的 `std::mutex` 来实现的环形队列。

注意 `CircularQueSeq.h` 文件里面为什么会有 `第一处的 do while` 和 `第二处的 do while`(肯定是防止线程安全用的，从流程来看)。


#### 3. `CircularQueLight`(三个`std::atomic`版本) 的一些问题


#### 3. `CircularQueSync`(优化`CircularQueLight`版本) 的一些问题


**具体细节看解析：https://llfc.club/category?catid=225RaiVNI8pFDD5L4m807g7ZwmF#!aid/2XkxkSnynSeJaEQtohQzWn20bSJ**

