#include "CircularQueLk.h"
#include "CircularqueSeq.h"
#include "CircularQueLight.h"
#include "CircularQueSync.h"

#include "myClass.h"

void TestCircularQue()
{
    // 最大容量为 max_size_ = Cap + 1 = 6
    CircularQueLk<myClass, 5> cq_lk;
    myClass mc1(1);
    myClass mc2(2);
    cq_lk.push(mc1);
    cq_lk.push(std::move(mc2));
    for (int i = 3; i <= 5; i++)
    {
        myClass mc(i);
        auto res = cq_lk.push(mc);
        if (res == false)
            break;
    }

    cq_lk.push(mc2); // push error ---> circularQueLk full!!!

    // 上面 push data_ 的顺序是：1，2，3，4，5, 所以下面 pop 的顺序是 1，2，3，4，5
    for (int i = 0; i < 5; i++)
    {
        myClass mc1;
        auto res = cq_lk.pop(mc1);
        if (!res)
            break;
        std::cout << "pop success, " << mc1 << std::endl;
    }

    auto res = cq_lk.pop(mc1);
}

void TestCircularQueSeq()
{
    CircularQueSeq<myClass, 3> cq_seq;
    for (int i = 0; i < 4; i++)
    {
        myClass mc1(i);
        auto res = cq_seq.push(mc1);
        if (!res)
            break;
    }
    for (int i = 0; i < 4; i++)
    {
        myClass mc1;
        auto res = cq_seq.pop(mc1);
        if (!res)
            break;
        std::cout << "pop success, " << mc1 << std::endl;
    }

    for (int i = 0; i < 4; i++)
    {
        myClass mc1(i);
        auto res = cq_seq.push(mc1);
        if (!res)
            break;
    }
    for (int i = 0; i < 4; i++)
    {
        myClass mc1;
        auto res = cq_seq.pop(mc1);
        if (!res)
            break;
        std::cout << "pop success, " << mc1 << std::endl;
    }
}

void TestCircularQueLight()
{
    CircularQueLight<myClass, 3> cq_seq;
    for (int i = 0; i < 4; i++)
    {
        myClass mc1(i);
        auto res = cq_seq.push(mc1);
        if (!res)
            break;
    }
    for (int i = 0; i < 4; i++)
    {
        myClass mc1;
        auto res = cq_seq.pop(mc1);
        if (!res)
            break;
        std::cout << "pop success, " << mc1 << std::endl;
    }

    for (int i = 0; i < 4; i++)
    {
        myClass mc1(i);
        auto res = cq_seq.push(mc1);
        if (!res)
            break;
    }
    for (int i = 0; i < 4; i++)
    {
        myClass mc1;
        auto res = cq_seq.pop(mc1);
        if (!res)
            break;
        std::cout << "pop success, " << mc1 << std::endl;
    }
}

void TestCircularQueSync()
{
    CircularQueSync<myClass, 3> cq_seq;
    for (int i = 0; i < 4; i++)
    {
        myClass mc1(i);
        auto res = cq_seq.push(mc1);
        if (!res)
            break;
    }
    for (int i = 0; i < 4; i++)
    {
        myClass mc1;
        auto res = cq_seq.pop(mc1);
        if (!res)
            break;
        std::cout << "pop success, " << mc1 << std::endl;
    }

    for (int i = 0; i < 4; i++)
    {
        myClass mc1(i);
        auto res = cq_seq.push(mc1);
        if (!res)
            break;
    }
    for (int i = 0; i < 4; i++)
    {
        myClass mc1;
        auto res = cq_seq.pop(mc1);
        if (!res)
            break;
        std::cout << "pop success, " << mc1 << std::endl;
    }

    for (int i = 0; i < 4; i++)
    {
        myClass mc1(i);
        auto res = cq_seq.push(mc1);
        if (!res)
            break;
    }
}

int main()
{
    // TestCircularQue();
    // TestCircularQueSeq();
    // TestCircularQueLight();
    TestCircularQueSync();
}
