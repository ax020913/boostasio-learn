#include <iostream>
#include <string>
#include <mutex>
#include <thread>
#include <chrono>
#include <stack>
#include <memory>
#include <exception>

using namespace std;

// mutex 的使用
std::mutex mutex1;
int shared_data = 100;
void use_lock()
{
    while (true)
    {
        mutex1.lock();
        shared_data++;
        std::cout << "current thread is " << std::this_thread::get_id() << std::endl;
        std::cout << "shared_data is " << shared_data << std::endl
                  << std::endl;
        mutex1.unlock();
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}
void test_lock()
{
    std::thread t1(use_lock);

    std::thread t2([]()
                   {
        mutex1.lock();
        shared_data--;
        std::cout << "current thread is " << std::this_thread::get_id() << std::endl;
        std::cout << "shared_data is" << shared_data << std::endl;
        mutex1.unlock();
        std::this_thread::sleep_for(std::chrono::seconds(1)); });

    t1.join();
    t2.join();
}

// pop 操作，容器为空怎么办
// 重载 std::exception::what()
struct empty_stack : public std::exception
{
    const char *what() const throw()
    {
        // 虚函数的定义
        return "std::stack::data_ is empty !!!";
    }
};
template <class K>
class threadsafe_stack1
{
private:
    std::stack<K> data_;
    // mutable 是 C++ 中的一个关键字，用于修饰类的成员变量。当一个成员变量被声明为 mutable 时，它可以在 const 成员函数中被修改，即使该函数本身是 const 限定的。
    // 注意：std::mutex.lock() 上锁和解锁操作就是修改了 std::mutex 的哦
    mutable std::mutex mutex_;

public:
    threadsafe_stack1() {}
    threadsafe_stack1(const threadsafe_stack1 &other)
    {
        std::lock_guard<std::mutex> lock(other.mutex_);
        // 在构造函数的函数体（constructor body）内进行复制操作
        data_ = other.data_;
    }
    threadsafe_stack1 &operator=(const threadsafe_stack1 &other) = delete;
    void push(K new_value)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        // 下面的 std::move 并没有让外面的实参悬空，是把形参悬空了
        data_.push(std::move(new_value));
    }

    /*
    // 有问题的代码
    K pop()
    {
        // 不判空的话是会有内存问题的。判空的话，返回的也是不符合预期的，应该得返回 nullptr
        // if (empty() == true)
        // return K();

        std::lock_guard<std::mutex> lock(mutex_);
        auto element = data_.top();
        data_.pop();
        return element;
    }
    */
    std::shared_ptr<K> pop()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        // 判断 data_ 是否为空(抛异常)
        if (data_.empty() == true)
        {
            // return nullptr; // error
            throw empty_stack();
        }
        // 改动 stack 前设置返回
        std::shared_ptr<K> const res(std::make_shared<K>(data_.top()));
        data_.pop();

        return res;
    }
    void pop(K &value)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if (data_.empty() == true)
        {
            // return nullptr; // error
            throw empty_stack();
        }
        value = data_.top();
        data_.pop();
    }

    bool empty() const
    {
        std::lock_guard<std::mutex> lock(mutex_);
        return data_.empty();
    }
};
void test_threadsafe_stack1()
{
    threadsafe_stack1<int> safe_stack;
    safe_stack.push(1);

    std::thread t1([&safe_stack]()
                   {
        if(!safe_stack.empty()){
            std::this_thread::sleep_for(std::chrono::seconds(1));
            std::cout << "t1 pop data = " << *(safe_stack.pop()) << std::endl;
        } });

    std::thread t2([&safe_stack]()
                   {
        if(safe_stack.empty() == false){
            std::this_thread::sleep_for(std::chrono::seconds(1));
            std::cout << "t2 pop data = " << *(safe_stack.pop()) << std::endl;
        } });

    t1.join();
    t2.join();
}

// 死锁的情况
std::mutex lock1;
std::mutex lock2;
int value1 = 0;
int value2 = 1;

void dead_lock1()
{
    while (true)
    {
        std::cout << "dead_lock1 begin" << std::endl;
        lock1.lock();
        value1 = 1024;
        lock2.lock();
        value2 = 2048;
        lock2.unlock();
        lock1.unlock();
        std::this_thread::sleep_for(std::chrono::seconds(1));
        std::cout << "dead_lock1 end" << std::endl;
    }
}
void dead_lock2()
{
    while (true)
    {
        std::cout << "dead_lock2 begin" << std::endl;
        lock2.lock();
        value2 = 2048;
        lock1.lock();
        value1 = 1024;
        lock1.unlock();
        lock2.unlock();
        std::this_thread::sleep_for(std::chrono::seconds(1));
        std::cout << "dead_lock2 end" << std::endl;
    }
}
void test_dead_lock()
{
    std::thread t1(dead_lock1);
    std::thread t2(dead_lock2);

    t1.join();
    t2.join();
}

// 加锁和解锁作为原子操作解耦和，各自只管自己的功能
void atomic_lock1()
{
    std::cout << "atomic_lock1 begin" << std::endl;
    lock1.lock();
    value1 = 1024;
    lock1.unlock();
    std::cout << "atomic_lock1 end" << std::endl;
}
void atomic_lock2()
{
    std::cout << "atomic_lock2 begin" << std::endl;
    lock2.lock();
    value2 = 2048;
    lock2.unlock();
    std::cout << "atomic_lock2 end" << std::endl;
}
void safe_lock1()
{
    while (true)
    {
        atomic_lock1();
        atomic_lock2();
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}
void safe_lock2()
{
    while (true)
    {
        atomic_lock2();
        atomic_lock1();
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}
void test_safe_lock()
{
    std::thread t1(safe_lock1);
    std::thread t2(safe_lock2);

    t1.join();
    t2.join();
}

// 假设这是一个很复杂的数据结构，假设不建议拷贝操作
class som_big_object
{
private:
    int data_;

public:
    som_big_object(int data) : data_(data) {}
    // 拷贝构造
    som_big_object(const som_big_object &other) : data_(other.data_) {}
    // 移动构造
    som_big_object(const som_big_object &&other) : data_(std::move(other.data_)) {}

    // 重载输出运算符
    friend std::ostream &operator<<(std::ostream &os, const som_big_object &other)
    {
        os << other.data_;
        return os; // 得连续输入输出
    }
    // 重载赋值运算符
    som_big_object &operator=(const som_big_object &other)
    {
        if (this == &other)
            return *this;

        data_ = other.data_;
        return *this;
    }
    // 交换数据
    friend void swap(som_big_object &other1, som_big_object &other2)
    {
        som_big_object temp = std::move(other1);
        other1 = std::move(other2);
        other2 = std::move(temp);
    }
};
// 假设这是一个结构包含了锁与复杂的成员对象
class big_object_mrg
{
private:
    std::mutex mutex_;
    som_big_object object_;

public:
    big_object_mrg(int data) : object_(data) {}
    void printinfo()
    {
        std::cout << "current obj data is " << object_ << std::endl;
    }
    friend void danger_swap(big_object_mrg &object1, big_object_mrg &object2);
    friend void safe_swap(big_object_mrg &object1, big_object_mrg &object2);
    friend void safe_swap_scope(big_object_mrg &object1, big_object_mrg &object2);
};
// 可能会有循环引用造成的死锁
void danger_swap(big_object_mrg &object1, big_object_mrg &object2)
{
    std::cout << "thread [" << std::this_thread::get_id() << " ] begin" << std::endl;
    if (&object1 == &object2)
        return;

    std::lock_guard<std::mutex> lock1(object1.mutex_);
    // 此处为了故意制造死锁，我们让线程小睡一会
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::lock_guard<std::mutex> lock2(object2.mutex_);
    swap(object1.object_, object2.object_);
    std::cout << "thread [ " << std::this_thread::get_id() << " ] end" << std::endl;
}
void test_danger_swap()
{
    big_object_mrg object1(5);
    big_object_mrg object2(100);

    std::thread t1(danger_swap, std::ref(object1), std::ref(object2));
    std::thread t2(danger_swap, std::ref(object2), std::ref(object1));

    t1.join();
    t2.join();

    object1.printinfo();
    object2.printinfo();
}

// std::adopt_lock 解决循环等待造成的死锁
void safe_swap(big_object_mrg &object1, big_object_mrg &object2)
{
    std::cout << "thread [" << std::this_thread::get_id() << " ] begin" << std::endl;
    if (&object1 == &object2)
        return;

    std::lock(object1.mutex_, object2.mutex_);
    // 领养锁管理它自动释放
    std::lock_guard<std::mutex> lock1(object1.mutex_, std::adopt_lock);

    // 此处为了故意制造死锁，我们让线程小睡一会
    std::this_thread::sleep_for(std::chrono::seconds(1));

    std::lock_guard<std::mutex> lock2(object2.mutex_, std::adopt_lock);

    swap(object1.object_, object2.object_);
    std::cout << "thread [ " << std::this_thread::get_id() << " ] end" << std::endl;
}
// 上述代码可以简化为以下方式
void safe_swap_scope(big_object_mrg &object1, big_object_mrg &object2)
{
    std::cout << "thread [ " << std::this_thread::get_id() << " ] begin" << std::endl;
    if (&object1 == &object2)
    {
        return;
    }

    std::scoped_lock guard(object1.mutex_, object2.mutex_);
    // 等价于
    // std::scoped_lock<std::mutex, std::mutex> guard(object1.mutex_, object2.mutex_);
    swap(object1.object_, object2.object_);
    std::cout << "thread [ " << std::this_thread::get_id() << " ] end" << std::endl;
}
void test_safe_swap()
{
    big_object_mrg object1(5);
    big_object_mrg object2(100);

    std::thread t1(safe_swap, std::ref(object1), std::ref(object2));
    std::thread t2(safe_swap, std::ref(object2), std::ref(object1));
    // std::thread t1(safe_swap_scope, std::ref(object1), std::ref(object2));
    // std::thread t2(safe_swap_scope, std::ref(object2), std::ref(object1));

    t1.join();
    t2.join();

    object1.printinfo();
    object2.printinfo();
}

// 对于现实开发中，我们很难保证嵌套加锁，所以尽可能将互斥操作封装为原子操作，尽量不要在一个函数里嵌套用两个锁
// 对于嵌套用锁，也可以采用权重的方式限制使用顺序
// 层级锁
class hierarchical_mutex
{
public:
    explicit hierarchical_mutex(unsigned long value)
        : _hierarchy_value(value), _previous_hierarchy_value(0) {}
    hierarchical_mutex(const hierarchical_mutex &mutex) = delete;
    hierarchical_mutex &operator=(const hierarchical_mutex &mutex) = delete;

    void lock()
    {
        check_for_hierarchy_violation();
        _internal_mutex.lock();
        update_hierarchy_value();
    }
    void unlock()
    {
        if (_this_thread_hierarchy_value != _hierarchy_value)
        {
            throw std::logic_error("mutex hierarchy violated !!!");
        }

        _this_thread_hierarchy_value = _hierarchy_value;
        _internal_mutex.unlock();
    }
    bool try_lock()
    {
        check_for_hierarchy_violation();
        if (!_internal_mutex.try_lock())
            return false;

        update_hierarchy_value();
        return true;
    }

private:
    std::mutex _internal_mutex;
    // 当前层级值
    unsigned long const _hierarchy_value;
    // 上一次层级值
    unsigned long _previous_hierarchy_value;
    // 本线程记录的层级值(thread_local 线程局部存储)
    static thread_local unsigned long _this_thread_hierarchy_value;

    // 判断线程变量的权重是否大于锁的权重(大于才能加锁，否则抛异常)
    void check_for_hierarchy_violation()
    {
        if (_this_thread_hierarchy_value <= _hierarchy_value)
        {
            // std::cout << "this_thread = " << _this_thread_hierarchy_value << " mutex = " << _hierarchy_value << std::endl;
            throw std::logic_error("mutex hierarchy violated !!!");
        }
    }

    // 更新对应的值
    void update_hierarchy_value()
    {
        _previous_hierarchy_value = _this_thread_hierarchy_value;
        _this_thread_hierarchy_value = _hierarchy_value;
    }
};
thread_local unsigned long hierarchical_mutex::_this_thread_hierarchy_value(ULONG_MAX);

void test_hierarchy_mutex()
{
    hierarchical_mutex hmtx1(1000);
    hierarchical_mutex hmtx2(500);
    std::thread t1([&hmtx1, &hmtx2]()
                   {
        hmtx2.lock();
        hmtx1.lock();
        hmtx1.unlock();
        hmtx2.unlock(); });

    std::thread t2([&hmtx1, &hmtx2]()
                   {
        hmtx2.lock();
        hmtx1.lock();
        hmtx1.unlock();
        hmtx2.unlock(); });

    t1.join();
    t2.join();
}

int main(void)
{
    // test_lock();
    // test_threadsafe_stack1();
    // test_dead_lock();

    // 加锁和解锁作为原子操作解耦和，各自只管自己的功能
    // test_safe_lock();

    // std::adopt_lock 解决循环等待造成的死锁
    // test_danger_swap();
    // test_safe_swap();

    // 层级锁
    test_hierarchy_mutex();

    return 0;
}
