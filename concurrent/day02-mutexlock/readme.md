### `std::mutex`、`sdt::stack` 的使用问题

#### 1. `std::stack` 为空是抛异常还是 `return nullptr` ？

```
// 重载 std::exception::what()
struct empty_stack : public std::exception
{
    const char *what() const throw()
    {
        // 虚函数的定义
        return "std::stack::data_ is empty !!!";
    }
};

std::shared_ptr<K> pop()
{
    std::lock_guard<std::mutex> lock(mutex_);
    // 判断 data_ 是否为空(抛异常)
    if (data_.empty() == true)
    {
        // return nullptr;
        throw empty_stack();
    }
    // 改动 stack 前设置返回
    std::shared_ptr<K> const res(std::make_shared<K>(data_.top()));
    data_.pop();

    return res;
}
```
如果使用 `return nullptr` 的话，会有内存上的问题，因为使用的时候是：
```
std::thread t2([&safe_stack]()
                {
    if(safe_stack.empty() == false){
        std::this_thread::sleep_for(std::chrono::seconds(1));
        std::cout << "t2 pop data = " << *(safe_stack.pop()) << std::endl;
    } });

t2.join();
```
所以返回 `nullptr` 的话，就会 `(*nullptr)` 崩溃。
所以使用抛异常的方式。


#### 2. 注意死 的情况，避免死锁


#### 3. 加锁和解锁作为原子操作解耦和，各自只管自己的功能(一次只使用一把 mutex)


#### 4. `std::adopt_lock` 解决循环等待造成的死锁
可能会有循环引用造成的死锁:
```
// 可能会有循环引用造成的死锁
void danger_swap(big_object_mrg &object1, big_object_mrg &object2)
{
    std::cout << "thread [" << std::this_thread::get_id() << " ] begin" << std::endl;
    if (&object1 == &object2)
        return;

    std::lock_guard<std::mutex> lock1(object1.mutex_);
    // 此处为了故意制造死锁，我们让线程小睡一会
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::lock_guard<std::mutex> lock2(object2.mutex_);
    swap(object1.object_, object2.object_);
    std::cout << "thread [ " << std::this_thread::get_id() << " ] end" << std::endl;
}
void test_danger_swap()
{
    big_object_mrg object1(5);
    big_object_mrg object2(100);

    std::thread t1(danger_swap, std::ref(object1), std::ref(object2));
    std::thread t2(danger_swap, std::ref(object2), std::ref(object1));

    t1.join();
    t2.join();

    object1.printinfo();
    object2.printinfo();
}
```

`std::adopt_lock` 解决循环等待造成的死锁:
```
// std::adopt_lock 解决循环等待造成的死锁
void safe_swap(big_object_mrg &object1, big_object_mrg &object2)
{
    std::cout << "thread [" << std::this_thread::get_id() << " ] begin" << std::endl;
    if (&object1 == &object2)
        return;

    std::lock(object1.mutex_, object2.mutex_);
    // 领养锁管理它自动释放
    std::lock_guard<std::mutex> lock1(object1.mutex_, std::adopt_lock);

    // 此处为了故意制造死锁，我们让线程小睡一会
    std::this_thread::sleep_for(std::chrono::seconds(1));

    std::lock_guard<std::mutex> lock2(object2.mutex_, std::adopt_lock);

    swap(object1.object_, object2.object_);
    std::cout << "thread [ " << std::this_thread::get_id() << " ] end" << std::endl;
}
// 上述代码可以简化为以下方式
void safe_swap_scope(big_object_mrg &object1, big_object_mrg &object2)
{
    std::cout << "thread [ " << std::this_thread::get_id() << " ] begin" << std::endl;
    if (&object1 == &object2)
    {
        return;
    }

    std::scoped_lock guard(object1.mutex_, object2.mutex_);
    // 等价于
    // std::scoped_lock<std::mutex, std::mutex> guard(object1.mutex_, object2.mutex_);
    swap(object1.object_, object2.object_);
    std::cout << "thread [ " << std::this_thread::get_id() << " ] end" << std::endl;
}
void test_safe_swap()
{
    big_object_mrg object1(5);
    big_object_mrg object2(100);

    std::thread t1(safe_swap, std::ref(object1), std::ref(object2));
    std::thread t2(safe_swap, std::ref(object2), std::ref(object1));
    // std::thread t1(safe_swap_scope, std::ref(object1), std::ref(object2));
    // std::thread t2(safe_swap_scope, std::ref(object2), std::ref(object1));

    t1.join();
    t2.join();

    object1.printinfo();
    object2.printinfo();
}
```


#### 5. 层级锁的使用
**场景**：现实开发中常常很难规避同一个函数内部加多个锁的情况，我们要尽可能避免循环加锁，所以可以自定义一个层级锁，保证实际项目中对多个互斥量加锁时是有序的。

