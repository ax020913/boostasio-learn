#include "myClass.hpp"
#include "threadSafe_stack.h"
#include "threadSafe_queue.h"

#include <iostream>
#include <thread>
#include <mutex>

std::mutex mtx_cout;
void PrintMyClass(std::string consumer, std::shared_ptr<myClass> data)
{
    std::lock_guard<std::mutex> lock(mtx_cout);
    std::cout << consumer << " pop data success , data is " << (*data) << std::endl;
}

//
void TestThreadSafeStack()
{
    // threadsafe_stack<myClass> stack;
    threadsafe_stack_waitable<myClass> stack;

    std::thread consumer1(
        [&]()
        {
            for (;;)
            {
                // std::shared_ptr<myClass> data = stack.pop();
                std::shared_ptr<myClass> data = stack.wait_and_pop();
                PrintMyClass("consumer1", data);
            }
        });

    std::thread consumer2([&]()
                          {
			for (;;)
			{
				// std::shared_ptr<myClass> data = stack.pop();
                std::shared_ptr<myClass> data = stack.wait_and_pop();
				PrintMyClass("consumer2", data);
			} });

    std::thread producer([&]()
                         {
			for(int i = 0 ; i < 100; i++)
			{
				myClass mc(i);
				stack.push(std::move(mc));
			} });

    consumer1.join();
    consumer2.join();
    producer.join();
}

//
void TestThreadSafeQue()
{
    // threadsafe_queue<myClass> safe_que;
    // threadsafe_queue_ptr<myClass> safe_que;
    threadsafe_queue_ht<myClass> safe_que;

    std::thread consumer1(
        [&]()
        {
            for (;;)
            {
                std::shared_ptr<myClass> data = safe_que.wait_and_pop();
                PrintMyClass("consumer1", data);
            }
        });

    std::thread consumer2([&]()
                          {
			for (;;)
			{
				std::shared_ptr<myClass> data = safe_que.wait_and_pop();
				PrintMyClass("consumer2", data);
			} });

    std::thread producer([&]()
                         {
			for (int i = 0; i < 100; i++)
			{
				myClass mc(i);
				safe_que.push(std::move(mc));
			} });

    consumer1.join();
    consumer2.join();
    producer.join();
}

int main(void)
{
    // TestThreadSafeStack();
    TestThreadSafeQue();

    return 0;
}