### 基于锁实现线程安全队列和栈容器


#### 1. 一个关于虚函数的报错
报错：
```
[ax@localhost day14-threadSafe_stack-queue]$ make
g++ -o ma main.cpp -std=c++20 -lpthread
/opt/rh/devtoolset-11/root/usr/libexec/gcc/x86_64-redhat-linux/11/ld: /tmp/cclCRBlK.o: in function `empty_stack::empty_stack()':
main.cpp:(.text._ZN11empty_stackC2Ev[_ZN11empty_stackC5Ev]+0x19): undefined reference to `vtable for empty_stack'
/opt/rh/devtoolset-11/root/usr/libexec/gcc/x86_64-redhat-linux/11/ld: /tmp/cclCRBlK.o: in function `empty_stack::~empty_stack()':
main.cpp:(.text._ZN11empty_stackD2Ev[_ZN11empty_stackD5Ev]+0xd): undefined reference to `vtable for empty_stack'
/opt/rh/devtoolset-11/root/usr/libexec/gcc/x86_64-redhat-linux/11/ld: /tmp/cclCRBlK.o: in function `threadsafe_stack<myClass>::pop()':
main.cpp:(.text._ZN16threadsafe_stackI7myClassE3popEv[_ZN16threadsafe_stackI7myClassE3popEv]+0x5c): undefined reference to `typeinfo for empty_stack'
collect2: error: ld returned 1 exit status
make: *** [ma] Error 1
[ax@localhost day14-threadSafe_stack-queue]$ 
```
原因继承 `std::exception` 的 `what` 虚函数成员没有重写：
```
struct empty_stack : std::exception{
    const char *what() const throw();
};
```


#### 2. 关于 `threadsafe_queue_ht` 中为什么大量使用 `std::unique_lock<std::mutex>`
通常我们使用 `std::lock_guard<std::mutex>` 也是挺多的，但是使用他一般是配合 `{ }` 来使用的(在临界区中不用手动解锁的便利性)。但是 `std::unique_lock<std::mutex>` 有一些不一样的特性。

`std::unique_lock` 是 `C++` 标准库中的一个互斥量封装类，用于实现对互斥量的加锁和解锁操作。它具有以下特性：

`1`. 灵活性：`std::unique_lock` 提供了更灵活的锁定和解锁机制。它可以在构造函数中锁定互斥量，并在析构函数中自动解锁，也可以手动调用 `lock()` 和 `unlock()` 方法进行锁定和解锁。

`2`. 所有权转移：`std::unique_lock` 对象可以在不同的作用域之间进行所有权转移。这意味着可以将一个 `std::unique_lock` 对象传递给函数或存储在容器中，而不必担心互斥量的所有权问题。

`3`. 可延迟加锁：`std::unique_lock` 允许在需要时延迟加锁。通过在构造函数中传递 `std::defer_lock` 参数，可以创建一个未加锁的 `std::unique_lock` 对象，然后在适当的时候手动调用 `lock()` 方法进行加锁。

`4`. 可以指定锁定策略：`std::unique_lock` 允许指定不同的锁定策略。通过在构造函数中传递 `std::defer_lock`、`std::try_to_lock` 或 `std::adopt_lock` 参数，可以选择不同的锁定方式。

`5`. 支持条件变量：`std::unique_lock` 可以与条件变量一起使用，用于实现线程间的同步和通信。

总的来说，`std::unique_lock` 提供了更灵活和功能丰富的互斥量封装，使得多线程编程更加方便和安全。它是 `C++` 中常用的线程同步工具之一。


#### 3. 关于 `std::unique_ptr` 

`std::unique_ptr` 是 `C++` 标准库中的一个智能指针类，用于管理动态分配的对象。它具有以下特性：

`1`. 独占所有权：`std::unique_ptr` 独占所管理的对象的所有权，即它是唯一拥有该对象的智能指针。这意味着在任何时候只能有一个 `std::unique_ptr` 指向同一个对象。

`2`. 自动释放：当 `std::unique_ptr` 超出其作用域时，它会自动释放所管理的对象。这意味着不需要手动调用 `delete` 来释放内存，从而避免了内存泄漏的风险。

`3`. 不可复制：`std::unique_ptr` 是不可复制的，即不能通过拷贝构造函数或拷贝赋值运算符来复制一个 `std::unique_ptr` 对象。这是为了确保只有一个 `std::unique_ptr` 指向同一个对象，避免了悬空指针和二次释放的问题。**std::unique_ptr 被赋值只能接受右值。**

`4`. 可移动：`std::unique_ptr` 是可移动的，即可以通过移动构造函数或移动赋值运算符来转移所有权。这使得在函数间传递 `std::unique_ptr` 对象成为可能，而不需要进行对象的复制。


#### 4. 想修改 `tail_` 类型为 `std::unique_ptr<node>` 失败
试着修改了下面三处代码但是都有问题：

```
    node *tail_; // std::unique_ptr<node> 等效于 node*
    // std::unique_ptr<node> tail_;

    // 尾部数据是不参数 pop 的，获取是因为判断队列是否为空的需要
    node *get_tail()
    {
        std::lock_guard<std::mutex> lock(tail_mutex_);
        return tail_;
        // return tail_.get();
    }

    // push
    void push(K new_value)
    {
        std::shared_ptr<K> new_data(std::make_shared<K>(std::move(new_value)));

        std::unique_ptr<node> p(new node);
        {
            std::lock_guard<std::mutex> tail_lock(tail_mutex_);
            tail_->data = new_data;
            node *new_tail = p.get();
            tail_->next = std::move(p);
            tail_ = new_tail; // node* tail_;

            // std::unique_ptr<node> tail_;
            // tail_.get() = new_tail; // tail_.get() 是一个左值 ---> error: lvalue required as left operand of assignment
            // tail_.reset(new_tail); // Segmentation fault (core dumped)
            // tail_.swap(p); // Segmentation fault (core dumped)
            // tail_ = std::move(p); // Segmentation fault (core dumped)
        }

        condition_variable_.notify_one();
    }
```


#### 5. `stack` 中的问题：

![Alt text](image-2.png)

```
    // pop
    std::shared_ptr<K> pop(){
        std::lock_guard<std::mutex> lock(mutex_);
        // bug1：抛异常的问题，对代码不太友好
        if (data_.empty() == true)
            throw empty_stack();

        // bug2：std::make_shared 类似于 malloc 可能会开辟空间失败的，就会抛异常。那么下面的数据就不会 pop 出去
        std::shared_ptr<K> const res(std::make_shared<K>(std::move(data_.top())));
        data_.pop();
        return res;
    }
    void pop(K &value){
        std::lock_guard<std::mutex> lock(mutex_);
        if (data_.empty() == true)
            throw empty_stack();

        value = std::move(data_.top());
        data_.pop();
    }
```
使用了 `std::condition_variable condition_variable_` 配合 `std::mutex mutex_` 实现了 `push` 和 `pop` 操作的同步。`pop` 为空的话，就不用抛异常了。
```
    // pop
    std::shared_ptr<K> wait_and_pop(){
        std::unique_lock<std::mutex> lock(mutex_);
        condition_variable_.wait(lock, [this]()
                                 {
            if(data_.empty() == true)
                return false;
            return true; });

        // bug2：std::make_shared 类似于 malloc 可能会开辟空间失败的，就会抛异常。那么下面的数据就不会 pop 出去
        std::shared_ptr<K> const res(std::make_shared<K>(std::move(data_.top())));
        data_.pop();
        return res;
    }

    // try_pop
    std::shared_ptr<K> try_pop(){
        std::lock_guard<std::mutex> lock(mutex_);
        if (data_.empty() == true)
            return std::make_shared<K>();

        // bug2：std::make_shared 类似于 malloc 可能会开辟空间失败的，就会抛异常。那么下面的数据就不会 pop 出去
        std::shared_ptr<K> res(std::make_shared<K>(std::move(data_.top())));
        data_.pop();
        return res;
    }
```
但是 `bug2` 的问题还是有的。

对于 `bug2：std::make_shared` 类似于 `malloc` 可能会开辟空间失败的，就会抛异常。造成下面的数据就不会 `pop` 出去的问题，可以把成员 `std::stack<K> data_` 的数据类型改成 `std::shared_ptr<K>` 来解决(**在 `queue` 中展示了**)。

尽管外面 `template<class K>` 传递进来的 `K` 是一个 `std::shared_ptr<K>` 类型，造成 `std::shared_ptr<std::shared_ptr<K>> data_` 也是可以的。**外来传递进来的 `K` 的类型最好是 `std::shared_ptr` 类型的是最好的，为了防止不是，所以是可以在内部再封装一层的。**


#### 6. `threadsafe_queue_ptr` 解决上面的 `bug2` 问题：

```
    std::queue<std::shared_ptr<K>> data_queue_;
    std::shared_ptr<K> wait_and_pop()
    {
        std::unique_lock<std::mutex> lock(mutex_);
        condition_variable_.wait(lock, [this]()
                                 {
            if(data_queue_.empty() == true) return false;
            return true; });

        // 就不会有 std::make_shared 类似于 malloc 可能会开辟空间失败的，就会抛异常的问题了。智能指针之间的赋值是不会抛异常的。
        std::shared_ptr<K> res = *(data_queue_.front());
        data_queue_.pop();
        return res;
    }
    std::shared_ptr<K> try_pop()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if (data_queue_.empty() == true)
            return std::shared_ptr<K>();

        // 就不会有 std::make_shared 类似于 malloc 可能会开辟空间失败的，就会抛异常的问题了。智能指针之间的赋值是不会抛异常的。
        std::shared_ptr<K> res = data_queue_.front();
        data_queue_.pop();
        return res;
    }
```


#### 7. `threadsafe_queue_ht` 分离 `push` 和 `pop` 的同步，实现 `queue` 的并发
队列和栈最本质的区别是队列是首尾操作。我们可以考虑将push和pop操作分化为分别对尾和对首部的操作。对首和尾分别用不同的互斥量管理就可以实现真正意义的并发了。

我们引入虚位节点的概念，表示一个空的节点，没有数据，是一个无效的节点，初始情况下，队列为空，`head` 和 `tail` 节点都指向这个虚位节点。

![Alt text](image.png)

当我们 `push` 一个数据，比如为 `MyClass` 类型的数据后(`MyClass` 的数据内容赋值给了虚位节点 ---> `MyClass` 就变成了虚位节点【造成 `push` 到尾部的现象】)， `tail` 向后移动一个位置(指向虚位节点)，并且仍旧指向这个虚位节点。

![Alt text](image-1.png)

**代码的具体细节看详情处：https://llfc.club/category?catid=225RaiVNI8pFDD5L4m807g7ZwmF#!aid/2YNlWGTdr08l6DcfWeb6Rf7a7E5**

