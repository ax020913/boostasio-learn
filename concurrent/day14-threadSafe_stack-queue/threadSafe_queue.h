#pragma once

#include <queue>
#include <mutex>

// bug2：std::make_shared 类似于 malloc 可能会开辟空间失败的，就会抛异常。那么下面的数据就不会 pop 出去
// std::queue<K> data_queue_;
template <class K>
class threadsafe_queue
{
private:
    mutable std::mutex mutex_;
    std::queue<K> data_queue_;
    std::condition_variable condition_variable_;

public:
    threadsafe_queue() {}

    // empty
    bool empty() const
    {
        std::lock_guard<std::mutex> lock(mutex_);
        return data_queue_.empty();
    }

    // push
    void push(K new_value)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        data_queue_.push(std::move(new_value));
        condition_variable_.notify_one();
    }

    // wait_and_pop
    void wait_and_pop(K &value)
    {
        std::unique_lock<std::mutex> lock(mutex_);
        condition_variable_.wait(lock, [this]()
                                 {
            if(data_queue_.empty() == true)
                return false;
            return true; });

        value = std::move(data_queue_.front());
        data_queue_.pop();
    }
    std::shared_ptr<K> wait_and_pop()
    {
        std::unique_lock<std::mutex> lock(mutex_);
        condition_variable_.wait(lock, [this]()
                                 {
            if(data_queue_.empty() == true)
                return false;
            return true; });

        // bug2：std::make_shared 类似于 malloc 可能会开辟空间失败的，就会抛异常。那么下面的数据就不会 pop 出去
        std::shared_ptr<K> const res(std::make_shared<K>(std::move(data_queue_.front())));
        data_queue_.pop();
        return res;
    }
    // try_pop
    void try_pop(K &value)
    {
        std::unique_lock<std::mutex> lock(mutex_);
        condition_variable_.wait(lock, [this]()
                                 {
            if(data_queue_.empty() == true) return false;
            return true; });

        value = std::move(data_queue_.front());
        data_queue_.pop();
    }
    std::shared_ptr<K> try_pop()
    {
        std::unique_lock<std::mutex> lock(mutex_);
        condition_variable_.wait(lock, [this]()
                                 {
            if(data_queue_.empty() == true) return false;
            return true; });

        // bug2：std::make_shared 类似于 malloc 可能会开辟空间失败的，就会抛异常。那么下面的数据就不会 pop 出去
        std::shared_ptr<K> const res(std::make_shared<K>(std::move(data_queue_.front())));
        data_queue_.pop();
        return res;
    }
};

// std::queue<std::shared_ptr<K>> data_queue_ 解决上面的 bug2
// std::queue<std::shared_ptr<K>> data_queue_
template <class K>
class threadsafe_queue_ptr
{
private:
    std::queue<std::shared_ptr<K>> data_queue_;
    mutable std::mutex mutex_;
    std::condition_variable condition_variable_;

public:
    threadsafe_queue_ptr() {}

    // empty
    bool empty() const
    {
        std::lock_guard<std::mutex> lock(mutex_);
        return data_queue_.empty();
    }

    // push
    void push(K new_value)
    {
        // data_queue_ 队列 push 操作需要的变量类型是 std::shared_ptr 的
        std::shared_ptr<K> new_data(std::make_shared<K>(new_value));

        std::lock_guard<std::mutex> lock(mutex_);
        data_queue_.push(new_data);
        condition_variable_.notify_one();
    }

    // wait_and_pop
    void wait_and_pop(K &value)
    {
        std::unique_lock<std::mutex> lock(mutex_);
        condition_variable_.wait(lock, [this]()
                                 {
            if(data_queue_.empty() == true) return false;
            return true; });

        // data_queue_ 的每一个元素都是 std::shared_ptr 类型的了
        value = std::move(*(data_queue_.front()));
        data_queue_.pop();
    }
    std::shared_ptr<K> wait_and_pop()
    {
        std::unique_lock<std::mutex> lock(mutex_);
        condition_variable_.wait(lock, [this]()
                                 {
            if(data_queue_.empty() == true) return false;
            return true; });

        // 就不会有 std::make_shared 类似于 malloc 可能会开辟空间失败的，就会抛异常的问题了。智能指针之间的赋值是不会抛异常的。
        std::shared_ptr<K> res = *(data_queue_.front());
        data_queue_.pop();
        return res;
    }
    // try_pop
    bool try_pop(K &value)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if (data_queue_.empty() == true)
            return false;

        value = std::move(*(data_queue_.front()));
        data_queue_.pop();
        return true;
    }
    std::shared_ptr<K> try_pop()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if (data_queue_.empty() == true)
            return std::shared_ptr<K>();

        // 就不会有 std::make_shared 类似于 malloc 可能会开辟空间失败的，就会抛异常的问题了。智能指针之间的赋值是不会抛异常的。
        std::shared_ptr<K> res = data_queue_.front();
        data_queue_.pop();
        return res;
    }
};

// 优化上一个版本：分离 push 和 pop 的同步，实现 queue 的并发
// std::queue<std::shared_ptr> data_queue_;
template <class K>
class threadsafe_queue_ht
{
private:
    struct node
    {
        std::shared_ptr<K> data;
        std::unique_ptr<node> next;
    };

    std::mutex head_mutex_;
    std::unique_ptr<node> head_;
    std::mutex tail_mutex_;
    node *tail_; // std::unique_ptr<node> 等效于 node*
    // std::unique_ptr<node> tail_; // 修改失败
    std::condition_variable condition_variable_;

    // 尾部数据是不参数 pop 的，获取是因为判断队列是否为空的需要
    node *get_tail()
    {
        std::lock_guard<std::mutex> lock(tail_mutex_);
        return tail_;
        // return tail_.get();
    }
    std::unique_ptr<node> pop_head()
    {
        std::unique_ptr<node> old_head = std::move(head_);
        head_ = std::move(old_head->next);
        return old_head;
    }

    // wait_pop_head
    std::unique_lock<std::mutex> wait_for_data()
    {
        std::unique_lock<std::mutex> lock(head_mutex_);
        condition_variable_.wait(lock, [this]()
                                 {
            // head_.get() == get_tail() 时为空的 queue
            if(head_.get() == get_tail()) return false;
            return true; });

        // std::unique_lock<std::mutex> 是可以移动构造的（看源码）
        // 把 std::mutex head_mutex_; 互斥量的所有权给转移出去了，是 std::unique_lock<std::mutex> 的特性之一
        return std::move(lock);
    }
    std::unique_ptr<node> wait_pop_head()
    {
        // wait_for_data 函数的返回值是 std::unique_lock<std::mutex> 类型的，所以得用一个 std::unique_lock<std::mutex> 类型的变量来接收
        std::unique_lock<std::mutex> lock(wait_for_data());
        return pop_head();
    }
    std::unique_ptr<node> wait_pop_head(K &value)
    {
        std::unique_lock<std::mutex> lock(wait_for_data());
        value = std::move(*head_->data);
        return pop_head();
    }
    // try_pop_head
    std::unique_ptr<node> try_pop_head()
    {
        std::lock_guard<std::mutex> lock(head_mutex_);
        // if(head_.get() == tail_.get())
        if (head_.get() == get_tail())
            return std::unique_ptr<node>();

        return pop_head();
    }
    std::unique_ptr<node> try_pop_head(K &value)
    {
        std::lock_guard<std::mutex> lock(head_mutex_);
        // if(head_.get() == tail_.get())
        if (head_.get() == get_tail())
            return std::unique_ptr<node>();

        value = std::move(*head_->data);
        return pop_head();
    }

public:
    threadsafe_queue_ht() : head_(new node), tail_(head_.get()) {}
    threadsafe_queue_ht(const threadsafe_queue_ht &other) = delete;
    threadsafe_queue_ht &operator=(const threadsafe_queue_ht &other) = delete;

    // empty
    bool empty() const
    {
        std::lock_guard<std::mutex> lock(head_mutex_);
        return (head_.get() == get_tail());
    }

    // push
    void push(K new_value)
    {
        std::shared_ptr<K> new_data(std::make_shared<K>(std::move(new_value)));

        std::unique_ptr<node> p(new node);
        {
            std::lock_guard<std::mutex> tail_lock(tail_mutex_);
            tail_->data = new_data;
            node *new_tail = p.get();
            tail_->next = std::move(p);
            tail_ = new_tail; // node* tail_;

            // std::unique_ptr<node> tail_;
            // tail_.get() = new_tail; // tail_.get() 是一个左值 ---> error: lvalue required as left operand of assignment
            // tail_.reset(new_tail); // Segmentation fault (core dumped)
            // tail_.swap(p); // Segmentation fault (core dumped)
            // tail_ = std::move(p); // Segmentation fault (core dumped)
            // tail_ = std::move(*new_tail); // Segmentation fault (core dumped)
        }

        condition_variable_.notify_one();
    }

    // wait_and_pop
    void wait_and_pop(K &value)
    {
        std::unique_ptr<node> const old_head = wait_pop_head(value);
    }
    std::shared_ptr<K> wait_and_pop()
    {
        std::unique_ptr<node> const old_head = wait_pop_head();
        return old_head->data;
    }
    // try_pop
    bool try_pop(K &value)
    {
        std::unique_ptr<node> const old_head = try_pop_head(value);
        return old_head;
    }
    std::shared_ptr<K> try_pop()
    {
        std::unique_ptr<node> const old_head = try_pop_head();
        return old_head ? old_head->data : std::shared_ptr<K>();
    }
};
