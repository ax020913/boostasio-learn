#pragma once

#include <iostream>
#include <stack>
#include <mutex>
#include <condition_variable>
#include <exception>

struct empty_stack : std::exception
{
    // const char *what() const throw()
    // const char *what() const override
    virtual const char *what() const noexcept override
    {
        const char *error_msg = "stack is empty";
        std::cout << error_msg << std::endl;
        return error_msg;
    }
};

//
template <class K>
class threadsafe_stack
{
private:
    std::stack<K> data_;
    mutable std::mutex mutex_;

public:
    threadsafe_stack() {}
    threadsafe_stack(const threadsafe_stack &other)
    {
        std::lock_guard<std::mutex> lock(other.mutex_); // 得加 other 的 mutex_，因为可以防止外面再使用 other
        data_ = other.data_;                            // 会调用 std::stack 自己的赋值函数
    }
    threadsafe_stack &operator=(const threadsafe_stack &other) = delete;

    // push
    void push(K new_value)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        data_.push(std::move(new_value));
    }

    // pop
    std::shared_ptr<K> pop()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        // bug：抛异常的问题，对代码不太友好
        if (data_.empty() == true)
            throw empty_stack();

        // bug：std::make_shared 类似于 malloc 可能会开辟空间失败的，就会抛异常。那么下面的数据就不会 pop 出去
        std::shared_ptr<K> const res(std::make_shared<K>(std::move(data_.top())));
        data_.pop();
        return res;
    }
    void pop(K &value)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if (data_.empty() == true)
            throw empty_stack();

        value = std::move(data_.top());
        data_.pop();
    }

    // empty
    bool empty() const
    {
        std::lock_guard<std::mutex> lock(mutex_);
        return data_.empty();
    }
};

//
template <class K>
class threadsafe_stack_waitable
{
private:
    std::stack<K> data_;
    mutable std::mutex mutex_;
    std::condition_variable condition_variable_;

public:
    threadsafe_stack_waitable() {}
    threadsafe_stack_waitable(const threadsafe_stack_waitable &other)
    {
        std::lock_guard<std::mutex> lock(other.mutex_); // 得加 other 的 mutex_，因为可以防止外面再使用 other
        data_ = other.data_;                            // 会调用 std::stack 自己的赋值函数
    }
    threadsafe_stack_waitable &operator=(const threadsafe_stack_waitable &other) = delete;

    // empty
    bool empty() const
    {
        std::lock_guard<std::mutex> lock(mutex_);
        return data_.empty();
    }

    // push
    void push(K new_value)
    {
        std::unique_lock<std::mutex> lock(mutex_);
        data_.push(std::move(new_value));
        condition_variable_.notify_one();
    }

    // pop
    std::shared_ptr<K> wait_and_pop()
    {
        std::unique_lock<std::mutex> lock(mutex_);
        condition_variable_.wait(lock, [this]()
                                 {
            if(data_.empty() == true)
                return false;
            return true; });

        // bug：std::make_shared 类似于 malloc 可能会开辟空间失败的，就会抛异常。那么下面的数据就不会 pop 出去
        std::shared_ptr<K> const res(std::make_shared<K>(std::move(data_.top())));
        data_.pop();
        return res;
    }
    void wait_and_pop(K &value)
    {
        std::unique_lock<std::mutex> lock(mutex_);
        condition_variable_.wait(lock, [this]()
                                 {
            if(data_.empty() == true)
                return false;
            return true; });

        value = std::move(data_.top());
        data_.pop();
    }
    // try_pop
    std::shared_ptr<K> try_pop()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if (data_.empty() == true)
            return std::make_shared<K>();

        // bug：std::make_shared 类似于 malloc 可能会开辟空间失败的，就会抛异常。那么下面的数据就不会 pop 出去
        std::shared_ptr<K> res(std::make_shared<K>(std::move(data_.top())));
        data_.pop();
        return res;
    }
    bool try_pop(K &value)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if (data_.empty() == true)
            return false;

        value = std::move(data_.top());
        data_.pop();
        return true;
    }
};