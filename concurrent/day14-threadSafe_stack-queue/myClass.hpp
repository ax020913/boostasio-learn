#pragma once

#include <iostream>
#include <string>

class myClass
{
public:
    myClass(int data) : data_(data) {}
    myClass(const myClass &mc) : data_(mc.data_) {}
    myClass(const myClass &&mc) : data_(std::move(mc.data_)) {}

    friend std::ostream &operator<<(std::ostream &out, const myClass &mc)
    {
        out << mc.data_;
        return out;
    }

private:
    int data_;
};
