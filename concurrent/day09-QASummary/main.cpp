#include <iostream>
#include <string>
#include <memory>
#include <chrono>
#include <functional>
#include <future>

// 局部变量返回值
class TestCopy
{
public:
    TestCopy()
    {
        std::cout << "TestCopy()" << std::endl;
    }
    TestCopy(const TestCopy &tp)
    {
        std::cout << "TestCopy(const TestCopy& tp)" << std::endl;
    }
    TestCopy(TestCopy &&tp)
    {
        std::cout << "TestCopy(const TestCopy &&tp)" << std::endl;
    }
};
TestCopy testCopy()
{
    TestCopy tp;
    return tp;
}

std::shared_ptr<int> ReturnUniquePtr()
{
    std::unique_ptr<int> unique_ptr_ = std::make_unique<int>(100);
    return unique_ptr_;
}
std::thread ReturnThread()
{
    std::thread myThread([]()
                         {
        int i = 0;
        while(true){
            std::cout << "i is " << i << std::endl;
            i++;
            if(i == 5)
                break;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        } });
    return myThread;
}

// 线程归属权问题
void threadOp()
{
    std::thread myThread1([]()
                          {
        int i = 0;
        while(i < 5){
            std::this_thread::sleep_for(std::chrono::seconds(1));
            i++;
        } });

    std::thread myThread2([]()
                          {
        int i = 0;
        while(i < 10){
            std::this_thread::sleep_for(std::chrono::seconds(1));
            i++;
        } });

    // 不能将一个线程归属权绑定给一个已经绑定线程的变量，否则会触发 terminate 导致崩溃
    myThread1 = std::move(myThread2);
    myThread1.join();
    myThread2.join();
}

// thread参数值拷贝
void exchangeValue()
{
    int value_ = 100;
    int value_move = 100;
    int value_ref = 100;

    // std::thread myThread_([](int &value_) // error
    std::thread myThread_([](int value_)
                          { value_++; },
                          value_);
    std::this_thread::sleep_for(std::chrono::seconds(2)); // 等待子线程 exchange 完成
    std::cout << "value_ = " << value_ << std::endl;

    // std::thread myThread_move([](int &value_move)
    //                           { value_move++; },
    //                           std::move(value_move));
    // std::this_thread::sleep_for(std::chrono::seconds(2)); // 等待子线程 exchange 完成
    // value_move 被 std::move() 了下面的也是使用不了了的
    // std::cout << "value_move = " << value_move << std::endl;

    std::thread myThread_ref([](int &value_ref)
                             { value_ref++; },
                             std::ref(value_ref));
    std::this_thread::sleep_for(std::chrono::seconds(2)); // 等待子线程 exchange 完成
    std::cout << "value_ref = " << value_ref << std::endl;

    myThread_.join();
    // myThread_move.join();
    myThread_ref.join();
}

// 看起来是同步的 async
void blockAsync()
{
    std::cout << "begin block async" << std::endl;
    {
        std::future<void> future = std::async(std::launch::async, []()
                                              {
            std::this_thread::sleep_for(std::chrono::seconds(1));
            std::cout << "std::async called" << std::endl; });
    }
    std::cout << "end block async" << std::endl;
}

// future 未来析构 ---> 死锁
void DeadLock()
{
    std::mutex mtx;
    std::cout << "DeadLock begin " << std::endl;
    std::lock_guard<std::mutex> dklock(mtx);
    {
        std::future<void> futures = std::async(std::launch::async, [&mtx]()
                                               {
            std::cout << "std::async called " << std::endl;
            std::lock_guard<std::mutex>  dklock(mtx);
            std::cout << "async working...." << std::endl; });
    }
    std::cout << "DeadLock end " << std::endl;
}

//
int asyncFunc()
{
    std::this_thread::sleep_for(std::chrono::seconds(3));
    std::cout << "this is asyncFunc" << std::endl;
    return 0;
}
void func1(std::future<int> &future_ref)
{
    std::cout << "this is func1" << std::endl;
    future_ref = std::async(std::launch::async, asyncFunc);
}
void func2(std::future<int> &future_ref)
{
    std::cout << "this is func2" << std::endl;
    auto future_res = future_ref.get();
    if (future_res == 0)
    {
        std::cout << "get asyncFunc result success !" << std::endl;
    }
    else
    {
        std::cout << "get asyncFunc result failed !" << std::endl;
        return;
    }
}
// 提供多种思路，这是第一种
void first_method()
{
    std::future<int> future_tmp;
    func1(future_tmp);
    func2(future_tmp);
}

// 纯异步
template <typename Func, typename... Args>
auto ParallenExe(Func &&func, Args &&...args) -> std::future<decltype(func(args...))>
{
    typedef decltype(func(args...)) RetType;
    std::function<RetType()> bind_func = std::bind(std::forward<Func>(func), std::forward<Args>(args)...);
    std::packaged_task<RetType()> task(bind_func);
    auto rt_future = task.get_future();
    std::thread t(std::move(task));
    t.detach();
    return rt_future;
}
void TestParallen1()
{
    int i = 0;
    std::cout << "Begin TestParallen1 ..." << std::endl;
    {
        ParallenExe([](int i)
                    {
			while (i < 3) {
				i++;
				std::cout << "ParllenExe thread func " << i << " times" << std::endl;
				std::this_thread::sleep_for(std::chrono::seconds(1));
			} },
                    i);
    }

    std::cout << "End TestParallen1 ..." << std::endl;
}
void TestParallen2()
{
    int i = 0;
    std::cout << "Begin TestParallen2 ..." << std::endl;

    auto rt_future = ParallenExe([](int i)
                                 {
			while (i < 3) {
				i++;
				std::cout << "ParllenExe thread func " << i << " times" << std::endl;
				std::this_thread::sleep_for(std::chrono::seconds(1));
			} },
                                 i);

    std::cout << "End TestParallen2 ..." << std::endl;

    rt_future.wait();
}

int main(void)
{
    //
    // testCopy();
    // auto ptr = ReturnUniquePtr();
    // std::cout << "ptr value is " << *ptr << std::endl;
    // std::thread tr_thread = ReturnThread();
    // tr_thread.join();

    //
    // threadOp();

    //
    // exchangeValue();

    //
    // blockAsync();

    //
    // DeadLock();

    //
    // first_method();

    //
    // TestParallen1();
    // 等待异步线程完成任务
    // std::this_thread::sleep_for(std::chrono::seconds(1));

    TestParallen2();

    return 0;
}