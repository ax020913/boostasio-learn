#include "util.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>

#define BUFFER_SIZE 1024

int main(void)
{
    // char buffer[BUFFER_SIZE] = "Hello World!";
    // std::cout << sizeof(buffer) << std::endl; // 1024
    // std::cout << strlen(buffer) << std::endl; // 12

    struct sockaddr_in server_addr;
    int i;
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0); // 创建套接字
    if (sockfd < 0)
    {                            // 判断套接字是否成功创建
        perror("socket failed"); // 如果创建失败，打印错误信息并退出程序
        exit(EXIT_FAILURE);      // 退出程序
    }
    server_addr.sin_family = AF_INET;                   // 设置地址家族为IPv4地址家族
    server_addr.sin_port = htons(PORT2);                // 设置端口号（网络字节序）
    server_addr.sin_addr.s_addr = inet_addr(SERVER_IP); // INADDR_ANY;  // 设置IP地址为本地地址（INADDR_ANY）
    bzero(&(server_addr.sin_zero), 8);                  // 清空剩余部分，防止影响结果（虽然在这个例子中不会影响结果）

    const char *buffer = "Hello World!";
    Send tlv;
    int type = 3;
    int length = strlen(buffer); // 末尾的 "\0" 这一个字符得加上
    // 如果用 TLV tlv 的话，int type, int length, char* value 在内存上是不连续的(recvfrom使用的时候会乱码)。所以直接使用一个连续的 char* value
    // memcpy(&tlv.type, &type, sizeof(int));
    // memcpy(&tlv.length, &length, sizeof(int));
    // tlv.value = (char *)malloc(sizeof(int) * 2 + sizeof(char) * length);
    tlv.value = (char *)malloc(sizeof(char) * BUFFER_SIZE); // 直接开一个大的 char 数组

    if (tlv.value == NULL)
    {
        perror("Memory allocation failed");
        exit(1);
    }
    memcpy(tlv.value, &type, sizeof(int));                 // type
    memcpy(tlv.value + sizeof(int), &length, sizeof(int)); // length
    memcpy(tlv.value + sizeof(int) * 2, buffer, length);   // content

    // 下面用 tlv.value，不用 &tlv
    int bytes_sent = sendto(sockfd, tlv.value, sizeof(int) * 2 + length, 0, (struct sockaddr *)&server_addr, sizeof(server_addr)); // 向服务器发送数据报文
    if (bytes_sent < 0)
    {                            // 如果发送失败，打印错误信息并退出程序
        perror("sendto failed"); // 如果发送失败，打印错误信息并退出程序
        exit(EXIT_FAILURE);      // 退出程序
    }
    else
    { // 如果发送成功，打印已发送的字节数和退出程序

        printf("Bytes sent : %d\n", bytes_sent); // 打印已发送的字节数

        for (int i = 0; i < 3; i++)
        {
            // sleep(1);

            const char *msg = "Hello World!!!";
            int type = 3;
            int length = strlen(msg);                                                                                                       // 末尾的 "\0" 这一个字符得加上
            memcpy(tlv.value, &type, sizeof(int));                                                                                          // type
            memcpy(tlv.value + sizeof(int), &length, sizeof(int));                                                                          // length
            memcpy(tlv.value + sizeof(int) * 2, msg, length);                                                                               // content
            bytes_sent = sendto(sockfd, tlv.value, sizeof(int) * 2 + strlen(msg), 0, (struct sockaddr *)&server_addr, sizeof(server_addr)); // 向服务器发送数据报文
            if (bytes_sent < 0)
            {
                perror("sendto failed");
                exit(EXIT_FAILURE);
            }
        }
        close(sockfd); // 关闭套接字，释放资源，并退出程序（此处并没有执行到，因为在调用sendto函数后就会直接结束）
    }
    free(tlv.value);

    return 0; // 返回0表示程序正常结束（此处并没有执行到，因为在调用sendto函数后就会直接结束）
}
