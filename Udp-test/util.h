#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <pthread.h>
#include <ctype.h>
#include <stdbool.h>

#define SERVER_IP "192.168.61.175"
#define PORT 12345
#define PORT2 12347
// #define MAX_BUFFER_SIZE 65500
#define MAX_BUFFER_SIZE 1024

#define buffername "IO.bin"
#define MAX_BUFFER_SIZE2 1024

typedef struct // 发送是使用的
{
    // int type;
    // int length;
    char *value;
} Send;

typedef struct // 接收时使用的
{
    int type;
    int length;
    char *value;
} TLV;

typedef struct
{
    int length;
    int blockNumber;

} PacketHeader;

int is_whitespace(char c);

int count_words(const char *str);

void copyArray(char *source, char *destination, int size);

int BSend();

int BSend2(char argv[]);

int BSend3(char argv[], int num);

int BSend4(char argv[], int num, int stime);

int BSend5(char argv[], int num, int stime, char myip[]);

int BSend6(char argv[], int num, int stime, char myip[], int myport);

int BSend7(char argv[], int num, int stime, char myip[], int myport, char myaddr[]);

int SendMessge(int recover, char *mip, int mport);

TLV unpack_tlv(const char *data);

void *rece_switch(void *arg);
