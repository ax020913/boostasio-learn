#include "util.h"

pthread_mutex_t mute;

char *words[10];

// 判断字符是否为空白字符（空格、制表符、换行等）
int is_whitespace(char c)
{
	return c == ' ' || c == '\t' || c == '\n' || c == '\r';
}

// 判断一个字符串中有多少个单词
int count_words(const char *str)
{
	int word_count = 0;
	int in_word = 0; // 标记是否在一个单词中

	while (*str)
	{
		// 如果当前字符是空白字符
		if (is_whitespace(*str))
		{
			in_word = 0;
		}
		else
		{
			// 如果当前字符不是空白字符，并且上一个字符是空白字符（或者是字符串的开始），说明进入了一个新的单词
			if (!in_word)
			{
				word_count++;
				in_word = 1;
			}
		}

		// 移动到下一个字符
		str++;
	}

	return word_count;
}

void copyArray(char *source, char *destination, int size)
{
	int i;
	for (i = 0; i < size; ++i)
	{
		destination[i] = source[i];
	}
}

int BSend()
{
	int client_socket;
	struct sockaddr_in server_addr;
	char buffer[MAX_BUFFER_SIZE] = buffername;

	FILE *file;
	client_socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (client_socket < 0)
	{
		printf("Could not create socket with error code: %d\n");
		return 1;
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(SERVER_IP);
	server_addr.sin_port = htons(PORT);

	// 从这里开始进行循环

	system("rdmem r 0x0e64e900 2000000 -o IO.bin");

	printf("Enter the %s to send now!\n", buffername);

	size_t bytesRead; // 这边定义了一个分包的参数

	int mysend = sendto(client_socket, buffer, MAX_BUFFER_SIZE, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
	if (mysend < 0)
	{
		printf("sendto() failed with error code:\n");
		close(client_socket);
		return 1;
	}

	file = fopen(buffername, "rb");
	if (file == NULL)
	{
		printf("Failed to open file: %s\n", buffername);
		// continue循环需要加;
	}

	if (fseek(file, 0, SEEK_END) != 0)
	{
		printf("You can't find file's tail\n");
		fclose(file);
	}

	long fileLength = ftell(file);

	if (fileLength == -1L)
	{
		printf("You can't get file's length\n");
	}
	else
	{
		printf("the send file size is %d\n", fileLength);
		int mysend2 = sendto(client_socket, (char *)&fileLength, sizeof(fileLength), 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
		if (mysend2 < 0)
		{
			printf("sendto() failed with error size: %d\n");
			close(client_socket);
			return 1;
		}
	}

	fclose(file);
	file = fopen(buffername, "rb");

	if (file == NULL)
	{
		printf("Failed to open file: %s\n", buffername);
	}

	int seq = 0;
	while (1)
	{

		int bytesRead = fread(buffer, 1, MAX_BUFFER_SIZE, file);
		if (bytesRead <= 0)
			break;

		if (sendto(client_socket, buffer, bytesRead, 0, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1)
		{
			printf("sendto failed!\n");
			close(client_socket);
			return 1;
		}
		printf("Sent packet %d,size:%d\n", seq, bytesRead);
		seq++;
	}

	printf("File sent successfully!\n");

	fclose(file);
	close(client_socket);
	return 0;
}

int BSend2(char argv[])
{
	int client_socket;
	struct sockaddr_in server_addr;
	// char buffer[MAX_BUFFER_SIZE] ;
	char buffer[MAX_BUFFER_SIZE] = buffername;

	FILE *file;
	client_socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (client_socket < 0)
	{
		printf("Could not create socket with error code: %d\n");
		return 1;
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(SERVER_IP);
	server_addr.sin_port = htons(PORT);

	// 从这里开始进行循环

	char command[100];

	sprintf(command, "rdmem r 0x0e64e900 %s -o %s", argv, buffername);
	int result = system(command);
	if (result == -1)
	{
		printf("command fail!\n");
		return 1;
	}
	else
	{
		printf("command success!\n");
	}

	printf("Enter the %s to send now!\n", buffername);

	size_t bytesRead; // 这边定义了一个分包的参数

	int mysend = sendto(client_socket, buffer, MAX_BUFFER_SIZE, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
	if (mysend < 0)
	{
		printf("sendto() failed with error code:\n");
		close(client_socket);
		return 1;
	}

	file = fopen(buffername, "rb");
	if (file == NULL)
	{
		printf("Failed to open file: %s\n", buffername);
		// continue循环需要加;
	}

	if (fseek(file, 0, SEEK_END) != 0)
	{
		printf("You can't find file's tail\n");
		fclose(file);
	}

	long fileLength = ftell(file);

	if (fileLength == -1L)
	{
		printf("You can't get file's length\n");
	}
	else
	{
		printf("the send file size is %d\n", fileLength);
		int mysend2 = sendto(client_socket, (char *)&fileLength, sizeof(fileLength), 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
		if (mysend2 < 0)
		{
			printf("sendto() failed with error size: %d\n");
			close(client_socket);
			return 1;
		}
	}

	fclose(file);
	file = fopen(buffername, "rb");

	if (file == NULL)
	{
		printf("Failed to open file: %s\n", buffername);
	}

	int seq = 0;
	while (1)
	{
		int bytesRead = fread(buffer, 1, MAX_BUFFER_SIZE, file);
		if (bytesRead <= 0)
			break;

		if (sendto(client_socket, buffer, bytesRead, 0, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1)
		{
			printf("sendto failed!\n");
			close(client_socket);
			return 1;
		}
		printf("Sent packet %d,size:%d\n", seq, bytesRead);
		seq++;
	}

	printf("File sent successfully!\n");

	fclose(file);
	close(client_socket);
	return 0;
}

int BSend3(char argv[], int num)
{

	int client_socket;
	struct sockaddr_in server_addr;
	// char buffer[MAX_BUFFER_SIZE] ;
	FILE *file;
	client_socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (client_socket < 0)
	{
		printf("Could not create socket with error code: %d\n");
		return 1;
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(SERVER_IP);
	server_addr.sin_port = htons(PORT);

	// 从这里开始进行循环
	while (num)
	{
		char command[100];

		char buffer[MAX_BUFFER_SIZE] = buffername;

		sprintf(command, "rdmem r 0x0e64e900 %s -o %s", argv, buffername);
		int result = system(command);
		if (result == -1)
		{
			printf("command fail!\n");
			return 1;
		}
		else
		{
			printf("command success!\n");
		}

		printf("Enter the %s to send now!\n", buffername);

		size_t bytesRead; // 这边定义了一个分包的参数

		int mysend = sendto(client_socket, buffer, MAX_BUFFER_SIZE, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
		if (mysend < 0)
		{
			printf("sendto() failed with error code:\n");
			close(client_socket);
			return 1;
		}

		file = fopen(buffername, "rb");
		if (file == NULL)
		{
			printf("Failed to open file: %s\n", buffername);
			// continue循环需要加;
		}

		if (fseek(file, 0, SEEK_END) != 0)
		{
			printf("You can't find file's tail\n");
			fclose(file);
		}

		long fileLength = ftell(file);

		if (fileLength == -1L)
		{
			printf("You can't get file's length\n");
		}
		else
		{
			printf("the send file size is %d\n", fileLength);
			int mysend2 = sendto(client_socket, (char *)&fileLength, sizeof(fileLength), 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
			if (mysend2 < 0)
			{
				printf("sendto() failed with error size: %d\n");
				close(client_socket);
				return 1;
			}
		}

		fclose(file);
		file = fopen(buffername, "rb");

		if (file == NULL)
		{
			printf("Failed to open file: %s\n", buffername);
		}

		int seq = 0;
		while (1)
		{
			int bytesRead = fread(buffer, 1, MAX_BUFFER_SIZE, file);
			if (bytesRead <= 0)
				break;

			if (sendto(client_socket, buffer, bytesRead, 0, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1)
			{
				printf("sendto failed!\n");
				close(client_socket);
				return 1;
			}
			printf("Sent packet %d,size:%d\n", seq, bytesRead);
			seq++;
		}

		printf("File sent successfully!\n");

		fclose(file);

		num--;
		sleep(50);
	}
	close(client_socket);
	return 0;
}

int BSend4(char argv[], int num, int stime)
{
	int client_socket;
	struct sockaddr_in server_addr;
	// char buffer[MAX_BUFFER_SIZE] ;

	FILE *file;
	client_socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (client_socket < 0)
	{
		printf("Could not create socket with error code: %d\n");
		return 1;
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(SERVER_IP);
	server_addr.sin_port = htons(PORT);

	// 从这里开始进行循环
	while (num)
	{
		char command[100];
		char buffer[MAX_BUFFER_SIZE] = buffername;
		sprintf(command, "rdmem r 0x0e64e900 %s -o %s", argv, buffername);
		int result = system(command);
		if (result == -1)
		{
			printf("command fail!\n");
			return 1;
		}
		else
		{
			printf("command success!\n");
		}

		printf("Enter the %s to send now!\n", buffername);

		size_t bytesRead; // 这边定义了一个分包的参数

		int mysend = sendto(client_socket, buffer, MAX_BUFFER_SIZE, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
		if (mysend < 0)
		{
			printf("sendto() failed with error code:\n");
			close(client_socket);
			return 1;
		}

		file = fopen(buffername, "rb");
		if (file == NULL)
		{
			printf("Failed to open file: %s\n", buffername);
			// continue循环需要加;
		}

		if (fseek(file, 0, SEEK_END) != 0)
		{
			printf("You can't find file's tail\n");
			fclose(file);
		}

		long fileLength = ftell(file);

		if (fileLength == -1L)
		{
			printf("You can't get file's length\n");
		}
		else
		{
			printf("the send file size is %d\n", fileLength);
			int mysend2 = sendto(client_socket, (char *)&fileLength, sizeof(fileLength), 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
			if (mysend2 < 0)
			{
				printf("sendto() failed with error size: %d\n");
				close(client_socket);
				return 1;
			}
		}

		fclose(file);
		file = fopen(buffername, "rb");

		if (file == NULL)
		{
			printf("Failed to open file: %s\n", buffername);
		}

		int seq = 0;
		while (1)
		{
			int bytesRead = fread(buffer, 1, MAX_BUFFER_SIZE, file);
			if (bytesRead <= 0)
				break;

			if (sendto(client_socket, buffer, bytesRead, 0, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1)
			{
				printf("sendto failed!\n");
				close(client_socket);
				return 1;
			}
			printf("Sent packet %d,size:%d\n", seq, bytesRead);
			seq++;
		}

		printf("File sent successfully!\n");

		fclose(file);

		num--;

		sleep(stime);
	}
	close(client_socket);
	return 0;
}
int BSend5(char argv[], int num, int stime, char myip[])
{
	int client_socket;
	struct sockaddr_in server_addr;
	// char buffer[MAX_BUFFER_SIZE] ;

	FILE *file;
	client_socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (client_socket < 0)
	{
		printf("Could not create socket with error code: %d\n");
		return 1;
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(myip);
	server_addr.sin_port = htons(PORT);

	// 从这里开始进行循环
	while (num)
	{
		char command[100];
		char buffer[MAX_BUFFER_SIZE] = buffername;

		sprintf(command, "rdmem r 0x0e64e900 %s -o %s", argv, buffername);
		int result = system(command);
		if (result == -1)
		{
			printf("command fail!\n");
			return 1;
		}
		else
		{
			printf("command success!\n");
		}

		printf("Enter the %s to send now!\n", buffername);

		size_t bytesRead; // 这边定义了一个分包的参数

		int mysend = sendto(client_socket, buffer, MAX_BUFFER_SIZE, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
		if (mysend < 0)
		{
			printf("sendto() failed with error code:\n");
			close(client_socket);
			return 1;
		}

		file = fopen(buffername, "rb");
		if (file == NULL)
		{
			printf("Failed to open file: %s\n", buffername);
			// continue循环需要加;
		}

		if (fseek(file, 0, SEEK_END) != 0)
		{
			printf("You can't find file's tail\n");
			fclose(file);
		}

		long fileLength = ftell(file);

		if (fileLength == -1L)
		{
			printf("You can't get file's length\n");
		}
		else
		{
			printf("the send file size is %d\n", fileLength);
			int mysend2 = sendto(client_socket, (char *)&fileLength, sizeof(fileLength), 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
			if (mysend2 < 0)
			{
				printf("sendto() failed with error size: %d\n");
				close(client_socket);
				return 1;
			}
		}

		fclose(file);
		file = fopen(buffername, "rb");

		if (file == NULL)
		{
			printf("Failed to open file: %s\n", buffername);
		}

		int seq = 0;

		while (1)
		{
			int bytesRead = fread(buffer, 1, MAX_BUFFER_SIZE, file);
			if (bytesRead <= 0)
				break;

			if (sendto(client_socket, buffer, bytesRead, 0, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1)
			{
				printf("sendto failed!\n");
				close(client_socket);
				return 1;
			}
			printf("Sent packet %d,size:%d\n", seq, bytesRead);
			seq++;
		}

		printf("File sent successfully!\n");

		fclose(file);
		num--;
		sleep(stime);
	}
	close(client_socket);
	return 0;
}
int BSend6(char argv[], int num, int stime, char myip[], int myport)
{
	int client_socket;
	struct sockaddr_in server_addr;
	// char buffer[MAX_BUFFER_SIZE] ;

	FILE *file;
	client_socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (client_socket < 0)
	{
		printf("Could not create socket with error code: %d\n");
		return 1;
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(myip);
	server_addr.sin_port = htons(myport);

	// 从这里开始进行循环
	while (num)
	{
		char command[100];
		char buffer[MAX_BUFFER_SIZE] = buffername;
		sprintf(command, "rdmem r 0x0e64e900 %s -o %s", argv, buffername);
		int result = system(command);
		if (result == -1)
		{
			printf("command fail!\n");
			return 1;
		}
		else
		{
			printf("command success!\n");
		}

		printf("Enter the %s to send now!\n", buffername);

		int mysend = sendto(client_socket, buffer, MAX_BUFFER_SIZE, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
		if (mysend < 0)
		{
			printf("sendto() failed with error code:\n");
			close(client_socket);
			return 1;
		}

		file = fopen(buffername, "rb");
		if (file == NULL)
		{
			printf("Failed to open file: %s\n", buffername);
			continue;
		}

		if (fseek(file, 0, SEEK_END) != 0)
		{
			printf("You can't find file's tail\n");
			fclose(file);
		}

		long fileLength = ftell(file);

		if (fileLength == -1L)
		{
			printf("You can't get file's length\n");
		}
		else
		{
			printf("the send file size is %d\n", fileLength);
			int mysend2 = sendto(client_socket, (char *)&fileLength, sizeof(fileLength), 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
			if (mysend2 < 0)
			{
				printf("sendto() failed with error size: %d\n");
				close(client_socket);
				return 1;
			}
		}

		fclose(file);
		file = fopen(buffername, "rb");

		if (file == NULL)
		{
			printf("Failed to open file: %s\n", buffername);
		}

		// 上面是发送文件名和文件大小的
		memset(buffer, 0, sizeof(buffer));

		PacketHeader *header = (PacketHeader *)buffer;
		// PacketHeader *header = NULL;

		int bytesRead;
		int blockNumber = 0;

		while ((bytesRead = fread(buffer + sizeof(PacketHeader), 1, MAX_BUFFER_SIZE - sizeof(PacketHeader), file)) > 0)
		{

			header->length = bytesRead;
			header->blockNumber = blockNumber;

			size_t bytesSent = sendto(client_socket, buffer, sizeof(PacketHeader) + bytesRead, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));

			if (bytesSent < 0)
			{
				printf("sendto failed!\n");
				close(client_socket);
				return 1;
			}
			else
			{
				printf("Sent packet %zu,size:%zd\n", blockNumber, bytesSent);
			}

			blockNumber++;
		}

		header->length = 0;
		header->blockNumber = blockNumber;
		// memcpy(buffer,header,sizeof(PacketHeader));
		sendto(client_socket, buffer, sizeof(PacketHeader) + 0, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));

		printf("File sent successfully!\n");

		fclose(file);
		num--;
		sleep(stime);
	}
	close(client_socket);
	return 0;
}

int BSend7(char argv[], int num, int stime, char myip[], int myport, char myaddr[])
{
	int client_socket;
	struct sockaddr_in server_addr;
	// char buffer[MAX_BUFFER_SIZE] ;

	FILE *file;
	client_socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (client_socket < 0)
	{
		printf("Could not create socket with error code: %d\n");
		return 1;
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(myip);
	server_addr.sin_port = htons(myport);

	// 从这里开始进行循环
	while (num)
	{
		char command[100];
		char buffer[MAX_BUFFER_SIZE] = buffername;
		sprintf(command, "rdmem r %s %s -o %s", myaddr, argv, buffername);
		int result = system(command);
		if (result == -1)
		{
			printf("command fail!\n");
			return 1;
		}
		else
		{
			printf("command success!\n");
		}

		printf("Enter the %s to send now!\n", buffername);

		size_t bytesRead; // 这边定义了一个分包的参数

		int mysend = sendto(client_socket, buffer, MAX_BUFFER_SIZE, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
		if (mysend < 0)
		{
			printf("sendto() failed with error code:\n");
			close(client_socket);
			return 1;
		}

		file = fopen(buffername, "rb");
		if (file == NULL)
		{
			printf("Failed to open file: %s\n", buffername);
			// continue循环需要加;
		}

		if (fseek(file, 0, SEEK_END) != 0)
		{
			printf("You can't find file's tail\n");
			fclose(file);
		}

		long fileLength = ftell(file);

		if (fileLength == -1L)
		{
			printf("You can't get file's length\n");
		}
		else
		{
			printf("the send file size is %d\n", fileLength);
			int mysend2 = sendto(client_socket, (char *)&fileLength, sizeof(fileLength), 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
			if (mysend2 < 0)
			{
				printf("sendto() failed with error size: %d\n");
				close(client_socket);
				return 1;
			}
		}

		fclose(file);
		file = fopen(buffername, "rb");

		if (file == NULL)
		{
			printf("Failed to open file: %s\n", buffername);
		}

		int seq = 0;
		while (1)
		{
			int bytesRead = fread(buffer, 1, MAX_BUFFER_SIZE, file);
			if (bytesRead <= 0)
				break;

			if (sendto(client_socket, buffer, bytesRead, 0, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1)
			{
				printf("sendto failed!\n");
				close(client_socket);
				return 1;
			}
			printf("Sent packet %d,size:%d\n", seq, bytesRead);
			seq++;
		}

		printf("File sent successfully!\n");

		fclose(file);
		num--;
		sleep(stime);
	}
	close(client_socket);
	return 0;
}

int SendMessge(int recover, char *mip, int mport)
{
	int client_socket;
	struct sockaddr_in server_addr;
	client_socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (client_socket < 0)
	{
		printf("Could not create socket with error code: %d\n");
		return 1;
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(mip); // 这个地方以后要改
	server_addr.sin_port = htons(mport);

	if (recover == 0)
	{
		// char buffer3[1] = {"1"};
		// char buffer3[2] = {"1"};
		const char *buffer3 = "1";

		int rembe = 0;
		while (1)
		{
			if (rembe == 3)
				break;

			int mysend = sendto(client_socket, buffer3, 1, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
			if (mysend < 0)
			{
				printf("sendto() failed with error code:\n");
				close(client_socket);
				return 1;
			}
			rembe++;
			sleep(5);
			printf("send command successful!\n");
		}
	}

	else
	{
		// char buffer3[1] = {"1"};
		// char buffer3[2] = {"1"};
		const char *buffer3 = "1";

		int remb = 0;
		while (1)
		{
			if (remb == 3)
				break;
			int mysend = sendto(client_socket, buffer3, 1, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
			if (mysend < 0)
			{
				printf("sendto() failed with error code:\n");
				close(client_socket);
				return 1;
			}
			remb++;
			sleep(5);
			printf("send command successful!\n");
		}
	}

	return 0;
}

// 下面是TLV的解包函数
TLV unpack_tlv(const char *data)
{

	TLV tlv;
	memcpy(&tlv.type, data, sizeof(int));
	memcpy(&tlv.length, data + sizeof(int), sizeof(int));
	tlv.value = (char *)malloc(tlv.length * sizeof(char));

	if (tlv.value == NULL)
	{
		perror("Memory allocation failed");
		exit(1);
	}

	memcpy(tlv.value, data + sizeof(int) + sizeof(int), tlv.length);

	return tlv;
}

void *rece_switch(void *arg)
{
	int server_socket = *((int *)arg);
	struct sockaddr_in client_addr;
	socklen_t client_addr_len = sizeof(client_addr); // 这个是套接字大小
	char buffer[MAX_BUFFER_SIZE2];

	TLV rece_tlv;
	rece_tlv.value = (char *)malloc(sizeof(char) * MAX_BUFFER_SIZE2); // 直接开一个大的 char 数组
	if (rece_tlv.value == NULL)
	{
		perror("Memory allocation failed");
		exit(1);
	}

	// FILE *filePointer = fopen("result.txt", "a"); // rece_tlv.type == 3 的情况，打开文件得在里面打开，不然会有问题的哦
	// if (filePointer == NULL)
	// printf("Can not open the file!\n");
	while (1)
	{
		printf("\ncommand is recving now!\n");
		memset(buffer, 0, MAX_BUFFER_SIZE2);
		int recv_len = recvfrom(server_socket, buffer, MAX_BUFFER_SIZE2, 0, (struct sockaddr *)&client_addr, &client_addr_len);

		if (recv_len < 0)
		{
			printf("buffer receive fail!\n");
			continue;
		}

		printf("recv success!\n");

		// TLV rece_tlv = unpack_tlv(buffer);
		// TLV rece_tlv;
		memcpy(&rece_tlv.type, buffer, sizeof(int));
		memcpy(&rece_tlv.length, buffer + sizeof(int), sizeof(int));
		// if (rece_tlv.value == NULL)
		// {
		// 	// rece_tlv.value = (char *)malloc(sizeof(char) * rece_tlv.length);
		// 	rece_tlv.value = (char *)malloc(sizeof(char) * MAX_BUFFER_SIZE2);
		// }
		// if (rece_tlv.value == NULL)
		// {
		// 	perror("Memory allocation failed");
		// 	exit(1);
		// }
		memcpy(rece_tlv.value, buffer + sizeof(int) + sizeof(int), rece_tlv.length);

		printf("Received TLV Packet:\n");
		printf("Tag: %d\n", rece_tlv.type);
		printf("Length: %d\n", rece_tlv.length);
		printf("value:");
		int i;
		for (i = 0; i < rece_tlv.length; i++)
			printf("%c", rece_tlv.value[i]);
		printf("\n");

		if (rece_tlv.type == 1)
		{
			int rember = 0;		 // 记录几个命令
			char *mysize = NULL; // 文件大小
			int num = 0;		 // 循环次数
			int stime = 0;		 // 睡眠时间
			char *myip = NULL;	 // ip地址
			int myport = 0;		 // 地址端口
			char *bsize = NULL;	 // 板子内存地址
			char *token;

			//--------------------这部分是分割字符串
			token = strtok(rece_tlv.value, " ");

			while (token != NULL)
			{
				if (rember == 0)
					mysize = token;

				else if (rember == 1)
					num = atoi(token);

				else if (rember == 2)
					stime = atoi(token);

				else if (rember == 3)
					myip = token;

				else if (rember == 4)
					myport = atoi(token);

				else if (rember == 5)
					bsize = token;

				token = strtok(NULL, " ");

				rember++;
			}
			//--------------------
			printf("The number is %d\n", rember);

			if (rember == 1) // 第一个是传一个大小
			{
				BSend2(mysize);
			}

			else if (rember == 2) // 第二个是大小和循环次数
			{
				BSend3(mysize, num);
			}

			else if (rember == 3) // 第三个是大小和循环次数和睡眠时间
			{
				BSend4(mysize, num, stime);
			}

			else if (rember == 4) // 第三个是大小和循环次数和睡眠时间和IP地址
			{
				BSend5(mysize, num, stime, myip);
			}

			else if (rember == 5) // 第三个是大小和循环次数和睡眠时间和IP地址和端口
			{
				BSend6(mysize, num, stime, myip, myport);
			}

			else if (rember == 6)
			{
				BSend7(mysize, num, stime, myip, myport, bsize);
			}

			free(rece_tlv.value);
		}
		else if (rece_tlv.type == 2)
		{
			int rem = 0;
			char *myip2 = NULL;
			int myport2 = 0;
			char *token2;
			token2 = strtok(rece_tlv.value, " ");
			while (token2 != NULL)
			{
				if (rem == 0)
					myip2 = token2;

				if (rem == 1)
					myport2 = atoi(token2);

				rem++;

				token2 = strtok(NULL, " ");
			}

			printf("NOW is running rftx.sh!\n");
			const char *scriptCommand = "rftx.sh";
			int scriptExitCode = system(scriptCommand);

			if (scriptExitCode == 0)
			{
				printf("Script execution successful!\n");
				SendMessge(0, myip2, myport2);
			}
			else
			{
				printf("Script execution failed!\n");
				SendMessge(1, myip2, myport2);
			}
		}
		else if (rece_tlv.type == 3)
		{
			printf("NOW writing answer in file!    ");
			FILE *filePointer = fopen("result.txt", "a"); // a: append 追加
			if (filePointer == NULL)
			{
				printf("Can not open the file!\n");
				continue;
			}
			for (int i = 0; i < rece_tlv.length; i++)
				fprintf(filePointer, "%c", rece_tlv.value[i]);

			fclose(filePointer);

			printf("result.txt is finish!\n");
		}
	}
	free(rece_tlv.value);
	rece_tlv.value = NULL;
	// fclose(filePointer);
	close(server_socket);
}

int main()
{
	int server_socket;
	struct sockaddr_in server_addr;
	server_socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (server_socket < 0)
	{
		printf("Could not create socket with error code\n");
		return 0;
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(PORT2);
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(server_socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
	{
		printf("Bind failed\n");
		close(server_socket);
		return 0;
	}

	pthread_t tid;

	pthread_create(&tid, NULL, rece_switch, (void *)&server_socket);

	pthread_join(tid, NULL);

	//    char num[20];
	//    gets(num);
	//    BSend6(num,5,3,"192.168.1.241",12345);

	return 0;
}
