### `windows` 下 `libevent` 网络库环境搭建
看网上的教程一般是会有要安装 `vs`、`openssl` 的前提的。我们这里 `vs` 的安装就不演示了。

#### 1. `windows` 下 `openssl` 的安装
##### 1.1 下载
`openssl` 官网： `Win32/Win64 OpenSSL Installer for Windows - Shining Light Productions`

![Alt text](image.png)

找个 `win64` 的下载就行。

##### 1.2 配置环境变量
把 `openssl` 安装路径 `bin` 的路径（例如 `C:\OpenSSL-Win64\bin`）加入到操作系统的系统环境变量 `Path` 中。

![Alt text](image-1.png)

##### 1.3 看是否安装好了 
**电脑是否重启可以考虑，只不过我没有重启。。。**
以后管理员的身份运行命令行工具查看 `openssl` 的版本，有看到类似下面的就说明安装成功。

![Alt text](image-2.png)

##### 1.4 可以随便配置 `RSA` 密钥对文件
依次使用下面的三行(注意你们自己的路径。进入 `bin` 文件夹下，也可以不进入)：
```
openssl genrsa -out rsa_private_key.pem 1024
openssl pkcs8 -topk8 -inform PEM -in rsa_private_key.pem -outform PEM -nocrypt
openssl rsa -in rsa_private_key.pem -pubout -out rsa_public_key.pem
```
![Alt text](image-8.png)

对应的就会在 `bin` 路径下生成：

![Alt text](image-7.png)

#### 2. `windows` 下 `libevent` 网络库的安装
##### 2.1 下载源码
`libevent` 官网：https://libevent.org/

![](image-3.png)

再进入解压的目录。

##### 2.2 修改源码
1. 下边三个文件加入宏定义 `#define _WIN32_WINNT 0x0500`
```
event_iocp.c 
 evthread_win32.c 
 listener.c 
```

2. 在 `minheap-internal.h` 文件第一行加入
```
#pragma comment(lib, "Iphlpapi.lib")
```
然后将文件中的两个 `UINT32_MAX` 的宏改成 `UINT_MAX`

3. 在 `Makefile.nmake` 文件找到 `CFLAGS=$(CFLAGS) /Ox /W3 /wd4996 /nologo` （第26行），加上 `/Zi` 方便调试。变成下面的样子：
```
CFLAGS=$(CFLAGS) /Ox /W3 /wd4996 /nologo /Zi
```

4. 在 `test` 目录的 `Makefile.nmake` 文件，第六行改为下面的样子：
```
SSL_LIBS=..\libevent_openssl.lib $(OPENSSL_DIR)\lib\libssl.lib $(OPENSSL_DIR)\lib\libcrypto.lib gdi32.lib User32.lib Crypt32.lib
```
注意：`openssl` 在 `1.0.x` 之前的版本中,文件为 `libeay32.lib` 和 `ssleay32.lib`，在 `1.1.x` 之后的版本中，名字是 `libssl.lib` 和 `libcrypto.lib`（这里修改了这两个文件，并追加入了 `Crypt32.lib`）

##### 2.3 编译源码
1. 用 `vs` 的命令行工具进入解压的目录。

![Alt text](image-4.png)

打开是在 `vs` 安装盘的路径上的，如果和 `libevent` 下载安装的路径不一样的话：
```
d: // 回车，就会到 d 盘
c: // 回车，就会到 c 盘
```

```
cd C:\Users\Administrator\Desktop\ftpProject\libevent-2.1.12-stable
```
2. 开始编译，`OPENSSL_DIR` 是 `OPENSSL` 安装的位置（理论上不加也可以）
```
nmake /f Makefile.nmake OPENSSL_DIR=D:\software\OpenSSL-Win64\OpenSSL-Win64
```
我的 `openssl` 目录：

![Alt text](image-5.png)

这样就会生成三个静态库:
```
libevent_core.lib
libevent_extras.lib
libevent.lib
```

3. 测试是否成功 
```
test\regress.exe
```
![Alt text](image-6.png)


#### 3. `windows` `vs` 环境，`libevent` 网络库的使用
##### 3.1 构建项目
新建一个 `vs` 空项目
在项目目录下建一个 `libevent` 文件夹
在 `libevent` 中新建一个 `lib` 文件夹，将上面三个 `lib` 文件 `copy` 到该目录下。
在 `libevent` 中再新建一个 `include` 文件夹。
将 `libevent-2.0.22-stable\include` 下的文件和文件夹 `copy` 到该目录下。
将 `libevent-2.0.22-stable\WIN32-Code` 下的文件和文件夹 `copy` 到该目录下，
整合两个 `event2` 目录下的资源，放同一个文件夹下

##### 3.2 配置项目
- `VC++`目录：
包含目录，添加刚刚新建的 `include` 目录库目录，添加刚刚的 `lib` 目录;

- `C/C++`：
代码生成–>运行库：
`Debug` 模式下选：多线程调试 (`/MTd`)，
`Release` 下模式下选：多线程 (`/MT`)

- 连接器：
输入->附加依赖项：
`ws2_32.lib`，
`wsock32.lib`，
`libevent.lib`，
`libevent_core.lib`，
`libevent_extras.lib`，
另外两个库 `ws2_32.lib` 和 `wsock32.lib` 是用来编译 `Windows` 网络相关的程序库。

##### 3.3 运行项目
新建一个 `test.cpp` 文件
从 `libevent-2.0.22-stable\sample` 例子目录下拷贝 `time-test.c` 文件中的代码到 `test.cpp` 中也去测试一下看有没有问题。


