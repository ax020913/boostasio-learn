### `centos` 升级 `gcc-g++` 版本
低版本可能不支持 `-std=c++14`、`-std=c++17`、`-std=c++20` 编译，所以需要升级 `gcc-g++` 版本。

#### 1. 查看 `gcc` 版本
`gcc -v` 和 `gcc --version` 都是可以的。一般最开始都是 `4.8` 的版本吧，最高的版本已经有 `13` 了，但是我们的软件源可能没有那么高的版本。(我这里有 `11` 的版本，反正就是试一下看有没有高的版本):
```
[x@localhost ~]$ yum install devtoolset-13-gcc*
Loaded plugins: fastestmirror, langpacks
You need to be root to perform this command.
[x@localhost ~]$ 
[x@localhost ~]$ yum install devtoolset-12-gcc*
Loaded plugins: fastestmirror, langpacks
You need to be root to perform this command.
[x@localhost ~]$ 
```

上面试了，是没有 `13` 和 `12` 版本的，我下面的是已经安装好了的 `11` 版本：
```
[x@localhost day15-threadsafe_hash]$ gcc --version
gcc (GCC) 11.2.1 20220127 (Red Hat 11.2.1-9)
Copyright (C) 2021 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

[x@localhost day15-threadsafe_hash]$ 
```


#### 2. 升级 `gcc` 版本

##### 2.1 切换用户
用 `root` 用户来升级也是可以的，普通用户来升级也是可以的。

`root` 和普通用户之间的切换(`su - username`):
普通用户到 `root` 要 `password`，反之不要。
```
[x@localhost ~]$ su - root
Password: 
Last login: Thu Dec 14 11:32:14 PST 2023 on pts/4
[root@localhost ~]# su - x
Last login: Thu Dec 14 11:32:17 PST 2023 on pts/4
[x@localhost ~]$ 
```

##### 2.2 假设为 `root` 用户
将 `gcc` 升级到 `11` 版本，使用下面的命令：
```
yum -y install centos-release-scl
yum install devtoolset-11-gcc*
scl enable devtoolset-11 bash
```
再查看 `gcc` 版本：
```
[x@localhost ~]$ gcc --version
gcc (GCC) 11.2.1 20220127 (Red Hat 11.2.1-9)
Copyright (C) 2021 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

#### 3. 存在问题（`gcc` 的升级只在当前的 `terminal` 生效）
此时重新开一个 `terminal`，输入：`gcc --version`。

显示的依旧是 `4` 的版本。

需要注意的是 `scl` 命令启用只是临时的，退出 `shell` 或重启就会恢复原系统 `gcc` 版本。

如果要长期使用 `gcc 11` 的话：

`echo "source /opt/rh/devtoolset-11/enable" >> /etc/profile`

这样退出 `shell` 重新打开就是新版的 `gcc` 了。

```
vi /etc/profile
source /opt/rh/devtoolset-11/enable
 
# 或者
echo "source /opt/rh/devtoolset-11/enable" >> /etc/profile
source /etc/profile
```

然而！

退出 `shell` 重新打开一个终端，输入：`gcc --version` 显示的依旧是 `4` 的版本，如果 `source /etc/profile`，就又显示是 `11` 的版本。

重启电脑，输入：`gcc --version`，依旧是旧版本 `4`。

`source` 一下之后，该终端里就变成 `11`，重开一个终端依旧是 `4` 的版本。



### 解决方法

开两个终端 `terminal。`
 
第一个(显然这个终端是 `4` 的版本)，输入：
```
gcc --version
which gcc

# 位置如下
/usr/local/gcc/bin/gcc
```

另一个先 `source` 一下（`source `后就是 `11` 的版本）：
```
source /etc/profile
gcc -v
which gcc
 
# 位置如下
/opt/rh/devtoolset-11/root/usr/bin/gcc
```

因为两个 `gcc` 所在的位置不同。
 
因此，将旧版本所在位置的 `gcc` 删掉，将新版本所在位置的 `gcc` 复制过去。
```
# 进入文件夹
cd /usr/local/gcc/bin/gcc  
# 删掉 gcc，回答 y
rm gcc  
# rm 新 gcc 旧 gcc，两个位置就是前面 which gcc 显示的那两个位置
mv /opt/rh/devtoolset-11/root/usr/bin/gcc /usr/local/gcc/bin/gcc  
```
 

```
# 查看 gcc 版本
[x@localhost ~]$ gcc -v  
[x@localhost ~]$ gcc -v
Using built-in specs.
COLLECT_GCC=gcc
Target: x86_64-redhat-linux
Configured with: ../configure --enable-bootstrap --enable-languages=c,c++,fortran,lto --prefix=/opt/rh/devtoolset-11/root/usr --mandir=/opt/rh/devtoolset-11/root/usr/share/man --infodir=/opt/rh/devtoolset-11/root/usr/share/info --with-bugurl=http://bugzilla.redhat.com/bugzilla --enable-shared --enable-threads=posix --enable-checking=release --enable-multilib --with-system-zlib --enable-__cxa_atexit --disable-libunwind-exceptions --enable-gnu-unique-object --enable-linker-build-id --with-gcc-major-version-only --with-linker-hash-style=gnu --with-default-libstdcxx-abi=gcc4-compatible --enable-plugin --enable-initfini-array --with-isl=/builddir/build/BUILD/gcc-11.2.1-20220127/obj-x86_64-redhat-linux/isl-install --enable-gnu-indirect-function --with-tune=generic --with-arch_32=x86-64 --build=x86_64-redhat-linux
Thread model: posix
Supported LTO compression algorithms: zlib
gcc version 11.2.1 20220127 (Red Hat 11.2.1-9) (GCC) 
```
此时 `gcc` 更新成功，打开新的终端，输入 `gcc --version`，也是 `11` 的版本

