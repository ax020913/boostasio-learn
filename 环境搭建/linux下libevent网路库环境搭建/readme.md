### `linux` 下的 `libevent` 网络库环境搭建

#### 1. 从官网下载 `libevent` 的 `.zip` 源码包，解压，进入解压好了的目录。

#### 2. 直接用下面的三步就可以了：

```
sudo ./configure
sudo make
sudo make install
```

#### 3. 查看是否安装好了：

![Alt text](image.png)

直接使用去把 `C:\Users\Administrator\Desktop\ftpProject\libevent-2.1.12-stable\sample` 目录下的 `time-test.c` 示例代码拿过来测试，
```
g++ -o ma test.cc -std=c++11 -levent -I/usr/local/include -L/usr/local/lib 
```
编译生成可执行程序 `ma`，运行 `./ma`。

![Alt text](image-1.png)
