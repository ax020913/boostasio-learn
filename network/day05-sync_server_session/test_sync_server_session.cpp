#include <iostream>
#include <string>

// #include "SESSION.h"
#include "SESSION_no_err.h"

using namespace std;

int main(void)
{
    try
    {
        boost::asio::io_service io_context;
        Server server(io_context, 3333);
        io_context.run();
    }
    catch (std::exception &error_code)
    {
        std::cerr << "Exception: " << error_code.what() << std::endl;
    }

    boost::asio::io_service io_context;

    return 0;
}
