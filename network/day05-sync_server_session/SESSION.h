#pragma once

#include <iostream>
#include <string>
#include <map>
#include <mutex>
#include <memory>

#include <boost/asio.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>

using namespace std;
using namespace boost::asio;

#define MAX_LENGTH 1024

// namespace SESSION
// {
// class Server;
// class MsgNode
// {
//     friend class Session;

// public:
//     MsgNode(char *msg, int max_length)
//     {
//     }
//     ~MsgNode()
//     {
//         delete[] _data;
//     }

// private:
//     char *_msg;
//     int _cur_len;
//     int _max_len;
// };

class Server;
class Session : public std::enable_shared_from_this<Session>
{
public:
    Session(boost::asio::io_service &ios, Server *server)
        : _sock(ios), _server(server)
    {
        // 自己要写的话，可以用雪花算法实现
        boost::uuids::uuid a_uuid = boost::uuids::random_generator()();
        _uuid = boost::uuids::to_string(a_uuid);
    }
    boost::asio::ip::tcp::socket &GetSock()
    {
        return _sock;
    }
    std::string &GetUuid()
    {
        return _uuid;
    }

    void Start();

private:
    void handle_read(const boost::system::error_code &error_code, size_t bytes_transferred, shared_ptr<Session> _self_shared);
    void handle_write(const boost::system::error_code &error_code, shared_ptr<Session> _self_shared);
    enum
    {
        max_length = MAX_LENGTH
    };
    boost::asio::ip::tcp::socket _sock;
    char _data[max_length];
    std::string _uuid;
    Server *_server;
};

class Server
{
public:
    Server(boost::asio::io_service &io_context, short port);
    void ClearSession(std::string &uuid);

private:
    void HandleAccept(shared_ptr<Session> new_session, const boost::system::error_code &error_code);

    void StartAccept();

    boost::asio::io_service &_io_context;
    boost::asio::ip::tcp::acceptor _acceptor;
    short _port;
    // 主线程控制全部的 Session 资源
    std::map<std::string, shared_ptr<Session>> _sessions;
};
// }

//----------------- session ---------------------
void Session::Start()
{
    memset(_data, 0, max_length);
    _sock.async_read_some(
        boost::asio::buffer(_data, max_length),
        std::bind(&Session::handle_read,
                  this, std::placeholders::_1,
                  std::placeholders::_2,
                  shared_from_this()));
}

void Session::handle_read(const boost::system::error_code &error_code,
                          size_t bytes_transferred,
                          shared_ptr<Session> _self_shared)
{
    if (!error_code)
    {
        cout << "server receive data is " << _data << std::endl;

        // 对于之前的隐患是没有了哦，可以多次发送也是可以多次接收的，也可以同时发送和监听事件都绑定
        _sock.async_send(
            boost::asio::buffer(_data, max_length),
            std::bind(&Session::handle_write, this, std::placeholders::_1, _self_shared));

        _sock.async_send(
            boost::asio::buffer(_data, max_length),
            std::bind(&Session::handle_write, this, std::placeholders::_1, _self_shared));
    }
    else
    {
        std::cout << "handle write failed, error is " << error_code.message() << std::endl;
        _server->ClearSession(_self_shared->GetUuid());
    }
}
void Session::handle_write(const boost::system::error_code &error_code,
                           shared_ptr<Session> _self_shared)
{
    if (!error_code)
    {
        memset(_data, 0, max_length);
        _sock.async_read_some(
            boost::asio::buffer(_data, max_length),
            std::bind(&Session::handle_read, this, std::placeholders::_1, std::placeholders::_2, _self_shared));
    }
    else
    {
        std::cout << "handle read failed, error is " << error_code.message() << std::endl;
        _server->ClearSession(_self_shared->GetUuid());
    }
}

// ------------------------- Server ----------------------------
Server::Server(boost::asio::io_service &io_context, short port)
    : _io_context(io_context), _port(port), _acceptor(io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port))
{
    StartAccept();
}

void Server::ClearSession(std::string &uuid)
{
    _sessions.erase(uuid);
}

void Server::StartAccept()
{
    shared_ptr<Session> new_session = make_shared<Session>(_io_context, this);
    _acceptor.async_accept(new_session->GetSock(),
                           std::bind(&Server::HandleAccept, this, new_session, std::placeholders::_1));
}

void Server::HandleAccept(shared_ptr<Session> new_session, const boost::system::error_code &error_code)
{
    if (!error_code)
    {
        new_session->Start();
        _sessions.insert(make_pair(new_session->GetUuid(), new_session));
    }
    else
    {
        std::cout << "session accept failed, error is " << error_code.message() << std::endl;
    }
    StartAccept();
}
