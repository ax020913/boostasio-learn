
// 模拟二次析构造成崩溃的问题：
/*
#include <iostream>
#include <memory>
#include <map>

class Data
{
public:
    Data(int data = 0) : _data(data) {}

    void test();
    void print();

private:
    int _data;
};

class Server
{
public:
    Server()
    {
        start();
    }

    void start()
    {
        // Data t(100);
        std::shared_ptr<Data> t = std::make_shared<Data>(100);
        t->test();
    }
};

void Data::test()
{
    std::cout << "test" << std::endl;
    print();

    // 可能一些事会造成崩溃，所以下面模拟执行销毁...
    delete this;
}
void Data::print()
{
    std::cout << "print" << std::endl;

    // 可能一些事会造成崩溃，所以下面模拟执行销毁...
    delete this;
}

int main(void)
{
    Server server;

    return 0;
}
*/

// shared_ptr 伪闭包保活解决上面的问题
#include <iostream>
#include <memory>
#include <map>

class Server;
class Data : public std::enable_shared_from_this<Data>
{
public:
    Data(int data = 0, Server *server = nullptr) : _data(data), _server(server) {}

    void test(std::shared_ptr<Data> t);
    void print(std::shared_ptr<Data> t);

private:
    int _data;
    Server *_server;
};

class Server
{
public:
    Server()
    {
        start();
    }

    void start()
    {
        // Data t(100);
        std::shared_ptr<Data> t = std::make_shared<Data>(100, this);
        std::cout << "Data(){}: " << t.use_count() << std::endl; // 1
        _Datas.insert(make_pair("t", t));
        std::cout << "insret: " << t.use_count() << std::endl; // 2

        t->test(t);
        std::cout << "Data(){}: " << t.use_count() << std::endl; // 1
    }
    void clear()
    {
        _Datas.erase("t");
    }

private:
    std::map<std::string, std::shared_ptr<Data>> _Datas;
};

void Data::test(std::shared_ptr<Data> t)
{
    std::cout << "start test: " << t.use_count() << std::endl; // 3

    // print(this);
    print(t);

    // 可能一些事会造成崩溃，所以下面模拟执行销毁...
    // std::cout << "delete test: " << shared_from_this().use_count() << std::endl; // 3
    _server->clear();
    std::cout << "delete test: " << t.use_count() << std::endl; // 2
}
void Data::print(std::shared_ptr<Data> t)
{
    std::cout << "start print: " << t.use_count() << std::endl; // 4

    // 可能一些事会造成崩溃，所以下面模拟执行销毁...
    // std::cout << "delete print: " << shared_from_this().use_count() << std::endl; // 5
    // std::cout << "delete print: " << t.use_count() << std::endl; // 4

    if (_server == nullptr)
        std::cout << "_server == nullptr" << std::endl;

    _server->clear();
    std::cout << "delete print: " << t.use_count() << std::endl; // 3
}

int main(void)
{
    Server server;

    return 0;
}
