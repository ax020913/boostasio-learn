### 一. 利用伪闭包实现连接保活, 解决了 `day04 server` 的问题

`shared_ptr<Session> _self_shared`
用智能指针的引用计数 + `bind` 形成函数调用对象的值传递

```
void handle_read(const boost::system::error_code &error_code, size_t bytes_transferred, shared_ptr<Session> \_self_shared);
void handle_write(const boost::system::error_code &error_code, shared_ptr<Session> \_self_shared);

void HandleAccept(shared_ptr<Session> new_session, const boost::system::error_code &error_code);
```

注意 `shared_from_this()` 参数，不要写成 `shared_ptr<Session> (this)` 了，相当于是创建了一个新的智能指针，我们要的是 `Server` 里面最初创建这个 `Session` 时候的智能指针。

```
void Session::Start()
{
    memset(_data, 0, max_length);
    _sock.async_read_some(
        boost::asio::buffer(_data, max_length),
        std::bind(&Session::handle_read,
                  this, std::placeholders::_1,
                  std::placeholders::_2,
                  shared_from_this()));
}
```

关于闭包延长 `Session` 的生命周期，我试了用引用也是可以的，所以和绑定的函数的参数形式没关系。但是必须用引用的方式(在函数调用栈生成了新的函数对象)才可以使得引用计数++，就延长了生命周期。
虽然：函数形参使用的是引用,则智能指针引用计数不会增加

```
void handle_read(const boost::system::error_code &error_code, size_t bytes_transferred, shared_ptr<Session> &_self_shared);
void handle_write(const boost::system::error_code &error_code, shared_ptr<Session> &_self_shared);

void HandleAccept(shared_ptr<Session> &new_session, const boost::system::error_code &error_code);
```

### 二. 闭包的用例

```
#include <iostream>
#include <vector>
#include <functional>

// 闭包延长局部变量的声明周期
std::function<const int *(void)> func_out(int aa)
{
    int a = aa;
    std::cout << "out: " << &a << std::endl;
    std::function<const int *(void)> func_in = [=]() -> const int *
    {
        std::cout << "in:  " << &a << std::endl;
        std::cout << a << std::endl;
        return &a;
    };
    return func_in;
}
// error
// std::function<const int *(void)> func_in = [=]() -> const int *
// {
//     std::cout << "in:  " << &a << std::endl;
//     std::cout << a << std::endl;
//     return &a;
// };

int main()
{
    auto func = func_out(10);

    func();

    return 0;
}
```

### 三. 伪闭包的实验......失败(前置声明在 `cpp` 里面是不起作用的，后面分文件编译也是有问题的)
#### 3.1 想把上面 `Session` 会话用伪闭包保活的做法自己再搞一下。好像是因为二次析构的原因，所以创造了下面的场景，再用 `shared_ptr` 试一试。
下面的代码是会有二次析构的危险的：
![Alt text](image.png)
```
#include <iostream>
#include <memory>
#include <map>

class Data
{
public:
    Data(int data = 0) : _data(data) {}

    void test();
    void print();

private:
    int _data;
};

class Server
{
public:
    Server()
    {
        start();
    }

    void start()
    {
        // Data t(100);
        std::shared_ptr<Data> t = std::make_shared<Data>(100);
        t->test();
    }
};

void Data::test()
{
    std::cout << "test" << std::endl;
    print();

    // 可能一些事会造成崩溃，所以下面模拟执行销毁...
    delete this;
}
void Data::print()
{
    std::cout << "print" << std::endl;

    // 可能一些事会造成崩溃，所以下面模拟执行销毁...
    delete this;
}

int main(void)
{
    Server server;

    return 0;
}
```

用 `shared_ptr` 伪闭包解决上面的问题，但是......

报错说找不到 `Server::clear` 函数的定义吧，但是我们有前置声明啊......
```
#include <iostream>
#include <memory>
#include <map>

class Server;
class Data : public std::enable_shared_from_this<Data>
{
public:
    Data(int data = 0, Server *server = nullptr) : _data(data), _server(server) {}

    void test(std::shared_ptr<Data> t);
    void print(std::shared_ptr<Data> t);

private:
    int _data;
    Server *_server;
};

void Data::test(std::shared_ptr<Data> t)
{
    std::cout << "start test" << std::endl;
    // print(this);
    print(t);

    // ...
    std::cout << "delete test" << std::endl;
    _server->clear();
}
void Data::print(std::shared_ptr<Data> t)
{
    std::cout << "start print" << std::endl;

    // ...
    std::cout << "delete print" << std::endl;
    _server->clear();
}

class Server
{
public:
    Server()
    {
        start();
    }

    void start()
    {
        // Data t(100);
        std::shared_ptr<Data> t = std::make_shared<Data>(100, this);
        _Datas.insert(make_pair("t", t));
        t->test(t);
    }
    void clear()
    {
        _Datas.erase("t");
    }

private:
    std::map<std::string, std::shared_ptr<Data>> _Datas;
};

int main(void)
{
    Server server;

    return 0;
}

```

#### 3.2 请教网友
上面的报错，改一下午到晚上，怀疑人生。。。。。

后面改成下面的，总算是可以跑起来了，但是 `shared_ptr` 解决伪闭包的问题还是没有解决。但是编译的问题是解决了，也问了别人，说是： `cpp` 里前置声明无效，前置声明是为了解决互引用问题，头文件前置声明告诉编译器其他头文件里有，但是编译 `cpp` 时会去查找。后来改成分文件编译：

`test.cpp`
```
#include "Data.h"
#include "Server.h"

int main(void)
{
	Server server;

	return 0;
}
```
`server.h`
```
#pragma once

class Server
{
public:
	Server()
	{
		start();
	}

	void start()
	{
		// Data t(100);
		std::shared_ptr<Data> t = std::make_shared<Data>(100);
		_Datas.insert(make_pair("t", t));
		t->test(t);
	}
	void clear()
	{
		_Datas.erase("t");
	}

private:
	std::map<std::string, std::shared_ptr<Data>> _Datas;
};
```
`Data.h`
```
#pragma once

#include <iostream>
#include <memory>
#include <map>

extern class Server;
class Data : public std::enable_shared_from_this<Data>
{
public:
	Data(int data = 0, Server* server = nullptr) : _data(data), _server(server) {}

	void test(std::shared_ptr<Data> t);
	void print(std::shared_ptr<Data> t);

private:
	int _data;
	Server* _server;
};
```
`Data.cpp`
```
#include "Data.h"

#include "Server.h"

void Data::test(std::shared_ptr<Data> t)
{
	std::cout << "start test" << std::endl;
	// print(this);
	print(t);

	// ...
	std::cout << "delete test" << std::endl;
	_server->clear();
}
void Data::print(std::shared_ptr<Data> t)
{
	std::cout << "start print" << std::endl;

	// ...
	std::cout << "delete print" << std::endl;
	_server->clear();
}
```
虽然分文件编译了，但是 `Linux` 环境的 `extern class Server` 关键字 `extern` 是有问题的。所以编译还是不通过，只是换了编译报错。。。。。。

#### 3.3 可能到了谷底才会反弹......，晚上有随便搞了，把 `Data` 的两个成员函数放下面去了，就可以通过编译了，哈哈哈
这两个函数真的搞人，也不知道为什么把这两个函数写 `class Server` 的定义上面去了，嗐......
```
void Data::test(std::shared_ptr<Data> t)
{
    std::cout << "start test" << std::endl;
    // print(this);
    print(t);

    // ...
    std::cout << "delete test" << std::endl;
    _server->clear();
}
void Data::print(std::shared_ptr<Data> t)
{
    std::cout << "start print" << std::endl;

    // ...
    std::cout << "delete print" << std::endl;
    _server->clear();
}
```

就得到了最终想要的效果，看看引用计数也没问题，最上面的二次析构的问题也就得以解决了：
![Alt text](image-2.png)
```
#include <iostream>
#include <memory>
#include <map>

class Server;
class Data : public std::enable_shared_from_this<Data>
{
public:
    Data(int data = 0, Server *server = nullptr) : _data(data), _server(server) {}

    void test(std::shared_ptr<Data> t);
    void print(std::shared_ptr<Data> t);

private:
    int _data;
    Server *_server;
};

class Server
{
public:
    Server()
    {
        start();
    }

    void start()
    {
        // Data t(100);
        std::shared_ptr<Data> t = std::make_shared<Data>(100, this); // 得传 this 不然 _server 成员为空同样崩溃
        std::cout << "Data(){}: " << t.use_count() << std::endl; // 1
        _Datas.insert(make_pair("t", t));
        std::cout << "insret: " << t.use_count() << std::endl; // 2

        t->test(t);
        std::cout << "Data(){}: " << t.use_count() << std::endl; // 1
    }
    void clear()
    {
        _Datas.erase("t");
    }

private:
    std::map<std::string, std::shared_ptr<Data>> _Datas;
};

void Data::test(std::shared_ptr<Data> t)
{
    std::cout << "start test: " << t.use_count() << std::endl; // 3

    // print(this);
    print(t);

    // 可能一些事会造成崩溃，所以下面模拟执行销毁...
    // std::cout << "delete test: " << shared_from_this().use_count() << std::endl; // 3
    _server->clear();
    std::cout << "delete test: " << t.use_count() << std::endl; // 2
}
void Data::print(std::shared_ptr<Data> t)
{
    std::cout << "start print: " << t.use_count() << std::endl; // 4

    // 可能一些事会造成崩溃，所以下面模拟执行销毁...
    // std::cout << "delete print: " << shared_from_this().use_count() << std::endl; // 5
    // std::cout << "delete print: " << t.use_count() << std::endl; // 4

    if (_server == nullptr)
        std::cout << "_server == nullptr" << std::endl;

    _server->clear();
    std::cout << "delete print: " << t.use_count() << std::endl; // 3
}

int main(void)
{
    Server server;

    return 0;
}
```

**成员函数参数换成引用计数也是不会影响最终的结果的，有个局部作用域的感觉：**
我成员函数参数用的是引用，我追踪打印了 shared_ptr 的引用计数，是没问题的，但是多线程使用这个shared_ptr释放的话就会出现问题。
单线程没问题。。。

还是用副本吧，本来指针就是设计来指向内存的，指针的副本就是来用的，当然如果要修改指针本身的值，你再考虑指针的指针或者引用。
![Alt text](image-3.png)

**以上是 shared_ptr 在延长局部变量生命周期中的作用。**
