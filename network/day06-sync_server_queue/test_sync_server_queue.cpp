#include "SESSION.hpp"

int main(void)
{
    try
    {
        boost::asio::io_context io_context;
        Server server(io_context, 3333);
        io_context.run();
    }
    catch (std::exception &error_code)
    {
        std::cerr << "Exception: " << error_code.what() << std::endl;
    }

    return 0;
}