### 一. centos系统：protobuf 序列化环境配置(windows系统得麻烦一点)

1. 更新系统软件包：

   ```c++
   sudo yum update
   ```

2. 安装依赖项：

   ```c++
   sudo yum install -y epel-release
   sudo yum install -y autoconf automake libtool make gcc-c++
   ```

3. 下载最新的Protocol Buffers源码包：

   ```c
   wget https://github.com/protocolbuffers/protobuf/releases/download/v3.18.0/protobuf-all-3.18.0.tar.gz
   ```

4. 解压源码包：

   ```c++
   tar -zxvf protobuf-all-3.18.0.tar.gz
   cd protobuf-3.18.0
   ```

5. 编译和安装protobuf：

   ```c
   ./configure
   make
   sudo make install
   ```

6. 配置动态链接库路径：

   打开 `/etc/ld.so.conf` 文件，添加一行 `/usr/local/lib` 并保存。 运行以下命令更新动态链接库缓存：

   ```c++
   sudo ldconfig
   ```

7. 验证安装是否成功：

   运行以下命令查看Protobuf版本信息：

   ```
   protoc --version
   ```

   如果显示Protobuf的版本号，则说明安装成功。现在你就可以在CentOS系统中使用Protocol Buffers了。



### 二. Linux环境protobuf的使用流程
1. 写一个 `.proto` 文件 `msg.proto` 用来对它进行序列化与反序列化。

   ```protobuf
   syntax = "proto3";
   
   message Book{
       string name = 1;
       int32 pages = 2;
       float price = 3;
   }
   ```

   编译：`protoc msg.proto --cpp_out=./`
   生成：`msg.pb.h` 和 `msg.pb.cc` （在当前目录下生成）

2. 就可以直接使用 `msg.pb.h` 文件了

   ```c++
   #include <iostream>
   
   #include "msg.pb.h"
   
   int main(void)
   {
       Book book;
       book.set_name("CPP programing");
       book.set_pages(100);
       book.set_price(200);
       std::string bookstr;
       book.SerializeToString(&bookstr);
       std::cout << "serialize str is " << bookstr << std::endl;
   
       Book book2;
       // book2.ParseFromString(&bookstr);
       book2.ParseFromString(bookstr.c_str());
   
       std::cout << "book2 name is " << book2.name()
                 << " price is " << book2.price()
                 << " pages is " << book2.pages()
                 << std::endl;
   
       getchar();
   
       return 0;
   }
   ```

   编译： `g++ -o ma protobuf_demo.cpp msg.pb.cc -std=c++11 -lprotobuf`

   生成：`ma` 可执行文件

