#include <iostream>

#include "msg.pb.h"

int main(void)
{
    Book book;
    book.set_name("CPP programing");
    book.set_pages(100);
    book.set_price(200);
    std::string bookstr;
    book.SerializeToString(&bookstr);
    std::cout << "serialize str is " << bookstr << std::endl;

    Book book2;
    // book2.ParseFromString(&bookstr);
    book2.ParseFromString(bookstr.c_str());

    std::cout << "book2 name is " << book2.name()
              << " price is " << book2.price()
              << " pages is " << book2.pages()
              << std::endl;

    getchar();

    return 0;
}

// g++ -o ma protobuf_demo.cpp msg.pb.cc -std=c++11 -lprotobuf