### 一. `MsgNode` 两个构造重载函数用来区分 `recvNode` 和 `sendNode`(现在得单独写成两个 `class`)
前面有说过下面的这两个重载的构造函数的作用：
区分发送数据构造的节点(需要发送数据的首地址，发送的长度)和接收数据构造的节点(内有一个接收数据的首地址，接收数据的长度)。
```
    MsgNode(const char *msg, int max_len)
        : _cur_len(0), _total_len(max_len + HEAD_LENGTH)
    {
        _data = new char[_total_len + 1]();                                                 // '\0' 结尾
        int max_len_host = boost::asio::detail::socket_ops::host_to_network_short(max_len); // 转为网络字节序(接收和send都要转化)
        memcpy(_data, &max_len_host, HEAD_LENGTH);                                          // _recv_msg_node->_total
        memcpy(_data + HEAD_LENGTH, msg, max_len);                                          // _recv_msg_node->_data
        _data[_total_len] = '\0';
    }

    MsgNode(short max_len) : _total_len(max_len), _cur_len(0)
    {
        _data = new char[_total_len + 1]();
        _data[_total_len] = '\0';
    }
```

### 二. 修改序列化切包方式为: 消息 `id` + 消息长度 + 消息体

![Alt text](image.png)


