### 一. 隐藏的隐患(`client` 关闭会触发 `server` 的可读事件)
下面的不能发送多次的哦,不然 `client` 主动退出的情况下， `server` 会崩溃。

**原因**： `client` 关闭会触发 `server` 的可读事件，下面都用的话就会发送错误 `error_code` 被设置，就二次析构 `server` 奔溃。

**解决**：后面用伪闭包，延长 `session` 的生命周期。


### 二. 四种 `IO` 模型的说明：同步阻塞 `IO` ，同步非阻塞 `IO` ，异步阻塞 `IO` (`IO` 多路复用)，异步非阻塞 `IO` (真正的异步 `IO`)
先来一下直观的感受：
![Alt text](image-5.png)
所以 `asio` 真正的异步读取函数都是： `OS` 自动把数据读入了我们传递的首地址， `OS` 再自动调用我们 `std::bind` 的回调函数。

所以只要一调用回调函数，数据就已经读入了，就直接使用了。
```
void Session::Start()
{
    memset(_data, 0, max_length);
    _sock.async_read_some(
        boost::asio::buffer(_data, max_length),
        std::bind(&Session::handle_read, this, std::placeholders::_1, std::placeholders::_2));
}

void Session::handle_read(const boost::system::error_code &error_code, size_t bytes_transferred)
{
    if (!error_code)
    {
        cout << "server receive data is " << _data << std::endl; // 直接使用数据

        // 1. 之前多了一个占位符 std::placeholders::_2 报的错都是懵的
        // 2. 隐患：
        //    2.1 下面的不能发送多次的哦,不然client主动退出的情况下，server会崩溃
        //    2.2 client关闭会触发server的可读事件，下面都用的话就会发送错误error_code被设置，就二次析构server奔溃
        //    2.3 后面用伪闭包，延长session的生命周期
        // boost::asio::async_write(
        //     _sock,
        //     boost::asio::buffer(_data, bytes_transferred),
        //     std::bind(&Session::handle_write, this, std::placeholders::_1));

        _sock.async_send(
            boost::asio::buffer(_data, max_length),
            std::bind(&Session::handle_write, this, std::placeholders::_1));

        _sock.async_send(
            boost::asio::buffer(_data, max_length),
            std::bind(&Session::handle_write, this, std::placeholders::_1));
    }
    else
    {
        delete this;
    }
}
void Session::handle_write(const boost::system::error_code &error_code)
{
    if (!error_code)
    {
        memset(_data, 0, max_length);
        _sock.async_read_some(
            boost::asio::buffer(_data, max_length),
            std::bind(&Session::handle_read, this, std::placeholders::_1, std::placeholders::_2));
    }
    else
    {
        delete this;
    }
}
```

#### 2.1 同步阻塞 **`BIO`**

我们需要知道，内核在处理数据的时候其实是分成了两个阶段：

- 数据准备(**等**)
- 数据复制(**拷贝**)

在网络 `IO` 中，**数据准备**可能是客户端还有部分数据还没有发送、或者正在发送的途中，当前内核 Buffer 中的数据并不完整；而**数据复制**则是将内核态 Buffer 中的数据复制到用户态的 Buffer 中去。

当**调用线程**发起 `read` 系统调用时，如果此时内核数据还没有 Ready，调用线程会**阻塞**住，等待内核 Buffer 的数据。内核数据准备就绪之后，会将内核态 Buffer 的数据复制到用户态 Buffer 中，这个过程中调用线程仍然是**阻塞**的，直到数据复制完成，整个流程用图来表示就张这样：

![Alt text](image.png)


#### 2.2 同步非阻塞 `NIO`

![Alt text](image-1.png)

还是分为两个阶段来讨论。

**数据准备阶段**。此时用户线程发起 read 系统调用，此时内核会立即返回一个错误，告诉用户态数据还没有 Read，然后用户线程不停地发起请求，询问内核当前数据的状态。

**数据复制阶段**。此时用户线程还在不断的发起请求，但是当数据 Ready 之后，**用户线程就会陷入阻塞**，**直到数据从内核态复制到用户态**。

稍微总结一下，如果内核态的数据没有 Ready，用户线程不会阻塞；但是如果内核态数据 Ready 了，即使当前的 IO 模型是同步非阻塞，**用户线程仍然会进入阻塞状态**，直到数据复制完成，并不是绝对的非阻塞。

那 NIO 的好处是啥呢？显而易见，实时性好，内核态数据没有 Ready 会立即返回。但是事情的两面性就来了，频繁的轮询内核，会**占用大量的 CPU 资源，降低效率**。


#### 2.3 异步阻塞 `IO`

IO 多路复用实际上就解决了 NIO 中的频繁轮询 CPU 的问题。在之前的 BIO 和 NIO 中只涉及到一种系统调用——`read`，在 IO 多路复用中要引入新的系统调用——`select`。

`read` 用于读取内核态 Buffer 中的数据，而 `select` 你可以理解成 MySQL 中的同名关键字，用于查询 IO 的就绪状态。

在 NIO 中，内核态数据没有 Ready 会导致用户线程不停的轮询，从而拉满 CPU。而在 IO 多路复用中调用了 `select` 之后，只要数据没有准备好，用户线程就会阻塞住(`select`函数是阻塞的)，**避免了频繁的轮询当前的 IO 状态**，用图来表示的话是这样：

![Alt text](image-2.png)


#### 2.4 异步非阻塞 `AIO`

该模型的实现就如其名，是异步的。用户线程发起 `read` 系统调用之后，无论内核 Buffer 数据是否 Ready，都不会阻塞，而是立即返回。

内核在收到请求之后，会开始准备数据，准备好了&复制完成之后会由内核发送一个 Signal 给用户线程，或者回调用户线程注册的接口进行通知。用户线程收到通知之后就可以去读取用户态 Buffer 的数据了。

![Alt text](image-3.png)

由于这种实现方式，异步 IO 有时也被叫做**信号驱动 IO**。相信你也发现了，这种方式最重要的是需要 OS 的支持，如果 OS 不支持就直接完蛋。

Linux 系统在 2.6 版本的时候才引入了异步IO，不过那个时候并不算真正的异步 IO，因为内核并不支持，底层其实是通过 IO 多路复用实现的。而到了 Linux 5.1 时，才通过 `io_uring` 实现了真 AIO。

