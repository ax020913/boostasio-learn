#include "session.h"

//----------------- session ---------------------
void Session::Start()
{
    memset(_data, 0, max_length);
    _sock.async_read_some(
        boost::asio::buffer(_data, max_length),
        std::bind(&Session::handle_read, this, std::placeholders::_1, std::placeholders::_2));
}

void Session::handle_read(const boost::system::error_code &error_code, size_t bytes_transferred)
{
    if (!error_code)
    {
        cout << "server receive data is " << _data << std::endl;

        // 1. 之前多了一个占位符 std::placeholders::_2 报的错都是懵的
        // 2. 隐患：
        //    2.1 下面的不能发送多次的哦,不然client主动退出的情况下，server会崩溃
        //    2.2 client关闭会触发server的可读事件，下面都用的话就会发送错误error_code被设置，就二次析构server奔溃
        //    2.3 后面用伪闭包，延长session的生命周期
        // boost::asio::async_write(
        //     _sock,
        //     boost::asio::buffer(_data, bytes_transferred),
        //     std::bind(&Session::handle_write, this, std::placeholders::_1));

        _sock.async_send(
            boost::asio::buffer(_data, max_length),
            std::bind(&Session::handle_write, this, std::placeholders::_1));

        _sock.async_send(
            boost::asio::buffer(_data, max_length),
            std::bind(&Session::handle_write, this, std::placeholders::_1));
    }
    else
    {
        delete this;
    }
}
void Session::handle_write(const boost::system::error_code &error_code)
{
    if (!error_code)
    {
        memset(_data, 0, max_length);
        _sock.async_read_some(
            boost::asio::buffer(_data, max_length),
            std::bind(&Session::handle_read, this, std::placeholders::_1, std::placeholders::_2));
    }
    else
    {
        delete this;
    }
}

// ----------------- server ---------------------
Server::Server(boost::asio::io_service &ios, const unsigned short server_port)
    : _ios(ios), _acceptor(ios, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), server_port))
{
    // _acceptor.listen(7);
    start_accept();
}

void Server::start_accept()
{
    Session *new_session = new Session(_ios);
    _acceptor.async_accept(new_session->Sock(),
                           std::bind(&Server::handle_accept, this, new_session, std::placeholders::_1));
}

void Server::handle_accept(Session *new_session, const boost::system::error_code &error_code)
{
    if (!error_code)
    {
        new_session->Start();
    }
    else
    {
        delete new_session;
        std::cout << "session accept failed, error is " << error_code.message() << std::endl;
    }

    start_accept();
}
