#pragma once

#include <iostream>
#include <string>
#include <boost/asio.hpp>

#include <memory>
#include <map>

using namespace std;
using namespace boost::asio;

class Session
{
public:
    Session(boost::asio::io_service &ios) : _sock(ios) {}
    boost::asio::ip::tcp::socket &Sock()
    {
        return _sock;
    }

    void Start();

private:
    void handle_read(const boost::system::error_code &error_code, size_t bytes_transferred);
    void handle_write(const boost::system::error_code &error_code);

    enum
    {
        max_length = 1024,
    };
    boost::asio::ip::tcp::socket _sock;
    char _data[max_length];
};

class Server
{
public:
    Server(boost::asio::io_service &ios, const unsigned short server_port);

private:
    void start_accept();
    void handle_accept(Session *new_session, const boost::system::error_code &error_code);
    boost::asio::io_service &_ios;
    boost::asio::ip::tcp::acceptor _acceptor;

public:
    // 主线程控制全部的 Session 资源
    std::map<std::string, shared_ptr<Session>> _sessions;
};