#include <iostream>
#include <boost/asio.hpp>

#include "session.cpp"

int main(void)
{
    try
    {
        boost::asio::io_service ios;
        using namespace std;
        Server server(ios, 3333);
        ios.run();
    }
    catch (std::exception &error_code)
    {
        std::cerr << "Exception: " << error_code.what() << "\n";
    }

    return 0;
}