### 一. 百度安装一下 json，centos 环境下

### 二. json 的应用
server 端的使用：
```
    // json 的应用：
    Json::Reader reader;
    Json::Value root;
    reader.parse(std::string(_recv_msg_node->_data, _recv_msg_node->_total_len), root);

    // 模拟业务处理
    std::cout << "receive msg id is " << root["id"].asInt() << " msg data is " << root["data"].asString() << std::endl;
    
    // 模拟业务响应
    root["data"] = "server has receive msg, msg data is " + root["data"].asString();
    std::string return_str = root.toStyledString();
    Send(return_str.c_str(), return_str.length());
```

client 端的使用：
```
    // 组织发送
    Json::Value root;
    root["id"] = 1001;
    root["data"] = "hello server";
    std::string request = root.toStyledString();
    size_t request_length = request.length();

    // 组织接收
    Json::Reader reader;
    reader.parse(std::string(msg, msg_length), root);
    std::cout << " msg id is: " << root["id"]
                << " msg data is: " << root["data"]
                << std::endl;
```

### 三. 本来想在 json_use 目录下改用分文件编译的，但是一直不行......
![Alt text](image.png)


