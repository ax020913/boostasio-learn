#pragma once

#include <iostream>
#include <string>
#include <chrono>
#include <ctime>

using namespace std;

std::string getNowTime()
{
    auto now = std::chrono::system_clock::now();                  // 获取当前系统时钟的时间点
    std::time_t time = std::chrono::system_clock::to_time_t(now); // 将时间点转换为时间结构体
    std::tm *local_time = std::localtime(&time);                  // 使用本地时间转换函数将时间结构体转换为本地时间
                                                                  // 打印本地时间信息

    std::string nowTime = "";
    nowTime += std::to_string(local_time->tm_year + 1900); // 年份需要加上1900
    nowTime += "-";
    nowTime += std::to_string(local_time->tm_mon + 1); // 月份需要加上1
    nowTime += "-";
    nowTime += std::to_string(local_time->tm_mday);
    nowTime += "_";
    nowTime += std::to_string(local_time->tm_hour + 16); // 得加上 16 小时
    nowTime += ":";
    nowTime += std::to_string(local_time->tm_min);
    nowTime += ":";
    nowTime += std::to_string(local_time->tm_sec);

    return nowTime;
}