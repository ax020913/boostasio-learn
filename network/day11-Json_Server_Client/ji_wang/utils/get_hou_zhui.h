#pragma once

#include <iostream>
#include <string>
#include <cstring>   // strlen 函数的头文件
#include <algorithm> // reverse 反转函数的头文件

using namespace std;

std::string get_hou_zhui(const char *filePath)
{
    int size = strlen(filePath);
    std::string hou_zhui = "";
    for (int i = size - 1; i >= 0 && filePath[i] != '.'; i--)
        hou_zhui += filePath[i];

    // std::reserve(hou_zhui.begin(), hou_zhui.end()); // error 扩容的函数
    reverse(hou_zhui.begin(), hou_zhui.end());
    hou_zhui = '.' + hou_zhui;

    return hou_zhui;
}