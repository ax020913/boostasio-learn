### 使用说明

使用的 `boost::asio` 跨平台异步网络库，实现简单的 `.txt`, `.cpp`, `.png`(已测) 发送。


#### 1. `client` 退出的时候会发生什么？

可以看到我们的 `server` 会打印：
```
handle write failed, error is End of file
```


#### 2. `server` 绑定的 `ip` 和 `port`

```
// ------------------------- Server ----------------------------
Server::Server(boost::asio::io_service &io_service, short port)
    : _io_service(io_service), _port(port), _acceptor(io_service, boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string("192.168.61.171"), 10086))
{
    StartAccept();
}
```


#### 3. 自定义协议

`client` 一次发送的数据：
```
    int len = fileLen + 5;
    memcpy(send_data, &len, 2);                  // len(bety) 为总共 server 端应该接收的数据大小
    memcpy(send_data + 2, fileContent, fileLen); // fileLen(bety) 为文件内容的大小
    memcpy(send_data + 2 + fileLen, hou_zhui.c_str(), 5); // 5(bety) 为文件后缀的大小
    // boost::asio::write(sock, boost::asio::buffer(send_data, 2 + len + 5)); // 不能再 +5 了，因为都加在 len 里面去了 // error
    // boost::asio::write(sock, boost::asio::buffer(send_data, 2 + len)); // no error
    boost::asio::write(sock, boost::asio::buffer(send_data, 2 + fileLen + 5)); // no error
```
直接上图：

![Alt text](image-1.png)


#### 4. 后期优化

`json` 格式化数据、多线程处理任务(锁和条件变量来控制同步互斥)、`qt` 界面 ......

