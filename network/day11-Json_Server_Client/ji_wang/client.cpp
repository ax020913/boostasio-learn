#include <iostream>
#include <string>
#include <fstream>
#include <boost/asio.hpp>

// 工具函数的头文件
#include "./utils/getNowTime.h"
#include "./utils/get_hou_zhui.h"

// 路径写全一点，一般是在/usr/include/路径下找文件的
// #include <jsoncpp/json/json.h>
// #include <jsoncpp/json/value.h>
// #include <jsoncpp/json/reader.h>

using namespace std;
using namespace boost::asio::ip;

const int MAX_LENGTH = 1024 * 1024;
const int HEAD_LENGTH = 2;

int main(void)
{
    try
    {
        // 1. endpoint
        std::string server_ip = "192.168.61.171"; // 127.0.0.1
        const unsigned short server_port = 10086;
        boost::asio::io_service ios;
        boost::asio::ip::address server_address = boost::asio::ip::address::from_string(server_ip);
        boost::asio::ip::tcp::endpoint endpoint(server_address, server_port);

        // 2. sock
        boost::asio::ip::tcp::socket sock(ios);

        // 3. connect
        boost::system::error_code error_code = boost::asio::error::host_not_found; // 先给一个默认值
        sock.connect(endpoint, error_code);
        if (error_code)
        {
            std::cout
                << "connect failed, code is " << error_code.message()
                << " error msg is " << error_code.message()
                << std::endl;
            return error_code.value();
        }

        // do something
        for (;;)
        {
            // 1. 读取键盘输入的 std::string filePath 路径
            std::cout << "Enter filePath: ";
            char filePath[MAX_LENGTH];
            std::cin.getline(filePath, MAX_LENGTH);

            size_t filePath_length = strlen(filePath);
            if (filePath_length == 0) // 防止client输入回车之后就不能再对server发起请求的情况>了
                continue;
            // else
            // filePath[filePath_length] = 0;

            // 根据输入的文件路径来提取文件的后缀
            std::string hou_zhui = get_hou_zhui(filePath);

            // 2. 读取 filePath 路径的文件输入到 char fileContent[MAX_LENGTH] 内，再发送出去
            // char fileContent[MAX_LENGTH];
            // std::string fileContent;
            // {
            std::ifstream file(filePath, std::ios::binary); // 发送图片 binary 好像可有可无
            if (file.is_open() == false)
                std::cerr << filePath << " 文件无法打开" << std::endl;
            // std::string fileContent((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

            char fileContent[MAX_LENGTH];
            file.seekg(0, std::ios::end);
            int fileLen = file.tellg(); // 文件的大小
            file.seekg(0, std::ios::beg);
            file.read(fileContent, fileLen);

            file.close();
            // }

            char send_data[MAX_LENGTH] = {0};
            // 下面注释的方式计算文件的大小是有问题的
            // size_t fileContent_length = strlen(fileContent);
            // memcpy(send_data, &fileContent_length, 2);
            // memcpy(send_data + 2, fileContent, fileContent_length);
            // boost::asio::write(sock, boost::asio::buffer(send_data, fileContent_length + 2));

            int len = fileLen + 5;
            memcpy(send_data, &len, 2);                  // len(bety) 为总共 server 端应该接收的数据大小
            memcpy(send_data + 2, fileContent, fileLen); // fileLen(bety) 为文件内容的大小
            // 最后再写 5 字节的文件后缀 //
            // char hou_zhui[5] = ".png";
            memcpy(send_data + 2 + fileLen, hou_zhui.c_str(), 5); // 5(bety) 为文件后缀的大小
            // boost::asio::write(sock, boost::asio::buffer(send_data, 2 + len + 5)); // 不能再 +5 了，因为都加在 len 里面去了 // error
            // boost::asio::write(sock, boost::asio::buffer(send_data, 2 + len)); // no error
            boost::asio::write(sock, boost::asio::buffer(send_data, 2 + fileLen + 5)); // no error

            // 先发送 _recv_head_node，再发送 _recv_msg_node
            char reply_head[HEAD_LENGTH];
            size_t reply_length = boost::asio::read(sock, boost::asio::buffer(reply_head, HEAD_LENGTH));
            short msglen = 0;
            memcpy(&msglen, reply_head, HEAD_LENGTH);
            // msglen = atoi(reply_head); // error
            msglen = boost::asio::detail::socket_ops::network_to_host_short(msglen); // 转为网络字节序(接收和send都要转化)
            std::cout << "Replay length = " << msglen << ' ' << std::endl;
            char msg[MAX_LENGTH] = {0};
            size_t msg_length = boost::asio::read(sock, boost::asio::buffer(msg, msglen));
            msg[msg_length] = '\0';
            std::cout << " msg len is: " << msg_length
                      << " msg data is: " << msg
                      << std::endl;

            // 卡住不死循环的发送，之前是有输入 std::cin.getline(char*, const size_t) 造成卡住效果的
            getchar();
        }
    }
    catch (std::exception &e)
    {
        std::cerr << "Exception: " << e.what() << endl;
    }

    return 0;
}
