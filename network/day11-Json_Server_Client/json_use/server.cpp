#include "Server.h"

int main(int argc, const char *argv[])
{
    try
    {
        boost::asio::io_service io_service;
        Server server(io_service, 10086);
        io_service.run();
    }
    catch (std::exception &error_code)
    {
        std::cerr << "Exception: " << error_code.what() << std::endl;
    }

    boost::asio::io_service io_service;

    return 0;
}
