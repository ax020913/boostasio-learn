#pragma once

#include <iostream>
#include <string>
#include <memory>
#include <map>

#include <boost/asio.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>

// 路径写全一点，一般是在/usr/include/路径下找文件的
#include <jsoncpp/json/json.h>
#include <jsoncpp/json/value.h>
#include <jsoncpp/json/reader.h>

#include "Session.h"

using namespace std;
using namespace boost::asio;

class Server
{
public:
    Server(boost::asio::io_service &io_service, short port);
    void ClearSession(std::string &uuid);

private:
    void HandleAccept(shared_ptr<Session> new_session, const boost::system::error_code &error_code);

    void StartAccept();

    boost::asio::io_service &_io_service;
    boost::asio::ip::tcp::acceptor _acceptor;
    short _port;
    // 主线程控制全部的 Session 资源
    std::map<std::string, shared_ptr<Session> > _sessions;
};
