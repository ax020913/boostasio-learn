#pragma once

#include <iostream>
#include <cstring>
#include <string>
#include <queue>
#include <mutex>
#include <memory>
#include <thread>
#include <iomanip>

#include <boost/asio.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>

// 路径写全一点，一般是在/usr/include/路径下找文件的
// #include <jsoncpp/json/json.h>
// #include <jsoncpp/json/value.h>
// #include <jsoncpp/json/reader.h>

using namespace std;
using namespace boost::asio;

#define MAX_LENGTH 1024 * 2 //
#define HEAD_LENGTH 2       //
#define MAX_SENDQUE 1000
#define SEND_MAX_LENGTH 1000

class Server;

class MsgNode
{
    friend class Session;

public:
    MsgNode(const char *msg, int max_len)
        : _cur_len(0), _total_len(max_len + HEAD_LENGTH)
    {
        _data = new char[_total_len + 1]();                                                 // '\0' 结尾
        int max_len_host = boost::asio::detail::socket_ops::host_to_network_short(max_len); // 转为网络字节序(接收和send都要转化)
        memcpy(_data, &max_len_host, HEAD_LENGTH);                                          // _recv_msg_node->_total
        memcpy(_data + HEAD_LENGTH, msg, max_len);                                          // _recv_msg_node->_data
        _data[_total_len] = '\0';
    }

    MsgNode(short max_len) : _total_len(max_len), _cur_len(0)
    {
        _data = new char[_total_len + 1]();
        _data[_total_len] = '\0';
    }

    ~MsgNode()
    {
        delete[] _data;
    }

    void Clear()
    {
        ::memset(_data, 0, _total_len);
        _cur_len = 0;
    }

private:
    char *_data;
    short _cur_len;
    short _total_len;
};

class Session : public std::enable_shared_from_this<Session>
{
public:
    Session(boost::asio::io_service &ios, Server *server)
        : _sock(ios), _server(server), _b_close(false), _b_head_parse(false)
    {
        // 自己要写的话，可以用雪花算法实现
        boost::uuids::uuid a_uuid = boost::uuids::random_generator()();
        _uuid = boost::uuids::to_string(a_uuid);
        // _recv_head_node = new make_shared<MsgNode>(HEAD_LENGTH); // error，怎么说莫名其妙的报错呢：expected type-specifier
        _recv_head_node = std::make_shared<MsgNode>(HEAD_LENGTH);
    }
    ~Session();
    boost::asio::ip::tcp::socket &GetSock()
    {
        return _sock;
    }
    std::string &GetUuid()
    {
        return _uuid;
    }
    void Start();
    void StartOtherThread();
    void Send(const char *msg, int max_length);
    void Close();
    std::shared_ptr<Session> SharedSelf();

private:
    void handle_read(const boost::system::error_code &error_code, size_t bytes_transferred, shared_ptr<Session> _self_shared);
    void handle_write(const boost::system::error_code &error_code, shared_ptr<Session> _self_shared);

    enum
    {
        max_length = MAX_LENGTH
    };
    boost::asio::ip::tcp::socket _sock;
    char _data[max_length];
    std::string _uuid;
    Server *_server;
    std::queue<std::shared_ptr<MsgNode> > _send_queue;
    std::mutex _send_mutex;

    bool _b_close;
    bool _b_head_parse;
    std::shared_ptr<MsgNode> _recv_head_node; // 收到的头部结构
    std::shared_ptr<MsgNode> _recv_msg_node;  // 收到的消息结构
};
