#include "Session.h"
#include "Server.h"

//----------------- session ---------------------

void Session::Close()
{
    _sock.close();
    _b_close = true;
}

Session::~Session()
{
    std::cout << "Session destruct" << std::endl;
}
std::shared_ptr<Session> Session::SharedSelf()
{
    return shared_from_this();
}

// g++ -o ma Server_StickyPackage.cpp -std=c++11 -lpthread -lboost_system
// ./ma 0
void Session::Start()
{
    memset(_data, 0, MAX_LENGTH);
    _sock.async_read_some(boost::asio::buffer(_data, MAX_LENGTH),
                          std::bind(&Session::handle_read, this, std::placeholders::_1, std::placeholders::_2, shared_from_this()));
}

void Session::Send(const char *msg, int max_length)
{
    bool pending = false;
    std::lock_guard<std::mutex> lock(_send_mutex);
    // 规定发送队列的长度不超过 SEND_MAX_LENGTH，用来防止发送过快的问题（MsgNode小而且快，MsgNode大都是不允许的）
    if (_send_queue.size() > SEND_MAX_LENGTH)
    {
        std::cout << "Session: " << _uuid << " _send_queue.size() > SEND_MAX_LENGTH" << std::endl;
        return;
    }

    if (_send_queue.size() > 0)
        pending = true;
    _send_queue.push(make_shared<MsgNode>(msg, max_length));
    if (pending == true)
        return;

    auto &msgnode = _send_queue.front();
    boost::asio::async_write(_sock, boost::asio::buffer(msgnode->_data, msgnode->_total_len),
                             std::bind(&Session::handle_write, this, std::placeholders::_1, shared_from_this()));
}

void Session::handle_write(const boost::system::error_code &error_code,
                           shared_ptr<Session> _self_shared)
{
    if (!error_code)
    {
        std::lock_guard<std::mutex> lock(_send_mutex);
        _send_queue.pop();
        if (!_send_queue.empty())
        {
            auto &msgnode = _send_queue.front();
            // memset(msgnode->_data, 0, msgnode->_total_len);
            boost::asio::async_write(_sock, boost::asio::buffer(msgnode->_data, msgnode->_total_len),
                                     std::bind(&Session::handle_write, this, std::placeholders::_1, _self_shared));
        }
    }
    else
    {
        std::cout << "handle read failed, error is " << error_code.message() << std::endl;
        Close();
        _server->ClearSession(_uuid);
    }
}

void Session::handle_read(const boost::system::error_code &error_code,
                          size_t bytes_transferred,
                          shared_ptr<Session> _self_shared)
{
    if (!error_code)
    {
        // bytes_transferred: 读取到的总长度  copyed_len: 从读取到的总长度已经拷贝出去的字符数
        int copyed_len = 0;
        while (bytes_transferred > 0)
        {
            if (!_b_head_parse) // 头部结构还没有接收完
            {
                // 收到的数据不足头部的大小
                if (bytes_transferred + _recv_head_node->_cur_len < HEAD_LENGTH)
                {
                    memcpy(_recv_head_node->_data + _recv_head_node->_cur_len, _data + copyed_len, bytes_transferred);
                    _recv_head_node->_cur_len += bytes_transferred;
                    // copyed_len +=
                    // bytes_transferred -=
                    // 不顾再读取
                    ::memset(_data, 0, MAX_LENGTH);
                    _sock.async_read_some(boost::asio::buffer(_data, MAX_LENGTH),
                                          std::bind(&Session::handle_read, this, std::placeholders::_1, std::placeholders::_2, _self_shared));
                    return;
                }
                // 收到的数据比头部多
                int head_remain = HEAD_LENGTH - _recv_head_node->_cur_len; // 头部剩余未复制的长度
                memcpy(_recv_head_node->_data + _recv_head_node->_cur_len, _data + copyed_len, head_remain);
                // _recv_head_node->_cur_len += bytes_transferred;
                // 更新已处理的data的长度，和剩余未处理的长度
                copyed_len += head_remain;
                bytes_transferred -= head_remain;
                // 获取头部数据(看一下值)
                short head_data_len = 0;
                memcpy(&head_data_len, _recv_head_node->_data, HEAD_LENGTH);
                // 网络字节序转化为本地字节序(接收和send都要转化)
                // head_data_len 在下面不用于网络传输，不用转了，不然还影响下面_recv_msg_node的大小
                // head_data_len = boost::asio::detail::socket_ops::network_to_host_short(head_data_len);
                std::cout << "head_data_len is " << head_data_len << std::endl;
                // 头部非法长度(头部数据值大于设置限制的值 ---> 后面发送的具体的数据大小过大，非法，直接断掉连接)
                if (head_data_len > MAX_LENGTH)
                {
                    std::cout << "invalied data length is " << head_data_len << std::endl;
                    _server->ClearSession(_uuid);
                    return;
                }

                // 上面的头部结构处理完了，自然就处理后面的消息结构了
                _recv_msg_node = std::make_shared<MsgNode>(head_data_len);
                // 消息的长度 小于 头部规定的长度，说明数据未读取完，则先将已经读取的消息发到节点里面去
                if (bytes_transferred < head_data_len)
                {
                    memcpy(_recv_msg_node->_data + _recv_msg_node->_cur_len, _data + copyed_len, bytes_transferred);
                    _recv_msg_node->_cur_len += bytes_transferred;
                    ::memset(_data, 0, MAX_LENGTH);
                    _sock.async_read_some(boost::asio::buffer(_data, MAX_LENGTH),
                                          std::bind(&Session::handle_read, this, std::placeholders::_1, std::placeholders::_2, _self_shared));
                    // 头部处理完成
                    _b_head_parse = true;
                    return;
                }
                // 消息的长度 >= 头部规定的长度，直接memcpy到_recv_msg_node->_data
                memcpy(_recv_msg_node->_data + _recv_msg_node->_cur_len, _data + copyed_len, head_data_len);
                _recv_msg_node->_cur_len += head_data_len;
                copyed_len += head_data_len;
                bytes_transferred -= head_data_len;
                _recv_msg_node->_data[_recv_msg_node->_total_len] = '\0';
                // {
                // 此处可以调用Send发送测试
                // Send(_recv_msg_node->_data, _recv_msg_node->_total_len);
                // std::cout << "receive data is: " << _recv_msg_node->_data << "  "
                //           << "receive data_len is: " << _recv_msg_node->_total_len << std::endl
                //           << std::endl;
                // }

                // json 的应用：
                Json::Reader reader;
                Json::Value root;
                reader.parse(std::string(_recv_msg_node->_data, _recv_msg_node->_total_len), root);
                std::cout << "receive msg id is " << root["id"].asInt() << " msg data is " << root["data"].asString() << std::endl;
                root["data"] = "server has receive msg, msg data is " + root["data"].asString();
                std::string return_str = root.toStyledString();
                Send(return_str.c_str(), return_str.length());

                // 继续轮询剩余未处理完的数据
                _b_head_parse = false;
                _recv_head_node->Clear();
                if (bytes_transferred <= 0)
                {
                    ::memset(_data, 0, MAX_LENGTH);
                    _sock.async_read_some(boost::asio::buffer(_data, MAX_LENGTH),
                                          std::bind(&Session::handle_read, this, std::placeholders::_1, std::placeholders::_2, _self_shared));
                    return;
                }
                continue;
            }

            // 头部数据已经处理完了，处理上次未接受完的消息数据
            // 注意 _recv_msg_node = std::make_shared<MsgNode>(head_data_len); 所以下面的 _total_len == head_data_len,别晕
            int msg_remain = _recv_msg_node->_total_len - _recv_msg_node->_cur_len;
            // 已经读取到的数据长度bytes_transferred < 还需要的消息长度
            if (bytes_transferred < msg_remain)
            {
                memcpy(_recv_msg_node->_data + _recv_msg_node->_cur_len, _data + copyed_len, bytes_transferred);
                _recv_msg_node->_cur_len += bytes_transferred;
                // 不够就继续读
                ::memset(_data, 0, MAX_LENGTH);
                _sock.async_read_some(boost::asio::buffer(_data, MAX_LENGTH),
                                      std::bind(&Session::handle_read, this, std::placeholders::_1, std::placeholders::_2, _self_shared));
                return;
            }
            // 已经读取到的数据长度bytes_transferred >= 还需要的消息长度
            memcpy(_recv_msg_node->_data + _recv_msg_node->_cur_len, _data + copyed_len, msg_remain);
            _recv_msg_node->_cur_len += msg_remain;
            bytes_transferred -= msg_remain;
            copyed_len += msg_remain;
            _recv_msg_node->_data[_recv_msg_node->_total_len] = '\0';
            // {
            // 此处可以调用Send发送测试
            // Send(_recv_msg_node->_data, _recv_msg_node->_total_len);
            // std::cout << "receive data is: " << _recv_msg_node->_data << "  "
            //           << "receive data_len is: " << _recv_msg_node->_total_len << std::endl
            //           << std::endl;
            // // }

            // json 的应用：
            Json::Reader reader;
            Json::Value root;
            reader.parse(std::string(_recv_msg_node->_data, _recv_msg_node->_total_len), root);
            std::cout << "receive msg id is " << root["id"].asInt() << " msg data is " << root["data"].asString() << std::endl;
            root["data"] = "server has received msg, msg data is " + root["data"].asString();
            std::string return_str = root.toStyledString();
            Send(return_str.c_str(), return_str.length());

            // 继续轮询剩余未处理完的数据
            _b_head_parse = false;
            _recv_head_node->Clear();
            if (bytes_transferred <= 0)
            {
                ::memset(_data, 0, MAX_LENGTH);
                _sock.async_read_some(boost::asio::buffer(_data, MAX_LENGTH),
                                      std::bind(&Session::handle_read, this, std::placeholders::_1, std::placeholders::_2, _self_shared));
                return;
            }
            continue;
        }
    }
    else
    {
        std::cout << "handle write failed, error is " << error_code.message() << std::endl;
        Close();
        _server->ClearSession(_self_shared->GetUuid());
    }
}
