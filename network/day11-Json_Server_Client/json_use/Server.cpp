#include "Server.h"

Server::Server(boost::asio::io_service &io_service, short port)
    : _io_service(io_service), _port(port), _acceptor(io_service, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port))
{
    StartAccept();
}

void Server::ClearSession(std::string &uuid)
{
    _sessions.erase(uuid);
}

void Server::StartAccept()
{
    shared_ptr<Session> new_session = make_shared<Session>(_io_service, this);
    _acceptor.async_accept(new_session->GetSock(),
                           std::bind(&Server::HandleAccept, this, new_session, std::placeholders::_1));
}

void Server::HandleAccept(shared_ptr<Session> new_session, const boost::system::error_code &error_code)
{
    if (!error_code)
    {
        new_session->Start();

        _sessions.insert(make_pair(new_session->GetUuid(), new_session));
    }
    else
    {
        std::cout << "session accept failed, error is " << error_code.message() << std::endl;
    }
    StartAccept();
}
