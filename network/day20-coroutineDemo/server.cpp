#include <iostream>
#include <string>

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/write.hpp>

// boost::asio 使用协程的了两个头文件
#include <boost/asio/co_spawn.hpp>
#include <boost/asio/detached.hpp>
// 服务器的优雅退出
#include <boost/asio/signal_set.hpp>

using namespace std;
using namespace boost::asio;

awaitable<void> echo(boost::asio::ip::tcp::socket sock)
{
    try
    {
        char data[1024];
        for (;;)
        {
            std::size_t n = co_await sock.async_read_some(boost::asio::buffer(data), use_awaitable);
            data[n] = 0; // '\0'
            std::cout << data << std::endl;
            co_await boost::asio::async_write(sock,
                                              boost::asio::buffer(data, n),
                                              use_awaitable);
        }
    }
    catch (std::exception &error_code)
    {
        std::cout << "exception: " << error_code.what() << std::endl;
    }
}

// awaitable 表示函数 void listener() { } 是一个可以被协程调用和等待的自定义的函数
awaitable<void> listener()
{
    // executor 是一个执行器
    auto executor = co_await this_coro::executor;
    boost::asio::ip::tcp::acceptor acceptor(executor, {boost::asio::ip::tcp::v4(), 3333});
    for (;;)
    {
        // use_awaitable 表示函数 async_accept 是一个可以被协程调用和等待的 api 函数
        // co_await 等待 acceptor 接收连接，如果没有连接到来则挂起协程（等待新的就绪事件(对端发数据，或者新连接)到来唤醒）
        boost::asio::ip::tcp::socket sock = co_await acceptor.async_accept(use_awaitable);
        // co_spawn(executor, echo(sock), detached); // error
        co_spawn(executor, echo(std::move(sock)), detached);
    }
}

int main(void)
{
    try
    {
        boost::asio::io_service ios(1);
        boost::asio::signal_set signals(ios, SIGINT, SIGTERM);
        signals.async_wait([&](auto, auto)
                           { ios.stop(); });

        // 启动一个 boost::asio 库的协程：co_spawn(调度器，执行的函数，启动方式);
        // deatched表示将协程对象分离出来，这种启动方式可以启动多个协程，他们都是独立的，如何调度取决于调度器，在用户的感知上更像是线程调度的模式，类似于并发运行，其实底层都是串行的。
        co_spawn(ios, listener(), boost::asio::detached);

        ios.run();
    }
    catch (std::exception &error_code)
    {
        std::cout << "exception: " << error_code.what() << std::endl;
    }

    return 0;
}