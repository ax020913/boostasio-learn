### 一. 关于 `boost::asio` 库没有协程？(先卸载，再重装)
版本低了，得提高一下 `boost` 库的版本。
```
#include <iostream>
#include <string>

#include <boost/version.hpp>
#include <boost/config.hpp>

// #include <boost/asio/co_spawn.hpp>
// #include <boost/asio/detached.hpp>
#include <boost/asio/io_service.hpp>

using namespace std;

int main(void)
{
    // BOOST_LIB_VERSION: 1_53
    cout << "BOOST_LIB_VERSION: " << BOOST_LIB_VERSION << endl;

    return 0;
}
```

方法一：命令行
```
// 卸载
sudo apt-get remove libboost-all-dev

// 安装
sudo apt-get install libboost-all-dev
```


方法二：源码编译
```
// 卸载
sudo rm -f /usr/local/lib/libboost*
sudo rm -rf /usr/local/include/boost
sudo rm -rf /usr/local/lib/cmake/boost*
sudo rm -rf /usr/local/lib/cmake/Boost*

// 安装
官网 https://www.boost.org/users/download/
tar -zxvf boost_1_78_0.tar.gz
cd boost_1_78_0
./bootstrap.sh
./b2
sudo ./b2 install
```

![Alt text](image.png)


### 二. 协程实现并发程序的两大好处
1. 将回调函数改写为顺序调用，提高开发效率。
2. 协程调度比线程调度更轻量化，因为协程是运行在用户空间的，线程切换需要在用户空间和内核空间切换。


