#include <iostream>
#include <string>
#include <boost/asio.hpp>

using namespace std;
using namespace boost::asio;

const int MAX_LENGTH = 1024;

int main(void)
{
    try
    {
        // 1. endpoint
        std::string server_ip = "127.0.0.1";
        const unsigned short server_port = 3333;
        boost::asio::io_service ios;
        boost::asio::ip::address server_address = boost::asio::ip::address::from_string(server_ip);
        boost::asio::ip::tcp::endpoint endpoint(server_address, server_port);

        // 2. sock
        boost::asio::ip::tcp::socket sock(ios);

        // 3. connect
        boost::system::error_code error_code = boost::asio::error::host_not_found; // 先给一个默认值
        sock.connect(endpoint, error_code);
        if (error_code)
        {
            std::cout
                << "connect failed, code is " << error_code.message()
                << " error msg is " << error_code.message()
                << std::endl;
            return error_code.value();
        }

        // 4. 业务
        for (;;)
        {
            std::cout << "Enter message: ";
            char request[MAX_LENGTH];
            std::cin.getline(request, MAX_LENGTH);
            size_t request_length = strlen(request);
            if (request_length == 0) // 防止client输入回车之后就不能再对server发起请求的情况了
                continue;
            boost::asio::write(sock, boost::asio::buffer(request, request_length));

            char replay[MAX_LENGTH];
            // size_t replay_length = sock.receive(boost::asio::buffer(replay));
            size_t replay_length = boost::asio::read(sock, boost::asio::buffer(replay, request_length));
            replay[replay_length] = '\0';
            std::cout << "Replay length = " << replay_length
                      << " message is: " << replay << std::endl;
        }
    }
    catch (std::exception &error_code)
    {
        std::cerr << "Exception: " << error_code.what() << std::endl;
    }

    return 0;
}

// g++ -o client client.cpp -std=c++11 -lpthread