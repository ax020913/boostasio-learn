### 优雅退出解决示例(`signal`回调函数的问题)
使用全局函数或静态函数作为信号处理函数。
```
#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <signal.h>

using namespace std;

class Data
{
public:
    static Data &GetInstance()
    {
        static Data data;
        return data;
    }
    void print()
    {
        std::cout << _data << std::endl;
    }
    ~Data()
    {
        workThread.join(); // main thread 不等的话就会有崩溃
    }

private:
    Data(int data = 100) : _data(data)
    {
        workThread = std::thread([this]()
                                 {
                                     while (_b_stop == false)
                                     {
                                         std::cout << "workThread......" << std::endl;
                                         std::this_thread::sleep_for(std::chrono::seconds(1));
                                     } });

        // ctrl d 产生 SIGINT 信号
        signal(SIGINT, signal_handler);
    }

    void signal_handler(int signal)
    {
        std::cout << "signal --> workThread quit......" << std::endl;
        // kill(std::this_thread::get_id(), signal);
        Data::GetInstance()._b_stop = true;
    }

    Data(const Data &data) = delete;
    Data &operator=(const Data &data) = delete;

    int _data;
    std::thread workThread;

public:
    bool _b_stop = false;
};

int main(void)
{
    Data::GetInstance().print();

    return 0;
}

```
#### 1. 报错
```
test.cpp: In constructor ‘Data::Data(int)’:
test.cpp:36:24: error: invalid use of non-static member function ‘void Data::signal_handler(int)’
   36 |         signal(SIGINT, signal_handler);
      |                        ^~~~~~~~~~~~~~
test.cpp:38:10: note: declared here
   38 |     void signal_handler(int signal)
      |  
```

要求我们的回调函数得是 static 类型的。
```
class Data {
public:
    static void signal_handler(int signum) {
        // 处理信号的逻辑
    }
};
```

#### 2. 法一： `signal` 的回调函数为全局函数
1. 把 `_b_stop` 变成公有成员。
2. `void signal_handler(int signal)` 函数变成全局函数。
3. `signal_handler` 函数的声明在 `class Data` 的声明前面，定义在其后面。


#### 3. 法二： `signal` 的回调函数为 `static` 函数，但是就得使用 `static` 成员变量，不行。
