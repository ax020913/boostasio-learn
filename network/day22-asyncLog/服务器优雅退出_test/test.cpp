#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <signal.h>

using namespace std;

// signal_handler 函数写在上面不行
// class Data;
// void signal_handler(int signal)
// {
//     std::cout << "hello world" << std::endl;
//     // kill(std::this_thread::get_id(), signal);
//     Data::GetInstance()._b_stop = true;
// }

void signal_handler(int signal);

class Data
{
public:
    static Data &GetInstance()
    {
        static Data data;
        return data;
    }
    void print()
    {
        std::cout << _data << std::endl;
    }
    ~Data()
    {
        workThread.join(); // main thread 不等的话就会有崩溃
    }

private:
    Data(int data = 100) : _data(data)
    {
        workThread = std::thread([this]()
                                 {
                                     while (_b_stop == false)
                                     {
                                         std::cout << "workThread......" << std::endl;
                                         std::this_thread::sleep_for(std::chrono::seconds(1));
                                     } });

        // ctrl d 产生 SIGINT 信号
        signal(SIGINT, signal_handler);
    }

    Data(const Data &data) = delete;
    Data &operator=(const Data &data) = delete;

    int _data;
    std::thread workThread;

public:
    bool _b_stop = false;
};

int main(void)
{
    Data::GetInstance().print();

    return 0;
}

void signal_handler(int signal)
{
    std::cout << "signal --> workThread quit......" << std::endl;
    // kill(std::this_thread::get_id(), signal);
    Data::GetInstance()._b_stop = true;
}
