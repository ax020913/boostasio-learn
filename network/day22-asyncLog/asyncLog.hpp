#pragma once

#include <iostream>
#include <queue>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <any>
#include <sstream>

#include <signal.h>

using namespace std;

namespace AsyncLog
{
    enum LogLv
    {
        DEBUG = 0,
        INFO = 1,
        WARN = 2,
        ERROR = 3,
    };

    class LogTask
    {
    public:
        LogTask() {}
        LogTask(const LogTask &src)
            : _level(src._level), _logdatas(src._logdatas) {}
        LogTask(const LogTask &&src)
            : _level(src._level), _logdatas(std::move(src._logdatas)) {}

        // private:
        LogLv _level;
        std::queue<std::any> _logdatas;
    };

    // std::condition_variable _empty_cond;
    // bool _b_stop;

    void signal_handler(int signal); // 得在前面声明一下

    class AsyncLog
    {
    public:
        // c++11 之后，下面的 static instance 之后初始化一次，不会有线程安全的问题
        static AsyncLog &Instance()
        {
            static AsyncLog instance;
            return instance;
        }
        // static std::shared_ptr<AsyncLog> &Instance()
        // {
        //     if (_asyncLog != nullptr)
        //         return _asyncLog;

        //     _mutex.lock();
        //     if (_asyncLog != nullptr)
        //     {
        //         _mutex.unlock();
        //         return _asyncLog;
        //     }

        //     _asyncLog = std::shared_ptr<AsyncLog>(new AsyncLog());
        //     _mutex.unlock();

        //     return _asyncLog;
        // }
        ~AsyncLog()
        {
            Stop();
            workthread.join();
            std::cout << "exit success" << std::endl;
        }
        void Stop()
        {
            _b_stop = true;
            _empty_cond.notify_one();
        }

        // std::any 是 C++17 中引入的一个特性，可以用来存储任意类型的值
        template <class K>
        std::any toAny(const K &value)
        {
            return std::any(value);
        }
        // 不支持C++11的话，可以采用下面的这个函数入队
        template <class Arg, class... Args>
        void TaskEnque(std::shared_ptr<LogTask> task, Arg &&arg, Args &&...args)
        {
            task->_logdatas.push(std::any(arg));
            TaskEnque(task, std::forward<Args>(args)...);
        }
        template <class Arg>
        void TaskEnque(std::shared_ptr<LogTask> task, Arg &&arg)
        {
            task->_logdatas.push(std::any(arg));
        }

        // 可变参数列表，异步写
        template <class... Args>
        void AsyncWrite(LogLv level, Args &&...args)
        {
            auto task = std::make_shared<LogTask>();
            // 折叠表达式依次将可变参数写入队列，需c++17版本支持
            (task->_logdatas.push(args), ...);
            // 如果不支持
            // TaskEnque(task, args...);
            task->_level = level;
            std::unique_lock<std::mutex> lock(_mutex);
            _queue.push(task);
            bool notify = (_queue.size() == 1) ? true : false;
            lock.unlock();
            // 通知等待的线程有新的任务可以去处理了
            if (notify == true)
                _empty_cond.notify_one();
        }

    private:
        // 下面三个函数的位置就是 instance 类的标配
        AsyncLog()
            : _b_stop(false)
        {
            workthread = std::thread([this]()
                                     {
                    for(;;){
                        std::unique_lock<std::mutex> lock(_mutex);
                        // 任务队列为空 && 没有暂停
                        while(_queue.empty() && !_b_stop){
                            _empty_cond.wait(lock);
                        }
                        if(_b_stop)
                            return;

                        auto logtask = _queue.front();
                        _queue.pop();
                        lock.unlock();

                        // workthread 线程去处理取出来的数据 data
                        processTask(logtask);
                    } });

            // 线程优雅退出(Ctrl C 收到二号信号 SIGINT，运行我们的异步日志库线程就退出了)
            // signal(SIGINT, signal_handler);
            // signal(SIGINT, [](int signal) // error 规定只能传递一个 int 类型的参数
            //    {
            //            // 用不了成员变量的
            //            // _empty_cond.notify_one();
            //            // _b_stop = true;

            //            // 获取的 pid 是调用这个打印日志的线程，这里也就是我们的主线程
            //            // kill(getpid(), signal);

            //            // 把下面的两个成员改成了全局的都不行
            //            // _empty_cond.notify_one();
            //            // _b_stop = true;

            //            // 下面的方式也是不行的哦
            //            // AsyncLog::Instance().Stop();
            //    });

            signal(SIGINT, signal_handler);
        }
        // void signal_handler(int signal) // error: invalid use of non-static member function ‘void AsyncLog::AsyncLog::signal_handler(int)’
        // {
        // _empty_cond.notify_one();
        // _b_stop = true;
        // }
        AsyncLog(const AsyncLog &src) = delete;
        AsyncLog &operator=(const AsyncLog &src) = delete;

        // std::thread workthread 线程实际工作的函数
        void processTask(std::shared_ptr<LogTask> task)
        {
            std::cout << "log level is " << task->_level << std::endl;
            if (task->_logdatas.empty() == true)
                return;

            // 队列首元素
            auto head = task->_logdatas.front();
            task->_logdatas.pop();

            std::string formatstr = "";
            bool bsuccess = conver2Str(head, formatstr);
            if (bsuccess == false)
                return;

            for (; !(task->_logdatas.empty());)
            {
                auto data = task->_logdatas.front();
                task->_logdatas.pop();
                formatstr = formatString(formatstr, data);
            }

            std::cout << "log string is " << formatstr << std::endl;
        }
        // 把 std::any 类型的数据变成 std::string 类型
        bool conver2Str(const std::any &data, std::string &str)
        {
            std::ostringstream ss;
            if (data.type() == typeid(int))
                ss << std::any_cast<int>(data);
            else if (data.type() == typeid(float))
                ss << std::any_cast<float>(data);
            else if (data.type() == typeid(double))
                ss << std::any_cast<double>(data);
            else if (data.type() == typeid(std::string))
                ss << std::any_cast<std::string>(data);
            else if (data.type() == typeid(char *))
                ss << std::any_cast<char *>(data);
            else if (data.type() == typeid(const char *))
                ss << std::any_cast<const char *>(data);
            else if (data.type() == typeid(char const *))
                ss << std::any_cast<char const *>(data);
            else
                return false;

            str = ss.str();
            return true;
        }
        // 把可变参数列表的参数变成 string 类型
        template <class... Args>
        std::string formatString(const std::string &format, Args... args)
        {
            std::string result = format;
            size_t pos = 0;
            // lambda 表达式查找并替换字符串
            auto replacePlaceholder = [&](const std::string &placeholder, const std::any &replacement)
            {
                std::string str_replacement = "";
                bool bsuccess = conver2Str(replacement, str_replacement);
                if (bsuccess == false)
                    return;

                // 在 string result 里面查找是否有 placeholder；有的话就用 replacement 转成的 str_repacement 替换掉，不然就直接加在 result 后面
                size_t placeholderPos = result.find(placeholder, pos);
                if (placeholderPos != std::string::npos)
                {
                    result.replace(placeholderPos, placeholder.length(), str_replacement);
                    pos = placeholderPos + str_replacement.length();
                }
                else
                {
                    result = result + " " + str_replacement;
                }
            };

            (replacePlaceholder("{}", args), ...);
            return result;
        }

        std::queue<std::shared_ptr<LogTask>> _queue;
        std::mutex _mutex;
        std::thread workthread;

    public:
        std::condition_variable _empty_cond;
        bool _b_stop;

        // static std::shared_ptr<AsyncLog> _asyncLog;
    };
    // std::shared_ptr<AsyncLog> AsyncLog::_asyncLog = nullptr;

    // 使得异步日志库的 workthread 线程退出，其他的地方再调用的时候也是没有用的
    void signal_handler(int signal)
    {
        AsyncLog::Instance()._empty_cond.notify_one();
        AsyncLog::Instance()._b_stop = true;
    }

    // 真实向外提供的可调用函数
    // debug
    template <class... Args>
    void DLog(Args &&...args)
    {
        AsyncLog::Instance().AsyncWrite(DEBUG, std::forward<Args>(args)...);
    }

    // info
    template <class... Args>
    void ILog(Args &&...args)
    {
        AsyncLog::Instance().AsyncWrite(INFO, std::forward<Args>(args)...);
    }

    // error
    template <class... Args>
    void ELog(Args &&...args)
    {
        AsyncLog::Instance().AsyncWrite(ERROR, std::forward<Args>(args)...);
    }

    // warn
    template <class... Args>
    void WLog(Args &&...args)
    {
        AsyncLog::Instance().AsyncWrite(WARN, std::forward<Args>(args)...);
    }
}