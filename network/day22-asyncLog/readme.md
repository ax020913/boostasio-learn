### `asyncLog.hpp` 异步的日志库

#### 1. 优雅退出不行啊实现不了
后面两种方式按理来说是没有问题的

```
// 线程优雅退出(Ctrl C 收到二号信号 SIGINT，运行我们的异步日志库线程就退出了)
// signal(SIGINT, signal_handler);
// signal(SIGINT, [](int signal) // error 规定只能传递一个 int 类型的参数
//        {
//            // 用不了成员变量的
//            // _empty_cond.notify_one();
//            // _b_stop = true;

//            // 获取的 pid 是调用这个打印日志的线程，这里也就是我们的主线程
//            // kill(getpid(), signal);

//            // 把下面的两个成员改成了全局的都不行
//            // _empty_cond.notify_one();
//            // _b_stop = true;

//            // 下面的方式也是不行的哦
//            // AsyncLog::Instance().Stop();
//        });
```

#### 2. 法一： `signal` 的回调函数为全局函数
1. 把 `_empty_cond` 和 `_b_stop` 变成公有成员。
2. `void signal_handler(int signal)` 函数变成全局函数。
3. `signal_handler` 函数的声明在 `class AsyncLog` 的声明前面，定义在其后面。

![Alt text](image.png)

#### 3. 法二： `signal` 的回调函数为 `static` 函数，但是就得使用 `static` 成员变量，不行。

