#include "asyncLog.hpp"

#include <chrono>

// void signal_handler_mainThread(int signal)
// {
//     std::cout << "main thread quit" << std::endl;

//     exit(0);
// }

int main(void)
{
    // 使用 SIGKILL 信号来终止 main thread 线程
    std::cout << "main thread pid = " << getpid() << std::endl;
    // signal(SIGKILL, signal_handler_mainThread);

    AsyncLog::DLog("hello {} {}", 1);

    while (1)
    {
        // 没有 sleep 的话，main thread 可能会先退出就直接打印一句 exit success
        this_thread::sleep_for(std::chrono::seconds(1));
        // ctrl d 使得异步日志库的 workthread 线程退出，其他的地方再调用的时候也是没有用的(下面的打印函数就没有作用了)
        AsyncLog::DLog("hello {} {}", 2);
    }

    return 0;
}

// /home/ax/boost_learn/boostasio_learn/boostasio-learn/network/day22-asyncLog