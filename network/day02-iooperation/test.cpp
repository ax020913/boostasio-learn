#include <iostream>
#include <cstring>

using namespace std;

// 运行：./main aaaaa bbbbb ccccc
// 获取得到：aaaaa bbbbb ccccc 的 char[] commend
int main(int argc, char *argv[])
{
    string commend = "";

    int i = 1;
    while (argv[i] != nullptr)
    {
        commend += argv[i++];
        commend += " ";
    }
    cout << commend << endl;

    return 0;
}