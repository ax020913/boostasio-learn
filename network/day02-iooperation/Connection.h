#pragma once

#include <boost/asio.hpp>
#include <iostream>

// #include "Session.h" // error
#include "Session.cpp"

using namespace std;
using namespace boost;

extern int accept_new_connection();

extern void use_buffer_str();
extern void use_buffer_array();
extern void use_const_buffer();
extern void use_strem_buffer();

extern int send_data_by_write_some();
extern int send_data_by_send();
extern int send_data_by_write();
extern int read_data_by_read_some();
extern int read_data_by_receive();
extern int read_data_by_read();

extern std::string read_data_by_until(boost::asio::ip::tcp::socket &sock, const char &str);
extern int asnyc_write_some();