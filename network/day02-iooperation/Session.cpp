#include "Session.h"

Session::Session(std::shared_ptr<boost::asio::ip::tcp::socket> sock)
    : _sock(sock), _send_pending(false), _recv_pending(false)
{
}

void Session::Connect(const boost::asio::ip::tcp::endpoint &endpoint)
{
    _sock->connect(endpoint);
}

// 多线程的时候可能会有发送数据的顺序乱序的问题
void Session::WriteToSocketErr(const std::string &buffer)
{
    _send_node = make_shared<messageNode>(buffer.c_str(), buffer.length());
    // 异步发送数据，因为异步所以不会一下发送完毕的
    this->_sock->async_write_some(
        boost::asio::buffer(_send_node->_msg, _send_node->_total_len),
        std::bind(&Session::WriteCallBackErr, this, std::placeholders::_1, std::placeholders::_2, _send_node));
}
void Session::WriteCallBackErr(const boost::system::error_code &error_code,
                               std::size_t bytes_transferred,
                               std::shared_ptr<messageNode> msg_node)
{
    if (bytes_transferred + msg_node->_cur_len < msg_node->_total_len)
    {
        _send_node->_cur_len += bytes_transferred;

        this->_sock->async_write_some(
            boost::asio::buffer(msg_node->_msg + _send_node->_cur_len,
                                _send_node->_total_len - _send_node->_cur_len),
            std::bind(&Session::WriteCallBackErr, this, std::placeholders::_1, std::placeholders::_2, _send_node));
    }
}

// STL 的 queue 的 emplace 是线程安全的吗？
// sock.async_write_some 多次发送完
void Session::WriteToSocket(const std::string &buffer)
{
    // 插入发送队列
    _send_queue.emplace(new messageNode(buffer.c_str(), buffer.length()));
    // pending 状态说明上一次有未发送完的数据(停止发送，避免发送数据乱序)
    // threadA 发送 "hello world"，发送 "hello" 完的时候，还有一部分没有发送完，_send_pending == true。
    // threadB 要也发送 "hello world"，如果没有 _send_pending == true。
    // 那么最终可能会发送成 "hello hello world world"
    if (_send_pending == true)
        return;

    // 异步发送数据，因为异步所以不会一下子发送完的
    this->_sock->async_write_some(
        boost::asio::buffer(buffer),
        std::bind(&Session::WriteCallBack, this, std::placeholders::_1, std::placeholders::_2));
    // 已经异步去发送了，设置为 true，其他的线程不能打扰当前正在发送的线程
    _send_pending = true;
}
void Session::WriteCallBack(const boost::system::error_code &error_code, std::size_t bytes_transferred)
{
    if (error_code.value() != 0)
    {
        std::cout << "Error, code is " << error_code.value() << " .Message: " << error_code.what() << std::endl;
        return;
    }

    // 取出队首元素即当前未发送完数据
    auto &send_data = _send_queue.front();
    send_data->_cur_len += bytes_transferred;
    // 数据未发送完，则继续发送
    if (send_data->_cur_len < send_data->_total_len)
    {
        this->_sock->async_write_some(
            boost::asio::buffer(send_data->_msg + send_data->_cur_len, send_data->_total_len - send_data->_cur_len),
            std::bind(&Session::WriteCallBack, this, std::placeholders::_1, std::placeholders::_2));
        return;
    }

    // 如果发送完，则 pop 出队首元素
    _send_queue.pop();

    // 如果队列为空，则说明所有的数据都已经发送完了，将pending设置为false
    if (_send_queue.empty())
        _send_pending = false;
    // 如果队列不为空，则继续将队首元素发送
    if (!_send_queue.empty())
    {
        auto &send_data = _send_queue.front();
        this->_sock->async_write_some(
            boost::asio::buffer(send_data->_msg + send_data->_cur_len, send_data->_total_len - send_data->_cur_len),
            std::bind(&Session::WriteCallBack, this, std::placeholders::_1, std::placeholders::_2));
    }
}

// 不能与 sock.async_write_some 混合使用
// sock.async_send 一次性发送完
void Session::WriteAllToSocket(const std::string &buffer)
{
    // 插入发送队列
    _send_queue.emplace(new messageNode(buffer.c_str(), buffer.length()));
    // pending 状态说明上一次有未发送完的数据(停止发送，避免发送数据乱序)
    // threadA 发送 "hello world"，发送 "hello" 完的时候，还有一部分没有发送完，_send_pending == true。
    // threadB 要也发送 "hello world"，如果没有 _send_pending == true。
    // 那么最终可能会发送成 "hello hello world world"
    if (_send_pending == true)
        return;

    // 异步发送数据，因为异步所以不会一下子发送完的
    this->_sock->async_send(
        boost::asio::buffer(buffer),
        std::bind(&Session::WriteAllCallBack, this, std::placeholders::_1, std::placeholders::_2));
    // 已经异步去发送了，设置为 true，其他的线程不能打扰当前正在发送的线程
    _send_pending = true;
}
void Session::WriteAllCallBack(const boost::system::error_code &error_code, std::size_t bytes_transferred)
{
    if (error_code.value() != 0)
    {
        std::cout << "Error occured! Error code = " << error_code.value()
                  << " .Message: " << error_code.what()
                  << std::endl;
        return;
    }

    // 如果发送完，则 pop 出队首元素
    _send_queue.pop();

    // 如果队列为空，则说明所有的数据都已经发送完了，将pending设置为false
    if (_send_queue.empty())
        _send_pending = false;
    // 如果队列不为空，则继续将队首元素发送
    if (!_send_queue.empty())
    {
        auto &send_data = _send_queue.front();
        this->_sock->async_send(
            boost::asio::buffer(send_data->_msg + send_data->_cur_len, send_data->_total_len - send_data->_cur_len),
            std::bind(&Session::WriteAllCallBack, this, std::placeholders::_1, std::placeholders::_2));
    }
}

// 不考虑粘包情况， 先用固定的字节接收
// sock.async_read_some 多次读取完
void Session::ReadFromSocket()
{
    if (_recv_pending == true)
        return;

    // 可以调用构造函数直接构造，但不可以用已经构造好智能指针赋值
    // auto _recv_node_ = std::make_shared<messageNode>(recvSize);
    // _recv_node = recv_node_;
    _recv_node = std::make_shared<messageNode>(recvSize);
    _sock->async_read_some(
        boost::asio::buffer(_recv_node->_msg, _recv_node->_total_len),
        std::bind(&Session::ReadCallBack, this, std::placeholders::_1, std::placeholders::_2));
    // 已经异步去接收了，设置为 true，其他的线程不能打扰当前正在接收的线程
    _recv_pending = true;
}
void Session::ReadCallBack(const boost::system::error_code &error_code, std::size_t bytes_transferred)
{
    _recv_node->_cur_len += bytes_transferred;
    // 没读完继续读取
    if (_recv_node->_cur_len < _recv_node->_total_len)
    {
        _sock->async_read_some(
            boost::asio::buffer(_recv_node->_msg + _recv_node->_cur_len, _recv_node->_total_len - _recv_node->_cur_len),
            std::bind(&Session::ReadCallBack, this, std::placeholders::_1, std::placeholders::_2));
        return;
    }

    // 将数据投递到队列里交给逻辑线程处理，此处省略
    // 如果读取完了，则将标记设置为false
    _recv_pending = false;
    // 指针置空
    _recv_node = nullptr;
}

// sock.async_receive 一次性读取完
void Session::ReadAllFromSocket(const std::string &buffer)
{
    if (_recv_pending == true)
        return;

    // 可以调用构造函数直接构造，但不可以用已经构造好智能指针赋值
    // auto _recv_node_ = std::make_shared<messageNode>(recvSize);
    // _recv_node = recv_node_;
    _recv_node = std::make_shared<messageNode>(recvSize);
    _sock->async_receive(
        boost::asio::buffer(_recv_node->_msg, _recv_node->_total_len),
        std::bind(&Session::ReadAllCallBack, this, std::placeholders::_1, std::placeholders::_2));
    // 已经异步去接收了，设置为 true，其他的线程不能打扰当前正在接收的线程
    _recv_pending = true;
}
void Session::ReadAllCallBack(const boost::system::error_code &error_code, std::size_t bytes_transferred)
{
    _recv_node->_cur_len += bytes_transferred;

    // 将数据投递到队列里交给逻辑线程处理，此处省略
    // 如果读取完了，则将标记设置为false
    _recv_pending = false;
    // 指针置空
    _recv_node = nullptr;
}

/*
// 测试上面的函数有没有语法错误
int main(void)
{
    std::string serverIp = "127.0.0.1";
    const unsigned short server_port = 33333;
    boost::asio::ip::address server_address = boost::asio::ip::address::from_string(serverIp.c_str());
    boost::asio::ip::tcp::endpoint endpoint(server_address, server_port);
    boost::asio::io_context ios;
    auto sock_ptr = std::make_shared<boost::asio::ip::tcp::socket>(ios, endpoint.protocol());
    Session session(sock_ptr);
    session.Connect(endpoint);
    std::string sendBuffer = "hello server";
    // session.WriteToSocket(sendBuffer);
    // session.WriteAllToSocket(sendBuffer);
    // session.ReadFromSocket();
    // session.ReadAllFromSocket(sendBuffer);
    session.WriteToSocketErr(sendBuffer);

    return 0;
}
*/