#pragma once

#include <boost/asio.hpp>
#include <iostream>
#include <queue>

using namespace std;
using namespace boost;

// 最大报文接收大小
const int recvSize = 1024;
class messageNode
{
public:
    messageNode(const char *msg, int total_len) : _total_len(total_len), _cur_len(0)
    {
        _msg = new char[total_len];
        memcpy(_msg, msg, total_len);
    }

    messageNode(int total_len) : _total_len(total_len), _cur_len(0)
    {
        _msg = new char[total_len];
    }

    ~messageNode()
    {
        delete[] _msg;
    }

    // private:
    // 消息的总长度
    int _total_len;
    // 当前消息的长度
    int _cur_len;
    // 消息的首地址
    char *_msg;
};

class Session
{
public:
    Session(std::shared_ptr<boost::asio::ip::tcp::socket> sock);
    // ~Session() {}

    void Connect(const boost::asio::ip::tcp::endpoint &endpoint);

    void WriteToSocket(const std::string &buffer);
    void WriteCallBack(const boost::system::error_code &error_code, std::size_t bytes_transferred);
    void WriteAllToSocket(const std::string &buffer);
    void WriteAllCallBack(const boost::system::error_code &error_code, std::size_t bytes_transferred);

    void ReadFromSocket();
    void ReadCallBack(const boost::system::error_code &error_code, std::size_t bytes_transferred);
    void ReadAllFromSocket(const std::string &buffer);
    void ReadAllCallBack(const boost::system::error_code &error_code, std::size_t bytes_transferred);

    // 有隐患的发送函数，不提倡
    void WriteToSocketErr(const std::string &buffer);
    void WriteCallBackErr(const boost::system::error_code &error_code, std::size_t bytes_transferred, std::shared_ptr<messageNode>);

private:
    std::queue<std::shared_ptr<messageNode>> _send_queue;

    std::shared_ptr<boost::asio::ip::tcp::socket> _sock;

    std::shared_ptr<messageNode> _send_node;
    std::shared_ptr<messageNode> _recv_node;

    bool _send_pending;
    bool _recv_pending;
};
