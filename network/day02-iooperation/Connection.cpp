#include "Connection.h"

int accept_new_connection()
{
    // 1. endpoint
    const int BackLogSize = 20;
    unsigned short port_num = 3333;
    boost::asio::ip::tcp::endpoint endpoint(
        boost::asio::ip::address_v4::any(),
        port_num);
    boost::asio::io_context ios;
    try
    {
        // 2. tcp::acceptor
        boost::asio::ip::tcp::acceptor acceptor(ios, endpoint.protocol());
        acceptor.bind(endpoint);
        acceptor.listen(BackLogSize);

        // 3. tcp::socket
        boost::asio::ip::tcp::socket sock(ios);
        acceptor.accept(sock);
    }
    catch (boost::system::system_error &error_code)
    {
        std::cout
            << "Error occured! Error code = " << error_code.code()
            << ". Message: " << error_code.what()
            << std::endl;
        return error_code.code().value();
    }

    return 0;
}

// boost::asio::buffer(const char* str) 构造 boost:asio::const_buffers_1
void use_buffer_str()
{
    boost::asio::const_buffers_1 output_buf = boost::asio::buffer("hello world");
}
// boost::asio::buffer(char[] str) 构造 boost:asio::const_buffers_1
void use_buffer_array()
{
    const int bufSizeBytes = 20;
    std::unique_ptr<char[]> buf(new char[bufSizeBytes]);
    auto input_buf = boost::asio::buffer(static_cast<void *>(buf.get()), bufSizeBytes);
    // input_buf 的类型也是 boost::asio::const_buffer_1
}
// BOOST_ASIO_NODISCARD inline BOOST_ASIO_CONST_BUFFER buffer(
//     const void* data, std::size_t size_in_bytes) BOOST_ASIO_NOEXCEPT
// {
//   return BOOST_ASIO_CONST_BUFFER(data, size_in_bytes);
// }
// # define BOOST_ASIO_CONST_BUFFER const_buffers_1

// boost::asio::const_buffer(char* str, size_t size) 构造 boost::asio::const_buffer
void use_const_buffer()
{
    std::string buf = "hello world";
    boost::asio::const_buffer asio_buf(buf.c_str(), buf.length());
    std::vector<boost::asio::const_buffer> buffers_sequence;
    buffers_sequence.push_back(asio_buf);
}

// 输入输出流 和 boost::asio::streambuf
void use_stream_buffer()
{
    // 输出缓冲区
    boost::asio::streambuf buf;
    std::ostream output(&buf);
    output << "Message1\nMessage2";

    // 从输出缓冲区中读取？？？？？
    std::istream input(&buf);
    std::string message1;
    std::getline(input, message1);
    std::getline(input, message1);

    std::cout
        << message1 << endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// sock.send 对于发送数据来说推荐（避免粘包问题，服务器底层的tcp接收缓冲区收到的数据为粘连在一起）
// sock.receive 对于接受数据来说推荐（可以记录下来用来排除错误）

// 直接写到下面的函数里面了（大项目当然得拆分了）
void write_to_socket(boost::asio::ip::tcp::socket &sock)
{
}
// client 通过 sock.write_some 发送部分数据
// 对于发送端来说不推荐。对于接受端来说推荐，可以记录下来用来排除错误
// 一次发送一点点，大概率不会全部发送出去。因为有tcp发送缓冲区和用户缓冲区 --> 毡包问题
int send_data_by_write_some()
{
    std::string serverIp = "127.0.0.1";
    unsigned short server_port = 3333;
    try
    {
        // 1. endpoint
        boost::asio::ip::address server_address = boost::asio::ip::address::from_string(serverIp.c_str());
        boost::asio::ip::tcp::endpoint endpoint(server_address, server_port);

        // 2. sock
        // boost::asio::io_service ios; // 也是可以的哦
        boost::asio::io_context ios;
        boost::asio::ip::tcp::socket sock(ios, endpoint.protocol());

        // 3. connect
        sock.connect(endpoint);

        // 4. 业务
        // write_to_socket(sock);
        std::string buf = "hello server";
        std::size_t total_bytes_written = 0;
        // 循环发送
        // write_some 函数每次返回写入的字节数
        // total_bytes_wirtten 是已经发送的字节数
        // 每一次发送 buf.length() - total_bytes_written 字节数据
        while (total_bytes_written != buf.length())
        {
            // total_bytes_written 得累加已发送的数据长度
            // buf.c_str() 得加上偏移地址，buf.length() - total_bytes_written) 为剩余待发送数据长度
            total_bytes_written += sock.write_some(
                boost::asio::buffer(buf.c_str() + total_bytes_written,
                                    buf.length() - total_bytes_written));
        }
    }
    catch (boost::system::system_error &error_code)
    {
        // 5. 异常
        std::cout << "Error occured! Error code = " << error_code.code()
                  << " .Message: " << error_code.what()
                  << std::endl;
        return error_code.code().value();
    }

    return 0;
}

// client 通过 sock.send 阻塞式的一次性发送数据
int send_data_by_send()
{
    std::string serverIp = "127.0.0.1";
    unsigned short server_port = 3333;
    try
    {
        // 1. endpoint
        boost::asio::ip::address server_address = boost::asio::ip::address::from_string(serverIp.c_str());
        boost::asio::ip::tcp::endpoint endpoint(server_address, server_port);

        // 2. sock
        // boost::asio::io_service ios; // 也是可以的哦
        boost::asio::io_context ios;
        boost::asio::ip::tcp::socket sock(ios, endpoint.protocol());

        // 3. connect
        sock.connect(endpoint);

        // 4. 业务
        std::string buf = "hello server";
        int send_length = sock.send(boost::asio::buffer(buf.c_str(), buf.length()));
        if (send_length <= 0)
        {
            // send_length == 0 说明 client 关闭了连接
            std::cout << "send failed" << std::endl;
            return -1;
        }
    }
    catch (boost::system::system_error &error_code)
    {
        // 5. 异常
        std::cout << "Error occured! Error code = " << error_code.code()
                  << " .Message: " << error_code.what()
                  << std::endl;
        return error_code.code().value();
    }

    return 0;
}

// client 通过 boost::asio::write 阻塞式的一次性发送数据
int send_data_by_write()
{
    std::string serverIp = "127.0.0.1";
    unsigned short server_port = 3333;
    try
    {
        // 1. endpoint
        boost::asio::ip::address server_address = boost::asio::ip::address::from_string(serverIp.c_str());
        boost::asio::ip::tcp::endpoint endpoint(server_address, server_port);

        // 2. sock
        // boost::asio::io_service ios; // 也是可以的哦
        boost::asio::io_context ios;
        boost::asio::ip::tcp::socket sock(ios, endpoint.protocol());

        // 3. connect
        sock.connect(endpoint);

        // 4. 业务
        std::string buf = "hello server";
        int send_length = boost::asio::write(sock, boost::asio::buffer(buf.c_str(), buf.length()));
        if (send_length <= 0)
        {
            std::cout << "send failed" << std::endl;
            return -1;
        }
    }
    catch (boost::system::system_error &error_code)
    {
        // 5. 异常
        std::cout << "Error occured! Error code = " << error_code.code()
                  << " .Message: " << error_code.what()
                  << std::endl;
        return error_code.code().value();
    }

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////
// sock.send 对于发送数据来说推荐（避免粘包问题，服务器底层的tcp接收缓冲区收到的数据为粘连在一起）
// sock.receive 对于接受数据来说推荐（可以记录下来用来排除错误）

// 直接写到下面的函数里面了（大项目当然得拆分了）
std::string read_from_socket(boost::asio::ip::tcp::socket &sock)
{
    // const unsigned char MESSAGESIZE = 7;
    // char readBuffer[MESSAGESIZE];
    // std::size_t total_bytes_read = 0;

    // while (total_bytes_read != MESSAGESIZE)
    // {
    //     total_bytes_read += sock.read_some(
    //         boost::asio::buffer(readBuffer + total_bytes_read,
    //                             MESSAGESIZE - total_bytes_read));
    // }

    // return std::string(readBuffer, total_bytes_read);
}
// client 通过 sock.read_some 接收部分数据
int read_data_by_read_some()
{
    std::string serverIp = "127.0.0.1";
    unsigned short server_port = 3333;

    try
    {
        // 1. endpoint
        boost::asio::ip::address server_address = boost::asio::ip::address::from_string(serverIp.c_str());
        boost::asio::ip::tcp::endpoint endpoint(server_address, server_port);

        // 2. sock
        // boost::asio::io_service ios; // 也是可以的哦
        boost::asio::io_context ios;
        boost::asio::ip::tcp::socket sock(ios, endpoint.protocol());

        // 3. connect
        sock.connect(endpoint);

        // 4. 业务
        // read_from_socket(sock);
        const unsigned char MESSAGESIZE = 7;
        char readBuffer[MESSAGESIZE];
        std::size_t total_bytes_read = 0;
        while (total_bytes_read != MESSAGESIZE)
        {
            total_bytes_read += sock.read_some(
                boost::asio::buffer(readBuffer + total_bytes_read,
                                    MESSAGESIZE - total_bytes_read));
        }
    }
    catch (boost::system::system_error &error_code)
    {
        // 5. 异常
        std::cout << "Error occuredd! Error code = " << error_code.code()
                  << " .Message: " << error_code.what()
                  << std::endl;
        return error_code.code().value();
    }

    return 0;
}

// client 通过 sock.receive 阻塞式的一次性的接收全部的数据
int read_data_by_receive()
{
    std::string serverIp = "127.0.0.1";
    unsigned short server_port = 3333;

    try
    {
        // 1. endpoint
        boost::asio::ip::address server_address = boost::asio::ip::address::from_string(serverIp.c_str());
        boost::asio::ip::tcp::endpoint endpoint(server_address, server_port);

        // 2. sock
        boost::asio::io_context ios;
        boost::asio::ip::tcp::socket sock(ios, endpoint.protocol());

        // 3. connect
        sock.connect(endpoint);

        // 4. 业务
        const unsigned char REDABUFFERSIZE = 7;
        char readBuffer[REDABUFFERSIZE];
        int read_length = sock.receive(boost::asio::buffer(readBuffer, REDABUFFERSIZE));
        if (read_length <= 0)
        {
            std::cout << "receive failed" << std::endl;
            return -1;
        }
    }
    catch (boost::system::system_error &error_code)
    {
        // 5. 异常
        std::cout << "Error occured! Error code = " << error_code.code()
                  << " .Message: " << error_code.what()
                  << std::endl;
        return error_code.code().value();
    }

    return 0;
}

// client 通过 boost::asio::read 阻塞式的一次性的接收全部的数据
int read_data_by_read()
{
    std::string serverIp = "127.0.0.1";
    unsigned short server_port = 3333;

    try
    {
        // 1. endpoint
        boost::asio::ip::address server_address = boost::asio::ip::address::from_string(serverIp.c_str());
        boost::asio::ip::tcp::endpoint endpoint(server_address, server_port);

        // 2. sock
        boost::asio::io_context ios;
        boost::asio::ip::tcp::socket sock(ios, endpoint.protocol());

        // 3. connect
        sock.connect(endpoint);

        // 4. 业务
        const unsigned char REDABUFFERSIZE = 7;
        char readBuffer[REDABUFFERSIZE];
        int read_length = boost::asio::read(sock, boost::asio::buffer(readBuffer, REDABUFFERSIZE));
        if (read_length <= 0)
        {
            std::cout << "receive failed" << std::endl;
            return -1;
        }
    }
    catch (boost::system::system_error &error_code)
    {
        // 5. 异常
        std::cout << "Error occured! Error code = " << error_code.code()
                  << " .Message: " << error_code.what()
                  << std::endl;
        return error_code.code().value();
    }

    return 0;
}

// 知道接收到 str 才停止接收数据
std::string read_data_by_until(boost::asio::ip::tcp::socket &sock, const char &str)
{
    boost::asio::streambuf buf;

    boost::asio::read_until(sock, buf, str);

    std::string message;
    std::istream input_stream(&buf);
    std::getline(input_stream, message);

    return message;
}

// 异步读取(非阻塞)
int async_write_data()
{
    std::string serverIp = "127.0.0.1";
    unsigned short server_port = 3333;

    try
    {
        boost::asio::ip::address server_address = boost::asio::ip::address::from_string(serverIp.c_str());
        boost::asio::ip::tcp::endpoint endpoint(server_address, server_port);

        boost::asio::io_context ios;
        auto sock_ptr = std::make_shared<boost::asio::ip::tcp::socket>(ios, endpoint.protocol());

        auto session_ptr = std::make_shared<Session>(sock_ptr);
        session_ptr->Connect(endpoint);
        std::string str = "hello server";
        session_ptr->WriteToSocket(str);
        ios.run();
    }
    catch (boost::system::system_error &error_code)
    {
        std::cout << "Error occured! Error code = " << error_code.code()
                  << error_code.what()
                  << std::endl;
        return error_code.code().value();
    }

    return 0;
}

// 测试上面的函数有没有语法错误
int main(void)
{
    // accept_new_connection();
    // use_buffer_str();
    // use_buffer_array();
    // use_const_buffer();
    // use_stream_buffer();

    // send_data_by_read_some();
    // send_data_by_send();
    // send_data_by_write();

    // read_data_by_read_some();
    // read_data_by_receive();
    // read_data_by_read();

    // std::string serverIp = "127.0.0.1";
    // const unsigned short server_port = 33333;
    // boost::asio::ip::address server_address = boost::asio::ip::address::from_string(serverIp.c_str());
    // boost::asio::ip::tcp::endpoint endpoint(server_address, server_port);
    // boost::asio::io_context ios;
    // boost::asio::ip::tcp::socket sock(ios, endpoint);
    // const char ch = '\n';
    // // char ch = '\n';
    // std::string readBuffer = read_data_by_until(sock, ch);
    // std::cout << "readBuffer: " << readBuffer << std::endl;

    async_write_data();

    return 0;
}