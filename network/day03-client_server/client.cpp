#include <iostream>
#include <string>
#include <boost/asio.hpp>

using namespace std;
using namespace boost::asio;

const int MAX_LENGTH = 1024;

int main(void)
{
    try
    {
        // 1. endpoint
        std::string server_ip = "127.0.0.1";
        const unsigned short server_port = 3333;
        boost::asio::io_service ios;
        boost::asio::ip::address server_address = boost::asio::ip::address::from_string(server_ip);
        boost::asio::ip::tcp::endpoint endpoint(server_address, server_port);

        // 2. sock
        boost::asio::ip::tcp::socket sock(ios);

        // 3. connect
        boost::system::error_code error_code = boost::asio::error::host_not_found; // 先给一个默认值
        sock.connect(endpoint, error_code);
        if (error_code)
        {
            std::cout
                << "connect failed, code is " << error_code.message()
                << " error msg is " << error_code.message()
                << std::endl;
            return error_code.value();
        }

        // 4. 业务
        for (;;)
        {
            // (对于 client 来说，发送推荐boost::asio::write、sock.send一次性发送完毕。接收推荐boost::asio::read、sock.receive一次性接收完毕)
            // (对于 server 来说，发送推荐boost::asio::write、sock.send一次性发送完毕。接收推荐sock.read_some多次接收)
            std::cout << "Enter message: ";
            char request[MAX_LENGTH];
            std::cin.getline(request, MAX_LENGTH);
            size_t request_length = strlen(request);
            if (request_length == 0) // 防止client输入回车之后就不能再对server发起请求的情况了
                continue;
            boost::asio::write(sock, boost::asio::buffer(request, request_length));

            // 注意第二个参数：不传递可能会乱码，传了注意传多少
            // client 发送请求：
            // client send：client send 是把client用户缓冲区数据，拷贝到client用户内核tcp发送缓冲区。
            // ......
            // server 接收请求并处理返回：
            // server receive/read_some：server receive 是把server内核tcp接收缓冲区数据，拷贝到server用户缓冲区。
            // server send: server send 是把server用户缓冲区数据，拷贝到server用户内核tcp发送缓冲区。
            // ......
            // client 接收响应：
            // client receive：client receive 是把client内核tcp接收缓冲区数据，拷贝到client用户缓冲区。

            char replay[MAX_LENGTH];
            // 对于client的一次性接收数据的话，接收的长度可以不设置：下面的两行，设置接收数据的长度不客观的话，可能接收到多余或者少的数据
            // size_t replay_length = boost::asio::read(sock, boost::asio::buffer(replay, replay_length)); // error
            // size_t replay_length = sock.receive(boost::asio::buffer(replay, MAX_LENGTH)); // error
            size_t replay_length = sock.receive(boost::asio::buffer(replay));
            replay[replay_length] = '\0';
            std::cout << "Replay length = " << replay_length
                      << " message is: " << replay << std::endl;
            // std::cout.write(replay, replay_length);
            // std::cout << "\n";
            // getchar();
        }
    }
    catch (std::exception &error_code)
    {
        std::cerr << "Exception: " << error_code.what() << std::endl;
    }

    return 0;
}

// g++ -o client client.cpp -std=c++11 -lpthread