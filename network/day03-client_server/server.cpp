#include <iostream>
#include <string>
#include <cstdlib>
#include <set>
// #include <pthread.h>
#include <thread>
#include <boost/asio.hpp>

using namespace std;
using namespace boost::asio;

const int MAX_LENGTH = 1024;
typedef std::shared_ptr<boost::asio::ip::tcp::socket> sock_ptr;
std::set<std::shared_ptr<std::thread>> thread_set;

// 类似于线程池了，从属线程处理业务
void sessionCallBack(sock_ptr sock)
{
    try
    {
        char data[MAX_LENGTH];
        boost::system::error_code error_code;
        for (;;)
        {
            size_t length = sock->read_some(boost::asio::buffer(data, MAX_LENGTH), error_code);
            data[length] = '\0';
            if (error_code == boost::asio::error::eof)
            {
                std::cout << "connection closed by perr" << std::endl;
                return;
            }
            else if (error_code)
            {
                throw boost::system::system_error(error_code);
            }

            std::cout << "receive from " << sock->remote_endpoint().address().to_string() << std::endl;
            std::cout << "receive message is: " << data << std::endl;

            // 回复信息
            const char *data_ = "hello client";
            // int send_length = boost::asio::write(*sock, boost::asio::buffer(data, length));
            int send_length = sock->send(boost::asio::buffer(data_, strlen(data_)));
            std::cout << "server send length = " << send_length << std::endl;
        }
    }
    catch (std::exception &error_code)
    {
        std::cerr << "Exception " << error_code.what() << std::endl;
    }
}

void server()
{
    // 1. endpoint
    const unsigned short server_port = 3333;
    // boost::asio::ip::address server_address = boost::asio::ip::tcp::v4(); // error
    // boost::asio::ip::tcp server_address = boost::asio::ip::tcp::v4(); //
    auto server_address = boost::asio::ip::tcp::v4(); //
    boost::asio::ip::tcp::endpoint endpoint(server_address, server_port);

    // 2. acceptor
    boost::asio::io_service ios;
    boost::asio::ip::tcp::acceptor acceptor(ios, endpoint);

    // 3. sock
    // auto sock = sock_ptr(new boost::asio::ip::tcp::socket(ios));

    // 3. listen
    // inline void boost::asio::ip::tcp::acceptor::listen(int backlog = 128)
    acceptor.listen(7);

    for (;;)
    {
        // 4. sock
        auto sock = sock_ptr(new boost::asio::ip::tcp::socket(ios));

        // 5. accept
        acceptor.accept(*sock);

        // 6. 业务
        auto thread = std::make_shared<std::thread>(sessionCallBack, sock);
        thread_set.insert(thread);
    }
}

int main(void)
{
    try
    {
        // server(); // 直接写到下面

        // 1. endpoint
        const unsigned short server_port = 3333;
        // boost::asio::ip::address server_address = boost::asio::ip::tcp::v4(); // error
        // boost::asio::ip::tcp server_address = boost::asio::ip::tcp::v4(); //
        auto server_address = boost::asio::ip::tcp::v4(); //
        boost::asio::ip::tcp::endpoint endpoint(server_address, server_port);

        // 2. acceptor
        boost::asio::io_service ios;
        boost::asio::ip::tcp::acceptor acceptor(ios, endpoint);

        // 3. sock
        // auto sock = sock_ptr(new boost::asio::ip::tcp::socket(ios));

        // 3. listen
        // inline void boost::asio::ip::tcp::acceptor::listen(int backlog = 128)
        acceptor.listen(7);

        // 主线程负责接收新的连接，并创建从属thread去处理client的业务
        for (;;)
        {
            // 4. sock
            auto sock = sock_ptr(new boost::asio::ip::tcp::socket(ios));

            // 5. accept
            acceptor.accept(*sock);

            // 6. 业务
            auto thread = std::make_shared<std::thread>(sessionCallBack, sock);
            thread_set.insert(thread);
        }

        for (auto &thread_ : thread_set)
            thread_->join();
    }
    catch (std::exception &error_code)
    {
        std::cerr << "Exception " << error_code.what() << std::endl;
    }

    return 0;
}

// g++ -o server server.cpp -std=c++11 -lpthread