1. 下面四个 from_string 重载函数
    // ctrl 点进去看 boost::asio::ip::address::from_string 的源码可以看到四个重载函数中，
    // 第一个参数是 const std::string& str 类型的函数最终调用的都是对应的参数都是 const char* str 的函数（相当于api函数内部帮你进行了一层的转化）。

/// Create an address from an IPv4 address string in dotted decimal form,
/// or from an IPv6 address in hexadecimal notation.
BOOST_ASIO_DECL static address from_string(const char* str);

/// Create an address from an IPv4 address string in dotted decimal form,
/// or from an IPv6 address in hexadecimal notation.
BOOST_ASIO_DECL static address from_string(
    const char* str, boost::system::error_code& ec);

/// Create an address from an IPv4 address string in dotted decimal form,
/// or from an IPv6 address in hexadecimal notation.
BOOST_ASIO_DECL static address from_string(const std::string& str);

/// Create an address from an IPv4 address string in dotted decimal form,
/// or from an IPv6 address in hexadecimal notation.
BOOST_ASIO_DECL static address from_string(
    const std::string& str, boost::system::error_code& ec);


2. 关于源码中 Multicast Address 
环回地址：
bool address::is_loopback() const
{
  return (type_ == ipv4)
    ? ipv4_address_.is_loopback()
    : ipv6_address_.is_loopback();
}
未说明地址：
bool address::is_unspecified() const
{
  return (type_ == ipv4)
    ? ipv4_address_.is_unspecified()
    : ipv6_address_.is_unspecified();
}
多播地址：
bool address::is_multicast() const
{
  return (type_ == ipv4)
    ? ipv4_address_.is_multicast()
    : ipv6_address_.is_multicast();
}
Multicast Address:
被称为组的多台主机使用，获取一个多播目的地址。这些主机不需要在地理上在一起。
如果有任何数据包被发送到这个多播地址，它将被分发到与该多播地址对应的所有接口。每个节点都以相同的方式配置。
简单来说，一个数据包同时发送到多个目的地。

3. 比较运算符重载函数的设计
/// Compare addresses for ordering.
BOOST_ASIO_DECL friend bool operator<(const address& a1, const address& a2);

/// Compare addresses for ordering.
friend bool operator>(const address& a1, const address& a2)
{
return a2 < a1;
}

/// Compare addresses for ordering.
friend bool operator<=(const address& a1, const address& a2)
{
return !(a2 < a1);
}

/// Compare addresses for ordering.
friend bool operator>=(const address& a1, const address& a2)
{
return !(a1 < a2);
}

4. .ipp 文件
.IPP文件扩展名的详细信息: Inline Guard Macro File 文件类型Inline Guard Macro File类别开发者文件 开发者N/A文件格式Text 描述IPP文件是Inline Guard Macro File。

5. 一个int server_end_point()函数，没有用到pthread，怎么还需要-lpthread呢？
盲猜是asio的api用到了pthread的api造成的
