#include "endPoint.h"

// using namespace boost;
using namespace boost::asio;

int client_end_point()
{
    std::string raw_ip_address = "127.0.0.1";
    unsigned short port_num = 3333;

    boost::system::error_code error_code;
    // ctrl 点进去看 boost::asio::ip::address::from_string 的源码可以看到四个重载函数中，
    // 第一个参数是 const std::string& str 类型的函数最终调用的都是对应的参数都是 const char* str 的函数（相当于api函数内部帮你进行了一层的转化，所以下面的两行代码是等效的）。
    // boost::asio::ip::address ip_address = boost::asio::ip::address::from_string(raw_ip_address.c_str(), error_code);
    ip::address ip_address = ip::address::from_string(raw_ip_address, error_code);
    if (error_code.value() != 0)
    {
        std::cout
            << "Failed to parse the IP address. Error code = "
            << error_code.value() << " .Message: " << error_code.message() << std::endl;
    }

    // typedef boost::asio::ip::basic_endpoint<boost::asio::ip::tcp> boost::asio::ip::tcp::endpoint
    // basic_endpoint(const boost::asio::ip::address& addr, unsigned short port_num)
    boost::asio::ip::tcp::endpoint endpoint(ip_address, port_num);

    return 0;
}

int server_end_point()
{
    unsigned short port_num = 3333;

    // 是任意地址，任意的client地址都是可以访问我们的server的
    boost::asio::ip::address ip_address = boost::asio::ip::address_v6::any();

    boost::asio::ip::tcp::endpoint endpoint(ip_address, port_num);

    return 0;
}

int create_tcp_socket()
{
    // 奇怪，昨天晚上还报错：命名空间 "boost::asio" 没有成员 "io_context"，刚刚去执行了下面的命令就可以了(相当于是重新搭建了一下centos的boost使用环境)：
    // yum install boost
    // yum install boost-devel
    // cd boost_1_82_0/
    // ./bootstrap.sh --with-libraries=all --with-toolset=gcc   // --with-liraries：需要编译的库 --with-toolset：编译时使用的编译器
    // ./b2 install --prefix=/usr  // 默认头文件在/usr/local/include，库文件在/usr/local/lib

    // 创建 boost::asio::ip::tcp::socket 通信 sock 需要上下文 boost::asio::io_context
    boost::asio::io_context ios;
    boost::asio::ip::tcp::socket sock(ios);

    boost::asio::ip::tcp protocol = boost::asio::ip::tcp::v4();
    boost::system::error_code error_code;
    sock.open(protocol, error_code);
    if (error_code.value() != 0)
    {
        std::cout
            << "Failed to open the socket! Error code = "
            << error_code.value()
            << " .Message: " << error_code.message() << std::endl;
        return error_code.value();
    }

    return 0;
}

int create_acceptor_socket()
{
    boost::asio::io_context ios;
    boost::asio::ip::tcp::acceptor acceptor(ios);

    boost::asio::ip::tcp protocol = boost::asio::ip::tcp::v6();
    boost::system::error_code error_code;
    acceptor.open(protocol, error_code);
    if (error_code.value() != 0)
    {
        std::cout
            << "Failed to open the acceptor socket!"
            << "Error code = "
            << error_code.value() << " .Message: " << error_code.message() << std::endl;
        return error_code.value();
    }

    return 0;
}

int bind_acceptor_socket()
{
    // 1. 创建 server 的 endpoint
    unsigned short port_num = 3333;
    boost::asio::ip::tcp::endpoint endpoint(
        boost::asio::ip::address_v4::any(),
        port_num);

    // 2. 创建 tcp::acceptor
    boost::asio::io_context ios;
    boost::asio::ip::tcp::acceptor acceptor(ios, endpoint.protocol());

    // 3. acceptor bind endpoint
    boost::system::error_code error_code;
    acceptor.bind(endpoint, error_code);
    if (error_code.value() != 0)
    {
        std::cout
            << "Failed to bind the acceptor socket."
            << "Error code = " << error_code.value()
            << " .Message: " << error_code.message()
            << std::endl;
        return error_code.value();
    }

    return 0;
}

int connect_to_end()
{
    // 1. 创建 client 的 endpoint
    std::string raw_ip_address = "127.0.0.1";
    boost::asio::ip::address client_address = boost::asio::ip::address::from_string(raw_ip_address.c_str());
    unsigned short port_num = 3333;
    boost::asio::ip::tcp::endpoint endpoint(client_address, port_num);

    // 2. 创建 tcp::socket
    boost::asio::io_context ios;
    boost::asio::ip::tcp::socket sock(ios, endpoint.protocol());

    // 3. sock connect endpoint
    boost::system::error_code error_code;
    sock.connect(endpoint, error_code);
    if (error_code.value() != 0)
    {
        std::cout
            << "Failed to connect the endpoint. "
            << "Error code = " << error_code.value()
            << " .Message: " << error_code.message()
            << std::endl;
        return error_code.value();
    }

    // 上面我们是设置了 boost::system::error_code，也是可以抛异常的
    // try{
    //     // ......
    // }catch(boost::system::error_code& error_code){
    //     // ......
    // }

    return 0;
}

// 大部分我们的 client 是清楚 server 的域名，所以得有一个 dns 解析一下域名
int dns_connect_to_end()
{
    std::string host = "samplehost.book";
    std::string port_num = "3333";
    boost::asio::ip::tcp::resolver::query resolver_query(host, port_num,
                                                         boost::asio::ip::tcp::resolver::query::numeric_service);

    boost::asio::io_context ios;
    boost::asio::ip::tcp::resolver resolver(ios);

    boost::asio::ip::tcp::resolver::iterator it = resolver.resolve(resolver_query);
    boost::asio::ip::tcp::socket sock(ios);
    boost::system::error_code error_code;
    boost::asio::connect(sock, it, error_code);
    if (error_code.value() != 0)
    {
        std::cout
            << "Failed to connect the socket. "
            << "Error code = " << error_code.value()
            << " .Message: " << error_code.message()
            << std::endl;
        return error_code.value();
    }

    return 0;
}
