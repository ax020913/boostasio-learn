#ifndef __endpoint_h
#define __endpoint_h

#pragma once

#include <iostream>
#include <boost/asio.hpp>

// 防止命名空间的重叠，造成歧义
// using namespace std;
using namespace boost;

// extern 表示的是一个函数声明
extern int client_end_point();
extern int server_end_point();
extern int create_tcp_socket();
extern int create_acceptor_socket();

extern int bind_acceptor_socket();
extern int connect_to_end();
extern int dns_connect_to_end();

#endif

