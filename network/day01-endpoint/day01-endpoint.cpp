#include <iostream>
#include "endPoint.cpp"

using namespace std;
using namespace boost;

/*
int main()
{
    // Asio 所有的程序都至少需要一个I/O执行上下文，比如 io_context 或者 thread_pool 对象
    // I/O执行上下文提供了对I/O功能的访问
    // 这里我们在主函数中首先声明一个io_context类型的对象。
    boost::asio::io_context io;

    // 接下来我们声明一个boost::asio::steady_timer对象
    // Asio中提供I/O功能的这些核心类，总是需要一个执行上下文的引用，作为其构造函数的第一个参数
    // steady_timer构造函数的第二个参数是一个duration类型的时间
    // 这里将计时器设置为从现在起 5 秒后到期
    boost::asio::steady_timer t(io, boost::asio::chrono::seconds(5));

    // 在这个简单的示例中，我们对计时器执行阻塞等待。也就是说，对 stable_timer::wait() 的调用不会立即返回，会一直等到计时器到期才会返回
    // 计时器始终处于两种状态之一：“过期”或“未过期”。如果在过期的定时器上调用 stable_timer::wait() 函数，它将立即返回。
    t.wait();

    // 最后，我们打印强制性的 "Hello, world!" 消息来显示计时器何时到期。
    std::cout << "Hello, world!" << std::endl;

    return 0;
}
*/

/*
int main(void)
{
    client_end_point();
    server_end_point();
    create_tcp_socket();

    bind_acceptor_socket();
    connect_to_end();
    dns_connect_to_end();

    return 0;
}
*/