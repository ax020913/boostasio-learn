#include <iostream>
#include <boost/asio.hpp>
#include <thread>

using namespace std;
using namespace boost::asio::ip;

const int MAX_LENGTH = 1024 * 2;
const int HEAD_LENGTH = 2;

int main(void)
{
    try
    {
        // 1. endpoint
        std::string server_ip = "127.0.0.1";
        const unsigned short server_port = 3333;
        boost::asio::io_service ios;
        boost::asio::ip::address server_address = boost::asio::ip::address::from_string(server_ip);
        boost::asio::ip::tcp::endpoint endpoint(server_address, server_port);

        // 2. sock
        boost::asio::ip::tcp::socket sock(ios);

        // 3. connect
        boost::system::error_code error_code = boost::asio::error::host_not_found; // 先给一>个默认值
        sock.connect(endpoint, error_code);
        if (error_code)
        {
            std::cout
                << "connect failed, code is " << error_code.message()
                << " error msg is " << error_code.message()
                << std::endl;
            return error_code.value();
        }

        // 4. 业务
        std::thread send_thread([&sock]()
                                {
            for(;;){
                std::this_thread::sleep_for(std::chrono::milliseconds(2));
                const char* request = "hello world";
                short request_length = strlen(request);
                char send_data[MAX_LENGTH] = {0};
                // 转化为网络字节序
                short request_host_length = boost::asio::detail::socket_ops::host_to_network_short(request_length);
                memcpy(send_data, &request_host_length, HEAD_LENGTH);
                memcpy(send_data + HEAD_LENGTH, request, request_length);
                boost::asio::write(sock, boost::asio::buffer(send_data, request_length + HEAD_LENGTH));
            } });

        std::thread recv_thread([&sock]()
                                {
            for(;;){
                std::this_thread::sleep_for(std::chrono::milliseconds(2));
                std::cout << "begin to receive......" << std::endl;
                char reply_head[HEAD_LENGTH] = {0};
                size_t reply_length = boost::asio::read(sock, boost::asio::buffer(reply_head, HEAD_LENGTH));
                short msg_len = 0;
                memcpy(&msg_len, reply_head, HEAD_LENGTH);
                // 转化为本地字节序
                msg_len = boost::asio::detail::socket_ops::network_to_host_short(msg_len);
                std::cout << "receive MsgNode.size() = " << msg_len << std::endl;

                char reply_msg[MAX_LENGTH] = {0};
                size_t msg_length = boost::asio::read(sock, boost::asio::buffer(reply_msg, msg_len));
                // if(msg_length != msg_len)
                //     return;
                // std::cout << "reply is: " << std::cout.write(reply_msg, msg_length) << std::endl; // error
                std::cout << "reply is: ";
                std::cout.write(reply_msg, msg_length) << std::endl;
                std::cout << "reply length is: " << msg_length << std::endl;
            } });

        send_thread.join();
        recv_thread.join();
    }
    catch (boost::system::error_code &error_code)
    {
        std::cout << "Exception Message: " << error_code.message()
                  << " .Error Code: " << error_code.value()
                  << std::endl;
    }
    // catch (std::exception &error_code)
    // {
    //     std::cout << "Exception: " << error_code.what() << std::endl;
    //     return -1;
    // }

    return 0;
}
