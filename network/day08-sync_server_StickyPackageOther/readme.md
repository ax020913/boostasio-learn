### 一. 粘包处理的另一种方式使用 `boost::asio::async_read` 函数一次性读取指定长度的内容
流程：
```
// g++ -o ma Server_StickyPackage.cpp -std=c++11 -lpthread -lboost_system
// ./ma
void Session::Start()
{
    _recv_head_node->Clear();
    boost::asio::async_read(_sock, boost::asio::buffer(_recv_head_node->_data, HEAD_LENGTH),
                            std::bind(&Session::handle_read_head, this, std::placeholders::_1, std::placeholders::_2, shared_from_this()));
}

void Session::handle_read_head(const boost::system::error_code &error_code, size_t bytes_transferred, std::shared_ptr<Session> shared_self)
{
    // ...
    _recv_msg_node = make_shared<MsgNode>(data_len);
    boost::asio::async_read(_sock, boost::asio::buffer(_recv_msg_node->_data, _recv_msg_node->_total_len),
                            std::bind(&Session::handle_read_msg, this, std::placeholders::_1, std::placeholders::_2, shared_from_this()));
    // ...
}

void Session::handle_read_msg(const boost::system::error_code &error_code, size_t bytes_transferred, std::shared_ptr<Session> shared_self)
{
    // ...
    _recv_head_node->Clear();
    boost::asio::async_read(_sock, boost::asio::buffer(_recv_head_node->_data, HEAD_LENGTH),
                            std::bind(&Session::handle_read_head, this, std::placeholders::_1, std::placeholders::_2, shared_self));
    // ...
}
```
有一个 `client` 来了的时候，是会创建一个 `Session` 会话的。
1. 会直接启动 `Start` 函数。
```
    _recv_head_node->Clear();
    boost::asio::async_read(_sock, boost::asio::buffer(_recv_head_node->_data, HEAD_LENGTH),
                            std::bind(&Session::handle_read_head, this, std::placeholders::_1, std::placeholders::_2, shared_from_this()));
```
2. `_recv_head_node->Clear()` 直接清空接收头部数据的空间。
3. 使用 `boost::asio::async_read` 函数来实现另一种粘包处理方式，**读取指定 `HEAD_LENGTH` 长度的数据到 `_recv_head_node->_data`**。也就是后面要读取消息的字节长度。
![Alt text](image-1.png)

上面都取到了就会调用设置的回调函数 `handle_read_head`。
1. 取出具体长度是多少 `data_len`，再使用 `boost::asio::async_read` 函数来读取 `data_len` 长度的数据到 `_recv_msg_node->_data`。
2. 读取到了就调用设置好的回调函数 `handle_read_msg` 去处理数据。
3. 发送处理好的数据 `Send(_recv_msg_node->_data, _recv_msg_node->_total_len);`。
4. 再循环了：
```
    _recv_head_node->Clear();
    boost::asio::async_read(_sock, boost::asio::buffer(_recv_head_node->_data, HEAD_LENGTH),
                            std::bind(&Session::handle_read_head, this, std::placeholders::_1, std::placeholders::_2, shared_self));
```

### 二. 上面发送的时候，虽然是 `Send` 了一次，但是也是把发送了多长字节的数据，写入到了前两个字节中
上面的问题也是为什么，下面两个构造函数的问题。
这个函数是发送数据的数据用的，也就是 `Send(_recv_msg_node->_data, _recv_msg_node->_total_len);` 为什么传递两个参数，第二个长度就是 `Send` 函数里面：`_send_queue.push(make_shared<MsgNode>(msg, max_length));` 构造 `MsgNode` 的时候用的。调用的是下面的构造函数。
```
MsgNode(const char *msg, int max_len)
    : _cur_len(0), _total_len(max_len + HEAD_LENGTH)
{
    _data = new char[_total_len + 1]();                                                 // '\0' 结尾
    int max_len_host = boost::asio::detail::socket_ops::host_to_network_short(max_len); // 转为网络字节序(接收和send都要转化)
    memcpy(_data, &max_len_host, HEAD_LENGTH);                                          // _recv_msg_node->_total
    memcpy(_data + HEAD_LENGTH, msg, max_len);                                          // _recv_msg_node->_data
    _data[_total_len] = '\0';
}
```

其他的都是调用下面的构造函数像：
接收头部的节点的大小是 `Session` 会话构造函数的时候设置好的：`_recv_head_node = std::make_shared<MsgNode>(HEAD_LENGTH);`
接收消息的节点的大小是知道了接收多少的数据时候设置的：`_recv_msg_node = make_shared<MsgNode>(data_len);`
```
MsgNode(short max_len) : _total_len(max_len), _cur_len(0)
{
    _data = new char[_total_len + 1]();
    _data[_total_len] = '\0';
}
```

**相当于是把接收数据的节点和发送数据的节点，用两个构造函数给区分开了，后面会单独设置 `class recvNode` 和 `class sendNode` 的。**


