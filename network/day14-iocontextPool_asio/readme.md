![Alt text](image-2.png)

### 1. 构造函数和析构函数
#### 1.1 `io_context` 池的初始化
下面图片中的写法是因为 `std::unique_ptr<Work>(new Work(_ioServices[i]));` 是一个右值 ？？？。
看起来也是复制和拷贝，但其实是移动赋值，毕竟是 `unique_ptr` 一个右值。

![Alt text](image.png)

也是可以用下面的方式：
```
    // 官方不推荐
    _works[i].reset(new Work(_ioServices[i]));  
```
但是不能：
```
    // unique ptr 不能复制和拷贝
    auto unptr = std::unique_ptr<Work>(new Work(_ioServices[i]));
    _works[i] = unptr;
```

#### 1.2 线程池的初始化
下面的方式不太好：
![Alt text](image-1.png)

#### 1.3 析构函数
```
AsioIOServicePool::~AsioIOServicePool(){
    std::cout << ""
}
```

### 2. `GetIOService` 函数
简单轮询的方式使用每一个 `io_context&`，如果考虑每一个 `io_context` 的压力的话，还得......，有点像负载均衡。

```
boost::asio::io_context& AsioIOServicePool::GetIOService() {
    auto& service = _ioServices[_nextIOService++];
    if (_nextIOService == _ioServices.size()) {
        _nextIOService = 0;
    }
    return service;
}
```


### 3. `Stop` 函数
阻塞函数
```
void AsioIOServicePool::Stop(){
    for (auto& work : _works) {
        work.reset();
    }
    for (auto& t : _threads) {
        t.join();
    }
}
```


