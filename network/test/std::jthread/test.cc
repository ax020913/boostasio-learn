#include <iostream>
#include <thread>
#include <chrono>

using namespace std;

void func(int data)
{
    std::cout << data << std::endl;
}

int main(void)
{
    std::jthread thread(func, 100);

    std::cout << thread.get_id() << std::endl;

    unsigned int n = std::jthread::hardware_concurrency();
    std::cout << n << std::endl;

    std::thread t1;
    n = t1.hardware_concurrency();
    std::cout << n << std::endl;

    thread.join();

    return 0;
}
