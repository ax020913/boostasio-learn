#include <iostream>
#include <boost/asio.hpp>

#include <google/protobuf/message.h>
#include "msg.pb.h"

using namespace std;
using namespace boost::asio::ip;

const int MAX_LENGTH = 1024 * 2;
const int HEAD_LENGTH = 2;

int main(void)
{
    try
    {
        // 1. endpoint
        std::string server_ip = "127.0.0.1";
        const unsigned short server_port = 3333;
        boost::asio::io_service ios;
        boost::asio::ip::address server_address = boost::asio::ip::address::from_string(server_ip);
        boost::asio::ip::tcp::endpoint endpoint(server_address, server_port);

        // 2. sock
        boost::asio::ip::tcp::socket sock(ios);

        // 3. connect
        boost::system::error_code error_code = boost::asio::error::host_not_found; // 先给一个默认值
        sock.connect(endpoint, error_code);
        if (error_code)
        {
            std::cout
                << "connect failed, code is " << error_code.message()
                << " error msg is " << error_code.message()
                << std::endl;
            return error_code.value();
        }

        // 4. 业务
        for (;;)
        {
            ////////// 使用 protobuf 前后对比：
            // std::cout << "Enter message: ";
            // char request[MAX_LENGTH];
            // std::cin.getline(request, MAX_LENGTH);
            // size_t request_length = strlen(request);
            MsgData msgdata;
            msgdata.set_id(1001);
            msgdata.set_data("hello server");
            std::string request;
            msgdata.SerializeToString(&request);
            short request_length = request.length();

            if (request_length == 0) // 防止client输入回车之后就不能再对server发起请求的情况>了
                continue;
            char send_data[MAX_LENGTH] = {0};
            memcpy(send_data, &request_length, 2);
            ////////// 使用 protobuf 前后对比：
            // memcpy(send_data + 2, request, request_length);
            memcpy(send_data + 2, request.c_str(), request_length);
            boost::asio::write(sock, boost::asio::buffer(send_data, request_length + 2));

            // 先发送 _recv_head_node，再发送 _recv_msg_node
            char reply_head[HEAD_LENGTH];
            size_t reply_length = boost::asio::read(sock, boost::asio::buffer(reply_head, HEAD_LENGTH));
            short msglen = 0;
            memcpy(&msglen, reply_head, HEAD_LENGTH);
            // msglen = atoi(reply_head); // error
            msglen = boost::asio::detail::socket_ops::network_to_host_short(msglen); // 转为网络字节序(接收和send都要转化)
            std::cout << "Replay length = " << msglen << ' ' << std::endl;
            char msg[MAX_LENGTH] = {0};
            size_t msg_length = boost::asio::read(sock, boost::asio::buffer(msg, msglen));
            msg[msg_length] = '\0';
            ////////// 使用 protobuf 前后对比：
            MsgData recvdata;
            recvdata.ParseFromArray(msg, msglen);
            // std::string msg_string(msg, MAX_LENGTH);
            // recvdata.ParseFromString(msg_string.c_str());
            std::cout << " msg id is: " << recvdata.id()
                      << " msg data is: " << recvdata.data()
                      << std::endl;
        }
    }
    catch (std::exception &e)
    {
        std::cerr << "Exception: " << e.what() << endl;
    }

    return 0;
}
