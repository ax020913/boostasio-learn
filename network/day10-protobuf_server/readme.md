### 一. protobuf 在网络中的应用
先为服务器定义一个用来通信的proto

```protobuf
syntax = "proto3";
message MsgData
{
   int32  id = 1;
   string data = 2;
}
```

`id` 代表消息 `id`，`data` 代表消息内容
我们用 `protoc` 生成对应的 `pb.h` 和 `pb.cc` 文件
将 `proto, pb.cc, pb.h` 三个文件复制到我们之前的服务器项目里并且配置。

我们修改服务器接收数据和发送数据的逻辑
当服务器收到数据后，完成切包处理后，将信息反序列化为具体要使用的结构,打印相关的信息，然后再发送给客户端

```protobuf
	MsgData msgdata;
    std::string receive_data;
    msgdata.ParseFromString(std::string(_recv_msg_node->_data, _recv_msg_node->_total_len));
    std::cout << "recevie msg id  is " << msgdata.id() << " msg data is " << msgdata.data() << endl;
    std::string return_str = "server has received msg, msg data is " + msgdata.data();
    MsgData msgreturn;
    msgreturn.set_id(msgdata.id());
    msgreturn.set_data(return_str);
    msgreturn.SerializeToString(&return_str);
    Send(return_str);
```

同样的道理，客户端在发送的时候也利用 `protobuf` 进行消息的序列化，然后发给服务器

```protobuf
	MsgData msgdata;
    msgdata.set_id(1001);
    msgdata.set_data("hello world");
    std::string request;
    msgdata.SerializeToString(&request);
```

### 二. protobuf 环境配置的有一些问题，后面 json 用的比较多，所以 protobuf 的代码先放一放
也是配置了三种 protobuf 的版本还是不行，所以 protobuf 就先放一放。
![Alt text](image.png)
