#include "Session.hpp"

int main(int argc, const char *argv[])
{
    try
    {
        boost::asio::io_service io_service;
        Server server(io_service, 3333);
        io_service.run();
    }
    catch (std::exception &error_code)
    {
        std::cerr << "Exception: " << error_code.what() << std::endl;
    }

    return 0;
}
