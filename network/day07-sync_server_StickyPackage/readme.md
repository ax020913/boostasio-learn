### 粘包处理 + 网络字节序

### `async_read_some` 之前......

async_read_some 之前一般得清空接收空间，不然打印接收到的数据的时候，会打印出乱码的多余的数据，因为得打印到 '\0' 为止。
所以我们先清空是得都是 '\0'，再接收的时候，打印出来的就是我们想要的数据。

```
memset(msgnode->_data, 0, msgnode->_total_len);
_sock.async_read_some(
    boost::asio::buffer(msgnode->_data, msgnode->_total_len),
    std::bind(&Session::handle_write, this, std::placeholders::_1, _self_shared)
);

memset(_data, 0, max_length);
_sock.async_read_some(
boost::asio::buffer(_data, max_length),
    std::bind(&Session::handle_read,
                this, std::placeholders::_1,
                std::placeholders::_2,
                shared_from_this())
);

memset(_data, 0, max_length);
_sock.async_read_some(
    boost::asio::buffer(_data, max_length),
    std::bind(&Session::handle_read, this, std::placeholders::_1, std::placeholders::_2, _self_shared)
);
```

### 关于异步写函数`async_write`

async_write 函数一调(数据就发送了)，至于回调函数是用来看还有没有数据待发送。

```
void Session::Send(char *msg, int max_length)
{
    bool pending = false;
    std::lock_guard<std::mutex> lock(_send_mutex);
    if (_send_queue.size() > 0)
        pending = true;
    _send_queue.push(make_shared<MsgNode>(msg, max_length));
    if (pending == true)
        return;

    // 下面的async_write函数一调用，msg的消息就发送出去了。至于再调用回调函数是看还有没有数据待发送(异步)。
    boost::asio::async_write(_sock, boost::asio::buffer(msg, max_length),
                             std::bind(&Session::handle_write, this, std::placeholders::_1, shared_from_this()));
}

void Session::handle_write(const boost::system::error_code &error_code,
                           shared_ptr<Session> _self_shared)
{
    if (!error_code)
    {
        std::lock_guard<std::mutex> lock(_send_mutex);
        // 从最开始的情况来看也是，上面已经发送了数据，就要pop掉对头已经发送的数据。
        // 所以下面一行一上来就pop
        _send_queue.pop();
        if (!_send_queue.empty())
        {
            auto &msgnode = _send_queue.front();
            memset(msgnode->_data, 0, msgnode->_total_len);
            boost::asio::async_write(
                boost::asio::buffer(msgnode->_data, msgnode->_total_len),
                std::bind(&Session::handle_write, this, std::placeholders::_1, _self_shared));
        }
    }
    else
    {
        std::cout << "handle read failed, error is " << error_code.what() << std::endl;
        Close();
        _server->ClearSession(_self_shared->GetUuid());
    }
}
```

### 我們發送的數據是分爲兩部分的：_recv_head_node(存儲後面消息長度) 和 _recv_msg_node(真正的消息)
我們并沒有看到 Send(_recv_head_node->_data, _recv_head_node->_total_len)，而是只有 Send(_recv_msg_node->_data, _recv_msg_node->_total_len)，因爲在構造真正的 _recv_msg_node 把消息長度 memcpy(_data, &max_len_host, HEAD_LENGTH) 進去了。........
當然也是可以先發送 _recv_head_node，再發送 _recv_msg_node 的哦。
```
在調用 Send 函數的時候：
void Session::Send(const char *msg, int max_length)
{
    bool pending = false;
    std::lock_guard<std::mutex> lock(_send_mutex);
    // 规定发送队列的长度不超过 SEND_MAX_LENGTH，用来防止发送过快的问题（MsgNode小而且快，MsgNode大都是不允许的）
    if (_send_queue.size() > SEND_MAX_LENGTH)
    {
        std::cout << "Session: " << _uuid << " _send_queue.size() > SEND_MAX_LENGTH" << std::endl;
        return;
    }

    if (_send_queue.size() > 0)
        pending = true;
    _send_queue.push(make_shared<MsgNode>(msg, max_length));
    if (pending == true)
        return;

    auto &msgnode = _send_queue.front();
    boost::asio::async_write(_sock, boost::asio::buffer(msgnode->_data, msgnode->_total_len),
                             std::bind(&Session::handle_write, this, std::placeholders::_1, shared_from_this()));
}


_send_queue.push(make_shared<MsgNode>(msg, max_length));


MsgNode(const char *msg, int max_len)
    : _cur_len(0), _total_len(max_len + HEAD_LENGTH)
{
    _data = new char[_total_len + 1]();                                                 // '\0' 结尾
    int max_len_host = boost::asio::detail::socket_ops::host_to_network_short(max_len); // 转为网络字节序(接收和send都要转化)
    memcpy(_data, &max_len_host, HEAD_LENGTH);                                          // _recv_msg_node->_total
    memcpy(_data + HEAD_LENGTH, msg, max_len);                                          // _recv_msg_node->_data
    _data[_total_len] = '\0';
}
```
