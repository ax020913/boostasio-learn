#include "Session.hpp"

int main(int argc, const char *argv[])
{
    try
    {
        // sync_client(0)  ClientTest_StickyPackage(1) 默認設置為 _acceptor 主綫程處理業務
        int client_ops = 0;
        if (argc == 2) // 设置了就取一下值，不然就是默认的 0
            client_ops = atoi(argv[1]);
        boost::asio::io_service io_service;
        Server server(io_service, 3333, client_ops);
        io_service.run();
    }
    catch (std::exception &error_code)
    {
        std::cerr << "Exception: " << error_code.what() << std::endl;
    }

    return 0;
}
