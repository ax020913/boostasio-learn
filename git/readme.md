# git 分布式版本控制系统


## # 撤销修改



### 1. 撤销修改情况一的演示

### 2. 撤销修改情况二的演示

### 3. 撤销修改情况三的演示

> 注意情况三可以撤回成功的前提条件：本地的这里仓库没有使用 `git push` 推送到远程仓库

![image-20231105123109409](./image/image-20231105123109409.png)



## # 删除文件/目录

### 1. 第一种方式

使用 `linux` 下的 `rm` 操作，三步完成。

![image-20231105124519848](./image/image-20231105124519848.png)

### 2. 第二种方式

使用 `git` 下的 `rm` 操作，两步完成。

![image-20231105125344687](./image/image-20231105125344687.png)



## # 分支管理

### 1. 基础

![image-20231105135045533](./image/image-20231105135045533.png)



**`master`**：`cat .git/refs/heads/master` 可以看到  `master` 里面存储的是什么，`master` 存储的是最新的一次 `commit` 提交版本。

![image-20231105140440227](./image/image-20231105140440227.png)

**`HEAD`指针**：1. 指向 `master` 变量的指针，也可以指向其它的分支。2. `HEAD` 指向哪个分支，这个分支就是当前工作的分支。

![image-20231105141259912](./image/image-20231105141259912.png)

### 2. 查看/创建/切换/合并分支

`git branch` 可以用来查看仓库的分支情况。

`git branch dev` 可以用来创建新的 `dev` 分支。

```git
Administrator@PC-20220109SQGI MINGW64 /d/gittest (master)
$ git branch
* master

Administrator@PC-20220109SQGI MINGW64 /d/gittest (master)
$ git branch dev

Administrator@PC-20220109SQGI MINGW64 /d/gittest (master)
$ git branch
  dev
* master
```

现在我们的仓库状态是下面的状态：

1. 下面的一条指令可以看到我们的仓库的分支的状态。只有一个主分支 `master` 分支，并且 `*` 表示的是当前的工作分支。

   ```git
   Administrator@PC-20220109SQGI MINGW64 /d/gittest (master)
   $ git branch
   * master
   ```

2. 我们创建新的分支是用的是 **`git branch dev `**来创建新的 `dev` 分支。并且是在 `master` 工作分支上，所以我们的 `dev` 存储的也是和 `master` 存储的一样的版本，如下图：

   ![image-20231105143356639](./image/image-20231105143356639.png)

3. 我们需要切换工作分支，使得 `dev` 分支成为新的工作分支，才能在 `dev` 分支上操作。

   下面的 **`git checkout` 分支名**，就可以切换到对应的分支上了。可以看到 `*` 是在 `dev` 分支前面的，所以工作分支已经切换到了新创建的 `dev` 分支了。

   （注意在切换分支之前是不能有修改文件的操作的，不然切换不成功）

   ```git
   Administrator@PC-20220109SQGI MINGW64 /d/gittest (master)
   $ git checkout dev
   Switched to branch 'dev'
   
   Administrator@PC-20220109SQGI MINGW64 /d/gittest (dev)
   $ git branch
   * dev
     master
   ```

   ![image-20231105144540326](./image/image-20231105144540326.png)

4. 在 `dev` 工作分支上 `commit` 一次(注意，我们还没有在 `dev` 分支 `touch` 等操作创建新的文件哦)：

   ![image-20231105151125967](./image/image-20231105151125967.png)

   就变成了下面的情况：

   ![image-20231105145540694](./image/image-20231105145540694.png)

   可以看到我们在 `dev` 分支下修改的文件的内容，在 `master` 分支是看不到的：
   
   ![image-20231105152041722](./image/image-20231105152041722.png)

   所以我们可以先切换到主分支，在主分支上 `git merge dev` 把 `dev` 分支合并起来，就可以看到了（可以看到 `master` 存储的值也是变化了，其实就是上面 `dev` 分支 `commit` 提交的那个 `commit_id` ）：
   
   ![image-20231105152431059](./image/image-20231105152431059.png)

![image-20231105152630807](./image/image-20231105152630807.png)

### 3. 删除分支

从上面的 `master` 主分支合并新的分支来看，我们的 `dev` 分支的使命就结束了，就可以 `git branch -d dev` 删除了。(注意不能在 `dev` 分支上删除 `dev` 分支，得在其它的分支上删除)

![image-20231105153236318](./image/image-20231105153236318.png)

实操：

![image-20231105153439299](./image/image-20231105153439299.png)

### 4. `merge` 合并冲突(手动解决)

**`merge`无合并冲突**：注意我们上面的过程中，我们在 `dev` 分支中修改了 `file1` 文件的内容，而且 `commit` 提交，并没有在 `master` 分支修改 `file1` 文件的内容 `commit`。而且也没有在 `dev` 分支上创建新的文件或者文件夹。最后我们的 `merge` 是成功的。

**`merge` 合并冲突**：在其它分支上修改文件的内容并且 `commit`，在 `master` 分支上也修改文件的内容并且 `commit`。为下图的情况：

![image-20231105160640223](./image/image-20231105160640223.png)


```cpp
Administrator@PC-20220109SQGI MINGW64 /d/gittest (master|MERGING)
$ cat file1
<<<<<<< HEAD
master
master
=======
dev
>>>>>>> dev
```

看一下 `merge` 合并冲突后，`file1` 文件的内容，可以得知 `master master` 就是 `master` 分支下 `file1` 文件的内容，`dev` 就是 `dev` 分支下 `file1` 文件的内容。

尖括号中的内容就是冲突的内容，`git` 没有帮我们选择，需要我们手动选择，再重新 `commit`。

`git log --graph --abbrev-commit`:

![image-20231105162051148](./image/image-20231105162051148.png)

### 5. 强制删除分支

![image-20231107093434003](./image/image-20231107093434003.png)

上面的 `commit` 改成 `merge.............................`



## # 远程操作

### 1. `git clone`



### 2. `git push`

本地仓库领先于远程仓库。

本地分支推送到远程分支。

![image-20231107164137105](./image/image-20231107164137105.png)

![image-20231107183723425](./image/image-20231107183723425.png)

### 3. `git pull` (拉取+合并)

远程仓库领先于本地仓库 `A`。

![image-20231107184718636](./image/image-20231107184718636.png)

`A` 主机可以 `git pull` 拉取远程分支到本地。

![image-20231107185116777](./image/image-20231107185116777.png)

### 4. 忽略特殊文件

`.gitignore` 文件的使用，根目录下。

![image-20231107192310881](./image/image-20231107192310881.png)

### 5. 配置命令别名

`git config --global alias.st status` 给`status`起了一个别名为`st`，所以 `git status` 等价于`git st`。

可以简化命令，初学的时候不建议。

![image-20231107192754955](./image/image-20231107192754955.png)



## # 标签管理

### 1. 本地标签 `tag`

对`commit`提交的一次代码起一个版本。

`git tag` ：查看历史标签。

`git tag 标签版本 commit_id`：给`commit_id`提交版本打一个标签。

`git tag -d 标签版本`：删除一个标签。

### 2. 远程标签 `tag`

`git push origin 标签版本`：推送本地标签到远程。

`git push origin :标签版本`：删除远程标签。



## # 多人协作



## # 企业级开发模式





## ##### `gitee` 开 `boostasio_learn` 仓库实战

### 1. `vsCode` 第二次提交

1. 下面的`git push`是会要求输入用户名和密码的（用户名：`ax020913`, 密码：`ax020913`）。

2. 注意得在`boostasio_learn`整个仓库的目录下的哦。
3. 后期推荐先`git branch newbranch`创建一个分支，在分支上写，再合并到`master`分支上去。

![image-20231116150556711](./image/image-20231116150556711.png)

### 2.  `Centos` 配置 `boost` 环境

```bash
Centos配置boost环境时：
No package python-dev available.
No package autotools-dev available.
No package libicu-dev available.
No package build-essential available.
No package libbz2-dev available.
No package libboost-all-dev available.

// 1. 百度一些好的 centos 系统安装 boost 库的文章

// 2. 上面的路径安装不成功的话，可以再试试下面的路径：
// yum search boost 看能不能搜索到boost库的一些信息，如果有的话可以直接用下面的操作：
// yum install boost-devel 就可以使用boost库的一些文件了。
```

### 3. 演示创建新的分支开发代码

![image-20231117124319465](./image/image-20231117124319465.png)



### 4. `boost::asio` 源码解读

https://segmentfault.com/a/1190000021230924

https://blog.csdn.net/Poisx/article/details/86588495

https://blog.csdn.net/weijianjain/category_12256551.html



### 5. 命名空间 `"boost::asio"` 没有成员 `"io_context"`

![image-20231117224145333](./image/image-20231117224145333.png)

```bash
// 奇怪，昨天晚上还报错：命名空间 "boost::asio" 没有成员 "io_context"，刚刚去执行了下面的命令就可以了(相当于是重新搭建了一下centos的boost使用环境)：
// yum install boost
// yum install boost-devel
// cd boost_1_82_0/  // 之前有下载boost源码包，并解压了 tar -xzf boost_1_82_0.tar.gz
// ./bootstrap.sh --with-libraries=all --with-toolset=gcc   // --with-liraries：需要编译的库 --with-toolset：编译时使用的编译器
// ./b2 install --prefix=/usr  // 默认头文件在/usr/local/include，库文件在/usr/local/lib
```

直接把 `centos` 系统的 `boost` 环境配置文章找过来了： [boost库介绍以及使用](https://blog.csdn.net/huangwenhuan/article/details/129923917)

![image-20231118131037713](./image/image-20231118131037713.png)

### 6. 下面的一个 `int server_end_point()` 函数，没有用到 `pthread`，怎么还需要 `-lpthread` 呢？

```c++
int server_end_point(){
    // 是任意地址，任意的client地址都是可以访问我们的server的
    boost::asio::ip::address ip_address = boost::asio::ip::address_v6::any();
    unsigned short port_num = 3333;

    boost::asio::ip::tcp::endpoint endpoint(ip_address, port_num);

    return 0;
}
```

![image-20231118135208130](./image/image-20231118135208130.png)



### 7. 注意：`git` 仓库不用 `rm` 删除文件(添加也得使用 `git add filename` )

#### 7.1 问题：

> 想删除远程仓库的 `endpoint.h` 和 `endpoint.cpp` 文件。
>
> 本地：在对应的路径下 `rm` 对应的文件。再 `git add，git commit`，但是 `commit` 一直失败。

![image-20231118144645820](./image/image-20231118144645820.png)

#### 7.2 解决：

> 1. 切回 `master` 主分支。
>
> 2. 删除没有用的 `make` 分支。
>
> 3. 用 `git rm filename` 删除文件，注意不要用 `rm` 删除文件。
> 4. 再 `git add，git commit，git push` 才修改了远程仓库。

![image-20231118144052565](./image/image-20231118144052565.png)



### 8. `STL` 的 `queue` 的 `emplace` 是线程安全的吗？



### 9. 大并发的情况下应采取异步网络处理

```tex
同步模型的局限性在于读写逻辑会影响逻辑的阻塞，如果将读写逻辑独立出单独的线程，就要分配线程资源。
一个连接分配一个线程，势必会造成线程资源吃紧，默认情况下一个进程开辟的线程为 1024 个，所以连接数受限。
可以提升一个进程分配的线程资源数，但是过多的线程切换造成时间片的开销也会影响我们程序的效率。
所以对于大并发的情况下应采取异步网络处理的方式，下一节介绍。
但对于连接数少并且逻辑实时性要求不高的应答型场景比如上下位机通信的情况，采取同步通信是一种高效简单的方式。
```

### 10. 缓冲区问题

[缓冲区问题再学习](http://wed.xjx100.cn/news/50147.html?action=onClick) http://wed.xjx100.cn/news/50147.html?action=onClick

![image-20231119174942163](./image/image-20231119174942163.png)

### 11. 重新安装了一下 `vs code`，`git push` 就有问题了

> 1、我是在 [vscode终端](https://so.csdn.net/so/search?q=vscode终端&spm=1001.2101.3001.7020) 里面上传的，`push` 失败，解决方法：设置 `(ctrl+,)` 。
>
> 2、再直接搜索 “[git](https://so.csdn.net/so/search?q=git&spm=1001.2101.3001.7020).terminalAuthentication”，并将该复选框前的对钩取消掉。
>
> 3、`(Ctrl+Shift+P)` 快捷键打开命令面板，输入 `”Reload Window“`，点击该命令重新加载窗口。
>
> 4、最后在 `git push origin main` 终于解决问题了。

![image-20231122121210655](./image/image-20231122121210655.png)

### 12. 粘包处理接收到的 `_recv_head_node->_data` 的数据有问题？

![image-20231123201957099](./image/image-20231123201957099.png)

![image-20231123201932024](./image/image-20231123201932024.png)

**造成的错误地方：接收的时候处理了，发送的时候也得处理的哦**

![image-20231124005343425](./image/image-20231124005343425.png)



### 13. 看着怎么没有 `send` 头部消息长度的数据呢？

![image-20231124005438443](./image/image-20231124005438443.png)



**而是直接对于 `send` 回来的 `_recv_msg_node` 做两次接收，好像 `_recv_msg_node` 包含了消息长度 `(2byte)` 又包含了消息。**

![image-20231124005514330](./image/image-20231124005514330.png)



**因为 `Send` 的时候，`_send_queue.push(make_shared<MsgNode>(msg, max_length));`  `MsgNode` 构造的时候拷贝进去了。**

![image-20231124011414878](./image/image-20231124011414878.png)

### 14. 闭环流程

![image-20231124010620951](./image/image-20231124010620951.png)



### 15. `centos` 系统配置 `protobuf` 序列化环境

1. 更新系统软件包：

   ```c++
   sudo yum update
   ```

2. 安装依赖项：

   ```c++
   sudo yum install -y epel-release
   sudo yum install -y autoconf automake libtool make gcc-c++
   # sudo yum install autoconf automake libtool curl make gcc-c++ unzip
   ```

3. 下载最新的 `Protocol Buffers` 源码包：

   ```c
   wget https://github.com/protocolbuffers/protobuf/releases/download/v3.18.0/protobuf-all-3.18.0.tar.gz
   ```

4. 解压源码包：

   ```c++
   tar -zxvf protobuf-all-3.18.0.tar.gz
   cd protobuf-3.18.0
   ```

5. 编译和安装 `protobuf`：

   ```bash
   # 第⼀步执⾏autogen.sh，但如果下载的是具体的某⼀⻔语⾔，不需要执⾏这⼀步。
   ./autogen.sh
   # 第⼆步执⾏configure，有两种执⾏⽅式，任选其⼀即可，如下：
   # 1、protobuf默认安装在 /usr/local ⽬录，lib、bin都是分散的
   ./configure 
   # 2、修改安装⽬录，统⼀安装在/usr/local/protobuf下
   ./configure --prefix=/usr/local/protobuf
       
   make
   make check
   sudo make install
   ```

6. 配置动态链接库路径：

   打开 `/etc/ld.so.conf` 文件，添加一行 `/usr/local/lib` 并保存。 运行以下命令更新动态链接库缓存：

   ```c++
   sudo ldconfig
   ```

7. 如果上面用的是 `./configure --prefix=/usr/local/protobuf` ，那么需要下面的步骤：

   ```bash
   sudo vim /etc/profile
   
   # 添加内容如下：
   
   #(动态库搜索路径) 程序加载运⾏期间查找动态链接库时指定除了系统默认路径之外的其他路径
   export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/protobuf/lib/
   #(静态库搜索路径) 程序编译期间查找动态链接库时指定查找共享库的路径
   export LIBRARY_PATH=$LIBRARY_PATH:/usr/local/protobuf/lib/
   #执⾏程序搜索路径
   export PATH=$PATH:/usr/local/protobuf/bin/
   #c程序头⽂件搜索路径
   export C_INCLUDE_PATH=$C_INCLUDE_PATH:/usr/local/protobuf/include/
   #c++程序头⽂件搜索路径
   export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/usr/local/protobuf/include/
   #pkg-config 路径
   export PKG_CONFIG_PATH=/usr/local/protobuf/lib/pkgconfig/
   ```

8. 最后执行：

   ```bash
   source /etc/profile
   ```

9. 验证安装是否成功：

   运行以下命令查看 `Protobuf` 版本信息：

   ```
   protoc --version
   ```

   如果显示 `Protobuf` 的版本号，则说明安装成功。现在你就可以在 `CentOS` 系统中使用 `Protocol Buffers` 了。



### 16. Linux环境protobuf的简单使用案例

1. 写一个 `.proto` 文件 `msg.proto` 用来对它进行序列化与反序列化。

   ```protobuf
   syntax = "proto3";
   
   message Book{
       string name = 1;
       int32 pages = 2;
       float price = 3;
   }
   ```

   编译：`protoc msg.proto --cpp_out=./`
   生成：`msg.pb.h` 和 `msg.pb.cc` （在当前目录下生成）

2. 就可以直接使用 `msg.pb.h` 文件了

   ```c++
   #include <iostream>
   
   #include "msg.pb.h"
   
   int main(void)
   {
       Book book;
       book.set_name("CPP programing");
       book.set_pages(100);
       book.set_price(200);
       std::string bookstr;
       book.SerializeToString(&bookstr);
       std::cout << "serialize str is " << bookstr << std::endl;
   
       Book book2;
       // book2.ParseFromString(&bookstr);
       book2.ParseFromString(bookstr.c_str());
   
       std::cout << "book2 name is " << book2.name()
                 << " price is " << book2.price()
                 << " pages is " << book2.pages()
                 << std::endl;
   
       getchar();
   
       return 0;
   }
   ```

   编译： `g++ -o ma protobuf_demo.cpp msg.pb.cc -std=c++11 -lprotobuf`

   生成：`ma` 可执行文件

### 17. protobuf 在网络中的应用

先为服务器定义一个用来通信的 `proto`

```protobuf
syntax = "proto3";
message MsgData
{
   int32  id = 1;
   string data = 2;
}
```

`id` 代表消息 `id`，`data` 代表消息内容
我们用 `protoc` 生成对应的 `pb.h` 和 `pb.cc` 文件
将 `proto, pb.cc, pb.h` 三个文件复制到我们之前的服务器项目里并且配置。

我们修改服务器接收数据和发送数据的逻辑
当服务器收到数据后，完成切包处理后，将信息反序列化为具体要使用的结构,打印相关的信息，然后再发送给客户端

```protobuf
	MsgData msgdata;
    std::string receive_data;
    msgdata.ParseFromString(std::string(_recv_msg_node->_data, _recv_msg_node->_total_len));
    std::cout << "recevie msg id  is " << msgdata.id() << " msg data is " << msgdata.data() << endl;
    std::string return_str = "server has received msg, msg data is " + msgdata.data();
    MsgData msgreturn;
    msgreturn.set_id(msgdata.id());
    msgreturn.set_data(return_str);
    msgreturn.SerializeToString(&return_str);
    Send(return_str);
```

同样的道理，客户端在发送的时候也利用 `protobuf` 进行消息的序列化，然后发给服务器

```protobuf
	MsgData msgdata;
    msgdata.set_id(1001);
    msgdata.set_data("hello world");
    std::string request;
    msgdata.SerializeToString(&request);
```

### 18. `protobuf` 使用时，我其他的目录下可以使用，但是这个目录下不可以使用，析构函数未定义？

![image-20231125112737478](./image/image-20231125112737478.png)

#### 18.1 报错： `undefined reference to 'MsgData::MsgData(google::protobuf::Arena*, bool)'`



#### 18.2 减级一下 `protobuf` :

```
wget https://github.com/protocolbuffers/protobuf/releases/download/v21.11/protobuf-cpp-3.21.11.zip
unzip protobuf-cpp-3.21.11.zip
1.第一步执行autogen.sh，但如果下载的是具体的某一门语言，不需要这一步。
./autogen.sh
2.第二步执行configure，有两种方式，任选其一即可：
(1)将protobuf默认安装在 /usr/local 目录下，但是lib和bin是分散的
./configure
(2)修改安装目录，统一安装在 /usr/local/protobuf下
./configure --prefix=/usr/local/protobuf

```



重新安装 `protobuf` 升级一下 ，之前是：

```bash
[ax@localhost client]$ protoc --version
libprotoc 3.18.0
```

减级，但是还是没有用：

![image-20231125230208317](./image/image-20231125230208317.png)

![image-20231125231128641](./image/image-20231125231128641.png)

高版本：https://github.com/protocolbuffers/protobuf/releases/tag/v3.6.1

低版本：https://github.com/protocolbuffers/protobuf/releases/tag/v2.6.1



### 19. 四种`IO`模型的说明：同步阻塞 **`IO`** ，同步非阻塞 `IO` ，异步阻塞 `IO` (`IO`多路复用)，异步非阻塞 `IO` (真正的异步 `IO`)

#### 19.1 同步阻塞 **`BIO`**

我们需要知道，内核在处理数据的时候其实是分成了两个阶段：

- 数据准备(**等**)
- 数据复制(**拷贝**)

在网络 `IO` 中，**数据准备**可能是客户端还有部分数据还没有发送、或者正在发送的途中，当前内核 `Buffer` 中的数据并不完整；而**数据复制**则是将内核态 `Buffer` 中的数据复制到用户态的 `Buffer` 中去。

当**调用线程**发起 `read` 系统调用时，如果此时内核数据还没有 `Ready`，调用线程会**阻塞**住，等待内核 `Buffer` 的数据。内核数据准备就绪之后，会将内核态 `Buffer`的数据复制到用户态 `Buffer` 中，这个过程中调用线程仍然是**阻塞**的，直到数据复制完成，整个流程用图来表示就张这样：

![image-20231128131315805](./image/image-20231128131315805.png)



#### 19.2 同步非阻塞 `NIO`

![image-20231128131522835](./image/image-20231128131522835.png)

还是分为两个阶段来讨论。

**数据准备阶段**。此时用户线程发起 `read` 系统调用，此时内核会立即返回一个错误，告诉用户态数据还没有 `Read`，然后用户线程不停地发起请求，询问内核当前数据的状态。

**数据复制阶段**。此时用户线程还在不断的发起请求，但是当数据 `Ready` 之后，**用户线程就会陷入阻塞**，**直到数据从内核态复制到用户态**。

稍微总结一下，如果内核态的数据没有 `Ready`，用户线程不会阻塞；但是如果内核态数据 `Ready` 了，即使当前的 `IO` 模型是同步非阻塞，**用户线程仍然会进入阻塞状态**，直到数据复制完成，并不是绝对的非阻塞。

那 `NIO` 的好处是啥呢？显而易见，实时性好，内核态数据没有 `Ready` 会立即返回。但是事情的两面性就来了，频繁的轮询内核，会**占用大量的 `CPU` 资源，降低效率**。

#### 19.3 异步阻塞 `IO`

`IO` 多路复用实际上就解决了 `NIO` 中的频繁轮询 `CPU` 的问题。在之前的 `BIO` 和 `NIO` 中只涉及到一种系统调用——`read`，在 `IO` 多路复用中要引入新的系统调用——`select`。

`read` 用于读取内核态 `Buffer` 中的数据，而 `select` 你可以理解成 `MySQL` 中的同名关键字，用于查询 `IO` 的就绪状态。

在 `NIO` 中，内核态数据没有 `Ready` 会导致用户线程不停的轮询，从而拉满 `CPU`。而在 `IO` 多路复用中调用了 `select` 之后，只要数据没有准备好，用户线程就会阻塞住(`select`函数是阻塞的)，**避免了频繁的轮询当前的 `IO` 状态**，用图来表示的话是这样：

![image-20231128131752582](./image/image-20231128131752582.png)

#### 19.4 异步非阻塞 `AIO`

该模型的实现就如其名，是异步的。用户线程发起 `read` 系统调用之后，无论内核 `Buffer` 数据是否 `Ready`，都不会阻塞，而是立即返回。

内核在收到请求之后，会开始准备数据，准备好了 & 复制完成之后会由内核发送一个 `Signal` 给用户线程，或者回调用户线程注册的接口进行通知。用户线程收到通知之后就可以去读取用户态 `Buffer` 的数据了。

![image-20231128131838062](./image/image-20231128131838062.png)

由于这种实现方式，异步 `IO` 有时也被叫做**信号驱动 `IO`**。相信你也发现了，这种方式最重要的是需要 `OS` 的支持，如果 `OS` 不支持就直接完蛋。

`Linux` 系统在 2.6 版本的时候才引入了异步 `IO`，不过那个时候并不算真正的异步 `IO`，因为内核并不支持，底层其实是通过 `IO` 多路复用实现的。而到了 `Linux` 5.1 时，才通过 `io_uring` 实现了真 `AIO`。



### 20. `git push` 提交 `gitee`上的代码没绿点？(虚拟机系统时间不一致导致)

![image-20231128151019951](./image/image-20231128151019951.png)

因为用的是虚拟机 `Linux` 环境 `push` 的，虚拟机的时间不对。

![image-20231128151101687](./image/image-20231128151101687.png)

设置之后再 `push`就可以了。

注意这样修改时间重启之后时间约又会变的。

![image-20231128152533789](./image/image-20231128152533789.png)

但是这样好像可以把之前没有绿点的地方打上绿点。这是 `gitee` 的 `bug` 吗？哈哈哈

今天2023.11.28，设置时间为2023.11.21，`push` 成功，但是没有绿点，绿点打在了28日，看来不是`bug`，但是前一天应该还是可以的。



### 21. 提高 `boost` 库的版本，低版本没有协程

方法一：命令行
```bash
// 卸载
sudo apt-get remove libboost-all-dev

// 安装
sudo apt-get install libboost-all-dev
```


方法二：源码编译
```bash
// 卸载
sudo rm -f /usr/local/lib/libboost*
sudo rm -rf /usr/local/include/boost
sudo rm -rf /usr/local/lib/cmake/boost*
sudo rm -rf /usr/local/lib/cmake/Boost*

// 安装
官网 https://www.boost.org/users/download/
tar -zxvf boost_1_78_0.tar.gz
cd boost_1_78_0
./bootstrap.sh
./b2
sudo ./b2 install
sudo vim ~/.bashrc // 加入下面的两行
export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
export CPLUS_INCLUDE_PATH=/usr/local/include:$CPLUS_INCLUDE_PATH
source ~/.bashrc
cat /usr/local/include/boost/version.hpp | grep "BOOST_LIB_VERSION" // 看一下安装的版本对不对
```

![image-20231201163431327](./image/image-20231201163431327.png)



### 22. `git commit` 的时间才是哪一天提交的时间



![image-20231207084852707](./image/image-20231207084852707.png)



### 23. 本地 `git` 仓库的配置得对（同一个用户用在多个仓库的情况下）

```bash
git config --global user.name "ax020913"

git config --global user.email "m18175549596@163.com"

find . -type f -exec cat {} \; | wc -l // 统计当前目录下面所有文件的总行数
```

![image-20231210094542826](./image/image-20231210094542826.png)



### 24. centos 虚拟机分配的磁盘空间不足(cd b + table 无效、vscode ssh 连接不上)

![image-20231211095815457](./image/image-20231211095815457.png)

![image-20231211095941777](./image/image-20231211095941777.png)

删除一些文件就可以了。



### 25. 最近虚拟机总是因为内存不足报错

```bash
[ax@localhost day14-threadSafe_stack-queue]$ make
g++ -o ma main.cpp -std=c++11 -lpthread -g
Cannot create temporary file in /tmp/: No space left on device
make: *** [ma] Aborted (core dumped)
```



![image-20231213155356287](./image/image-20231213155356287.png)

![image-20231213155711507](./image/image-20231213155711507.png)

### 26. git bash 中 c 盘和 d 盘的跳转

```bash
Administrator@PC-20220109SQGI MINGW64 ~
$ pwd
/c/Users/Administrator

Administrator@PC-20220109SQGI MINGW64 ~
$ cd d:

Administrator@PC-20220109SQGI MINGW64 /d
$ pwd
/d

Administrator@PC-20220109SQGI MINGW64 /d
$ cd c:

Administrator@PC-20220109SQGI MINGW64 /c
$ pwd
/c
```

### 27. `sudo date` 查看虚拟机的时间

```
[x@localhost boostasio-learn]$ sudo date
[sudo] password for x: 
Mon Jan  1 01:24:25 PST 2024
```

可以看到是和当地时间晚了 `16` 个小时的。

### 28. 注意仓库提交者的问题

`git config --global user.email "your_email@example.com"` 是可以设置仓库的提交者的。也是有一些注意事项的：

你 github 或者 gitee 账号设置的邮箱就是这里设置的邮箱，如果你 github 和 gitee 设置的邮箱不同的话，你拉代码到本地，设置仓库的提交者使用 `git config --global user.email "your_email@example.com"` 那么你 github 和 gitee 本地设置的提交者就都是 你设置的这个邮箱了，所以谨慎使用 `--global` 参数。

