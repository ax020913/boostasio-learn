#include <iostream>

#include "utils.hpp"
#include "level.hpp"

using namespace std;
using namespace ax;

int main(void)
{
  cout << "hello world" << endl;

  Date().Now();

  File().createDirectory("./logs/log");

  cout << File().exists("./test.txt") << endl;

  cout << "LogLevel: " << LogLevel::toString(LogLevel::value::DEBUG) << endl;

  return 0;
}