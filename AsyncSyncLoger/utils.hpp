/*

使用工具类的实现：

    1. 获取系统时间
    2. 判断文件是否存在
    3. 获取文件所在路径
    4. 创建目录


*/

#ifndef __UTILS_HPP
#define __UTILS_HPP

#include <iostream>
#include <ctime>
#include <time.h>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

namespace ax
{
  class Date
  {
  public:
    // 获取系统的当前时间
    static size_t Now()
    {
      // #include <ctime>
      return (size_t)time(nullptr);

      // #include <time.h>
      // 下面的代码是 https://legacy.cplusplus.com/reference/ctime/localtime/ 官方的一段代码
      // time_t nowTime;
      // struct tm *timeinfo;

      // time(&nowTime);
      // timeinfo = localtime(&nowTime);
      // printf("Current local time and date: %s", asctime(timeinfo)); // 先输出到控制台上，后面再改
    }
  };

  class File
  {
  public:
    // 判断一个文件路径是否存在
    static bool exists(const std::string &fileName)
    {
      // return access(pathname.c_str(),F_OK)==0; // 无法跨平台

      // 使用 fopen / open 函数都是可以的
      struct stat fileInfo;
      return stat(fileName.c_str(), &fileInfo) < 0 ? false : true;
    }

    // 根据 pathName 提取父级目录(去掉文件名)
    static std::string path(const std::string &pathName)
    {
      // 注意：最后一个 '/'
      size_t pos = pathName.find_last_of("/\\"); // windows下是 \, 需要转义
      if (pos == std::string::npos)
        return "."; // 没有找到说明在当前目录下
      else
        return pathName.substr(0, pos + 1); // 第二个参数表示长度，需要带上pos，就得+1
      // 这里带上/的好处是，后面我们在创建父级目录的时候，需要不断的寻找/，因此要加上
      // 其实不加上也是可以的，因为我们可以直接通过npos位置的mkdir创建目录的。
    }

    // 创建文件夹
    static void createDirectory(const std::string &pathName) // pathName 表示的是：文件 + 路径名
    {
      if (pathName == ".")
        return;

      // ./log
      // ./logs/log
      size_t pos = 0, idx = 0, pathLength = pathName.size();
      while (idx < pathLength)
      {
        pos = pathName.find_first_of("/\\", idx); // 注意：从 idx 位置往后找

        // 1. 说明当前是最后一个文件夹了
        if (pos == std::string::npos)
        {
          mkdir(pathName.c_str(), 0777); // 有文件掩码，所以实际创建的文件的权限不一定是 -rwxrwxrwx
          return;
        }
        // 2. 需要创建文件夹 pathName.substr(0, pos + 1);  [ )
        else
        {
          // 一路建立父级目录
          std::string parent_dir = pathName.substr(0, pos + 1); // 注意细节
          if (exists(parent_dir) == true)
          {
            idx = pos + 1;
            continue;
          }
          else
          {
            mkdir(parent_dir.c_str(), 0777);
            idx = pos + 1;
          }
        }
      }
    }
  };
}

#endif __UTILS_HPP