/*



*/

#ifndef __FORMAT_HPP
#define __FORMAT_HPP

#include "message.hpp"
#include <ctime>
#include <string>
#include <vector>
#include <cassert>
#include <sstream>
#include <utility>

namespace ax
{
  // 子项基类
  class FormatItem
  {
  public:
    using ptr = std::shared_ptr<FormatItem>;
    virtual void format(std::ostream &out, LogMsg &msg);
  };

  // 派生格式化子项子类：消息、等级、时间、文件名、行号、线程ID、日志器名称、制表符、换行、其他。
  // 日志产生的时间
  class TimeFormatItem : public FormatItem
  {
  public:
    TimeFormatItem(const std::string &fmt = "%H%M%S") : _fmt_time(fmt) {}

    void format(std::ostream &out, LogMsg &msg) override
    {
      struct tm t;
      localtime_r(&msg._ctime, &t);
      char temp[32] = {0};
      strftime(temp, 31, _fmt_time.c_str(), &t);
      out << temp;
    }

  private:
    std::string _fmt_time;
  };
  // 日志等级
  class LevelFormatItem : public FormatItem
  {
  public:
    void format(std::ostream &out, LogMsg &msg) override
    {
      out << LogLevel::toString(msg._level);
    }
  };
  // 行号
  class LineFormatItem : public FormatItem
  {
  public:
    void format(std::ostream &out, LogMsg &msg)
    {
      out << msg._line;
    }
  };
  // 线程 id
  class ThreadFormatItem : public FormatItem
  {
  public:
    void format(std::ostream &out, LogMsg &msg)
    {
      out << msg._tid;
    }
  };
  // 文件名
  class FileFormatItem : public FormatItem
  {
  public:
    void format(std::ostream &out, LogMsg &msg)
    {
      out << msg._file;
    }
  };
  // 日志器名称
  class LoggerFormatItem : public FormatItem
  {
  public:
    void format(std::ostream &out, LogMsg &msg)
    {
      out << msg._logger;
    }
  };
  // 日志消息有效载荷
  class MsgFormatItem : public FormatItem
  {
  public:
    void format(std::ostream &out, LogMsg &msg) override
    {
      out << msg._playload;
    }
  };
  // \t
  class TabFormatItem : public FormatItem
  {
  public:
    void format(std::ostream &out, LogMsg &msg)
    {
      out << "\t";
    }
  };
  // \n
  class NLineFormatItem : public FormatItem
  {
  public:
    void format(std::ostream &out, LogMsg &msg)
    {
      out << "\n";
    }
  };
  // 其他字符串
  class OtherFormatItem : public FormatItem
  {
  public:
    OtherFormatItem(const std::string &str = "") : _str(str) {}
    void format(std::ostream &out, LogMsg &msg)
    {
      out << _str;
    }

  private:
    std::string _str;
  };

  /*

  %d 表示日期，包含子格式 {%H:%M:%S}
  %p 表示日志级别
  %f:%l 表示文件名称:行号
  %t 表示线程 id
  %c 表示日志器名称
  %m 表示消息主体
  %T 表示水平制表符号
  %n 表示换行

  */
  class Formatter
  {
  public:
    using ptr = std::shared_ptr<Formatter>;
    Formatter(const std::string &pattern = "[%d{%H:%M:%S}][%t][%c][%f:%l][%p]%T%m%n")
        : _pattern(pattern)
    {
      assert(parsePattern());
    }

    // 对格式化字符串解析
    bool parsePattern()
    {
      // 1. 解析 pattern
      std::vector<std::pair<std::string, std::string>> fmt_order;
      size_t pos = 0;
      std::string kay, value;
      while (pos < _pattern.size())
      {
      }

      // 2. 根据解析的数据初始化数组成员
      for (auto &it : fmt_order)
        _items.push_back(createItem(it.first, it.second));

      return true;
    }

  private:
    // 根据不同格式化字符串创建不同格式化子项对象
    FormatItem::ptr
    createItem(const std::string &key, const std::string &value)
    {
      if (key == "d")
        return std::make_shared<TimeFormatItem>(value);
      if (key == "t")
        return std::make_shared<ThreadFormatItem>(value);
      if (key == "c")
        return std::make_shared<LoggerFormatItem>(value);
      if (key == "f")
        return std::make_shared<FileFormatItem>(value);
      if (key == "p")
        return std::make_shared<LevelFormatItem>(value);
      if (key == "l")
        return std::make_shared<LineFormatItem>(value);
      if (key == "T")
        return std::make_shared<TabFormatItem>(value);
      if (key == "m")
        return std::make_shared<MsgFormatItem>(value);
      if (key == "n")
        return std::make_shared<NLineFormatItem>(value);
      if (key.empty() == true)
        return std::make_shared<OtherFormatItem>(value);

      // 例如 %g 这种没有定义过的就加不进去
      std::cout << "没有对应的格式化字符串%" << key << std::endl;
      return FormatItem::ptr();
    }

  private:
    std::string _pattern; // 格式化规则字符串
    std::vector<FormatItem::ptr> _items;
  };
}

#endif