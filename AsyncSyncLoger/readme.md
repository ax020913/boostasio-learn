# 同步异步日志系统

项目内容来自：

CSDN 博主<不熬夜不抽烟不喝酒>

博主源码地址：https://gitee.com/wenge-c-language/Logs

代码量相对少，可以锻炼锻炼一些代码熟练度和手感。对于新人还是很友好的。

在此非常感谢，哈哈哈


## 一. 项目整体认识

同步写日志: 指的是在输出日志的时候，必须等待日志输出语句执行完毕，才能执行后面的业务逻辑语句，**执行日志输出线程和业务逻辑处理线程是同一个**。

异步写日志: 指的是**执行日志输出线程与业务逻辑处理线程不是同一个**，而是有专门的日志线程负责日志的输出操作，业务线程只需要把日志放入一个缓冲区(日志的生产者)，不用等待即可执行后面的业务逻辑。而日志如何从缓冲区进行落地文件、控制台由单独的日志线程来完成(作为日志的消费者)。

![Alt text](./images/a7beac4e31359831e19f1ed2068e9c4.png)

实现工具：centos 7，vscode/vim，g++/gdb，makefile

实现技术：
- 类层次设计(继承，多态)
- C++11(智能指针，锁，右值引用，多线程)
- 双缓冲区
- 生产消费者模型(业务逻辑线程和日志线程的连接交互点)
- 多线程(线程池)
- 设计模式(单例模式、工厂模式、建造者模式、模板模式)

实现功能：
- 支持多级别的日志消息
- 支持同步异步兼容
- 支持可靠写入日志到控制台、指定文件、滚动文件中
- 支持多线程并发写日志
- 支持扩展不同日志落地到目标地



## 二. 相关的类

### 工具类 utils.hpp

```cpp
namespace ax{
  class Date{
  public:
    // 获取系统的当前时间
    static void Now()
  };

  class File{
  public:
    // 判断一个文件路径是否存在
    static bool exists(const std::string &fileName);

    // 根据 pathName 提取父级目录(去掉文件名)
    static std::string path(const std::string &pathName)
    
    // 创建文件夹
    static void createDirectory(const std::string &pathName)
  };
}
```

测试代码：

```cpp
#include <iostream>

#include "utils.hpp"

using namespace std;
using namespace ax;

int main(void){
  cout << "hello world" << endl;

  Date().Now();

  File().createDirectory("./logs/log");

  cout << File().exists("./test.txt") << endl;

  return 0;
}
```

### 日志等级类实现 level.hpp

```cpp
1. 枚举出日志等级：
enum ax::LogLevel::value

2. 把对应的日志等级转化为对应的字符串：
static const char* toString(LogLevel::value level);
```

### 日志消息类实现 message.hpp

```cpp
namespace ax
{
  class LogMsg
  {
  public:
    time_t _ctime;          // 日志产生的时间
    LogLevel::value _level; // 日志等级
    size_t _line;           // 行号
    std::thread::id _tid;   // 线程 id
    std::string _file;      // 文件名
    std::string _logger;    // 日志器名称
    std::string _playload;  // 日志消息有效载荷

    LogMsg(LogLevel::value level, size_t line, std::thread::id tid, std::string file, std::string logger, std::string playload)
        : _ctime(Date::Now()), _level(level), _line(line), _tid(tid), _file(file), _logger(logger), _playload(playload)
    {
    }
  };
}
```

### 日志输出格式化类实现 format.hpp

```cpp

```

### 日志落地类实现 sink.hpp

```cpp

```

### 日志器类实现 logger.hpp

```cpp

```

### 异步日志器双缓冲区类实现 loo