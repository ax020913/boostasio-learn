/*

LogMsg 日志消息类数据成员的定义和初始化

*/

#ifndef __MESSAGE_HPP
#define __MESSAGE_HPP

#include "level.hpp"
#include "utils.hpp"
#include <iostream>
#include <thread>
#include <string>

namespace ax
{
  class LogMsg
  {
  public:
    time_t _ctime;          // 日志产生的时间
    LogLevel::value _level; // 日志等级
    size_t _line;           // 行号
    std::thread::id _tid;   // 线程 id
    std::string _file;      // 文件名
    std::string _logger;    // 日志器名称
    std::string _playload;  // 日志消息有效载荷

    LogMsg(LogLevel::value level, size_t line, std::thread::id tid, std::string file, std::string logger, std::string playload)
        : _ctime(Date::Now()), _level(level), _line(line), _tid(tid), _file(file), _logger(logger), _playload(playload)
    {
    }
  };
}

#endif