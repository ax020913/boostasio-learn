/*

1. 枚举出日志等级：
enum ax::LogLevel::value

2. 把对应的日志等级转化为对应的字符串：
static const char* toString(LogLevel::value level);

*/

#ifndef __LEVEL_HPP
#define __LEVEL_HPP

namespace ax
{
  class LogLevel
  {
  public:
    enum class value
    {
      UNKNOWN = 0,
      DEBUG,
      INFO,
      WARN,
      ERROR,
      FATAL,
      OFF, // 最后一个元素后面的 , 是可加可不加的哦
    };

    // 不需要创建对象再来访问这个函数
    static const char *toString(LogLevel::value level)
    {
      switch (level)
      {
// 也是可以使用预处理的宏来完成这里的过程的
#define TOSTRING(name) #name // 表示输出 name 的字符串形式
      // 例子：case LogLevel::value::DEBUG: return TOSTRING(DEBUG);
      // 写完 case 后还需要输入 #undef TOSTRING
      case LogLevel::value::DEBUG:
        return "DEBUG";
      case LogLevel::value::ERROR:
        return "ERROR";
      case LogLevel::value::FATAL:
        return "FATAL";
      case LogLevel::value::INFO:
        return "INFO";
      case LogLevel::value::OFF:
        return "OFF";
      case LogLevel::value::WARN:
        return "WARN";
        // default:
        // return "toString 函数参数输入有问题"; // toString 函数参数输入有问题
      }
      return "UNKNOWN"; // 参数不存在
    }
  };
}

#endif