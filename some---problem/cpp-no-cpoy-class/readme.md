### `C++` 中常见不可拷贝的对象，比较晦涩的问题

在 `C++` 中，有一些对象是不可拷贝的，意味着它们的拷贝构造函数和拷贝赋值运算符被删除或者不可访问。以下是一些常见的不可拷贝的对象：


#### 1. `std::thread`

![Alt text](image-1.png)

对于 `std::thread`，`C++` 不允许其执行拷贝构造和拷贝赋值, 所以只能通过移动和局部变量返回的方式将线程变量管理的线程转移给其他变量管理。
`C++` 中类似的类型还有 `std::mutex`, `std::ifstream`, `std::unique_ptr`。

**`std::thread` 用 `std::vector` 存储的问题：当你想将 `std::thread `对象添加到 `std::vector` 中时，建议使用 `emplace_back` 而不是 `push_back`。**

原因如下：

1. **移动语义**： s`td::thread` 对象通常是不可拷贝的，因为拷贝 `std::thread` 可能导致不可预测的行为。使用 `emplace_back` 可以直接在 `vector` 内构造 `std::thread` 对象，避免了可能的拷贝操作。

2. **构造参数**： `emplace_back` 允许你将构造 `std::thread` 对象所需的参数直接传递给 `std::thread` 构造函数。这样可以在 `vector` 内直接构造 `std::thread` 对象，而不需要创建临时对象。

`../../concurrent/day01-thread_jthread_vector/main.cpp` 文件里面的 `thread` 的存储问题：
```
// vector 的 push_back(会调用插入对象的拷贝构造函数) 和 emplace_back 对比
void param_function(int value)
{
    while (true)
    {
        std::cout << "param is " << value << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}
void use_vector(int count)
{
    std::vector<std::thread> threads;
    for (unsigned i = 0; i < count; i++)
    {
        // threads.push_back(param_function, i); // error
        // threads.push_back(std::thread(param_function, i)); // 低效一些相对于 push_back
        threads.emplace_back(param_function, i);
    }
    for (auto &entry : threads)
        entry.join();
}
```
`emplace_back` 是 `C++11` 引入的一个成员函数，用于在容器的末尾直接构造元素，而不是先创建一个临时对象再将其拷贝或移动到容器。这可以在某些情况下提高性能，因为它避免了额外的拷贝或移动操作。

在这个例子中，`emplace_back` 直接在 `threads` 中构造了一个新的 `std::thread` 对象，调用了 `param_function` 函数并传递了参数 `i`。
```
threads.emplace_back(param_function, i);
```
`push_back` 是标准容器的成员函数，用于将一个已经构造好的对象添加到容器的末尾。

在这个例子中，先创建了一个临时的 `std::thread` 对象，通过 `param_function` 函数和参数 `i` 进行构造，然后将这个临时对象拷贝或移动到 `threads` 容器中。
```
threads.push_back(std::thread(param_function, i));  
```


#### 2. `std::unique_ptr`

**`std::unique_ptr` 被赋值只能接受右值。**

![Alt text](image-3.png)

`std::unique_ptr` 被设计为具有独占所有权的智能指针，因此它的拷贝构造函数和拷贝赋值运算符被删除，但支持移动构造函数和移动赋值运算符。这意味着你可以通过右值（临时对象或使用 `std::move`）来传递所有权。

以下是一个示例，演示了 `std::unique_ptr` 被赋值只能接受右值的情况：

```
#include <iostream>
#include <memory>

class MyClass {
public:
    MyClass() {
        std::cout << "Constructor" << std::endl;
    }

    ~MyClass() {
        std::cout << "Destructor" << std::endl;
    }
};

int main() {
    std::unique_ptr<MyClass> ptr1 = std::make_unique<MyClass>();  // 使用 make_unique 创建对象

    // std::unique_ptr 不允许直接拷贝
    // std::unique_ptr<MyClass> ptr2 = ptr1;  // 错误，拷贝构造函数被删除

    // 但可以使用移动构造函数或移动赋值运算符
    std::unique_ptr<MyClass> ptr3 = std::move(ptr1);  // 使用 std::move 转移所有权

    // ptr1 不再拥有资源
    if (!ptr1) {
        std::cout << "ptr1 is nullptr" << std::endl;
    }

    // ptr3 现在拥有资源
    if (ptr3) {
        std::cout << "ptr3 is not nullptr" << std::endl;
    }

    return 0;
}
```
在这个例子中，`ptr1` 是通过 `make_unique` 创建的 `std::unique_ptr`，然后通过移动构造函数，它的所有权被转移到了 `ptr3`。由于拷贝构造函数被删除，不能直接进行拷贝。移动语义确保了资源的所有权在转移时的高效传递。


#### 3. `std::unique_lock<std::mutex> lock(mutex)`

![Alt text](image-2.png)

**可以通过移动 `std::unique_lock` 对象来实现锁的所有权的转移。**

`std::unique_lock` 是一个管理 `std::mutex` 的 `RAII`（资源获取即初始化）封装。它允许你在构造函数和析构函数中获取和释放锁，从而使锁的管理更加安全。

`../../concurrent/day14-threadSafe_stack-queue/threadSafe_queue.h` 文件里面的 `class threadsafe_queue_ht` 中：
```
    std::unique_ptr<node> pop_head()
    {
        std::unique_ptr<node> old_head = std::move(head_);
        head_ = std::move(old_head->next);
        return old_head;
    }
    // wait_pop_head
    std::unique_lock<std::mutex> wait_for_data()
    {
        std::unique_lock<std::mutex> lock(head_mutex_);
        condition_variable_.wait(lock, [this]()
                                 {
            // head_.get() == get_tail() 时为空的 queue
            if(head_.get() == get_tail()) return false;
            return true; });

        // std::unique_lock<std::mutex> 是可以移动构造的（看源码）
        // 把 std::mutex head_mutex_; 互斥量的所有权给转移出去了，是 std::unique_lock<std::mutex> 的特性之一
        return std::move(lock);
    }
    std::unique_ptr<node> wait_pop_head()
    {
        // wait_for_data 函数的返回值是 std::unique_lock<std::mutex> 类型的，所以得用一个 std::unique_lock<std::mutex> 类型的变量来接收
        std::unique_lock<std::mutex> lock(wait_for_data());
        return pop_head();
    }
    std::unique_ptr<node> wait_pop_head(K &value)
    {
        std::unique_lock<std::mutex> lock(wait_for_data());
        value = std::move(*head_->data);
        return pop_head();
    }
```


#### 4. `std::future` 和 `std::promise`

![Alt text](image.png)

**`get_future` 函数返回的是一个右值引用 (`rvalue reference`)。这是为了防止在将 `std::future` 对象返回给调用者时触发不必要的拷贝操作。**

`std::packaged_task` 是 `C++11` 引入的一个类模板，用于包装可调用对象（函数、函数对象或者 `Lambda` 表达式）以便在异步操作中使用。`std::packaged_task` 的目的是将一个可调用对象与一个 `std::future` 关联起来，以便获取异步操作的结果。

`std::packaged_task` 类模板有一个成员函数叫做 `get_future()`，它返回一个与 `std::packaged_task` 关联的 `std::future` 对象。通过这个 `std::future` 对象，你可以异步地获取任务的结果。

以下是一个简单的示例，演示了如何使用 `std::packaged_task` 和 `get_future()`：
```
#include <iostream>
#include <future>
#include <functional>

int add(int a, int b) {
    return a + b;
}

int main() {
    // 创建一个 std::packaged_task，将 add 函数包装起来
    std::packaged_task<int(int, int)> task(add);

    // 获取与 packaged_task 关联的 future 对象
    std::future<int> result = task.get_future();

    // 在另一个线程中执行 packaged_task
    std::thread taskThread(std::move(task), 3, 4);
    taskThread.join();  // 等待线程执行完成

    // 获取异步操作的结果
    int sum = result.get();
    
    std::cout << "Result: " << sum << std::endl;

    return 0;
}
```
在这个例子中，`std::packaged_task<int(int, int)> task(add);` 创建了一个 `std::packaged_task` 对象，将 `add` 函数包装起来。然后，通过 `task.get_future()` 获取与 `packaged_task` 关联的 `std::future` 对象。在另一个线程中执行 `taskThread(std::move(task), 3, 4);`，并通过 `result.get()` 获取异步操作的结果。


`../../concurrent/day09-QASummary/main.cpp` 文件中：
```
// 纯异步
template <typename Func, typename... Args>
auto ParallenExe(Func &&func, Args &&...args) -> std::future<decltype(func(args...))>
{
    typedef decltype(func(args...)) RetType;
    std::function<RetType()> bind_func = std::bind(std::forward<Func>(func), std::forward<Args>(args)...);
    std::packaged_task<RetType()> task(bind_func);
    auto rt_future = task.get_future();
    std::thread t(std::move(task));
    t.detach();
    return rt_future;
}
```

