## `C++` 重写 `exception` 实现自定义异常类

### 关于 `C++` 中"重写函数的异常规范比基本版本更宽松"的奇怪错误

`qt` 中自定义样式异常类的时候遇到的问题：

```
class StyleException : public QException{
private:
    QByteArray error;

public:
    StyleException(QString errorMessage) : error(errorMessage.toUtf8()) {}

//    virtual const char *what() const override // error: Exception specification of overriding function is more lax than base version
//    virtual const char *what() const throw()
    virtual const char *what() const noexcept override // c++11 之后 throw() 被 noexcept 代替
    {
        return error.constData();
    }
};

```

所以，以后还是使用 `noexcept` 来代替 `throw()` 吧。

