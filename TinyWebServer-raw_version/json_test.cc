#include <iostream>
#include <memory>
#include <sstream>
#include <jsoncpp/json/json.h>

int main(int argc, char *argv[])
{
  const char *name = "小明";
  int age = 18;
  float score[] = {88.5, 98, 58};
  Json::Value val;
  val["姓名"] = name;
  val["年龄"] = age;
  val["成绩"].append(score[0]);
  val["成绩"].append(score[1]);
  val["成绩"].append(score[2]);
  
  Json::StreamWriterBuilder swb;
  Json::StreamWriter* sw = swb.newStreamWriter();
  std::ostringstream ostring;
  sw->write(val, &ostring);
  std::string str = ostring.str();
  std::cout << str <<std::endl;

  return 0;
}