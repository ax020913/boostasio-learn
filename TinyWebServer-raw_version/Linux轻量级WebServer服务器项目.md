# Linux轻量级WebServer服务器项目

[TOC]

## 1. 前期工作

### 1.1 下拉代码

https://github.com/qinguoyi/TinyWebServer



### 1.2 centos7下mysql的安装

安装mysql之前可以看看是否有mysql包什么的进行必要的卸载：

```mysql
[x@localhost r_jian]$ rpm -qa | grep mysql
mysql57-community-release-el7-11.noarch
mysql80-community-release-el7-7.noarch
[x@localhost r_jian]$ 
[x@localhost r_jian]$ 
[x@localhost r_jian]$ sudo yum remove mysql80-community-release-el7-7.noarch --Centos7一般使用的是57版的
```



1、进入Centos7 虚拟机，使用wget下载Mysql相应的rpm包

```mysql
wget http://repo.mysql.com/mysql57-community-release-el7-8.noarch.rpm
```

如果没有wget命令，可以使用yum安装，yum install wget

2、执行rpm命令，安装rpm

```mysql
sudo rpm -ivh mysql57-community-release-el7-8.noarch.rpm
```

3、安装Mysql社区版（忽略gpg）

```mysql
yum -y install mysql-community-server --nogpgcheck  -- 社区版
```

4、开启mysql服务

```mysql
systemctl start mysqld.service
```

5、查看mysql默认密码并登陆

```mysql
2024-04-06T10:04:13.780593Z 6 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: d2hqbcNQMG<u
```

6、查看Mysql进程

```mysql
ps -ef | grep mysqld  
```

7、使用临时密码d2hqbcNQMG<u登陆

```mysql
mysql -uroot -p
```

8、重新设置一个复杂的密码，在修改密码

```mysql
mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY 'ax020913AXax'; --不够复杂
ERROR 1819 (HY000): Your password does not satisfy the current policy requirements
mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY 'ax020913AX#@'; -- 够复杂，就成功了
Query OK, 0 rows affected (0.01 sec)

mysql> show variables like 'validate_password%'; -- 看看能够修改什么
+-------------------------------------------------+--------+
| Variable_name                                   | Value  |
+-------------------------------------------------+--------+
| validate_password.changed_characters_percentage | 0      |
| validate_password.check_user_name               | ON     |
| validate_password.dictionary_file               |        |
| validate_password.length                        | 8      |
| validate_password.mixed_case_count              | 1      |
| validate_password.number_count                  | 1      |
| validate_password.policy                        | MEDIUM |
| validate_password.special_char_count            | 1      |
+-------------------------------------------------+--------+
8 rows in set (0.01 sec)

mysql> set global validate_password.policy = LOW; -- 改成低强度
Query OK, 0 rows affected (0.00 sec)
mysql> set global validate_password.length = 6; -- 再改成6位数长度的密码
Query OK, 0 rows affected (0.00 sec)
mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY '020913'; -- 在设置就成功了
Query OK, 0 rows affected (0.01 sec)
```



### 1.3 安装 mysql 的开发包

编译器无法找到 mysql/mysql.h 文件，可能是因为缺少 MySQL 的开发包或者 MySQL 的头文件没有正确安装。

在 CentOS/RHEL 上，使用 yum 安装：
```mysql
sudo yum install -y mysql-community-devel
```

上面的时候容易出现下面的问题：`GPG Keys are configured as:  file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql`

```mysql
Downloading packages:
warning: /var/cache/yum/x86_64/7/mysql80-community/packages/mysql-community-devel-8.0.36-1.el7.x86_64.rpm: Header V4 RSA/SHA256 Signature, key ID a8d3785c: NOKEY
Retrieving key from file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql-2022
Retrieving key from file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql


The GPG keys listed for the "MySQL 8.0 Community Server" repository are already installed but they are not correct for this package.
Check that the correct key URLs are configured for this repository.


 Failing package is: mysql-community-devel-8.0.36-1.el7.x86_64
 GPG Keys are configured as: file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql-2022, file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql
```

本来是：`sudo rpm --import https://repo.mysql.com/RPM-GPG-KEY-mysql-2022`

如果还出错的话，可以安装2023的：`sudo rpm --import https://repo.mysql.com/RPM-GPG-KEY-mysql-2023`

再安装MySQL 的开发包：

```mysql
sudo yum install -y mysql-community-devel
```



### 1.4 cannot find -lmysqlclient的问题

在centos7下使用make server 编译 TinyWebServer 项目的时候，出现下面的问题：

```sh
/opt/rh/devtoolset-11/root/usr/libexec/gcc/x86_64-redhat-linux/11/ld: cannot find -lmysqlclient
collect2: error: ld returned 1 exit status
make: *** [server] Error 1
```

查看一下链接的情况：

```mysql
[x@localhost r_jian]$ ll /usr/lib64/libmysqlclient.so
ls: cannot access /usr/lib64/libmysqlclient.so: No such file or directory
```

但是我们是存在libmysqlclient的:

```mysql
[x@localhost r_jian]$ locate -l1 mysqlclient
/usr/lib64/mysql/libmysqlclient.so.21
```

所以在makefile使用的时候，就加上 -L 指定寻找库的路径：

```makefile
-L/usr/lib64/mysql -lmysqlclient
```



### 1.5 本地 navicat 连接虚拟机mySQL数据库

https://blog.csdn.net/m0_62744746/article/details/134341024

![image-20240406232828730](./assests/image-20240406232828730.png)

### 1.6 部署环境跑起来

我们的是centos，部署环境，下面的结果，终于是跑起来了。

![image-20231110193511012](./assests/image-20231110193511012.png)



### 1.7 CentOS 离线/在线安装Google Chrome

https://blog.csdn.net/qq_45083975/article/details/130190631



### 1.8、对于git clone下来的代码，进行压力测试的时候，有段错误的问题

叫同学拉了一份最新的代码，也是有问题的：

```cpp
[x@localhost TinyWebServer-master]$ ./server 9006
/home/x/boost_learn/TinyWebServer-master
段错误(吐核)
```

最后改了两处即可：

1. TinyWebServer-master/CGImysql/sql_connection_pool.cpp 中 0 == connList.size() 的判断，放到lock的范围内，因为connList也是共享变量的哦。

   ```cpp
   // 当有请求时，从数据库连接池中返回一个可用连接，更新使用和空闲连接数
   MYSQL *connection_pool::GetConnection()
   {
   	MYSQL *con = NULL;
   
   	// if (0 == connList.size())
   	// 	return NULL;
   
   	reserve.wait();
   
   	lock.lock();
   	if (0 == connList.size())
   		return NULL;
   
   	con = connList.front();
   	connList.pop_front();
   
   	--m_FreeConn;
   	++m_CurConn;
   
   	lock.unlock();
   	return con;
   }
   ```

2. 因为上面获取mysql连接有问题，所以TinyWebServer-master/threadpool/threadpool.h等地方用了上面的那个函数就需要判断的。

   ```cpp
   template <typename T>
   void threadpool<T>::run()
   {
       while (true)
       {
           m_queuestat.wait();
           m_queuelocker.lock();
           if (m_workqueue.empty())
           {
               m_queuelocker.unlock();
               continue;
           }
           T *request = m_workqueue.front();
           m_workqueue.pop_front();
           m_queuelocker.unlock();
           if (!request)
               continue;
           if (1 == m_actor_model)
           {
               if (0 == request->m_state)
               {
                   if (request->read_once())
                   {
                       request->improv = 1;
                       connectionRAII mysqlcon(&request->mysql, m_connPool);
                       request->process();
                   }
                   else
                   {
                       request->improv = 1;
                       request->timer_flag = 1;
                   }
               }
               else
               {
                   if (request->write())
                   {
                       request->improv = 1;
                   }
                   else
                   {
                       request->improv = 1;
                       request->timer_flag = 1;
                   }
               }
           }
           else
           {
               connectionRAII mysqlcon(&request->mysql, m_connPool);
               // 从数据库连接池中返回一个可用连接，可能会有问题
               if (request->mysql == NULL)
               {
                   std::cout << "request->mysql == NULL" << std::endl;
                   continue;
               }
   
               request->process();
           }
       }
   }
   ```

### 1.9、TinyWebServer项目框架图

![image-20231110193511012](./assests/TinyWebServer项目框架图.png)



## 2. 前置相关知识

### 2.1 IO同步和IO异步

[廖雪峰：异步IO一节给出解释](https://www.liaoxuefeng.com/wiki/1016959663602400/1017959540289152#0)

在IO编程一节中，我们已经知道，CPU的速度远远快于磁盘、网络等IO。在一个线程中，CPU执行代码的速度极快，然而，一旦遇到IO操作，如读写文件、发送网络数据时，就需要等待IO操作完成，才能继续进行下一步操作。这种情况称为同步IO。

在IO操作的过程中，当前线程被挂起，而其他需要CPU执行的代码就无法被当前线程执行了。

因为一个IO操作就阻塞了当前线程，导致其他代码无法执行，所以我们必须使用多线程或者多进程来并发执行代码，为多个用户服务。每个用户都会分配一个线程，如果遇到IO导致线程被挂起，其他用户的线程不受影响。

多线程和多进程的模型虽然解决了并发问题，但是系统不能无上限地增加线程。由于系统切换线程的开销也很大，所以，一旦线程数量过多，CPU的时间就花在线程切换上了，真正运行代码的时间就少了，结果导致性能严重下降。

由于我们要解决的问题是CPU高速执行能力和IO设备的龟速严重不匹配，多线程和多进程只是解决这一问题的一种方法。

另一种解决IO问题的方法是异步IO。当代码需要执行一个耗时的IO操作时，它只发出IO指令，并不等待IO结果，然后就去执行其他代码了。一段时间后，当IO返回结果时，再通知CPU进行处理。

可以想象如果按普通顺序写出的代码实际上是没法完成异步IO的：

```cpp
do_some_code()
f = open('/path/to/file', 'r')
r = f.read() # <== 线程停在此处等待IO操作结果
# IO操作完成后线程才能继续执行:
do_some_code(r)
```

所以，同步IO模型的代码是无法实现异步IO模型的。

异步IO模型需要一个消息循环，在消息循环中，主线程不断地重复“读取消息-处理消息”这一过程：

```cpp
loop = get_event_loop()
while True:
    event = loop.get_event()
    process_event(event)
```

消息模型其实早在应用在桌面应用程序中了。一个GUI程序的主线程就负责不停地读取消息并处理消息。所有的键盘、鼠标等消息都被发送到GUI程序的消息队列中，然后由GUI程序的主线程处理。

由于GUI线程处理键盘、鼠标等消息的速度非常快，所以用户感觉不到延迟。某些时候，GUI线程在一个消息处理的过程中遇到问题导致一次消息处理时间过长，此时，用户会感觉到整个GUI程序停止响应了，敲键盘、点鼠标都没有反应。这种情况说明在消息模型中，处理一个消息必须非常迅速，否则，主线程将无法及时处理消息队列中的其他消息，导致程序看上去停止响应。

消息模型是如何解决同步IO必须等待IO操作这一问题的呢？当遇到IO操作时，代码只负责发出IO请求，不等待IO结果，然后直接结束本轮消息处理，进入下一轮消息处理过程。当IO操作完成后，将收到一条“IO完成”的消息，处理该消息时就可以直接获取IO操作结果。

在“发出IO请求”到收到“IO完成”的这段时间里，同步IO模型下，主线程只能挂起，但异步IO模型下，主线程并没有休息，而是在消息循环中继续处理其他消息。这样，在异步IO模型下，一个线程就可以同时处理多个IO请求，并且没有切换线程的操作。对于大多数IO密集型的应用程序，使用异步IO将大大提升系统的多任务处理能力。



**例子1：**

```c++
# 真正意义上的 异步IO 是说内核直接将数据拷贝至用户态的内存单元，再通知程序直接去读取数据。
# select / poll / epoll 都是同步IO的多路复用模式

# 1.同步和异步
# 同步和异步关注的是消息通信机制
# 所谓同步，就是在发出一个*调用*时，没得到结果之前，该*调用*就不返回。但是一旦调用返回就得到返回值了，*调用者*主动等待这个*调用*的结果
# 所谓异步，就是在发出一个*调用*时，这个*调用*就直接返回了，不管返回有没有结果。当一个异步过程调用发出后，*被调用者*通过状态，通知来通知*调用者*，或者通过回调函数处理这个调用

# 2.阻塞和非阻塞
# 阻塞和非阻塞关注的是程序在等待调用结果时的状态
# 阻塞调用是指调用结果返回之前，当前线程会被挂起。调用线程只有在得到结果之后才返回
# 非阻塞调用是指在不能立即得到结果之前，该调用不会阻塞当前线程

# 网络上的例子
#老张爱喝茶，废话不说，煮开水。
#出场人物：老张，水壶两把（普通水壶，简称水壶；会响的水壶，简称响水壶）。
#1 老张把水壶放到火上，立等水开。（同步阻塞）；立等就是阻塞了老张去干别的事，老张得一直主动的看着水开没，这就是同步
#2 老张把水壶放到火上，去客厅看电视，时不时去厨房看看水开没有。（同步非阻塞）；老张去看电视了，这就是非阻塞了，但是老张还是得关注着水开没，这也就是同步了
#3 老张把响水壶放到火上，立等水开。（异步阻塞）；立等就是阻塞了老张去干别的事，但是老张不用时刻关注水开没，因为水开了，响水壶会提醒他，这就是异步了
#4 老张把响水壶放到火上，去客厅看电视，水壶响之前不再去看它了，响了再去拿壶。（异步非阻塞）；老张去看电视了，这就是非阻塞了，而且，等水开了，响水壶会提醒他，这就是异步了
#所谓同步异步，只是对于水壶而言。普通水壶，同步；响水壶，异步。对应的也就是消息通信机制
#虽然都能干活，但响水壶可以在自己完工之后，提示老张水开了。这是普通水壶所不能及的。同步只能让调用者去轮询自己（情况2中），造成老张效率的低下。
#所谓阻塞非阻塞，仅仅对于老张而言。立等的老张，阻塞；对应的也就是程序等待结果时的状态
#看电视的老张，非阻塞。
#情况1和情况3中老张就是阻塞的，媳妇喊他都不知道。虽然3中响水壶是异步的，可对于立等的老张没有太大的意义。所以一般异步是配合非阻塞使用的，这样才能发挥异步的效用。
```

**例子2：**

```tex
I/O模型,同步异步,阻塞非阻塞是些概念困扰了我很长时间. 能上图就好了,一图胜千言.

图在UNIX网络编程 第三版 卷1 中文盗版 第六章 图6-6 5种I/O模型的比较

UNIX有5种I/O模型,阻塞会发生在两个阶段上:

1.阻塞式I/O 等待数据时阻塞 数据从内核复制到用户空间时阻塞 2.非阻塞式I/O 等待数据不阻塞,但是轮询会占用cpu资源 数据从内核复制到用户空间时阻塞 3.I/O复用 考虑到轮询占用cpu资源的问题,阻塞在选择器上,减轻处理器负担 将数据从内核复制到用户空间时阻塞 4.信号驱动式I/O 等待数据不阻塞,数据准备好时通知接收数据,将数据从内核复制到用户空间时阻塞

以上四种或多或少均有阻塞现象存在,它们都是同步I/O模型

5.异步I/O 等待数据时不阻塞 将数据从内核复制到用户空间时也不阻塞. 数据到了用户空间以后才发信号,就像你在网上下了订单,快递员拿着你的快件站在你家门口才通知你开门签收的样子.

而在网上下了订单,货物到了离你家最近的自提点,商城通知你去自提点取提货.你专门抽出时间去提货.这是4.信号驱动式I/O.因为在来回自提点的路上你其实是阻塞的.(数据从内核复制到用户空间 <==> 自提点的东西到自家门口)
```



### 2.2 Linux下三种IO复用方式：epoll，select和poll




## 3. 主要流程分析



### 3.1 一条请求下主线程和工作线程的主要流程

查看：主线程运行`./server 9006`, 客户端`wget http://192.168.61.188:9006/0`的主要处理流程



设置打印信息：

主线程 TinyWebServer-master/webserver.cpp

```cpp
// 客户连接可读
void WebServer::dealwithread(int sockfd)
{
    // 主线程在运行 dealclientdata 函数的时候，已经把新的客户端连接 sockfd，通过 timer(connfd, client_address) 注册了定时器
    util_timer *timer = users_timer[sockfd].timer;

    // reactor
    // 主线程（I/O 处理单元）: 只负责监听文件描述符上是否有事件发生，接受新的连接并为其创建定时器

    // 工作线程：1. 读写数据(read_once 和 write)都交给工作线程来处理 ===> 工作线程得先读写数据，来判断是否需要删除定时器 ===> 主线程不需要等待工作线程的信息来处理定时器 ===> 同步的表现
    //          2. 负责业务逻辑
    if (1 == m_actormodel) // 同步 I/O 模型通常用于实现 Reactor 模式
    {
        if (timer)
        {
            adjust_timer(timer);
        }

        // 若监测到读事件，将该事件放入请求队列
        m_pool->append(users + sockfd, 0); // m_pool->append(users[sockfd]);
        std::cout << "reactor主线程 " << pthread_self() << " 推送http_conn任务到工作线程池任务队列" << std::endl;

        while (true)
        {
            if (1 == users[sockfd].improv)
            {
                if (1 == users[sockfd].timer_flag)
                {
                    deal_timer(timer, sockfd);
                    users[sockfd].timer_flag = 0;
                }
                users[sockfd].improv = 0;
                break;
            }
        }
    }

    // proactor
    // 主线程（I/O 处理单元）: 负责监听文件描述符上是否有事件发生，接受新的连接并为其创建定时器

    // 工作线程：1. 将所有 I/O 操作(read_once 和下面的 write)都交给主线程和内核来处理
    //          2. 工作线程仅仅负责业务逻辑 ===> 主线程不需要等待工作线程的信息来处理定时器 ===> 异步的表现
    else // 异步 I/O 模型则用于实现 Proactor 模式
    {
        if (users[sockfd].read_once())
        {
            LOG_INFO("deal with the client(%s)", inet_ntoa(users[sockfd].get_address()->sin_addr));

            // 若监测到读事件，将该事件放入线程池的请求队列
            m_pool->append_p(users + sockfd); // m_pool->append(users[sockfd]);
            std::cout << "proactor主线程 " << pthread_self() << " read_once(), 并推送http_conn任务到工作线程池任务队列" << std::endl;

            if (timer)
            {
                // 有数据传输，则将定时器往后延迟3个单位
                adjust_timer(timer);
            }
        }
        else
        {
            // 没有数据传输，则移除连接的定时器
            deal_timer(timer, sockfd);
        }
    }
}

// 客户连接可写
void WebServer::dealwithwrite(int sockfd)
{
    util_timer *timer = users_timer[sockfd].timer;
    // reactor
    if (1 == m_actormodel) // 同步 I/O 模型通常用于实现 Reactor 模式
    {
        if (timer)
        {
            adjust_timer(timer);
        }

        m_pool->append(users + sockfd, 1); // m_pool->append(users[sockfd], 1);
        std::cout << "reactor主线程 " << pthread_self() << " 推送http_conn任务到工作线程池任务队列" << std::endl;

        while (true)
        {
            if (1 == users[sockfd].improv)
            {
                if (1 == users[sockfd].timer_flag)
                {
                    deal_timer(timer, sockfd);
                    users[sockfd].timer_flag = 0;
                }
                users[sockfd].improv = 0;
                break;
            }
        }
    }
    else // 异步 I/O 模型则用于实现 Proactor 模式
    {
        if (users[sockfd].write())
        {
            LOG_INFO("send data to the client(%s)", inet_ntoa(users[sockfd].get_address()->sin_addr));
            std::cout << "proactor主线程 " << pthread_self() << " write()" << std::endl;

            if (timer)
            {
                // 有数据传输，则将定时器往后延迟3个单位
                adjust_timer(timer);
            }
        }
        else
        {
            // 没有数据传输，则移除连接的定时器
            deal_timer(timer, sockfd);
        }
    }
}
```

工作线程池线程从任务队列获取http_conn任务 TinyWebServer-master/threadpool/threadpool.h

```cpp
template <typename T>
void threadpool<T>::run()
{
    while (true)
    {
        // 1. 处理业务工作线程池中的线程，没有获取到 http_conn 客户端连接的话，是会挂起到 sem 信号量的等待队列中的(防止循环询问，占用cpu资源)
        // 2. 有 m_queuestat.post() 的话，会自动从 sem 信号量的等待队列中唤醒一个线程去处理 http_conn 业务
        m_queuestat.wait();
        m_queuelocker.lock();
        if (m_workqueue.empty())
        {
            m_queuelocker.unlock();
            continue;
        }
        T *request = m_workqueue.front();
        m_workqueue.pop_front();
        m_queuelocker.unlock();

        if (!request)
            continue;

        if (1 == m_actor_model) // 同步 I/O 模型通常用于实现 Reactor 模式
        {
            if (0 == request->m_state)
            {
                if (request->read_once())
                {
                    request->improv = 1;
                    connectionRAII mysqlcon(&request->mysql, m_connPool);

                    std::cout << "工作线程池 " << pthread_self() << " 线程 read_once()，并调用 process() 开始工作" << std::endl;
                    request->process();
                }
                else
                {
                    request->improv = 1;
                    request->timer_flag = 1;
                }
            }
            else
            {
                if (request->write())
                {
                    std::cout << "工作线程池 " << pthread_self() << " 线程 write()" << std::endl;
                    request->improv = 1;
                }
                else
                {
                    request->improv = 1;
                    request->timer_flag = 1;
                }
            }
        }
        else // 异步 I/O 模型则用于实现 Proactor 模式
        {
            // 从数据库连接池单例对象中返回一个可用连接
            connectionRAII mysqlcon(&request->mysql, m_connPool);
            // 可能会有问题
            if (request->mysql == NULL)
            {
                std::cout << "request->mysql == NULL" << std::endl;
                continue;
            }

            std::cout << "工作线程池 " << pthread_self() << " 线程开始接单, 调用 process() 开始工作" << std::endl;
            request->process();
        }
    }
}
```

工作线程池线程执行任务 TinyWebServer-master/http/http_conn.cpp

```cpp
// 线程池中的线程工作的函数
void http_conn::process()
{
    HTTP_CODE read_ret = process_read();
    std::cout << "工作线程池 " << pthread_self() << " 线程已process_read()" << std::endl;
    if (read_ret == NO_REQUEST)
    {
        modfd(m_epollfd, m_sockfd, EPOLLIN, m_TRIGMode);

        std::cout << "工作线程池 " << pthread_self() << " 线程no_request" << std::endl;
        return;
    }

    bool write_ret = process_write(read_ret);
    if (!write_ret)
    {
        close_conn();
    }
    std::cout << "工作线程池 " << pthread_self() << " 线程已process_write()" << std::endl;
    modfd(m_epollfd, m_sockfd, EPOLLOUT, m_TRIGMode);
}
```



打印信息如下：

```cpp
// proactor 模式：
proactor主线程 140167778744448 read_once(), 并推送http_conn任务到工作线程池任务队列

工作线程池 140167455889152 线程开始接单, 调用 process() 开始工作
工作线程池 140167455889152 线程已process_read()
工作线程池 140167455889152 线程已process_write()
    
proactor主线程 140167778744448 write()
```

```cpp
// reactor 模式：
reactor主线程 139661531912320 推送http_conn任务到工作线程池任务队列
    
工作线程池 139661209057024 线程 read_once()，并调用 process() 开始工作
工作线程池 139661209057024 线程已process_read()
工作线程池 139661209057024 线程已process_write()
    
reactor主线程 139661531912320 推送http_conn任务到工作线程池任务队列
    
工作线程池 139661200664320 线程 write()
```

可以看到proactor模式下：

```cpp
// 主线程（I/O 处理单元）: 负责监听文件描述符上是否有事件发生，接受新的连接并为其创建定时器

// 工作线程：1. 将所有 I/O 操作(read_once 和下面的 write)都交给主线程和内核来处理
//          2. 工作线程仅仅负责业务逻辑 ===> 主线程不需要等待工作线程的信息来处理定时器 ===> 异步的表现
```

可以看到reactor 模式下：

```cpp
// 主线程（I/O 处理单元）: 只负责监听文件描述符上是否有事件发生，接受新的连接并为其创建定时器

// 工作线程：1. 读写数据(read_once 和 write)都交给工作线程来处理 ===> 工作线程得先读写数据，来判断是否需要删除定时器 ===> 主线程不需要等待工作线程的信息来处理定时器 ===> 同步的表现
//          2. 负责业务逻辑
```

用WebBench多client情况测试来看proactor是比reactor更好一点的，但是没达到数量级上的差距。


### 3.2 登陆改用ajax请求 && 返回json数据 && 其中的一些细节

#### 登陆改用ajax请求

前端，登陆页面不用form表单自带的提交功能，使用ajax请求：

```
    <!-- <form action="2CGISQL.cgi" method="post" id="login_form"> -->
    <form id="login_form">
        <div align="center" class="ksd-el-items"><input type="text" name="user" placeholder="用户名" class="ksd-login-input" required="required"></div><br/>
        <div align="center" class="ksd-el-items"><input type="password" name="password" placeholder="登录密码" class="ksd-login-input" required="required"></div><br/>
        <!-- <div align="center" class="ksd-el-items"><input type="submit" class="ksd-login-btn"></input></div> type="submit" 是得当前行具有提交表单的能力 -->

        <div align="center" class="ksd-el-items"><input type="button" class="ksd-login-btn" id="login_btn"></input></div> <!-- 使用下面的 ajax 中转 -->
    </form>

    <!-- <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
    <script src="./static/js/jquery-3.7.1.js"></script>
    <script>
        $(function () {//页面框架加载完成之后，自动运行这里面的代码
            Bind();//绑定事件函数
        })

        function Bind() {
            $("#login_btn").click(function () {
                RequestLogin();
            })
        }

        function RequestLogin() {
            // 表单通过验证，执行 AJAX 请求     // 不能重定向
            $.ajax({
                // 提交 form 表单的数据到 /2 （是验证登陆的 url）
                url: "/2",
                type: "POST",
                data: $("#login_form").serialize(),
                success: function (response) {
                console.log(response); 
                // alert(response);
                window.location.href = response.redirect;
                },
                error: function (response) {
                console.log(response.redirect);
                }
            });
        }
    </script>
```

后端，提交form表单数据到 /2，登陆判断成功的话，会 val["redirect"] = "/8" 重定向到 /8，再返回 welcome.html 页面：

```
    // 如果是登录，直接判断
    // 若浏览器端输入的用户名和密码在表中可以查找到，返回1，否则返回0
    else if (*(p + 1) == '2')
    {
        if (users.find(name) != users.end() && users[name] == password)
        {
            // 登陆页面：不使用 form 重定向，使用ajax中转一些(ajax本身是不能重定向的哦)
            // http response
            Json::Value val;
            val["redirect"] = "/8";
            Json::StreamWriterBuilder swb;
            Json::StreamWriter *sw = swb.newStreamWriter();
            std::ostringstream ostring;
            sw->write(val, &ostring);
            std::string str = "HTTP/1.1 200 🤣\r\nContent-Type : application/json\r\n \r\n\r\n" + ostring.str();
            const char *data = str.c_str(); // 重定向到 /8，再返回下面的 welcome.html
            delete sw;
            // write 也是可以的，都是向浏览器对应 socket_fd 的缓冲区中写入数据
            // int temp = send(m_sockfd, data, strlen(data), 0);
            int temp = ::write(m_sockfd, data, strlen(data));
            std::cout << "temp = " << temp << std::endl;
        }
        else
            strcpy(m_url, "/logError.html");
    }


    else if (*(p + 1) == '8') // 登陆成功重定向到 welcome.html 页面
    {
        std::cout << "登陆成功重定向到 welcome.html 页面" << std::endl;
        char *m_url_real = (char *)malloc(sizeof(char) * 200);
        strcpy(m_url_real, "/welcome.html");
        strncpy(m_real_file + len, m_url_real, strlen(m_url_real));

        free(m_url_real);
    }
```

上面的代码：

1. 去掉form表单的默认提交功能

2. 执行 AJAX 请求，不能重定向

3. 业务线程组织 `std::string str = "HTTP/1.1 200 🤣\r\nContent-Type : application/json\r\n \r\n\r\n" + ostring.str();` 返回的流程

4. 业务线程通过 `send(m_sockfd, data, strlen(data), 0)` 和 `::write(m_sockfd, data, strlen(data))` 都是可以的，都是linux系统调用，都是往socketfd的缓冲区中写入要发送的数据，tcp在自行组织发送；**write 前面的 `::` 表示的是用的是外部的 write 函数，因为 conn_http 类有一个成员函数名就是 write。**

#### 返回json数据

增加一个 /9 url，用来返回用户名和密码：

```
//http_conn::HTTP_CODE http_conn::do_request(): 添加一个 /9 url，并设置 users_map = true
    else if (*(p + 1) == '9') // 返回数据库用户名和密码
    {
        std::cout << "返回数据库用户名和密码" << std::endl;
        users_map = true; // 置为请求 users_map 数据
    }

// bool http_conn::process_write(HTTP_CODE ret): 上面的返回是 return BAD_REQUEST（请求的是一个目录，因为 m_real_file 中没有拷贝要打开的文件，所以是一个目录，就返回 BAD_REQUEST）
    case BAD_REQUEST: // 2(404客户端错误，客户端输入的url在服务端不存在)
    {
        std::cout << "BAD_REQUEST" << std::endl;
        if (users_map == true) // 返回 json 数据
        {
            std::cout << "返回 json 数据" << std::endl;

            Json::Value val;
            int num = 1;
            Json::StreamWriterBuilder swb;
            std::unique_ptr<Json::StreamWriter> sw(swb.newStreamWriter());
            std::ostringstream ostring;
            for (auto data : users)
            {
                // 每一个用户的 username 和 password 用一个数组包裹，第一个是 username，第二个是 password
                val[std::to_string(num)].append(data.first);
                val[std::to_string(num)].append(data.second);
                num++;
                std::cout << data.first << ' ' << data.second << std::endl;
            }
            sw->write(val, &ostring);
            std::string result = ostring.str();

            add_status_line(200, ok_200_title);
            add_headers(result.size(), users_map);
            if (!add_content(result.c_str())) // 写入 json 数据到 content
                return false;

            users_map = false; // 置回去

            break;
        }

        add_status_line(404, error_404_title);
        add_headers(strlen(error_404_form), users_map);
        if (!add_content(error_404_form))
            return false;
        break;
    }
```

写入 json 数据到 http 中的 content 返回。


### 3.3 welcome.html 页面优化，json数据渲染到页面上



### 3.4 改成 reactor 同步并发模式



