// g++ -o ma helloBoost.cpp -lboost_system -lboost_filesystem

// #pragma once

// 1. 百度一些好的 centos 系统安装 boost 库的文章

// 2. 上面的路径安装不成功的话，可以再试试下面的路径：
// yum search boost 看能不能搜索到boost库的一些信息，如果有的话可以直接用下面的操作：
// yum install boost-devel 就可以使用boost库的一些文件了。

#include <iostream>
#include <boost/version.hpp>

using namespace std;

int main(void)
{
    cout << "Boost 版本" << BOOST_LIB_VERSION << endl;

    return 0;
}

/*-------------------------------------------------------------------------------------------------------------------*/
// 因为重新搞了一下 boost 库，所以下面的代码是 centos 系统下测试 boost 环境是否安装好了
// yum install boost
// yum install boost-devel
// tar -xzf boost_1_81_0.tar.gz
// cd boost_1_81_0/
// ./bootstrap.sh --with-libraries=all --with-toolset=gcc  // #--with-liraries：需要编译的库 --with-toolset：编译时使用的编译器
// ./b2 install --prefix=/usr  //  默认头文件在/usr/local/include，库文件在/usr/local/lib
/*
#include <iostream>
#include <boost/version.hpp> //包含 Boost 头文件
#include <boost/config.hpp>  //包含 Boost 头文件

using namespace std;

int main(void)
{
    cout << BOOST_VERSION << endl;     // Boost 版本号
    cout << BOOST_LIB_VERSION << endl; // Boost 版本号
    cout << BOOST_PLATFORM << endl;    // 操作系统
    cout << BOOST_COMPILER << endl;    // 编译器
    cout << BOOST_STDLIB << endl;      // 标准库

    return 0;
}
*/
// 编译命令：g++ -o ma helloBoost.cpp
// 108200
// 1_82
// linux
// GNU C++ version 8.3.1 20190311 (Red Hat 8.3.1-3)
// GNU libstdc++ version 20190311

/*-------------------------------------------------------------------------------------------------------------------*/
/*
#include <iostream>
#include <boost/filesystem.hpp>
using namespace boost::filesystem;
int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cout << "用法: app path\n";
        return 1;
    }
    std::cout << argv[1] << ":" << file_size(argv[1]) << std::endl;
    return 0;
}
*/
// 编译命令：g++ -o ma helloBoost.cpp -lboost_system -lboost_filesystem
// 用法: app path

/*-------------------------------------------------------------------------------------------------------------------*/

// #include <boost/thread/thread.hpp>
// #include <iostream>

// using namespace std;

// void NewThread()
// {
//     cout << "New thread is running..." << endl;
// }

// int main(int argc, char *argv[])
// {
//     boost::thread newthread(&NewThread);
//     newthread.join();
//     return 0;
// }

// 编译命令：g++ -o ma helloBoost.cpp -lboost_system -lboost_thread -lpthread
// 注意 -lpthread 的使用，看着好像没有使用 #include <pthread.h> 但是编译的时候就是给你来一个 collect2: error: ld returned 1 exit status
