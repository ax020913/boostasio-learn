# `boostasio-learn`

#### 1. 介绍
`asio` 网络库示例代码和 `c++` 并发编程代码示例。

#### 2. 内容介绍
具体的使用每一个目录里面基本都会有 `.md` 文件的

##### 2.1 网络编程部分

1. `day01:` `endpoint` 的认识
2. `day02:` `asio` 中的 `io` 函数的认识
3. `day03:` 回响的 `client` 和 `server` 的使用
4. `day04:` 异步 `server` ， `session` 隐藏二次析构的问题
5. `day05:` `shared_ptr` 伪闭包保活，解决 `session` 隐藏二次析构的问题
6. `day06:` 增加了缓冲队列，异步全双工的通信模式
7. `day07:` `async_read_some` 处理粘包 + 网络字节序处理
8. `day08:` `async_read` 处理粘包
9. `day09:` `protobuf` 序列化环境配置和本机简单使用
10. `day10:` `protobuf` 序列化网络传输中使用失败，放弃使用
11. `day11:` `json` 序列化环境配置和简单使用

12. `day12:` 更新 `class MsgNode` 用两个重载的构造函数区分的 `recvNode` 和 `sendNode`
13. `day13:` 单例逻辑系统(用业务逻辑队列区分网络层和逻辑层实现解耦)和服务器的优雅退出(`signal` 两种方式)
14. `day14:` `asio` 多线程模型一：每一个线程运行一个 `io_context` 
15. `day15:` `asio` 多线程模型二：多个线程运行同一个 `io_context`
16. `day16` 基于 `boost::asio` 实现 `http server`
17. `day17` 基于 `beast` 网络库实现 `http` 服务器
18. `day18` 基于 `beast` 网络库实现 `websocket` 服务器
19. `day19` 基于 `grpc` 实现服务器， `grpc` 多用于后台内部服务。
20. `day20:` `boost::asio::co_spawn` 协程的简单使用
21. `day21` 将前面的服务器改写为协程
22. `day22` `AsyncLog` 异步日志库
23. `day23` 生产者消费者案例实现 `demo`

**存在的问题：**
```
目录下的分离编译好像不行，怎么就 ld 报错了呢
/home/x/boost_learn/boostasio-learn/network/day11-Json_Server_Client/json_use 
```
**使用：**
```
大三上---计网小课设
/home/x/boost_learn/boostasio-learn/network/day11-Json_Server_Client/ji_wang 
```


##### 2.2 `C++` 并发编程部分

1. `std::thread` 的回调函数及参数_线程退出 `demo`
2. `std::thread`：存储归属权转移并行计算 `demo`
3. 配置 `centos` 的 `boost` 库使用环境，`unique_lock,shared_lock,recursive_mutex` 的使用 `demo`
4. 单例模式的一些细节问题 `demo`
5. `threadSafeQueue` 的封装
6. `cpp`异步-异步线程池
7. 快排-函数式编程-并行计算-异步线程池计算
8. `Actor` 和 `CSP` 设计模式
9. 前面的一些问题的汇总
10. `cpp` 内存模型
11. 用内存顺序实现内存模型
12. 几种版本的 `CirculauQueue` 环形队列
13. 利用 `std::atomic_thread_fence` 栅栏实现同步
14. 基于锁实现线程安全队列和栈容器【想修改 `tail_` 类型为 `std::unique_ptr<node>` 失败】
15. 实现线程安全的查找表 `map`
16. 线程安全的链表
17. 无锁栈
18. 运用风险指针实现无锁栈
19. 利用引用计数实现无锁并发栈
20.



学习内容来自 `b` 站: 恋恋风辰 `up`。 
他的官网：https://llfc.club/

